package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.ui.base.BaseViewHolder;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by air-water on 1/25/17.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<ChatResponse.Retrieve.Data> chatModels;
    private Listener callback;
    private Context context;

    public static final int EMPTY = 0;
    private static final int RIGHT_MSG = 1;
    private static final int LEFT_MSG = 2;
    private String Id;
    private String temporaryTime = "";


    public ChatMessageAdapter(Context context) {
        this.context = context;
        chatModels = new ArrayList<>();
    }

    public void update(List<ChatResponse.Retrieve.Data> chatModels, String Id) {
        this.chatModels = chatModels;
        this.Id = Id;
        notifyDataSetChanged();
    }

    public void addChat(ChatResponse.Retrieve.Data chatModel, String Id) {
        chatModels.add(chatModel);
        this.Id = Id;
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener callback) {
        this.callback = callback;
    }

    public ChatResponse.Retrieve.Data getItem(int position) {
        return chatModels.get(position);
    }

    public ChatResponse.Retrieve.Data getChat(String percakapan_id) {
        ChatResponse.Retrieve.Data data = new ChatResponse.Retrieve.Data();
        for (int i = 0; i < chatModels.size(); i++) {
            if (chatModels.get(i).getId().equals(percakapan_id)) {
                data = chatModels.get(i);
            }
        }
        return data;
    }

    public void remove(String percakapan_id) {
        for (int i = 0; i < chatModels.size(); i++) {
            if (chatModels.get(i).getId().equals(percakapan_id)) {
                chatModels.remove(i);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RIGHT_MSG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_right, parent, false);
            return new Holder(view);
        } else if (viewType == LEFT_MSG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_left, parent, false);
            return new Holder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false);
            return new EmptyViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (chatModels != null && chatModels.size() > 0) {
            ChatResponse.Retrieve.Data model = getItem(position);
            if (!model.getFrom_id().equals(Id)) return LEFT_MSG;
            else return RIGHT_MSG;
        } else {
            return EMPTY;
        }

    }

    @Override
    public int getItemCount() {
        if (chatModels != null && chatModels.size() > 0) {
            return chatModels.size();
        } else {
            return 1;
        }
    }

    public class Holder extends BaseViewHolder {
        @BindView(R.id.img_status) ImageView img_status;
        @BindView(R.id.txt_time_stamp) TextView txtTimestamp;
        @BindView(R.id.txt_message) TextView txtMessage;
        @BindView(R.id.txt_time) TextView txt_time;


        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {
            txtMessage.setText("");
            txtTimestamp.setText("");
            txt_time.setText("");
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            ChatResponse.Retrieve.Data data = chatModels.get(position);

            if (data.getPesan() != null) txtMessage.setText(data.getPesan());
            if (data.getCreated_on() != null) txtTimestamp.setText(TimeUtils.getClock(data.getCreated_on()));

            if (data.getStatus_sending() == 0) img_status.setImageResource(R.drawable.ic_pending_status);
            if (data.getStatus_sending() == 1) img_status.setImageResource(R.drawable.ic_send_status);

            if (data.isVisible()){
                txt_time.setVisibility(View.VISIBLE);
                txt_time.setText(TimeUtils.getTime(data.getCreated_on()));
            } else {
                txt_time.setVisibility(View.GONE);
            }

            /*if (TimeUtils.getVisibleTimeChat(data.getCreated_on())){
                Log.e("onBind", temporaryTime);
                if (temporaryTime.equals(TimeUtils.getDate(data.getCreated_on()))){
                    txt_time.setVisibility(View.GONE);
                } else {
                    txt_time.setVisibility(View.VISIBLE);
                    temporaryTime = TimeUtils.getDate(data.getCreated_on());
                    txt_time.setText(TimeUtils.getFeedTime(TimeUtils.getMilis(data.getCreated_on()),
                            TimeUtils.getMilis(TimeUtils.getCurrentDateTime())));
                }
            }*/

            itemView.setOnClickListener(view -> {
                if (callback != null){
                    callback.onClick(data);
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry) Button retryButton;
        @BindView(R.id.tv_message) TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            /*if (callback != null)
                callback.onEmptyViewRetryClick();*/
        }
    }

    public interface Listener {
        void onClick(ChatResponse.Retrieve.Data chatModel);
        //void onEmptyViewRetryClick();
    }
}
