package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.data.model.ChatModel;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class ChatAdapter extends RecyclerView.Adapter {

    private List<ChatResponse.ListMessage.Data> chatModels;
    Listener listener;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public ChatAdapter(Context context) {
        this.context = context;
        chatModels = new ArrayList<>();
    }

    public void update(List<ChatResponse.ListMessage.Data> chatModels) {
        this.chatModels = chatModels;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<ChatResponse.ListMessage.Data> chatModels) {
        this.chatModels.addAll(chatModels);
        notifyItemInserted(this.chatModels.size());
        //notifyDataSetChanged();
    }

    public void addLoading() {
        this.chatModels.add(new ChatResponse.ListMessage.Data(true));
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.chatModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            return new Holder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((Holder) holder).setValue(chatModels.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (chatModels != null) return chatModels.size();
        else return 0;
    }

    private ChatResponse.ListMessage.Data getItem(int position) {
        return chatModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).isLoading()) return VIEW_TYPE_ITEM;
        else return VIEW_TYPE_LOADING;
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Log.e("load","LoadHolder");
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_user) CircleImageView img_user;
        @BindView(R.id.txt_name) TextView txt_name;
        @BindView(R.id.txt_last_message) TextView txt_last_message;
        @BindView(R.id.txt_time_last_message) TextView txt_time_last_message;
        @BindView(R.id.txt_unread_message) TextView txt_unread_message;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final ChatResponse.ListMessage.Data chatModel) {
            if (chatModel.getTipe_user() != null) {
                if (chatModel.getTipe_user().equals("perekrut")){
                    txt_name.setText(chatModel.getInfo().getNama_perusahaan());
                    Glide.with(context).load(chatModel.getInfo().getLogo()).into(img_user);
                } else {
                    txt_name.setText(chatModel.getInfo().getNama_ktp());
                    Glide.with(context).load(chatModel.getFoto()).into(img_user);
                }
            }

            txt_last_message.setText(chatModel.getPesan());

            if (!TimeUtils.getVisibleTimeChat(chatModel.getCreated_on())){
                txt_time_last_message.setText(TimeUtils.getClock(chatModel.getCreated_on()));
            } else {
                txt_time_last_message.setText(TimeUtils.getTime(chatModel.getCreated_on()));
            }

            if (chatModel.getStatus() != null) {
                if (chatModel.getStatus().equals("0")) {
                    txt_unread_message.setVisibility(View.GONE);
                } else {
                    txt_unread_message.setVisibility(View.VISIBLE);
                    txt_unread_message.setText(chatModel.getStatus());
                }
            } else {
                txt_unread_message.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(chatModel);
                }
            });
        }
    }

    public interface Listener{
        void onClick(ChatResponse.ListMessage.Data model);
    }
}
