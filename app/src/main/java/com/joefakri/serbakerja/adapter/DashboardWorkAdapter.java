package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class DashboardWorkAdapter extends RecyclerView.Adapter {

    private List<DashboardResponse.Candidate.Data> workModels;
    Listener listener;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public DashboardWorkAdapter(Context context) {
        this.context = context;
        workModels = new ArrayList<>();
    }

    public void update(List<DashboardResponse.Candidate.Data> workModels) {
        this.workModels = workModels;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<DashboardResponse.Candidate.Data> workModels) {
        this.workModels.addAll(workModels);
        notifyItemInserted(this.workModels.size());
        //notifyDataSetChanged();
    }

    public void addLoading() {
        this.workModels.add(new DashboardResponse.Candidate.Data(true));
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.workModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void buttonLamar(String id) {
        for (int i = 0; i < workModels.size(); i++) {
            if (id.equals(workModels.get(i).getId())){
                workModels.get(i).setStatus_lamar("waiting");
            }
        }
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard_work, parent, false);
            return new Holder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((Holder) holder).setValue(workModels.get(position));
        }

    }

    private DashboardResponse.Candidate.Data getItem(int position) {
        return workModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).isLoading()) return VIEW_TYPE_ITEM;
        else return VIEW_TYPE_LOADING;
    }

    @Override
    public int getItemCount() {
        if (workModels != null) return workModels.size();
        else return 0;
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Log.e("load","LoadHolder");
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_company) CircleImageView img_company;
        @BindView(R.id.txt_job) TextView txt_job;
        @BindView(R.id.txt_area) TextView txt_area;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_company) TextView txt_company;
        @BindView(R.id.btn_detail) LinearLayout btn_detail;
        @BindView(R.id.btn_detail_2) LinearLayout btn_detail_2;
        @BindView(R.id.btn_apply) LinearLayout btn_apply;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final DashboardResponse.Candidate.Data workModel) {
            txt_area.setText(workModel.getLokasi() + ", " +workModel.getKota());
            txt_job.setText(workModel.getJenis_pekerjaan());
            txt_company.setText(workModel.getNama_perusahaan());
            txt_salary.setText(Constants.sallary(context, workModel.getGaji_min(), workModel.getGaji_max()));
            Glide.with(context).load(workModel.getLogo()).into(img_company);

            /*img_company.setOnClickListener(view -> {
                if (listener != null){
                    listener.onImageClick(workModel);
                }
            });*/

            if (!workModel.getStatus_lamar().equals("0"))
                btn_apply.setBackgroundColor(ContextCompat.getColor(context, R.color.grey400));

            btn_apply.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onApply(workModel);
                }
            });

            btn_detail.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onClick(workModel);
                }
            });

            btn_detail_2.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onClick(workModel);
                }
            });
        }
    }

    public interface Listener{
        void onClick(DashboardResponse.Candidate.Data model);
        void onApply(DashboardResponse.Candidate.Data model);
    }
}
