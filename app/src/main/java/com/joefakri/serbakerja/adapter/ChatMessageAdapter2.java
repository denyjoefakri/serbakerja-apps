package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.ChatResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class ChatMessageAdapter2 extends RecyclerView.Adapter {

    private ArrayList<ChatResponse.Retrieve.Data> data;
    Listener listener;
    private Context context;

    private static final int RIGHT_MSG = 1;
    private static final int LEFT_MSG = 2;
    private String Id;

    public ChatMessageAdapter2(Context context) {
        this.context = context;
        data = new ArrayList<>();
    }

    public void update(ArrayList<ChatResponse.Retrieve.Data> data, String Id) {
        this.data = data;
        this.Id = Id;
        notifyDataSetChanged();
    }

    public void addChat(ChatResponse.Retrieve.Data chatModel, String Id) {
        data.add(chatModel);
        this.Id = Id;
        notifyDataSetChanged();
    }

    public ChatResponse.Retrieve.Data getItem(int position) {
        return data.get(position);
    }

    public ChatResponse.Retrieve.Data getChat(String percakapan_id) {
        ChatResponse.Retrieve.Data chat = new ChatResponse.Retrieve.Data();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getId().equals(percakapan_id)) {
                chat = data.get(i);
            }
        }
        return chat;
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RIGHT_MSG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_right, parent, false);
            return new Holder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_left, parent, false);
            return new Holder(view);
        }

        /*View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new Holder(v);*/
    }

    @Override
    public int getItemViewType(int position) {
        ChatResponse.Retrieve.Data model = getItem(position);
        if (!model.getFrom_id().equals(Id)) return LEFT_MSG;
        else return RIGHT_MSG;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(data.get(position));
    }

    @Override
    public int getItemCount() {
        if (data != null && data.size() > 0) {
            return data.size();
        } else {
            return 1;
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_status) ImageView img_status;
        @BindView(R.id.txt_time_stamp) TextView txtTimestamp;
        @BindView(R.id.txt_message) TextView txtMessage;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final ChatResponse.Retrieve.Data data) {

            if (data.getPesan() != null) txtMessage.setText(data.getPesan());
            if (data.getPesan() != null) txtTimestamp.setText(data.getId());

            if (data.getStatus_sending() == 0) img_status.setImageResource(R.drawable.ic_pending_status);
            if (data.getStatus_sending() == 1) img_status.setImageResource(R.drawable.ic_send_status);

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(data);
                }
            });
        }
    }

    public interface Listener{
        void onClick(ChatResponse.Retrieve.Data data);
    }
}
