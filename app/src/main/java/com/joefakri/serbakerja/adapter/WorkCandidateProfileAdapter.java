package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.joefakri.serbakerja.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class WorkCandidateProfileAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkResponse.CandidateWorkList.Data> workModels;
    Listener listener;
    private Context context;

    public WorkCandidateProfileAdapter(Context context) {
        this.context = context;
        workModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkResponse.CandidateWorkList.Data> workModels) {
        this.workModels = workModels;
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_candidate, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(workModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (workModels != null) return workModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.divider_top) View divider_top;
        @BindView(R.id.divider_bottom) View divider_bottom;
        @BindView(R.id.txt_work_header) TextView txt_work_header;
        @BindView(R.id.txt_work) TextView txt_work;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_city) TextView txt_city;
        @BindView(R.id.txt_area) TextView txt_area;
        @BindView(R.id.txt_work_time) TextView txt_work_time;
        @BindView(R.id.txt_work_day) TextView txt_work_day;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkResponse.CandidateWorkList.Data experienceWorkModel, int position) {
            divider_top.setVisibility(View.GONE);
            txt_work_header.setText(experienceWorkModel.getJenis_pekerjaan());
            txt_work.setText(experienceWorkModel.getJenis_pekerjaan());
            txt_salary.setText(Constants.sallary(context, experienceWorkModel.getGaji_min(), experienceWorkModel.getGaji_max()));
            txt_city.setText(Util.getCitysNameCandidate(experienceWorkModel.getKota()));
            txt_area.setText(experienceWorkModel.getDaerah());
            txt_work_day.setText(experienceWorkModel.getHari_kerja());

            if (experienceWorkModel.getWaktu_kerja() != null) {
                if (!experienceWorkModel.getWaktu_kerja().equals("2"))
                    txt_work_time.setText(Constants.waktuKerjaValue(context, experienceWorkModel.getWaktu_kerja()));
                else txt_work_time.setText(experienceWorkModel.getWaktu_kerja_lainnya());
            }

            if (getAdapterPosition() == getItemCount() - 1) divider_bottom.setVisibility(View.GONE);
            else divider_bottom.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(experienceWorkModel, position);
                }
            });
        }
    }

    public interface Listener{
        void onClick(WorkResponse.CandidateWorkList.Data model, int position);
    }
}
