package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class WorkExperienceAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkModel> experienceWorkModels;
    Listener listener;
    private Context context;

    public WorkExperienceAdapter(Context context) {
        this.context = context;
        experienceWorkModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkModel> experienceWorkModels) {
        this.experienceWorkModels = experienceWorkModels;
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    public ArrayList<WorkModel> getData(){
        return experienceWorkModels;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_experience, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(experienceWorkModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (experienceWorkModels != null) return experienceWorkModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.divider_top) View divider_top;
        @BindView(R.id.txt_company_name) TextView txt_company_name;
        @BindView(R.id.txt_work_long) TextView txt_work_long;
        @BindView(R.id.txt_work_as) TextView txt_work_as;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkModel experienceWorkModel, int position) {
            txt_company_name.setText(Util.checkNull(experienceWorkModel.getCompany_name(), "-"));
            txt_work_long.setText(Util.checkNull(experienceWorkModel.getWork_long(), "-")+ " "
                    + Util.checkNull(Constants.getPeriod(experienceWorkModel.getPeriod()), ""));
            txt_work_as.setText(Util.checkNull(experienceWorkModel.getWork_as(), "-"));

            if (getAdapterPosition() == 0) divider_top.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(experienceWorkModel, position);
                }
            });
        }
    }

    public interface Listener{
        void onClick(WorkModel model, int position);
    }
}
