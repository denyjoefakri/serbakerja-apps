package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.data.model.WorkModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class DataCityAdapter extends RecyclerView.Adapter {

    private List<AreaResponse.CityResponse.Data> models;
    Listener listener;
    private Context context;
    private int mSelectedItem = -1;

    public DataCityAdapter(Context context) {
        this.context = context;
        models = new ArrayList<>();
    }

    public void update(List<AreaResponse.CityResponse.Data> models) {
        this.models.add(new AreaResponse.CityResponse.Data("0", "Pilih Semua Kota / Kabupaten"));
        this.models.addAll(models);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(models.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (models != null) return models.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_job) TextView txt_job;
        @BindView(R.id.img_checked) ImageView img_checked;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final AreaResponse.CityResponse.Data city, int position) {
            txt_job.setText(city.getName());
            if (position == mSelectedItem){
                img_checked.setVisibility(View.VISIBLE);
            } else {
                img_checked.setVisibility(View.INVISIBLE);
            }

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(city);
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, models.size());
                }
            });
        }
    }

    public interface Listener{
        void onClick(AreaResponse.CityResponse.Data model);
    }
}
