package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.data.model.HeaderModel;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.city.DataCityActivity;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class DashboardCandidateAdapter2 extends RecyclerView.Adapter {

    private ArrayList<DashboardResponse.Recruter.Data> candidateModels;
    private Context context;

    private final int VIEW_TYPE_HEADER = 0;
    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 2;


    private Listener listener;
    private LoadMoreListener loadMoreListener;
    private HeaderListener headerListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    private HeaderModel headerModel;


    public DashboardCandidateAdapter2(Context context, ArrayList<DashboardResponse.Recruter.Data> candidateModels) {
        this.context = context;
        this.candidateModels = candidateModels;
        headerModel = new HeaderModel();
    }

    public void update(ArrayList<DashboardResponse.Recruter.Data> candidateModels) {
        this.candidateModels = candidateModels;
        notifyDataSetChanged();
    }

    /*public void updateAll(ArrayList<DashboardResponse.Recruter.Data> candidateModels) {
        this.candidateModels.addAll(candidateModels);
        notifyItemInserted(this.candidateModels.size());
        //notifyDataSetChanged();
    }

    public void add(DashboardResponse.Recruter.Data models) {
        this.candidateModels.add(models);
        notifyItemInserted(this.candidateModels.size());
    }

    public void addLoading() {
        this.candidateModels.add(new DashboardResponse.Recruter.Data(true));
        //notifyItemInserted(getItemCount());
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.candidateModels.remove(getItemCount() - 1);
        //notifyItemRemoved(getItemCount() - 1);
        notifyDataSetChanged();
    }*/

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    public void setLoadMoreListener(LoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setHeaderListener(HeaderListener headerListener){
        this.headerListener = headerListener;
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }

    public void setJobHeader(String job){
        this.headerModel.setJob(job);
        notifyItemChanged(0);
    }

    public void setCityHeader(String city){
        this.headerModel.setCity(city);
        notifyItemChanged(0);
    }

    public void setCustomLocationHeader(String custom_location){
        this.headerModel.setCustom_location(custom_location);
        notifyItemChanged(0);
    }

    public void setTotalHeader(String total){
        this.headerModel.setTotal(total);
        notifyItemChanged(0);
    }

    public void setQeryHeader(String qery){
        this.headerModel.setQuery(qery);
        notifyItemChanged(0);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter, parent, false);
            return new HeaderHolder(v, headerListener);
        } else if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard_candidate, parent, false);
            return new Holder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((Holder) holder).setValue(candidateModels.get(position));
        } else if (getItemViewType(position) == VIEW_TYPE_HEADER) {
            ((HeaderHolder) holder).setValue(headerModel);
        }

    }

    private DashboardResponse.Recruter.Data getItem(int position) {
        return candidateModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return VIEW_TYPE_HEADER;
        else if (getItem(position).isLoading()) return VIEW_TYPE_LOADING;
        else return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return candidateModels == null ? 0 : candidateModels.size();
    }

    public class HeaderHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.btn_filter) LinearLayout btn_filter;
        @BindView(R.id.view_filter) LinearLayout view_filter;
        @BindView(R.id.view_custom_location) LinearLayout view_custom_location;
        @BindView(R.id.txt_job) AutoCompleteTextView txt_job;
        @BindView(R.id.txt_city) AutoCompleteTextView txt_city;
        @BindView(R.id.img_location) ImageView img_location;
        @BindView(R.id.txt_custom_location) TextView txt_custom_location;
        @BindView(R.id.ed_query) EditText ed_query;
        @BindView(R.id.btn_oke) Button btn_oke;
        @BindView(R.id.txt_total) TextView txt_total;

        HeaderHolder(View itemView, HeaderListener headerListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btn_filter.setOnClickListener(view -> {
                if (view_filter.getVisibility() == View.VISIBLE) collapse(view_filter);
                else expand(view_filter);
            });

            txt_job.setOnFocusChangeListener((view, b) -> {
                if (b){
                    headerListener.onClickJob();
                }
            });

            txt_city.setOnFocusChangeListener((view, b) -> {
                if (b){
                    headerListener.onClickCity();
                }
            });

            btn_oke.setOnClickListener(view -> {
                if (TextUtils.isEmpty(ed_query.getText().toString())) {
                    ed_query.setError("Cannot be empty");
                    return;
                }

                headerListener.onClickOke(ed_query.getText().toString());
            });

            img_location.setOnClickListener(view -> {
                headerListener.onClickLocation();
            });
        }

        private void setValue(HeaderModel headerModel){
            if (!headerModel.getJob().isEmpty()) txt_job.setText(headerModel.getJob());
            if (!headerModel.getCity().isEmpty()) txt_city.setText(headerModel.getCity());
            if (!headerModel.getTotal().isEmpty()) txt_total.setText(headerModel.getTotal());
            if (!headerModel.getQuery().isEmpty()) ed_query.setText("");
            if (!headerModel.getJob().isEmpty()) {
                txt_custom_location.setText(headerModel.getJob());
                view_custom_location.setVisibility(View.VISIBLE);
            }
        }
    }

    public class LoadHolder extends RecyclerView.ViewHolder {
        LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name) TextView txt_name;
        @BindView(R.id.txt_age) TextView txt_age;
        @BindView(R.id.txt_jobs) TextView txt_jobs;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.btn_detail) LinearLayout btn_detail;
        @BindView(R.id.btn_chat) LinearLayout btn_chat;
        @BindView(R.id.btn_detail_2) LinearLayout btn_detail_2;
        @BindView(R.id.img_user) CircleImageView img_user;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final DashboardResponse.Recruter.Data candidateModel) {
            txt_name.setText(candidateModel.getNama_ktp());
            txt_age.setText(Constants.genderValue(context, candidateModel.getJenis_kelamin()) + ", " + TimeUtils.getAge(candidateModel.getTgl_lahir()) + " Tahun");
            txt_jobs.setText(candidateModel.getNama_pekerjaan());
            txt_salary.setText(Constants.sallary(context, candidateModel.getGaji_min(), candidateModel.getGaji_max()));
            Glide.with(context).load(candidateModel.getFoto_terbaru()).into(img_user);

            btn_chat.setOnClickListener(view -> {
                if (listener != null){
                    listener.onChat(candidateModel);
                }
            });

            btn_detail_2.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(candidateModel);
                }
            });

            btn_detail.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(candidateModel);
                }
            });
        }
    }

    public interface Listener{
        void onClick(DashboardResponse.Recruter.Data model);
        void onChat(DashboardResponse.Recruter.Data model);
    }

    public interface HeaderListener{
        void onClickJob();
        void onClickCity();
        void onClickOke(String query);
        void onClickLocation();
    }

    public interface LoadMoreListener {
        void onLoadMore();
    }

    private void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
