package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class DataWorkAdapter extends RecyclerView.Adapter {

    private List<JobResponse.Data> workModels;
    private ArrayList<WorkModel> checkedData = new ArrayList<>();
    Listener listener;
    private Context context;
    private int mSelectedItem = -1;

    public DataWorkAdapter(Context context, ArrayList<WorkModel> checkedData) {
        this.context = context;
        this.checkedData = checkedData;
        workModels = new ArrayList<>();
    }

    public void update(boolean dashboard, List<JobResponse.Data> workModels) {
        if (dashboard) this.workModels.add(new JobResponse.Data("0", "Pilih Semua Pekerjaan"));
        this.workModels.addAll(workModels);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(workModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (workModels != null) return workModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_job) TextView txt_job;
        @BindView(R.id.img_checked) ImageView img_checked;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final JobResponse.Data job, int position) {
            txt_job.setText(job.getNama());
            if (position == mSelectedItem){
                img_checked.setVisibility(View.VISIBLE);
            } else {
                img_checked.setVisibility(View.INVISIBLE);
            }

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    if (checkedData.size() != 0){
                        for (WorkModel name : checkedData){
                            if (!name.getWork().equals(job.getNama())){
                                listener.onClick(job);
                                mSelectedItem = getAdapterPosition();
                                notifyItemRangeChanged(0, workModels.size());
                            } else {
                                listener.onDisabledClick();
                            }
                        }
                    } else {
                        listener.onClick(job);
                        mSelectedItem = getAdapterPosition();
                        notifyItemRangeChanged(0, workModels.size());
                    }
                }
            });
        }
    }

    public interface Listener{
        void onClick(JobResponse.Data model);
        void onDisabledClick();
    }
}
