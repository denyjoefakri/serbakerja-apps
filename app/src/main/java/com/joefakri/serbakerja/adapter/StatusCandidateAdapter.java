package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.StatusModel;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class StatusCandidateAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkResponse.Apply.Data> statusModels;
    private Listener listener;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public StatusCandidateAdapter(Context context) {
        this.context = context;
        statusModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkResponse.Apply.Data> statusModels) {
        this.statusModels = statusModels;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<WorkResponse.Apply.Data> workModels) {
        this.statusModels.addAll(workModels);
        notifyItemInserted(this.statusModels.size());
        //notifyDataSetChanged();
    }

    public void addLoading() {
        this.statusModels.add(new WorkResponse.Apply.Data(true));
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.statusModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_status_candidate_jobs, parent, false);
            return new JobHolder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((JobHolder) holder).setValue(statusModels.get(position));
        }

    }

    @Override
    public int getItemCount() {
        if (statusModels != null) return statusModels.size();
        else return 0;
    }

    private WorkResponse.Apply.Data getItem(int position) {
        return statusModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).isLoading()) return VIEW_TYPE_ITEM;
        else return VIEW_TYPE_LOADING;
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class JobHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_company) CircleImageView img_company;
        @BindView(R.id.txt_jobs) TextView txt_jobs;
        @BindView(R.id.txt_company) TextView txt_company;
        @BindView(R.id.txt_address) TextView txt_address;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_status) TextView txt_status;
        @BindView(R.id.txt_status_job) TextView txt_status_job;

        JobHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkResponse.Apply.Data statusModel) {
            txt_jobs.setText(statusModel.getNama_pekerjaan());
            txt_company.setText(statusModel.getNama_perusahaan());
            txt_address.setText(statusModel.getAlamat());
            txt_salary.setText(Constants.sallary(context, statusModel.getGaji_min(), statusModel.getGaji_max()));
            txt_status.setText("Status : " + Constants.getStatus(statusModel.getStatus()));
            Glide.with(context).load(statusModel.getLogo()).into(img_company);

            if (statusModel.getStatus_pekerjaan() != null && statusModel.getExpired_status() != null) {
                if (statusModel.getStatus_pekerjaan().equals("close") && statusModel.getExpired_status().equals("1")){
                    txt_status_job.setVisibility(View.VISIBLE);
                    txt_status_job.setText("Ditutup");
                } else if (statusModel.getStatus_pekerjaan().equals("close")) {
                    txt_status_job.setVisibility(View.VISIBLE);
                    txt_status_job.setText("Ditutup");
                } else if (statusModel.getExpired_status().equals("1")) {
                    txt_status_job.setVisibility(View.VISIBLE);
                    txt_status_job.setText("Ditutup");
                } else {
                    txt_status_job.setVisibility(View.GONE);
                }
            }

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onJobClick(statusModel);
                }
            });

        }
    }


    public interface Listener{
        void onJobClick(WorkResponse.Apply.Data model);
    }
}
