package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.NotificationResponse;
import com.joefakri.serbakerja.data.model.ChatModel;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by dev_deny on 1/25/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter {

    private List<NotificationResponse.ListNotif.Data> notificationModels;
    Listener listener;
    private Context context;

    public NotificationAdapter(Context context) {
        this.context = context;
        notificationModels = new ArrayList<>();
    }

    public void update(List<NotificationResponse.ListNotif.Data> notificationModels) {
        this.notificationModels = notificationModels;
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(notificationModels.get(position));
    }

    @Override
    public int getItemCount() {
        if (notificationModels != null) return notificationModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.view_item) LinearLayout view_item;
        @BindView(R.id.img_category) ImageView img_category;
        @BindView(R.id.txt_title) TextView txt_title;
        @BindView(R.id.txt_description) TextView txt_description;
        @BindView(R.id.txt_date) TextView txt_date;
        @BindView(R.id.txt_cs) TextView txt_cs;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final NotificationResponse.ListNotif.Data notificationModel) {
            if (notificationModel.getTipe() != null) {
                if (notificationModel.getTipe().equals("cs")){
                    view_item.setBackgroundResource(R.drawable.bg_button_grey);
                } else {
                    view_item.setBackgroundResource(R.drawable.bg_button_white);
                }

                if (notificationModel.getTipe().equals("pekerjaan")) {
                    txt_cs.setVisibility(View.GONE);
                    img_category.setImageResource(R.drawable.ic_work);
                } else if (notificationModel.getTipe().equals("chat")){
                    txt_cs.setVisibility(View.GONE);
                    img_category.setImageResource(R.drawable.ic_chat);
                } else {
                    txt_cs.setVisibility(View.VISIBLE);
                    img_category.setImageResource(R.drawable.ic_call_center);
                }
            }

            txt_description.setText(notificationModel.getPesan());
            txt_date.setText(TimeUtils.getTime(notificationModel.getDate()));

            SpannableStringBuilder title = new SpannableStringBuilder();
            title.append(notificationModel.getTipe());

            CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "fonts/SourceSansPro-Bold.ttf"));
            title.setSpan(typefaceSpan, 0, notificationModel.getTipe().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            txt_title.setText(title, TextView.BufferType.SPANNABLE);

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(notificationModel);
                }
            });
        }
    }

    public interface Listener{
        void onClick(NotificationResponse.ListNotif.Data notificationModel);
    }
}
