package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.HistoryResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.data.model.HistoryModel;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter {

    private ArrayList<HistoryResponse.Data> historyModels;
    Listener listener;
    private Context context;

    public HistoryAdapter(Context context) {
        this.context = context;
        historyModels = new ArrayList<>();
    }

    public void update(ArrayList<HistoryResponse.Data> historyModels) {
        this.historyModels = historyModels;
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(historyModels.get(position));
    }

    @Override
    public int getItemCount() {
        if (historyModels != null) return historyModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_category) ImageView img_category;
        @BindView(R.id.txt_category) TextView txt_category;
        @BindView(R.id.txt_message) TextView txt_message;
        @BindView(R.id.txt_date) TextView txt_date;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final HistoryResponse.Data historyModel) {
            if (historyModel.getTitle().equals("Pekerjaan")) img_category.setImageResource(R.drawable.ic_work);
            else img_category.setImageResource(R.drawable.ic_chat);

            txt_category.setText(historyModel.getTitle());
            txt_message.setText(historyModel.getActivity());
            txt_date.setText(TimeUtils.getTime(historyModel.getCreated_on()));

        }
    }

    public interface Listener{
        void onClick(CandidateModel model);
    }
}
