package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class WorkCandidateRegisterAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkModel> workModels;
    Listener listener;
    private Context context;

    public WorkCandidateRegisterAdapter(Context context) {
        this.context = context;
        workModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkModel> workModels) {
        this.workModels = workModels;
        notifyDataSetChanged();
    }

    public ArrayList<WorkModel> data(){
        return workModels;
    }

    public WorkModel workModel(int position){
        return workModels.get(position);
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_candidate, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(workModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (workModels != null) return workModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.divider_bottom) View divider_bottom;
        @BindView(R.id.txt_work_header) TextView txt_work_header;
        @BindView(R.id.txt_work) TextView txt_work;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_city) TextView txt_city;
        @BindView(R.id.txt_area) TextView txt_area;
        @BindView(R.id.txt_work_time) TextView txt_work_time;
        @BindView(R.id.txt_work_day) TextView txt_work_day;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkModel workModel, int position) {
            txt_work_header.setText(workModel.getWork());
            txt_work.setText(workModel.getWork());
            txt_salary.setText(workModel.getSalary());
            txt_area.setText(workModel.getArea());
            txt_work_time.setText(workModel.getWork_time());
            txt_work_day.setText(workModel.getWork_day());

            txt_city.setText(Util.getCitysName(workModel.getCitys()));

            if (getAdapterPosition() == getItemCount() - 1) divider_bottom.setVisibility(View.GONE);

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(workModel, position);
                }
            });
        }
    }

    public interface Listener{
        void onClick(WorkModel model, int position);
    }
}
