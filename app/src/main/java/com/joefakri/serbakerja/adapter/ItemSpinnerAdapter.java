package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.joefakri.serbakerja.R;

import java.util.ArrayList;

/**
 * Created by pristyanchandra on 10/14/16.
 */

public class ItemSpinnerAdapter extends ArrayAdapter {

    Context context;
    ArrayList<String> item;
    int itemLayout;
    LayoutInflater inflater;
    boolean img;

    public ItemSpinnerAdapter(Context context, LayoutInflater inflater, ArrayList<String> item, boolean img) {
        super(context, R.layout.item_single_spinner_adapter, item);
        this.context = context;
        this.inflater = inflater;
        this.itemLayout = R.layout.item_single_spinner_adapter;
        this.item = item;
        this.img = img;
    }

    public View getCustomView(int position, ViewGroup parent) {
        View layout = inflater.inflate(R.layout.item_single_spinner_adapter, parent, false);
        ImageView img_spinner = layout.findViewById(R.id.img_spinner);
        TextView tvTitle = layout.findViewById(R.id.item_spinner_text);
        tvTitle.setText(item.get(position));
        if (img){
            if (position == 0) img_spinner.setVisibility(View.VISIBLE);
            else img_spinner.setVisibility(View.GONE);
        }
        return layout;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, parent);
    }

    public int position(String value){
        return item.indexOf(value);
    };
}