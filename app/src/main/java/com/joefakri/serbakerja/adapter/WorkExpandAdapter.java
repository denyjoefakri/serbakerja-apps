package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class WorkExpandAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkModel> workModels;
    private Context context;

    public WorkExpandAdapter(Context context) {
        this.context = context;
        workModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkModel> workModels) {
        this.workModels = workModels;
        notifyDataSetChanged();
    }

    public ArrayList<WorkModel> data(){
        return workModels;
    }

    public WorkModel workModel(int position){
        return workModels.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_expand, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((Holder) holder).setValue(workModels.get(position));
    }

    @Override
    public int getItemCount() {
        if (workModels != null) return workModels.size();
        else return 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.divider_bottom) View divider_bottom;
        @BindView(R.id.view_base) View view_base;
        @BindView(R.id.view_expand) View view_expand;
        @BindView(R.id.txt_work_header) TextView txt_work_header;
        @BindView(R.id.txt_work) TextView txt_work;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_city) TextView txt_city;
        @BindView(R.id.txt_area) TextView txt_area;
        @BindView(R.id.txt_work_time) TextView txt_work_time;
        @BindView(R.id.txt_work_day) TextView txt_work_day;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkModel workModel) {

            txt_work_header.setText(Util.checkNull(workModel.getWork(), "-"));
            txt_work.setText(Util.checkNull(workModel.getWork(), "-"));
            txt_salary.setText(Util.checkNull(workModel.getSalary(), "-"));
            if (workModel.getWork_time() != null) {
                if (!workModel.getWork_time().equals("2"))
                    txt_work_time.setText(Util.checkNull(Constants.waktuKerjaValue(context, workModel.getWork_time()), "-"));
                else
                    txt_work_time.setText(Util.checkNull(workModel.getWork_time_other(), "-"));
            }

            txt_work_day.setText(Util.checkNull(workModel.getWork_day(), "-"));
            txt_area.setText(Util.checkNull(workModel.getArea(), "-"));

            txt_city.setText(Util.getCitysName(workModel.getCitys()));

            if (getAdapterPosition() == getItemCount() - 1) divider_bottom.setVisibility(View.GONE);

            view_base.setOnClickListener(view -> {
                if (view_expand.getVisibility() == View.VISIBLE) collapse(view_expand);
                else expand(view_expand);
            });
        }
    }

    public void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
