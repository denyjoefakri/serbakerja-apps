package com.joefakri.serbakerja.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class StatusRecruitersVerifyAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkResponse.List.Data> statusModels;
    private ApplicantsListener applicantsListener;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public StatusRecruitersVerifyAdapter(Context context) {
        this.context = context;
        statusModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkResponse.List.Data> statusModels) {
        this.statusModels = statusModels;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<WorkResponse.List.Data> workModels) {
        this.statusModels.addAll(workModels);
        notifyItemInserted(this.statusModels.size());
    }

    public void addLoading() {
        this.statusModels.add(new WorkResponse.List.Data(true));
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.statusModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void setApplicantsListener(ApplicantsListener listener){
        this.applicantsListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_status_applicants_verify, parent, false);
            return new ApplicantsHolder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((ApplicantsHolder) holder).setValue(statusModels.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (statusModels != null) return statusModels.size();
        else return 0;
    }

    private WorkResponse.List.Data getItem(int position) {
        return statusModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).isLoading()) return VIEW_TYPE_ITEM;
        else return VIEW_TYPE_LOADING;
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Log.e("load","LoadHolder");
        }
    }

    public class ApplicantsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_user) CircleImageView img_user;
        @BindView(R.id.txt_job_name) TextView txt_job_name;
        @BindView(R.id.txt_name) TextView txt_name;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_work_time) TextView txt_work_time;
        @BindView(R.id.txt_company) TextView txt_company;
        @BindView(R.id.txt_status) TextView txt_status;
        @BindView(R.id.txt_education) TextView txt_education;
        @BindView(R.id.btn_accept) LinearLayout btn_accept;
        @BindView(R.id.btn_reject) LinearLayout btn_reject;
        @BindView(R.id.btn_chat) LinearLayout btn_chat;
        @BindView(R.id.view_waiting) LinearLayout view_waiting;

        ApplicantsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("SetTextI18n")
        private void setValue(final WorkResponse.List.Data statusModel) {
            txt_job_name.setText("Melamar Pekerjaan " + statusModel.getNama_pekerjaan() + " di " + statusModel.getDaerah()
                    + ", " + statusModel.getName());
            txt_name.setText(statusModel.getNama_ktp());
            txt_salary.setText(Constants.sallary(context, statusModel.getGaji_min(), statusModel.getGaji_max()));
            if (statusModel.getWaktu_kerja() != null) {
                if (!statusModel.getWaktu_kerja().equals("2")) txt_work_time.setText(Constants.waktuKerjaValue(context, statusModel.getWaktu_kerja()));
                else txt_work_time.setText(statusModel.getWaktu_kerja_lainnya());
            }
            txt_company.setText(statusModel.getPosisi());
            txt_education.setText(Constants.education(context, statusModel.getPend_terakhir()));

            Glide.with(context).load(statusModel.getFoto_terbaru()).into(img_user);

            view_waiting.setVisibility(View.VISIBLE);
            btn_chat.setVisibility(View.GONE);

            if (statusModel.getStatus_lamaran() != null) {
                if (statusModel.getStatus_lamaran().equals("accept")){
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText(Constants.getStatus(statusModel.getStatus_lamaran()));
                } else if (statusModel.getStatus_lamaran().equals("reject")){
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText(Constants.getStatus(statusModel.getStatus_lamaran()));
                }

                if (statusModel.getStatus_lamaran().equals("accept") || statusModel.getStatus_lamaran().equals("reject")){
                    view_waiting.setVisibility(View.GONE);
                    btn_chat.setVisibility(View.GONE);
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText(Constants.getStatus(statusModel.getStatus_lamaran()));
                    if (statusModel.getStatus_lamaran().equals("accept")){
                        txt_status.setBackgroundColor(ContextCompat.getColor(context, R.color.green400));
                    } else {
                        txt_status.setBackgroundColor(ContextCompat.getColor(context, R.color.red400));
                    }
                } else {
                    view_waiting.setVisibility(View.VISIBLE);
                    btn_chat.setVisibility(View.GONE);
                    txt_status.setVisibility(View.GONE);
                }
            }

            itemView.setOnClickListener(view -> {
                if (applicantsListener != null){
                    applicantsListener.onApplicant(statusModel);
                }
            });

            btn_accept.setOnClickListener(view -> {
                if (applicantsListener != null){
                    applicantsListener.onAccept(statusModel);
                }
            });
            btn_reject.setOnClickListener(view -> {
                if (applicantsListener != null){
                    applicantsListener.onReject(statusModel);
                }
            });



        }
    }

    public interface ApplicantsListener{
        void onAccept(WorkResponse.List.Data model);
        void onReject(WorkResponse.List.Data model);
        void onApplicant(WorkResponse.List.Data model);
    }


}
