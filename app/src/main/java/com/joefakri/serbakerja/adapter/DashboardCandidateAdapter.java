package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.joefakri.serbakerja.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class DashboardCandidateAdapter extends RecyclerView.Adapter {

    private ArrayList<DashboardResponse.Recruter.Data> candidateModels;
    Listener listener;
    OnLoadMoreListener onLoadMoreListener;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading = false;
    private boolean isMoreDataAvailable = true;

    public DashboardCandidateAdapter(Context context) {
        this.context = context;
        candidateModels = new ArrayList<>();
    }

    public void update(ArrayList<DashboardResponse.Recruter.Data> candidateModels) {
        this.candidateModels = candidateModels;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<DashboardResponse.Recruter.Data> candidateModels) {
        this.candidateModels.addAll(candidateModels);
        notifyItemInserted(this.candidateModels.size());
        //notifyDataSetChanged();
    }

    public void add(DashboardResponse.Recruter.Data models) {
        this.candidateModels.add(models);
        notifyItemInserted(this.candidateModels.size());
    }

    public void addLoading() {
        this.candidateModels.add(new DashboardResponse.Recruter.Data(true));
        //notifyItemInserted(getItemCount());
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.candidateModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard_candidate, parent, false);
            return new Holder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        /*if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && onLoadMoreListener != null) {
            isLoading = true;
            onLoadMoreListener.onLoadMore();
        }*/

        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((Holder) holder).setValue(candidateModels.get(position));
        }

    }

    private DashboardResponse.Recruter.Data getItem(int position) {
        return candidateModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).isLoading()) return VIEW_TYPE_ITEM;
        else return VIEW_TYPE_LOADING;
    }

    @Override
    public int getItemCount() {
        return candidateModels == null ? 0 : candidateModels.size();
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Log.e("load","LoadHolder");
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_name) TextView txt_name;
        @BindView(R.id.txt_age) TextView txt_age;
        @BindView(R.id.txt_jobs) TextView txt_jobs;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.btn_detail) LinearLayout btn_detail;
        @BindView(R.id.btn_chat) LinearLayout btn_chat;
        @BindView(R.id.btn_detail_2) LinearLayout btn_detail_2;
        @BindView(R.id.img_user) CircleImageView img_user;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final DashboardResponse.Recruter.Data candidateModel) {
            txt_name.setText(candidateModel.getNama_ktp());
            txt_age.setText(Constants.genderValue(context, candidateModel.getJenis_kelamin()) + ", " + TimeUtils.getAge(candidateModel.getTgl_lahir()) + " Tahun");
            txt_jobs.setText(candidateModel.getNama_pekerjaan());
            txt_salary.setText(Constants.sallary(context, candidateModel.getGaji_min(), candidateModel.getGaji_max()));
            Glide.with(context).load(candidateModel.getFoto_terbaru()).into(img_user);

            btn_chat.setOnClickListener(view -> {
                if (listener != null){
                    listener.onChat(candidateModel);
                }
            });

            btn_detail_2.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(candidateModel);
                }
            });

            btn_detail.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(candidateModel);
                }
            });
        }
    }

    public interface Listener{
        void onClick(DashboardResponse.Recruter.Data model);
        void onChat(DashboardResponse.Recruter.Data model);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }
}
