package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.StatusModel;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dev_deny on 1/25/17.
 */

public class StatusRecruitersAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkResponse.List.Data> statusModels;
    private Listener listener;
    private ApplicantsListener applicantsListener;
    private Context context;
    private int type;

    public StatusRecruitersAdapter(Context context) {
        this.context = context;
        statusModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkResponse.List.Data> statusModels, int type) {
        this.statusModels = statusModels;
        this.type = type;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<WorkResponse.List.Data> workModels, int type) {
        this.statusModels.addAll(workModels);
        this.type = type;
        notifyItemInserted(this.statusModels.size());
    }

    public void addLoading() {
        this.statusModels.add(new WorkResponse.List.Data(true));
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.statusModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    public void setApplicantsListener(ApplicantsListener listener){
        this.applicantsListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_status_jobs, parent, false);
            return new JobHolder(v);
        } else if (viewType == 2) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_status_applicants, parent, false);
            return new ApplicantsHolder(v);
        } else if (viewType == 3) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == 1){
            ((JobHolder) holder).setValue(statusModels.get(position));
        } else if (getItemViewType(position) == 2){
            ((ApplicantsHolder) holder).setValue(statusModels.get(position));
        }
    }

    private WorkResponse.List.Data getItem(int position) {
        return statusModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if(type == 1 && !getItem(position).isLoading()){
            return 1;
        } else if (type == 2 && !getItem(position).isLoading()){
            return 2;
        } else {
            return 3;
        }
    }

    @Override
    public int getItemCount() {
        if (statusModels != null) return statusModels.size();
        else return 0;
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Log.e("load","LoadHolder");
        }
    }

    public class JobHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_user) CircleImageView img_user;
        @BindView(R.id.txt_status) TextView txt_status;
        @BindView(R.id.txt_jobs) TextView txt_jobs;
        @BindView(R.id.txt_address) TextView txt_address;
        @BindView(R.id.txt_work_expired_date) TextView txt_work_expired_date;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.btn_closed) LinearLayout btn_closed;

        JobHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkResponse.List.Data model) {
            txt_jobs.setText(model.getJenis_pekerjaan());
            txt_work_expired_date.setText(TimeUtils.getTime(model.getExpired_date()));
            txt_address.setText(model.getLokasi() + ", " + model.getKota());
            txt_salary.setText(Constants.sallary(context, model.getGaji_min(), model.getGaji_max()));

            Glide.with(context).load(model.getLogo_perusahaan()).into(img_user);

            if (model.getStatus() != null){
                if (model.getStatus().equals("close") && model.getExpired_status().equals("1")){
                    btn_closed.setVisibility(View.GONE);
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText("Tutup");
                } else if (model.getStatus().equals("close")) {
                    btn_closed.setVisibility(View.GONE);
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText("Tutup");
                } else if (model.getExpired_status().equals("1")) {
                    btn_closed.setVisibility(View.GONE);
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText("Kadaluarsa");
                } else {
                    btn_closed.setVisibility(View.VISIBLE);
                    txt_status.setVisibility(View.GONE);
                }
            }

            btn_closed.setOnClickListener(view -> {
                if (listener != null){
                    listener.onJobClosed(model);
                }
            });

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onJobClick(model);
                }
            });

        }
    }

    public class ApplicantsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_user) CircleImageView img_user;
        @BindView(R.id.txt_name) TextView txt_name;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_work_time) TextView txt_work_time;
        @BindView(R.id.txt_company) TextView txt_company;
        @BindView(R.id.txt_education) TextView txt_education;
        @BindView(R.id.txt_status) TextView txt_status;
        @BindView(R.id.btn_accept) LinearLayout btn_accept;
        @BindView(R.id.btn_reject) LinearLayout btn_reject;
        @BindView(R.id.btn_chat) LinearLayout btn_chat;
        @BindView(R.id.view_waiting) LinearLayout view_waiting;

        ApplicantsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkResponse.List.Data statusModel) {
            txt_name.setText(statusModel.getNama_ktp());
            txt_salary.setText(Constants.sallary(context, statusModel.getGaji_min(), statusModel.getGaji_max()));
            if (statusModel.getWaktu_kerja() != null) {
                if (!statusModel.getWaktu_kerja().equals("2"))
                    txt_work_time.setText(Constants.waktuKerjaValue(context, statusModel.getWaktu_kerja()));
                else txt_work_time.setText(statusModel.getWaktu_kerja_lainnya());
            }
            txt_company.setText(statusModel.getPosisi());
            txt_education.setText(Constants.education(context, statusModel.getPend_terakhir()));

            Glide.with(context).load(statusModel.getFoto_terbaru()).into(img_user);

            txt_status.setText(Constants.getStatus(statusModel.getStatus_lamaran()));
            if (statusModel.getStatus_lamaran() != null) {
                if (statusModel.getStatus_lamaran().equals("pending")){
                    txt_status.setVisibility(View.VISIBLE);
                    view_waiting.setVisibility(View.GONE);
                    btn_chat.setVisibility(View.VISIBLE);
                } else if (statusModel.getStatus_lamaran().equals("reject")){
                    txt_status.setVisibility(View.VISIBLE);
                    view_waiting.setVisibility(View.GONE);
                    btn_chat.setVisibility(View.GONE);
                } else {
                    txt_status.setVisibility(View.GONE);
                    view_waiting.setVisibility(View.VISIBLE);
                    btn_chat.setVisibility(View.GONE);
                }
            }

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onApplicantClick(statusModel);
                }
            });

            btn_accept.setOnClickListener(view -> {
                if (applicantsListener != null){
                    applicantsListener.onAccept(statusModel);
                }
            });
            btn_reject.setOnClickListener(view -> {
                if (applicantsListener != null){
                    applicantsListener.onReject(statusModel);
                }
            });

            btn_chat.setOnClickListener(view -> {
                if (applicantsListener != null){
                    applicantsListener.onChat(statusModel);
                }
            });

        }
    }

    public interface ApplicantsListener{
        void onAccept(WorkResponse.List.Data model);
        void onReject(WorkResponse.List.Data model);
        void onChat(WorkResponse.List.Data model);
    }



    public interface Listener{
        void onJobClick(WorkResponse.List.Data model);
        void onJobClosed(WorkResponse.List.Data model);
        void onApplicantClick(WorkResponse.List.Data model);
    }
}
