package com.joefakri.serbakerja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_deny on 1/25/17.
 */

public class WorkPostAdapter extends RecyclerView.Adapter {

    private ArrayList<WorkResponse.List.Data> workModels;
    Listener listener;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public WorkPostAdapter(Context context) {
        this.context = context;
        workModels = new ArrayList<>();
    }

    public void update(ArrayList<WorkResponse.List.Data> workModels) {
        this.workModels = workModels;
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<WorkResponse.List.Data> workModels) {
        this.workModels.addAll(workModels);
        notifyItemInserted(this.workModels.size());
        //notifyDataSetChanged();
    }

    public void addLoading() {
        this.workModels.add(new WorkResponse.List.Data(true));
        notifyDataSetChanged();
    }

    public void removeLoading() {
        this.workModels.remove(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void setOnclickListener(Listener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_post_recruiter, parent, false);
            return new Holder(v);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            ((Holder) holder).setValue(workModels.get(position), position);
        }
    }

    @Override
    public int getItemCount() {
        if (workModels != null) return workModels.size();
        else return 0;
    }

    private WorkResponse.List.Data getItem(int position) {
        return workModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (!getItem(position).isLoading()) return VIEW_TYPE_ITEM;
        else return VIEW_TYPE_LOADING;
    }

    public class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_loading) ProgressBar pb_loading;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Log.e("load","LoadHolder");
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.divider_bottom) View divider_bottom;
        @BindView(R.id.txt_work_header) TextView txt_work_header;
        @BindView(R.id.txt_work) TextView txt_work;
        @BindView(R.id.txt_salary) TextView txt_salary;
        @BindView(R.id.txt_city) TextView txt_city;
        @BindView(R.id.txt_area) TextView txt_area;
        @BindView(R.id.txt_work_address) TextView txt_work_address;
        @BindView(R.id.txt_work_time) TextView txt_work_time;
        @BindView(R.id.txt_work_day) TextView txt_work_day;
        @BindView(R.id.txt_work_minimal_experience) TextView txt_work_minimal_experience;
        @BindView(R.id.txt_work_education) TextView txt_work_education;
        @BindView(R.id.txt_work_gender) TextView txt_work_gender;
        @BindView(R.id.txt_work_custom_requirement) TextView txt_work_custom_requirement;
        @BindView(R.id.txt_work_scope) TextView txt_work_scope;
        @BindView(R.id.txt_work_expired) TextView txt_work_expired;
        @BindView(R.id.txt_status) TextView txt_status;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void setValue(final WorkResponse.List.Data experienceWorkModel, int position) {

            txt_work_header.setText(experienceWorkModel.getJenis_pekerjaan());
            txt_work.setText(experienceWorkModel.getJenis_pekerjaan());
            txt_salary.setText(Constants.sallary(context, experienceWorkModel.getGaji_min(), experienceWorkModel.getGaji_max()));
            txt_city.setText(experienceWorkModel.getKota());
            txt_area.setText(experienceWorkModel.getLokasi());
            txt_work_address.setText(experienceWorkModel.getAlamat());
            txt_work_day.setText(experienceWorkModel.getHari_kerja());
            txt_work_minimal_experience.setText(Constants.minimal_experience(context, experienceWorkModel.getMin_pengalaman()));
            String positionEducation = Constants.education(context, experienceWorkModel.getMin_pendidikan());
            txt_work_education.setText(positionEducation);
            txt_work_scope.setText(experienceWorkModel.getLingkup_pekerjaan());
            txt_work_custom_requirement.setText(experienceWorkModel.getSyarat_tambahan());
            txt_work_gender.setText(Constants.genderValue(context, experienceWorkModel.getJenis_kelamin()));
            txt_work_expired.setText(TimeUtils.getTime(experienceWorkModel.getExpired_date()));

            if (experienceWorkModel.getWaktu_kerja() != null) {
                if (!experienceWorkModel.getWaktu_kerja().equals("2"))
                    txt_work_time.setText(Constants.waktuKerjaValue(context, experienceWorkModel.getWaktu_kerja()));
                else txt_work_time.setText(experienceWorkModel.getWaktu_kerja_lainnya());
            }

            if (getAdapterPosition() == getItemCount() - 1) divider_bottom.setVisibility(View.GONE);
            else divider_bottom.setVisibility(View.VISIBLE);

            if (experienceWorkModel.getStatus() != null && experienceWorkModel.getExpired_status() != null) {
                if (experienceWorkModel.getStatus().equals("close") && experienceWorkModel.getExpired_status().equals("1")){
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText("Tutup");
                    itemView.setEnabled(false);
                } else if (experienceWorkModel.getStatus().equals("close")) {
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText("Tutup");
                    itemView.setEnabled(false);
                } else if (experienceWorkModel.getExpired_status().equals("1")) {
                    txt_status.setVisibility(View.VISIBLE);
                    txt_status.setText("Kadaluarsa");
                    itemView.setEnabled(false);
                } else {
                    txt_status.setVisibility(View.GONE);
                    itemView.setEnabled(true);
                }
            }

            itemView.setOnClickListener(view -> {
                if (listener != null){
                    listener.onClick(experienceWorkModel, position);
                }
            });
        }
    }

    public interface Listener{
        void onClick(WorkResponse.List.Data model, int position);
    }
}
