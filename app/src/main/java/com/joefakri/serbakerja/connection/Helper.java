/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.connection;

import com.joefakri.serbakerja.connection.request.AreaRequest;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.ChatRequest;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.request.WorkRequest;
import com.joefakri.serbakerja.connection.response.ChatResponse;

import org.json.JSONObject;

import java.io.File;

import io.reactivex.Observable;

/**
 * Created by janisharali on 27/01/17.
 */

public interface Helper {

    Header getApiHeader();

    Observable<JSONObject> register(UserRequest.Register request);

    Observable<JSONObject> login(UserRequest.Login request);

    Observable<JSONObject> fcm(UserRequest.FCM request);

    Observable<JSONObject> changePassword(UserRequest.Password request);

    Observable<JSONObject> verifikasi(UserRequest.Verification verification);

    Observable<JSONObject> saran(UserRequest.Saran request);

    Observable<JSONObject> listChat(UserRequest.Id id, String offset);

    Observable<JSONObject> chatSend(ChatRequest.SendMessage request, String TAG);

    Observable<JSONObject> chatRead(ChatRequest.ReadMessage request);

    Observable<JSONObject> getChat(ChatRequest.GetMessage request);

    Observable<JSONObject> getJenisPekerjaan();

    Observable<JSONObject> getCity();

    Observable<JSONObject> getDistrict(AreaRequest.District district);

    Observable<JSONObject> postRCompany(RecruiterRequest.PostCompany post_company, File file);

    Observable<JSONObject> postRWork(RecruiterRequest.PostWork postWork);

    Observable<JSONObject> postRInfo(RecruiterRequest.PostInfo postInfo, RecruiterRequest.PostInfoFile file);

    Observable<JSONObject> postCInfo(CandidateRequest.PostInfo postInfo);

    Observable<JSONObject> postCWork(CandidateRequest.PostDocument userid, CandidateRequest.PostWork postWork);

    Observable<JSONObject> postCExperience(CandidateRequest.PostExperience postExperience, CandidateRequest.Experience experience);

    Observable<JSONObject> postCEducation(CandidateRequest.PostEducation postEducation);

    Observable<JSONObject> postCDocument(CandidateRequest.PostDocument postDocument, CandidateRequest.PostDocumentFile file);

    Observable<JSONObject> dashboardPerekrut(DashboardRequest.Recruter recruter, DashboardRequest.CustomLocation location, String offset);

    Observable<JSONObject> dashboardPerekrutDetail(DashboardRequest.RecruterDetail recruter);

    Observable<JSONObject> dashboardPerekrutSearch(DashboardRequest.RecruterSearch recruter, String offset);

    Observable<JSONObject> recruterProfile(RecruiterRequest.Profile recruter);

    Observable<JSONObject> changeRecruterCompanyProfile(UserRequest.Id id,
                                                        RecruiterRequest.ChangeCompany changeProfile,
                                                        RecruiterRequest.ChangeCompanyFile file);

    Observable<JSONObject> jobRecruterProfile(UserRequest.Id id, String offset);

    Observable<JSONObject> history(UserRequest.Id id);

    Observable<JSONObject> closeJob(WorkRequest.Status id);

    Observable<JSONObject> applicant(UserRequest.Id id, String offset);

    Observable<JSONObject> update_status_applicant(UserRequest.Status status);

    Observable<JSONObject> verify(UserRequest.Id id, String offset);

    Observable<JSONObject> recruter(UserRequest.Id id);

    Observable<JSONObject> changeRecruterProfile(UserRequest.Id id, RecruiterRequest.ChangeProfile profile,
                                                 RecruiterRequest.ChangeProfileFile profileFile);

    Observable<JSONObject> changeRecruterWork(UserRequest.Id id, RecruiterRequest.PostWork2 postWork);

    Observable<JSONObject> dashboardCandidate(UserRequest.Id id, DashboardRequest.Candidate candidate, DashboardRequest.CustomLocation location, String offset);

    Observable<JSONObject> dashboardCandidateDetail(UserRequest.Pengguna pengguna, DashboardRequest.CandidateDetail candidate);

    Observable<JSONObject> dashboardCandidateSearch(UserRequest.Id id, DashboardRequest.CandidateSearch candidate, String offset);

    Observable<JSONObject> dashboardCandidateApply(UserRequest.Id id, UserRequest.Pengguna pengguna);

    Observable<JSONObject> candidateStatusApply(UserRequest.Id id, String offset);

    Observable<JSONObject> profileCandidate(UserRequest.Id id);

    Observable<JSONObject> profileUpdateCandidate(UserRequest.Id id, CandidateRequest.ChangeProfile profile,
                                                  CandidateRequest.ChangeProfileFile profileFile);

    Observable<JSONObject> profileCandidateWork(UserRequest.Id id);

    Observable<JSONObject> profileUpdateCandidateWork(UserRequest.Id id, CandidateRequest.ChangeProfileWork profile);

    Observable<JSONObject> profileCandidateEducation(UserRequest.Id id);

    Observable<JSONObject> profileUpdateCandidateEducation(UserRequest.Id id, CandidateRequest.ChangeProfileEducation profile);

    Observable<JSONObject> profileCandidateDocument(UserRequest.Id id);

    Observable<JSONObject> profileUpdateCandidateDocument(UserRequest.Id id, CandidateRequest.PostDocumentFile profile);

    Observable<JSONObject> profileCandidateExperience(UserRequest.Id id);

    Observable<JSONObject> profileUpdateCandidateExperience(UserRequest.Id id, CandidateRequest.ChangeProfileExperience profile);

    Observable<JSONObject> profileUpdateCandidateExperienceDetail(UserRequest.Id id, CandidateRequest.ChangeProfileExperience profile);

    Observable<JSONObject> profileUpdateCandidateExperienceDetailInsert(CandidateRequest.ChangeProfileExperience profile);

    Observable<JSONObject> notification(UserRequest.Id id);

    Observable<JSONObject> readNotification(UserRequest.Id id);

    Observable<JSONObject> deactiveAccount(UserRequest.Pengguna id);

}
