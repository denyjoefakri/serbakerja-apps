package com.joefakri.serbakerja.connection.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deny on bandung.
 */

public class UserResponse {

    public static class Register {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Register that = (Register) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String id;
            private String nama;
            private String no_hp;
            private String email;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

            public String getNo_hp() {
                return no_hp;
            }

            public void setNo_hp(String no_hp) {
                this.no_hp = no_hp;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Data)) return false;

                Data data = (Data) o;

                if (!id.equals(data.id)) return false;
                if (!nama.equals(data.nama)) return false;
                if (!no_hp.equals(data.no_hp)) return false;
                return email.equals(data.email);

            }

            @Override
            public int hashCode() {
                int result = id.hashCode();
                result = 31 * result + nama.hashCode();
                result = 31 * result + no_hp.hashCode();
                result = 31 * result + email.hashCode();
                return result;
            }
        }
    }

    public static class Login {

        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public static class Data {
            private String id;
            private String tipe_user;
            private String nama;
            private String no_hp;
            private String email;
            private String status;
            private String verifikasi_hp;
            private String count_proses_perekrut;
            private String count_proses_kandidat;
            private Info info;
            private ProsesDetail proses_detail;
            private ArrayList<Kota> kota;
            private ArrayList<Jenis_pekerjaan> jenis_pekerjaan;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTipe_user() {
                return tipe_user;
            }

            public void setTipe_user(String tipe_user) {
                this.tipe_user = tipe_user;
            }

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

            public String getNo_hp() {
                return no_hp;
            }

            public void setNo_hp(String no_hp) {
                this.no_hp = no_hp;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getVerifikasi_hp() {
                return verifikasi_hp;
            }

            public void setVerifikasi_hp(String verifikasi_hp) {
                this.verifikasi_hp = verifikasi_hp;
            }

            public String getCount_proses_perekrut() {
                return count_proses_perekrut;
            }

            public void setCount_proses_perekrut(String count_proses_perekrut) {
                this.count_proses_perekrut = count_proses_perekrut;
            }

            public String getCount_proses_kandidat() {
                return count_proses_kandidat;
            }

            public void setCount_proses_kandidat(String count_proses_kandidat) {
                this.count_proses_kandidat = count_proses_kandidat;
            }

            public ProsesDetail getProses_detail() {
                return proses_detail;
            }

            public void setProses_detail(ProsesDetail proses_detail) {
                this.proses_detail = proses_detail;
            }

            public ArrayList<Kota> getKota() {
                return kota;
            }

            public void setKota(ArrayList<Kota> kota) {
                this.kota = kota;
            }

            public ArrayList<Jenis_pekerjaan> getJenis_pekerjaan() {
                return jenis_pekerjaan;
            }

            public void setJenis_pekerjaan(ArrayList<Jenis_pekerjaan> jenis_pekerjaan) {
                this.jenis_pekerjaan = jenis_pekerjaan;
            }

            public Info getInfo() {
                return info;
            }

            public void setInfo(Info info) {
                this.info = info;
            }

            public static class Kota {
                private String id;
                private String name;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }

            public static class Jenis_pekerjaan {
                private String id;
                private String jenis_pekerjaan;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getJenis_pekerjaan() {
                    return jenis_pekerjaan;
                }

                public void setJenis_pekerjaan(String jenis_pekerjaan) {
                    this.jenis_pekerjaan = jenis_pekerjaan;
                }
            }

            public static class ProsesDetail {
                private String p_info_perusahaan;
                private String p_pekerjaan;
                private String p_info_perekrut;
                private String k_info;
                private String k_minat;
                private String k_pengalaman;
                private String k_pendidikan;
                private String k_dokumen;

                public String getP_info_perusahaan() {
                    return p_info_perusahaan;
                }

                public void setP_info_perusahaan(String p_info_perusahaan) {
                    this.p_info_perusahaan = p_info_perusahaan;
                }

                public String getP_pekerjaan() {
                    return p_pekerjaan;
                }

                public void setP_pekerjaan(String p_pekerjaan) {
                    this.p_pekerjaan = p_pekerjaan;
                }

                public String getP_info_perekrut() {
                    return p_info_perekrut;
                }

                public void setP_info_perekrut(String p_info_perekrut) {
                    this.p_info_perekrut = p_info_perekrut;
                }

                public String getK_info() {
                    return k_info;
                }

                public void setK_info(String k_info) {
                    this.k_info = k_info;
                }

                public String getK_minat() {
                    return k_minat;
                }

                public void setK_minat(String k_minat) {
                    this.k_minat = k_minat;
                }

                public String getK_pengalaman() {
                    return k_pengalaman;
                }

                public void setK_pengalaman(String k_pengalaman) {
                    this.k_pengalaman = k_pengalaman;
                }

                public String getK_pendidikan() {
                    return k_pendidikan;
                }

                public void setK_pendidikan(String k_pendidikan) {
                    this.k_pendidikan = k_pendidikan;
                }

                public String getK_dokumen() {
                    return k_dokumen;
                }

                public void setK_dokumen(String k_dokumen) {
                    this.k_dokumen = k_dokumen;
                }
            }

            public static class Info {
                private String id;
                private String pengguna_id;
                private String nama_perusahaan;
                private String alamat_perusahaan;
                private String kota_id;
                private String daerah;
                private String jumlah_karyawan;
                private String website;
                private String logo;
                private String nama_ktp;
                private String tempat_lahir;
                private String tgl_lahir;
                private String alamat_ktp;
                private String kecamatan;
                private String kelurahan;
                private String rt_rw;
                private String jenis_kelamin;
                private String agama;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getPengguna_id() {
                    return pengguna_id;
                }

                public void setPengguna_id(String pengguna_id) {
                    this.pengguna_id = pengguna_id;
                }

                public String getNama_perusahaan() {
                    return nama_perusahaan;
                }

                public void setNama_perusahaan(String nama_perusahaan) {
                    this.nama_perusahaan = nama_perusahaan;
                }

                public String getAlamat_perusahaan() {
                    return alamat_perusahaan;
                }

                public void setAlamat_perusahaan(String alamat_perusahaan) {
                    this.alamat_perusahaan = alamat_perusahaan;
                }

                public String getKota_id() {
                    return kota_id;
                }

                public void setKota_id(String kota_id) {
                    this.kota_id = kota_id;
                }

                public String getDaerah() {
                    return daerah;
                }

                public void setDaerah(String daerah) {
                    this.daerah = daerah;
                }

                public String getJumlah_karyawan() {
                    return jumlah_karyawan;
                }

                public void setJumlah_karyawan(String jumlah_karyawan) {
                    this.jumlah_karyawan = jumlah_karyawan;
                }

                public String getWebsite() {
                    return website;
                }

                public void setWebsite(String website) {
                    this.website = website;
                }

                public String getLogo() {
                    return logo;
                }

                public void setLogo(String logo) {
                    this.logo = logo;
                }

                public String getNama_ktp() {
                    return nama_ktp;
                }

                public void setNama_ktp(String nama_ktp) {
                    this.nama_ktp = nama_ktp;
                }

                public String getTempat_lahir() {
                    return tempat_lahir;
                }

                public void setTempat_lahir(String tempat_lahir) {
                    this.tempat_lahir = tempat_lahir;
                }

                public String getTgl_lahir() {
                    return tgl_lahir;
                }

                public void setTgl_lahir(String tgl_lahir) {
                    this.tgl_lahir = tgl_lahir;
                }

                public String getAlamat_ktp() {
                    return alamat_ktp;
                }

                public void setAlamat_ktp(String alamat_ktp) {
                    this.alamat_ktp = alamat_ktp;
                }

                public String getKecamatan() {
                    return kecamatan;
                }

                public void setKecamatan(String kecamatan) {
                    this.kecamatan = kecamatan;
                }

                public String getKelurahan() {
                    return kelurahan;
                }

                public void setKelurahan(String kelurahan) {
                    this.kelurahan = kelurahan;
                }

                public String getRt_rw() {
                    return rt_rw;
                }

                public void setRt_rw(String rt_rw) {
                    this.rt_rw = rt_rw;
                }

                public String getJenis_kelamin() {
                    return jenis_kelamin;
                }

                public void setJenis_kelamin(String jenis_kelamin) {
                    this.jenis_kelamin = jenis_kelamin;
                }

                public String getAgama() {
                    return agama;
                }

                public void setAgama(String agama) {
                    this.agama = agama;
                }
            }
        }

    }

}
