package com.joefakri.serbakerja.connection.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deny on bandung.
 */

public class DashboardResponse {

    public static class Recruter implements Serializable{

        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public static class Data implements Serializable{

            public Data(boolean loading) {
                this.loading = loading;
            }

            private boolean loading = false;
            private String pengguna_id;
            private String nama_ktp;
            private String tgl_lahir;
            private String jenis_kelamin;
            private String gaji_min;
            private String gaji_max;
            private String foto_terbaru;
            private String foto_ktp;
            private String nama_pekerjaan;

            public boolean isLoading() {
                return loading;
            }

            public void setLoading(boolean loading) {
                this.loading = loading;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getFoto_terbaru() {
                return foto_terbaru;
            }

            public void setFoto_terbaru(String foto_terbaru) {
                this.foto_terbaru = foto_terbaru;
            }

            public String getFoto_ktp() {
                return foto_ktp;
            }

            public void setFoto_ktp(String foto_ktp) {
                this.foto_ktp = foto_ktp;
            }

            public String getNama_pekerjaan() {
                return nama_pekerjaan;
            }

            public void setNama_pekerjaan(String nama_pekerjaan) {
                this.nama_pekerjaan = nama_pekerjaan;
            }
        }
    }

    public static class Candidate implements Serializable {
        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public static class Data implements Serializable {

            public Data(boolean loading) {
                this.loading = loading;
            }

            private boolean loading = false;
            private String id;
            private String jenis_pekerjaan;
            private String gaji_min;
            private String gaji_max;
            private String lokasi;
            private String alamat;
            private String kota;
            private String nama_perusahaan;
            private String logo;
            private String expired_date;
            private String status;
            private String status_lamar;

            public boolean isLoading() {
                return loading;
            }

            public void setLoading(boolean loading) {
                this.loading = loading;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getJenis_pekerjaan() {
                return jenis_pekerjaan;
            }

            public void setJenis_pekerjaan(String jenis_pekerjaan) {
                this.jenis_pekerjaan = jenis_pekerjaan;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getLokasi() {
                return lokasi;
            }

            public void setLokasi(String lokasi) {
                this.lokasi = lokasi;
            }

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getKota() {
                return kota;
            }

            public void setKota(String kota) {
                this.kota = kota;
            }

            public String getNama_perusahaan() {
                return nama_perusahaan;
            }

            public void setNama_perusahaan(String nama_perusahaan) {
                this.nama_perusahaan = nama_perusahaan;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getExpired_date() {
                return expired_date;
            }

            public void setExpired_date(String expired_date) {
                this.expired_date = expired_date;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getStatus_lamar() {
                return status_lamar;
            }

            public void setStatus_lamar(String status_lamar) {
                this.status_lamar = status_lamar;
            }
        }
    }

    public static class Apply implements Serializable {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public static class Data implements Serializable {

            private String message;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }
}
