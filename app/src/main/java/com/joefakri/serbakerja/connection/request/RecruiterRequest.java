package com.joefakri.serbakerja.connection.request;

import java.io.File;
import java.util.HashMap;

/**
 * Created by deny on bandung.
 */

public class RecruiterRequest {

    public static class PostCompany {

        private String pengguna_id;
        private String nama_perusahaan;
        private String alamat_perusahaan;
        private String kota_id;
        private String daerah;
        private String jumlah_karyawan;
        private String website;

        public PostCompany(String pengguna_id, String nama_perusahaan, String alamat_perusahaan,
                           String kota_id, String daerah, String jumlah_karyawan, String website) {
            this.pengguna_id = pengguna_id;
            this.nama_perusahaan = nama_perusahaan;
            this.alamat_perusahaan = alamat_perusahaan;
            this.kota_id = kota_id;
            this.daerah = daerah;
            this.jumlah_karyawan = jumlah_karyawan;
            this.website = website;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostCompany)) return false;

            PostCompany that = (PostCompany) o;

            if (pengguna_id != null ? !pengguna_id.equals(that.pengguna_id) : that.pengguna_id != null)
                return false;
            if (nama_perusahaan != null ? !nama_perusahaan.equals(that.nama_perusahaan) : that.nama_perusahaan != null)
                return false;
            if (alamat_perusahaan != null ? !alamat_perusahaan.equals(that.alamat_perusahaan) : that.alamat_perusahaan != null)
                return false;
            if (kota_id != null ? !kota_id.equals(that.kota_id) : that.kota_id != null)
                return false;
            if (daerah != null ? !daerah.equals(that.daerah) : that.daerah != null) return false;
            if (jumlah_karyawan != null ? !jumlah_karyawan.equals(that.jumlah_karyawan) : that.jumlah_karyawan != null)
                return false;
            return website != null ? website.equals(that.website) : that.website == null;

        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (nama_perusahaan != null ? nama_perusahaan.hashCode() : 0);
            result = 31 * result + (alamat_perusahaan != null ? alamat_perusahaan.hashCode() : 0);
            result = 31 * result + (kota_id != null ? kota_id.hashCode() : 0);
            result = 31 * result + (daerah != null ? daerah.hashCode() : 0);
            result = 31 * result + (jumlah_karyawan != null ? jumlah_karyawan.hashCode() : 0);
            result = 31 * result + (website != null ? website.hashCode() : 0);
            return result;
        }
    }

    public static class PostWork {

        private String pengguna_id;
        private String jenis_pekerjaan_id;
        private String gaji_min;
        private String gaji_max;
        private String expired_date;
        private String alamat;
        private String lokasi;
        private String kota_id;
        private String waktu_kerja;
        private String waktu_kerja_lainnya;
        private String hari_kerja;
        private String jenis_kelamin;
        private String min_pengalaman;
        private String min_pendidikan;
        private String syarat_tambahan;
        private String lingkup_pekerjaan;

        public PostWork(String pengguna_id, String jenis_pekerjaan_id,
                        String gaji_min, String gaji_max, String expired_date, String alamat,
                        String lokasi, String kota_id, String waktu_kerja, String waktu_kerja_lainnya,
                        String hari_kerja, String jenis_kelamin, String min_pengalaman,
                        String min_pendidikan, String syarat_tambahan, String lingkup_pekerjaan) {
            this.pengguna_id = pengguna_id;
            this.jenis_pekerjaan_id = jenis_pekerjaan_id;
            this.gaji_min = gaji_min;
            this.gaji_max = gaji_max;
            this.expired_date = expired_date;
            this.alamat = alamat;
            this.lokasi = lokasi;
            this.kota_id = kota_id;
            this.waktu_kerja = waktu_kerja;
            this.waktu_kerja_lainnya = waktu_kerja_lainnya;
            this.hari_kerja = hari_kerja;
            this.jenis_kelamin = jenis_kelamin;
            this.min_pengalaman = min_pengalaman;
            this.min_pendidikan = min_pendidikan;
            this.syarat_tambahan = syarat_tambahan;
            this.lingkup_pekerjaan = lingkup_pekerjaan;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostWork)) return false;

            PostWork post_work = (PostWork) o;

            if (pengguna_id != null ? !pengguna_id.equals(post_work.pengguna_id) : post_work.pengguna_id != null)
                return false;
            if (jenis_pekerjaan_id != null ? !jenis_pekerjaan_id.equals(post_work.jenis_pekerjaan_id) : post_work.jenis_pekerjaan_id != null)
                return false;
            if (gaji_min != null ? !gaji_min.equals(post_work.gaji_min) : post_work.gaji_min != null)
                return false;
            if (gaji_max != null ? !gaji_max.equals(post_work.gaji_max) : post_work.gaji_max != null)
                return false;
            if (expired_date != null ? !expired_date.equals(post_work.expired_date) : post_work.expired_date != null)
                return false;
            if (alamat != null ? !alamat.equals(post_work.alamat) : post_work.alamat != null)
                return false;
            if (lokasi != null ? !lokasi.equals(post_work.lokasi) : post_work.lokasi != null)
                return false;
            if (kota_id != null ? !kota_id.equals(post_work.kota_id) : post_work.kota_id != null)
                return false;
            if (waktu_kerja != null ? !waktu_kerja.equals(post_work.waktu_kerja) : post_work.waktu_kerja != null)
                return false;
            if (waktu_kerja_lainnya != null ? !waktu_kerja_lainnya.equals(post_work.waktu_kerja_lainnya) : post_work.waktu_kerja_lainnya != null)
                return false;
            if (hari_kerja != null ? !hari_kerja.equals(post_work.hari_kerja) : post_work.hari_kerja != null)
                return false;
            if (jenis_kelamin != null ? !jenis_kelamin.equals(post_work.jenis_kelamin) : post_work.jenis_kelamin != null)
                return false;
            if (min_pengalaman != null ? !min_pengalaman.equals(post_work.min_pengalaman) : post_work.min_pengalaman != null)
                return false;
            if (min_pendidikan != null ? !min_pendidikan.equals(post_work.min_pendidikan) : post_work.min_pendidikan != null)
                return false;
            if (syarat_tambahan != null ? !syarat_tambahan.equals(post_work.syarat_tambahan) : post_work.syarat_tambahan != null)
                return false;
            return lingkup_pekerjaan != null ? lingkup_pekerjaan.equals(post_work.lingkup_pekerjaan) : post_work.lingkup_pekerjaan == null;

        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (jenis_pekerjaan_id != null ? jenis_pekerjaan_id.hashCode() : 0);
            result = 31 * result + (gaji_min != null ? gaji_min.hashCode() : 0);
            result = 31 * result + (gaji_max != null ? gaji_max.hashCode() : 0);
            result = 31 * result + (expired_date != null ? expired_date.hashCode() : 0);
            result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
            result = 31 * result + (lokasi != null ? lokasi.hashCode() : 0);
            result = 31 * result + (kota_id != null ? kota_id.hashCode() : 0);
            result = 31 * result + (waktu_kerja != null ? waktu_kerja.hashCode() : 0);
            result = 31 * result + (waktu_kerja_lainnya != null ? waktu_kerja_lainnya.hashCode() : 0);
            result = 31 * result + (hari_kerja != null ? hari_kerja.hashCode() : 0);
            result = 31 * result + (jenis_kelamin != null ? jenis_kelamin.hashCode() : 0);
            result = 31 * result + (min_pengalaman != null ? min_pengalaman.hashCode() : 0);
            result = 31 * result + (min_pendidikan != null ? min_pendidikan.hashCode() : 0);
            result = 31 * result + (syarat_tambahan != null ? syarat_tambahan.hashCode() : 0);
            result = 31 * result + (lingkup_pekerjaan != null ? lingkup_pekerjaan.hashCode() : 0);
            return result;
        }
    }

    public static class PostWork2 {
        private String jenis_pekerjaan_id;
        private String gaji_min;
        private String gaji_max;
        private String expired_date;
        private String alamat;
        private String lokasi;
        private String kota_id;
        private String waktu_kerja;
        private String waktu_kerja_lainnya;
        private String hari_kerja;
        private String jenis_kelamin;
        private String min_pengalaman;
        private String min_pendidikan;
        private String syarat_tambahan;
        private String lingkup_pekerjaan;

        public PostWork2( String jenis_pekerjaan_id,
                        String gaji_min, String gaji_max, String expired_date, String alamat,
                        String lokasi, String kota_id, String waktu_kerja, String waktu_kerja_lainnya,
                        String hari_kerja, String jenis_kelamin, String min_pengalaman,
                        String min_pendidikan, String syarat_tambahan, String lingkup_pekerjaan) {
            this.jenis_pekerjaan_id = jenis_pekerjaan_id;
            this.gaji_min = gaji_min;
            this.gaji_max = gaji_max;
            this.expired_date = expired_date;
            this.alamat = alamat;
            this.lokasi = lokasi;
            this.kota_id = kota_id;
            this.waktu_kerja = waktu_kerja;
            this.waktu_kerja_lainnya = waktu_kerja_lainnya;
            this.hari_kerja = hari_kerja;
            this.jenis_kelamin = jenis_kelamin;
            this.min_pengalaman = min_pengalaman;
            this.min_pendidikan = min_pendidikan;
            this.syarat_tambahan = syarat_tambahan;
            this.lingkup_pekerjaan = lingkup_pekerjaan;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostWork2)) return false;

            PostWork2 postWork2 = (PostWork2) o;

            if (jenis_pekerjaan_id != null ? !jenis_pekerjaan_id.equals(postWork2.jenis_pekerjaan_id) : postWork2.jenis_pekerjaan_id != null)
                return false;
            if (gaji_min != null ? !gaji_min.equals(postWork2.gaji_min) : postWork2.gaji_min != null)
                return false;
            if (gaji_max != null ? !gaji_max.equals(postWork2.gaji_max) : postWork2.gaji_max != null)
                return false;
            if (expired_date != null ? !expired_date.equals(postWork2.expired_date) : postWork2.expired_date != null)
                return false;
            if (alamat != null ? !alamat.equals(postWork2.alamat) : postWork2.alamat != null)
                return false;
            if (lokasi != null ? !lokasi.equals(postWork2.lokasi) : postWork2.lokasi != null)
                return false;
            if (kota_id != null ? !kota_id.equals(postWork2.kota_id) : postWork2.kota_id != null)
                return false;
            if (waktu_kerja != null ? !waktu_kerja.equals(postWork2.waktu_kerja) : postWork2.waktu_kerja != null)
                return false;
            if (waktu_kerja_lainnya != null ? !waktu_kerja_lainnya.equals(postWork2.waktu_kerja_lainnya) : postWork2.waktu_kerja_lainnya != null)
                return false;
            if (hari_kerja != null ? !hari_kerja.equals(postWork2.hari_kerja) : postWork2.hari_kerja != null)
                return false;
            if (jenis_kelamin != null ? !jenis_kelamin.equals(postWork2.jenis_kelamin) : postWork2.jenis_kelamin != null)
                return false;
            if (min_pengalaman != null ? !min_pengalaman.equals(postWork2.min_pengalaman) : postWork2.min_pengalaman != null)
                return false;
            if (min_pendidikan != null ? !min_pendidikan.equals(postWork2.min_pendidikan) : postWork2.min_pendidikan != null)
                return false;
            if (syarat_tambahan != null ? !syarat_tambahan.equals(postWork2.syarat_tambahan) : postWork2.syarat_tambahan != null)
                return false;
            return lingkup_pekerjaan != null ? lingkup_pekerjaan.equals(postWork2.lingkup_pekerjaan) : postWork2.lingkup_pekerjaan == null;
        }

        @Override
        public int hashCode() {
            int result = jenis_pekerjaan_id != null ? jenis_pekerjaan_id.hashCode() : 0;
            result = 31 * result + (gaji_min != null ? gaji_min.hashCode() : 0);
            result = 31 * result + (gaji_max != null ? gaji_max.hashCode() : 0);
            result = 31 * result + (expired_date != null ? expired_date.hashCode() : 0);
            result = 31 * result + (alamat != null ? alamat.hashCode() : 0);
            result = 31 * result + (lokasi != null ? lokasi.hashCode() : 0);
            result = 31 * result + (kota_id != null ? kota_id.hashCode() : 0);
            result = 31 * result + (waktu_kerja != null ? waktu_kerja.hashCode() : 0);
            result = 31 * result + (waktu_kerja_lainnya != null ? waktu_kerja_lainnya.hashCode() : 0);
            result = 31 * result + (hari_kerja != null ? hari_kerja.hashCode() : 0);
            result = 31 * result + (jenis_kelamin != null ? jenis_kelamin.hashCode() : 0);
            result = 31 * result + (min_pengalaman != null ? min_pengalaman.hashCode() : 0);
            result = 31 * result + (min_pendidikan != null ? min_pendidikan.hashCode() : 0);
            result = 31 * result + (syarat_tambahan != null ? syarat_tambahan.hashCode() : 0);
            result = 31 * result + (lingkup_pekerjaan != null ? lingkup_pekerjaan.hashCode() : 0);
            return result;
        }
    }

    public static class PostInfo {

        private String pengguna_id;
        private String nama_ktp;
        private String tempat_lahir;
        private String tgl_lahir;
        private String alamat_ktp;
        private String rt_rw;
        private String kelurahan;
        private String kecamatan;

        public PostInfo(String pengguna_id, String nama_ktp, String tempat_lahir, String tgl_lahir,
                        String alamat_ktp, String rt_rw, String kelurahan, String kecamatan) {
            this.pengguna_id = pengguna_id;
            this.nama_ktp = nama_ktp;
            this.tempat_lahir = tempat_lahir;
            this.tgl_lahir = tgl_lahir;
            this.alamat_ktp = alamat_ktp;
            this.rt_rw = rt_rw;
            this.kelurahan = kelurahan;
            this.kecamatan = kecamatan;
        }

        public String getPengguna_id() {
            return pengguna_id;
        }

        public String getNama_ktp() {
            return nama_ktp;
        }

        public String getTempat_lahir() {
            return tempat_lahir;
        }

        public String getTgl_lahir() {
            return tgl_lahir;
        }

        public String getAlamat_ktp() {
            return alamat_ktp;
        }

        public String getRt_rw() {
            return rt_rw;
        }

        public String getKelurahan() {
            return kelurahan;
        }

        public String getKecamatan_id() {
            return kecamatan;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostInfo)) return false;

            PostInfo post_info = (PostInfo) o;

            if (pengguna_id != null ? !pengguna_id.equals(post_info.pengguna_id) : post_info.pengguna_id != null)
                return false;
            if (nama_ktp != null ? !nama_ktp.equals(post_info.nama_ktp) : post_info.nama_ktp != null)
                return false;
            if (tempat_lahir != null ? !tempat_lahir.equals(post_info.tempat_lahir) : post_info.tempat_lahir != null)
                return false;
            if (tgl_lahir != null ? !tgl_lahir.equals(post_info.tgl_lahir) : post_info.tgl_lahir != null)
                return false;
            if (alamat_ktp != null ? !alamat_ktp.equals(post_info.alamat_ktp) : post_info.alamat_ktp != null)
                return false;
            if (rt_rw != null ? !rt_rw.equals(post_info.rt_rw) : post_info.rt_rw != null)
                return false;
            if (kelurahan != null ? !kelurahan.equals(post_info.kelurahan) : post_info.kelurahan != null)
                return false;
            return kecamatan != null ? kecamatan.equals(post_info.kecamatan) : post_info.kecamatan == null;

        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (nama_ktp != null ? nama_ktp.hashCode() : 0);
            result = 31 * result + (tempat_lahir != null ? tempat_lahir.hashCode() : 0);
            result = 31 * result + (tgl_lahir != null ? tgl_lahir.hashCode() : 0);
            result = 31 * result + (alamat_ktp != null ? alamat_ktp.hashCode() : 0);
            result = 31 * result + (rt_rw != null ? rt_rw.hashCode() : 0);
            result = 31 * result + (kelurahan != null ? kelurahan.hashCode() : 0);
            result = 31 * result + (kecamatan != null ? kecamatan.hashCode() : 0);
            return result;
        }
    }

    public static class PostInfoFile {

        private HashMap<String, File> foto;

        public PostInfoFile(File foto, File ktp) {
            this.foto = new HashMap<>();
            if (foto != null)
                this.foto.put("foto", foto);
            if (ktp != null)
                this.foto.put("ktp", ktp);
        }

        public HashMap<String, File> getFoto() {
            return foto;
        }

    }

    public static class Profile{
        private String id;

        public Profile(String id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Profile)) return false;

            Profile profile = (Profile) o;

            return id != null ? id.equals(profile.id) : profile.id == null;

        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }
    }

    public static class ChangeCompany {
        private String perusahaan_id;
        private String nama_perusahaan;
        private String alamat_perusahaan;
        private String kota_id;
        private String daerah;
        private String jumlah_karyawan;
        private String website;

        public ChangeCompany(String perusahaan_id, String nama_perusahaan, String alamat_perusahaan,
                             String kota_id, String daerah, String jumlah_karyawan, String website) {
            this.perusahaan_id = perusahaan_id;
            this.nama_perusahaan = nama_perusahaan;
            this.alamat_perusahaan = alamat_perusahaan;
            this.kota_id = kota_id;
            this.daerah = daerah;
            this.jumlah_karyawan = jumlah_karyawan;
            this.website = website;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChangeCompany)) return false;

            ChangeCompany that = (ChangeCompany) o;

            if (perusahaan_id != null ? !perusahaan_id.equals(that.perusahaan_id) : that.perusahaan_id != null)
                return false;
            if (nama_perusahaan != null ? !nama_perusahaan.equals(that.nama_perusahaan) : that.nama_perusahaan != null)
                return false;
            if (alamat_perusahaan != null ? !alamat_perusahaan.equals(that.alamat_perusahaan) : that.alamat_perusahaan != null)
                return false;
            if (kota_id != null ? !kota_id.equals(that.kota_id) : that.kota_id != null)
                return false;
            if (daerah != null ? !daerah.equals(that.daerah) : that.daerah != null) return false;
            if (jumlah_karyawan != null ? !jumlah_karyawan.equals(that.jumlah_karyawan) : that.jumlah_karyawan != null)
                return false;
            return website != null ? website.equals(that.website) : that.website == null;

        }

        @Override
        public int hashCode() {
            int result = perusahaan_id != null ? perusahaan_id.hashCode() : 0;
            result = 31 * result + (nama_perusahaan != null ? nama_perusahaan.hashCode() : 0);
            result = 31 * result + (alamat_perusahaan != null ? alamat_perusahaan.hashCode() : 0);
            result = 31 * result + (kota_id != null ? kota_id.hashCode() : 0);
            result = 31 * result + (daerah != null ? daerah.hashCode() : 0);
            result = 31 * result + (jumlah_karyawan != null ? jumlah_karyawan.hashCode() : 0);
            result = 31 * result + (website != null ? website.hashCode() : 0);
            return result;
        }
    }

    public static class ChangeCompanyFile {

        private HashMap<String, File> foto;

        public ChangeCompanyFile(File foto) {
            this.foto = new HashMap<>();
            if (foto != null)
                this.foto.put("logo", foto);
        }

        public HashMap<String, File> getFoto() {
            return foto;
        }

    }

    public static class ChangeProfile {
        private String nama_ktp;
        private String tempat_lahir;
        private String tgl_lahir;
        private String alamat_ktp;
        private String rt_rw;
        private String kelurahan;
        private String kecamatan;

        public ChangeProfile(String nama_ktp, String tempat_lahir, String tgl_lahir, String alamat_ktp, String rt_rw,
                             String kelurahan, String kecamatan) {
            this.nama_ktp = nama_ktp;
            this.tempat_lahir = tempat_lahir;
            this.tgl_lahir = tgl_lahir;
            this.alamat_ktp = alamat_ktp;
            this.rt_rw = rt_rw;
            this.kelurahan = kelurahan;
            this.kecamatan = kecamatan;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChangeProfile)) return false;

            ChangeProfile that = (ChangeProfile) o;

            if (nama_ktp != null ? !nama_ktp.equals(that.nama_ktp) : that.nama_ktp != null)
                return false;
            if (tempat_lahir != null ? !tempat_lahir.equals(that.tempat_lahir) : that.tempat_lahir != null)
                return false;
            if (tgl_lahir != null ? !tgl_lahir.equals(that.tgl_lahir) : that.tgl_lahir != null)
                return false;
            if (alamat_ktp != null ? !alamat_ktp.equals(that.alamat_ktp) : that.alamat_ktp != null)
                return false;
            if (rt_rw != null ? !rt_rw.equals(that.rt_rw) : that.rt_rw != null) return false;
            if (kelurahan != null ? !kelurahan.equals(that.kelurahan) : that.kelurahan != null)
                return false;
            return kecamatan != null ? kecamatan.equals(that.kecamatan) : that.kecamatan == null;
        }

        @Override
        public int hashCode() {
            int result = nama_ktp != null ? nama_ktp.hashCode() : 0;
            result = 31 * result + (tempat_lahir != null ? tempat_lahir.hashCode() : 0);
            result = 31 * result + (tgl_lahir != null ? tgl_lahir.hashCode() : 0);
            result = 31 * result + (alamat_ktp != null ? alamat_ktp.hashCode() : 0);
            result = 31 * result + (rt_rw != null ? rt_rw.hashCode() : 0);
            result = 31 * result + (kelurahan != null ? kelurahan.hashCode() : 0);
            result = 31 * result + (kecamatan != null ? kecamatan.hashCode() : 0);
            return result;
        }
    }

    public static class ChangeProfileFile {

        private HashMap<String, File> foto;

        public ChangeProfileFile(String foto, String ktp) {
            this.foto = new HashMap<>();
            if (foto != null)
                this.foto.put("foto", new File(foto));
            if (ktp != null)
                this.foto.put("ktp", new File(ktp));
        }

        public HashMap<String, File> getFoto() {
            return foto;
        }

    }
}
