package com.joefakri.serbakerja.connection.response;

import java.util.List;

/**
 * Created by deny on bandung.
 */

public class AreaResponse {

    public static class CityResponse{
        private String status;
        private String message;
        private List<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CityResponse that = (CityResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String id;
            private String type;
            private String province_id;
            private String province_name;
            private String name;
            private String postal_code;

            public Data(String id, String name) {
                this.id = id;
                this.name = name;
            }

            public Data() {
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getProvince_id() {
                return province_id;
            }

            public void setProvince_id(String province_id) {
                this.province_id = province_id;
            }

            public String getProvince_name() {
                return province_name;
            }

            public void setProvince_name(String province_name) {
                this.province_name = province_name;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPostal_code() {
                return postal_code;
            }

            public void setPostal_code(String postal_code) {
                this.postal_code = postal_code;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Data)) return false;

                Data data = (Data) o;

                if (!id.equals(data.id)) return false;
                if (!type.equals(data.type)) return false;
                if (!province_id.equals(data.province_id)) return false;
                if (!province_name.equals(data.province_name)) return false;
                if (!name.equals(data.name)) return false;
                return postal_code.equals(data.postal_code);

            }

            @Override
            public int hashCode() {
                int result = id.hashCode();
                result = 31 * result + type.hashCode();
                result = 31 * result + province_id.hashCode();
                result = 31 * result + province_name.hashCode();
                result = 31 * result + name.hashCode();
                result = 31 * result + postal_code.hashCode();
                return result;
            }
        }
    }

    public static class DistrictResponse {
        private String status;
        private String message;
        private List<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof DistrictResponse)) return false;

            DistrictResponse that = (DistrictResponse) o;

            if (status != null ? !status.equals(that.status) : that.status != null) return false;
            if (message != null ? !message.equals(that.message) : that.message != null)
                return false;
            return data != null ? data.equals(that.data) : that.data == null;

        }

        @Override
        public int hashCode() {
            int result = status != null ? status.hashCode() : 0;
            result = 31 * result + (message != null ? message.hashCode() : 0);
            result = 31 * result + (data != null ? data.hashCode() : 0);
            return result;
        }

        public static class Data {

            private String id;
            private String kota_id;
            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getKota_id() {
                return kota_id;
            }

            public void setKota_id(String kota_id) {
                this.kota_id = kota_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Data)) return false;

                Data data = (Data) o;

                if (!id.equals(data.id)) return false;
                if (!kota_id.equals(data.kota_id)) return false;
                return name.equals(data.name);

            }

            @Override
            public int hashCode() {
                int result = id.hashCode();
                result = 31 * result + kota_id.hashCode();
                result = 31 * result + name.hashCode();
                return result;
            }
        }
    }

}
