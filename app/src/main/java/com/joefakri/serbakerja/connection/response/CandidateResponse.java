package com.joefakri.serbakerja.connection.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deny on bandung.
 */

public class CandidateResponse {

    public static class InfoResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InfoResponse)) return false;

            InfoResponse that = (InfoResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String pengguna_id;
            private String nama_ktp;
            private String tempat_lahir;
            private String tgl_lahir;
            private String alamat_ktp;
            private String rt_rw;
            private String kecamatan;
            private String kelurahan;
            private String jenis_kelamin;
            private String agama;
            private String id;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTempat_lahir() {
                return tempat_lahir;
            }

            public void setTempat_lahir(String tempat_lahir) {
                this.tempat_lahir = tempat_lahir;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getAlamat_ktp() {
                return alamat_ktp;
            }

            public void setAlamat_ktp(String alamat_ktp) {
                this.alamat_ktp = alamat_ktp;
            }

            public String getRt_rw() {
                return rt_rw;
            }

            public void setRt_rw(String rt_rw) {
                this.rt_rw = rt_rw;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getAgama() {
                return agama;
            }

            public void setAgama(String agama) {
                this.agama = agama;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getKecamatan() {
                return kecamatan;
            }

            public void setKecamatan(String kecamatan) {
                this.kecamatan = kecamatan;
            }

            public String getKelurahan() {
                return kelurahan;
            }

            public void setKelurahan(String kelurahan) {
                this.kelurahan = kelurahan;
            }


        }
    }

    public static class WorkResponse{
        private String status;
        private String message;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof WorkResponse)) return false;

            WorkResponse that = (WorkResponse) o;

            if (!status.equals(that.status)) return false;
            return message.equals(that.message);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            return result;
        }
    }

    public static class ExperienceResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InfoResponse)) return false;

            InfoResponse that = (InfoResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String pengguna_id;
            private String nama_ktp;
            private String tempat_lahir;
            private String tgl_lahir;
            private String alamat_ktp;
            private String rt_rw;
            private String kota_id;
            private String daerah;
            private String jenis_kelamin;
            private String agama;
            private String no_hp;
            private String id;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTempat_lahir() {
                return tempat_lahir;
            }

            public void setTempat_lahir(String tempat_lahir) {
                this.tempat_lahir = tempat_lahir;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getAlamat_ktp() {
                return alamat_ktp;
            }

            public void setAlamat_ktp(String alamat_ktp) {
                this.alamat_ktp = alamat_ktp;
            }

            public String getRt_rw() {
                return rt_rw;
            }

            public void setRt_rw(String rt_rw) {
                this.rt_rw = rt_rw;
            }

            public String getKota_id() {
                return kota_id;
            }

            public void setKota_id(String kota_id) {
                this.kota_id = kota_id;
            }

            public String getDaerah() {
                return daerah;
            }

            public void setDaerah(String daerah) {
                this.daerah = daerah;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getAgama() {
                return agama;
            }

            public void setAgama(String agama) {
                this.agama = agama;
            }

            public String getNo_hp() {
                return no_hp;
            }

            public void setNo_hp(String no_hp) {
                this.no_hp = no_hp;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

    public static class EducationResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InfoResponse)) return false;

            InfoResponse that = (InfoResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String pengguna_id;
            private String thn_pend_terakhir;
            private String pend_terakhir;
            private String sekolah_terakhir;
            private String kursus;
            private String id;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getThn_pend_terakhir() {
                return thn_pend_terakhir;
            }

            public void setThn_pend_terakhir(String thn_pend_terakhir) {
                this.thn_pend_terakhir = thn_pend_terakhir;
            }

            public String getPend_terakhir() {
                return pend_terakhir;
            }

            public void setPend_terakhir(String pend_terakhir) {
                this.pend_terakhir = pend_terakhir;
            }

            public String getSekolah_terakhir() {
                return sekolah_terakhir;
            }

            public void setSekolah_terakhir(String sekolah_terakhir) {
                this.sekolah_terakhir = sekolah_terakhir;
            }

            public String getKursus() {
                return kursus;
            }

            public void setKursus(String kursus) {
                this.kursus = kursus;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

    public static class DocumentResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InfoResponse)) return false;

            InfoResponse that = (InfoResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String pengguna_id;
            private String ktp;
            private String sim;
            private String ijazah;
            private String foto;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getKtp() {
                return ktp;
            }

            public void setKtp(String ktp) {
                this.ktp = ktp;
            }

            public String getSim() {
                return sim;
            }

            public void setSim(String sim) {
                this.sim = sim;
            }

            public String getIjazah() {
                return ijazah;
            }

            public void setIjazah(String ijazah) {
                this.ijazah = ijazah;
            }

            public String getFoto() {
                return foto;
            }

            public void setFoto(String foto) {
                this.foto = foto;
            }
        }
    }

    public static class DetailInfo {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            private String pengguna_id;
            private String no_hp;
            private String nama_ktp;
            private String tgl_lahir;
            private String jenis_kelamin;
            private String foto_terbaru;
            private String foto_ktp;
            private String pend_terakhir;
            private String thn_pend_terakhir;
            private String sekolah_terakhir;
            private String kursus;
            private String status_pekerjaan_terakhir;
            private String status_pekerjaan_terakhir_lainnya;
            private String gaji_min_terakhir;
            private String gaji_max_terakhir;
            private ArrayList<Pengalaman> pengalaman;
            private ArrayList<Minat> minat;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNo_hp() {
                return no_hp;
            }

            public void setNo_hp(String no_hp) {
                this.no_hp = no_hp;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getFoto_terbaru() {
                return foto_terbaru;
            }

            public void setFoto_terbaru(String foto_terbaru) {
                this.foto_terbaru = foto_terbaru;
            }

            public String getFoto_ktp() {
                return foto_ktp;
            }

            public void setFoto_ktp(String foto_ktp) {
                this.foto_ktp = foto_ktp;
            }

            public String getPend_terakhir() {
                return pend_terakhir;
            }

            public void setPend_terakhir(String pend_terakhir) {
                this.pend_terakhir = pend_terakhir;
            }

            public String getThn_pend_terakhir() {
                return thn_pend_terakhir;
            }

            public void setThn_pend_terakhir(String thn_pend_terakhir) {
                this.thn_pend_terakhir = thn_pend_terakhir;
            }

            public String getSekolah_terakhir() {
                return sekolah_terakhir;
            }

            public void setSekolah_terakhir(String sekolah_terakhir) {
                this.sekolah_terakhir = sekolah_terakhir;
            }

            public String getKursus() {
                return kursus;
            }

            public void setKursus(String kursus) {
                this.kursus = kursus;
            }

            public String getStatus_pekerjaan_terakhir() {
                return status_pekerjaan_terakhir;
            }

            public void setStatus_pekerjaan_terakhir(String status_pekerjaan_terakhir) {
                this.status_pekerjaan_terakhir = status_pekerjaan_terakhir;
            }

            public String getStatus_pekerjaan_terakhir_lainnya() {
                return status_pekerjaan_terakhir_lainnya;
            }

            public void setStatus_pekerjaan_terakhir_lainnya(String status_pekerjaan_terakhir_lainnya) {
                this.status_pekerjaan_terakhir_lainnya = status_pekerjaan_terakhir_lainnya;
            }

            public String getGaji_min_terakhir() {
                return gaji_min_terakhir;
            }

            public void setGaji_min_terakhir(String gaji_min_terakhir) {
                this.gaji_min_terakhir = gaji_min_terakhir;
            }

            public String getGaji_max_terakhir() {
                return gaji_max_terakhir;
            }

            public void setGaji_max_terakhir(String gaji_max_terakhir) {
                this.gaji_max_terakhir = gaji_max_terakhir;
            }

            public ArrayList<Pengalaman> getPengalaman() {
                return pengalaman;
            }

            public void setPengalaman(ArrayList<Pengalaman> pengalaman) {
                this.pengalaman = pengalaman;
            }

            public ArrayList<Minat> getMinat() {
                return minat;
            }

            public void setMinat(ArrayList<Minat> minat) {
                this.minat = minat;
            }

            public class Pengalaman {
                private String pengalaman_kerja;
                private String posisi;
                private String lama_bekerja;
                private String period;
                private String nama_perusahaan;

                public String getPengalaman_kerja() {
                    return pengalaman_kerja;
                }

                public void setPengalaman_kerja(String pengalaman_kerja) {
                    this.pengalaman_kerja = pengalaman_kerja;
                }

                public String getPosisi() {
                    return posisi;
                }

                public void setPosisi(String posisi) {
                    this.posisi = posisi;
                }

                public String getLama_bekerja() {
                    return lama_bekerja;
                }

                public void setLama_bekerja(String lama_bekerja) {
                    this.lama_bekerja = lama_bekerja;
                }

                public String getPeriod() {
                    return period;
                }

                public void setPeriod(String period) {
                    this.period = period;
                }

                public String getNama_perusahaan() {
                    return nama_perusahaan;
                }

                public void setNama_perusahaan(String nama_perusahaan) {
                    this.nama_perusahaan = nama_perusahaan;
                }
            }

            public class Minat {
                private String gaji_min;
                private String gaji_max;
                private String waktu_kerja;
                private String waktu_kerja_lainnya;
                private String hari_kerja;
                private String jenis_pekerjaan_id;
                private String jenis_pekerjaan;
                private String daerah;
                private ArrayList<Kota> kota;

                public String getGaji_min() {
                    return gaji_min;
                }

                public void setGaji_min(String gaji_min) {
                    this.gaji_min = gaji_min;
                }

                public String getGaji_max() {
                    return gaji_max;
                }

                public void setGaji_max(String gaji_max) {
                    this.gaji_max = gaji_max;
                }

                public String getWaktu_kerja() {
                    return waktu_kerja;
                }

                public void setWaktu_kerja(String waktu_kerja) {
                    this.waktu_kerja = waktu_kerja;
                }

                public String getHari_kerja() {
                    return hari_kerja;
                }

                public void setHari_kerja(String hari_kerja) {
                    this.hari_kerja = hari_kerja;
                }

                public String getJenis_pekerjaan_id() {
                    return jenis_pekerjaan_id;
                }

                public void setJenis_pekerjaan_id(String jenis_pekerjaan_id) {
                    this.jenis_pekerjaan_id = jenis_pekerjaan_id;
                }

                public String getJenis_pekerjaan() {
                    return jenis_pekerjaan;
                }

                public void setJenis_pekerjaan(String jenis_pekerjaan) {
                    this.jenis_pekerjaan = jenis_pekerjaan;
                }

                public String getDaerah() {
                    return daerah;
                }

                public void setDaerah(String daerah) {
                    this.daerah = daerah;
                }

                public ArrayList<Kota> getKota() {
                    return kota;
                }

                public void setKota(ArrayList<Kota> kota) {
                    this.kota = kota;
                }

                public String getWaktu_kerja_lainnya() {
                    return waktu_kerja_lainnya;
                }

                public void setWaktu_kerja_lainnya(String waktu_kerja_lainnya) {
                    this.waktu_kerja_lainnya = waktu_kerja_lainnya;
                }

                public class Kota {
                    private String id;
                    private String name;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }
                }
            }
        }
    }


    public static class CandidateProfile implements Serializable{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InfoResponse)) return false;

            InfoResponse that = (InfoResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data implements Serializable {

            private String kandidat_id;
            private String pengguna_id;
            private String no_hp;
            private String nama_ktp;
            private String tempat_lahir;
            private String tgl_lahir;
            private String alamat_ktp;
            private String kecamatan;
            private String kelurahan;
            private String rt_rw;
            private String jenis_kelamin;
            private String agama;
            private String foto_terbaru;

            public String getKandidat_id() {
                return kandidat_id;
            }

            public void setKandidat_id(String kandidat_id) {
                this.kandidat_id = kandidat_id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNo_hp() {
                return no_hp;
            }

            public void setNo_hp(String no_hp) {
                this.no_hp = no_hp;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTempat_lahir() {
                return tempat_lahir;
            }

            public void setTempat_lahir(String tempat_lahir) {
                this.tempat_lahir = tempat_lahir;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getAlamat_ktp() {
                return alamat_ktp;
            }

            public void setAlamat_ktp(String alamat_ktp) {
                this.alamat_ktp = alamat_ktp;
            }

            public String getKecamatan() {
                return kecamatan;
            }

            public void setKecamatan(String kecamatan) {
                this.kecamatan = kecamatan;
            }

            public String getKelurahan() {
                return kelurahan;
            }

            public void setKelurahan(String kelurahan) {
                this.kelurahan = kelurahan;
            }

            public String getRt_rw() {
                return rt_rw;
            }

            public void setRt_rw(String rt_rw) {
                this.rt_rw = rt_rw;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getAgama() {
                return agama;
            }

            public void setAgama(String agama) {
                this.agama = agama;
            }

            public String getFoto_terbaru() {
                return foto_terbaru;
            }

            public void setFoto_terbaru(String foto_terbaru) {
                this.foto_terbaru = foto_terbaru;
            }
        }
    }

    public static class ChangeProfile implements Serializable{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {
            private String message;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }

    }

    public static class Education implements Serializable {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data implements Serializable{
            private String id;
            private String pengguna_id;
            private String pend_terakhir;
            private String thn_pend_terakhir;
            private String sekolah_terakhir;
            private String kursus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getPend_terakhir() {
                return pend_terakhir;
            }

            public void setPend_terakhir(String pend_terakhir) {
                this.pend_terakhir = pend_terakhir;
            }

            public String getThn_pend_terakhir() {
                return thn_pend_terakhir;
            }

            public void setThn_pend_terakhir(String thn_pend_terakhir) {
                this.thn_pend_terakhir = thn_pend_terakhir;
            }

            public String getSekolah_terakhir() {
                return sekolah_terakhir;
            }

            public void setSekolah_terakhir(String sekolah_terakhir) {
                this.sekolah_terakhir = sekolah_terakhir;
            }

            public String getKursus() {
                return kursus;
            }

            public void setKursus(String kursus) {
                this.kursus = kursus;
            }
        }


    }


    public static class Experience implements Serializable {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data implements Serializable{
            private String id;
            private String pengguna_id;
            private String status_pekerjaan;
            private String status_pekerjaan_lainnya;
            private String gaji_min;
            private String gaji_max;
            private ArrayList<Pengalaman> pengalaman;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getStatus_pekerjaan() {
                return status_pekerjaan;
            }

            public void setStatus_pekerjaan(String status_pekerjaan) {
                this.status_pekerjaan = status_pekerjaan;
            }

            public String getStatus_pekerjaan_lainnya() {
                return status_pekerjaan_lainnya;
            }

            public void setStatus_pekerjaan_lainnya(String status_pekerjaan_lainnya) {
                this.status_pekerjaan_lainnya = status_pekerjaan_lainnya;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public ArrayList<Pengalaman> getPengalaman() {
                return pengalaman;
            }

            public void setPengalaman(ArrayList<Pengalaman> pengalaman) {
                this.pengalaman = pengalaman;
            }

            public class Pengalaman implements Serializable{
                private String id;
                private String pengalaman_kerja;
                private String posisi;
                private String lama_bekerja;
                private String period;
                private String nama_perusahaan;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getPengalaman_kerja() {
                    return pengalaman_kerja;
                }

                public void setPengalaman_kerja(String pengalaman_kerja) {
                    this.pengalaman_kerja = pengalaman_kerja;
                }

                public String getPosisi() {
                    return posisi;
                }

                public void setPosisi(String posisi) {
                    this.posisi = posisi;
                }

                public String getLama_bekerja() {
                    return lama_bekerja;
                }

                public void setLama_bekerja(String lama_bekerja) {
                    this.lama_bekerja = lama_bekerja;
                }

                public String getPeriod() {
                    return period;
                }

                public void setPeriod(String period) {
                    this.period = period;
                }

                public String getNama_perusahaan() {
                    return nama_perusahaan;
                }

                public void setNama_perusahaan(String nama_perusahaan) {
                    this.nama_perusahaan = nama_perusahaan;
                }
            }
        }
    }

    public static class ExperienceMan implements Serializable {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {
            private String message;
            private String pengalaman_detail_id;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getPengalaman_detail_id() {
                return pengalaman_detail_id;
            }

            public void setPengalaman_detail_id(String pengalaman_detail_id) {
                this.pengalaman_detail_id = pengalaman_detail_id;
            }
        }
    }

    public static class Document implements Serializable {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public static class Data implements Serializable{
            private String id;
            private String pengguna_id;
            private String no_ktp;
            private String foto_ktp;
            private String foto_terbaru;
            private String foto_sim;
            private String foto_ijazah;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNo_ktp() {
                return no_ktp;
            }

            public void setNo_ktp(String no_ktp) {
                this.no_ktp = no_ktp;
            }

            public String getFoto_ktp() {
                return foto_ktp;
            }

            public void setFoto_ktp(String foto_ktp) {
                this.foto_ktp = foto_ktp;
            }

            public String getFoto_terbaru() {
                return foto_terbaru;
            }

            public void setFoto_terbaru(String foto_terbaru) {
                this.foto_terbaru = foto_terbaru;
            }

            public String getFoto_sim() {
                return foto_sim;
            }

            public void setFoto_sim(String foto_sim) {
                this.foto_sim = foto_sim;
            }

            public String getFoto_ijazah() {
                return foto_ijazah;
            }

            public void setFoto_ijazah(String foto_ijazah) {
                this.foto_ijazah = foto_ijazah;
            }
        }


    }
}
