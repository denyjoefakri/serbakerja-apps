package com.joefakri.serbakerja.connection.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class ChatResponse {

    public static class Retrieve {
        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ChatResponse.Retrieve that = (ChatResponse.Retrieve) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String id;
            private String pekerjaan_id;
            private String from_id;
            private String to_id;
            private String pesan;
            private String created_on;
            private boolean visible = true;
            private int status_sending = 1;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPekerjaan_id() {
                return pekerjaan_id;
            }

            public void setPekerjaan_id(String pekerjaan_id) {
                this.pekerjaan_id = pekerjaan_id;
            }

            public String getFrom_id() {
                return from_id;
            }

            public void setFrom_id(String from_id) {
                this.from_id = from_id;
            }

            public String getTo_id() {
                return to_id;
            }

            public void setTo_id(String to_id) {
                this.to_id = to_id;
            }

            public String getPesan() {
                return pesan;
            }

            public void setPesan(String pesan) {
                this.pesan = pesan;
            }

            public String getCreated_on() {
                return created_on;
            }

            public void setCreated_on(String created_on) {
                this.created_on = created_on;
            }

            public int getStatus_sending() {
                return status_sending;
            }

            public void setStatus_sending(int status_sending) {
                this.status_sending = status_sending;
            }

            public boolean isVisible() {
                return visible;
            }

            public void setVisible(boolean visible) {
                this.visible = visible;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Data)) return false;

                Data data = (Data) o;

                if (visible != data.visible) return false;
                if (status_sending != data.status_sending) return false;
                if (id != null ? !id.equals(data.id) : data.id != null) return false;
                if (pekerjaan_id != null ? !pekerjaan_id.equals(data.pekerjaan_id) : data.pekerjaan_id != null)
                    return false;
                if (from_id != null ? !from_id.equals(data.from_id) : data.from_id != null)
                    return false;
                if (to_id != null ? !to_id.equals(data.to_id) : data.to_id != null) return false;
                if (pesan != null ? !pesan.equals(data.pesan) : data.pesan != null) return false;
                return created_on != null ? created_on.equals(data.created_on) : data.created_on == null;
            }

            @Override
            public int hashCode() {
                int result = id != null ? id.hashCode() : 0;
                result = 31 * result + (pekerjaan_id != null ? pekerjaan_id.hashCode() : 0);
                result = 31 * result + (from_id != null ? from_id.hashCode() : 0);
                result = 31 * result + (to_id != null ? to_id.hashCode() : 0);
                result = 31 * result + (pesan != null ? pesan.hashCode() : 0);
                result = 31 * result + (created_on != null ? created_on.hashCode() : 0);
                result = 31 * result + (visible ? 1 : 0);
                result = 31 * result + status_sending;
                return result;
            }
        }
    }

    public static class ListMessage {
        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public static class Data implements Serializable{
            public Data(boolean loading) {
                this.loading = loading;
            }

            private boolean loading;
            private String chat_id;
            private String pekerjaan_id;
            private String jenis_pekerjaan;
            private String tipe_user;
            private String pesan;
            private String nama;
            private String created_on;
            private String to_id;
            private String foto;
            private String status;
            private String no_hp;
            private Info info;

            public boolean isLoading() {
                return loading;
            }

            public void setLoading(boolean loading) {
                this.loading = loading;
            }

            public String getChat_id() {
                return chat_id;
            }

            public void setChat_id(String chat_id) {
                this.chat_id = chat_id;
            }

            public String getPekerjaan_id() {
                return pekerjaan_id;
            }

            public void setPekerjaan_id(String pekerjaan_id) {
                this.pekerjaan_id = pekerjaan_id;
            }

            public String getJenis_pekerjaan() {
                return jenis_pekerjaan;
            }

            public void setJenis_pekerjaan(String jenis_pekerjaan) {
                this.jenis_pekerjaan = jenis_pekerjaan;
            }

            public String getTipe_user() {
                return tipe_user;
            }

            public void setTipe_user(String tipe_user) {
                this.tipe_user = tipe_user;
            }

            public String getPesan() {
                return pesan;
            }

            public void setPesan(String pesan) {
                this.pesan = pesan;
            }

            public String getNama() {
                return nama;
            }

            public void setNama(String nama) {
                this.nama = nama;
            }

            public String getCreated_on() {
                return created_on;
            }

            public void setCreated_on(String created_on) {
                this.created_on = created_on;
            }

            public String getTo_id() {
                return to_id;
            }

            public void setTo_id(String to_id) {
                this.to_id = to_id;
            }

            public String getFoto() {
                return foto;
            }

            public void setFoto(String foto) {
                this.foto = foto;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getNo_hp() {
                return no_hp;
            }

            public void setNo_hp(String no_hp) {
                this.no_hp = no_hp;
            }

            public Info getInfo() {
                return info;
            }

            public void setInfo(Info info) {
                this.info = info;
            }

            public static class Info {
                private String id;
                private String pengguna_id;
                private String nama_perusahaan;
                private String alamat_perusahaan;
                private String kota_id;
                private String daerah;
                private String jumlah_karyawan;
                private String website;
                private String logo;
                private String nama_ktp;
                private String tempat_lahir;
                private String tgl_lahir;
                private String alamat_ktp;
                private String kecamatan;
                private String kelurahan;
                private String rt_rw;
                private String jenis_kelamin;
                private String agama;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getPengguna_id() {
                    return pengguna_id;
                }

                public void setPengguna_id(String pengguna_id) {
                    this.pengguna_id = pengguna_id;
                }

                public String getNama_perusahaan() {
                    return nama_perusahaan;
                }

                public void setNama_perusahaan(String nama_perusahaan) {
                    this.nama_perusahaan = nama_perusahaan;
                }

                public String getAlamat_perusahaan() {
                    return alamat_perusahaan;
                }

                public void setAlamat_perusahaan(String alamat_perusahaan) {
                    this.alamat_perusahaan = alamat_perusahaan;
                }

                public String getKota_id() {
                    return kota_id;
                }

                public void setKota_id(String kota_id) {
                    this.kota_id = kota_id;
                }

                public String getDaerah() {
                    return daerah;
                }

                public void setDaerah(String daerah) {
                    this.daerah = daerah;
                }

                public String getJumlah_karyawan() {
                    return jumlah_karyawan;
                }

                public void setJumlah_karyawan(String jumlah_karyawan) {
                    this.jumlah_karyawan = jumlah_karyawan;
                }

                public String getWebsite() {
                    return website;
                }

                public void setWebsite(String website) {
                    this.website = website;
                }

                public String getLogo() {
                    return logo;
                }

                public void setLogo(String logo) {
                    this.logo = logo;
                }

                public String getNama_ktp() {
                    return nama_ktp;
                }

                public void setNama_ktp(String nama_ktp) {
                    this.nama_ktp = nama_ktp;
                }

                public String getTempat_lahir() {
                    return tempat_lahir;
                }

                public void setTempat_lahir(String tempat_lahir) {
                    this.tempat_lahir = tempat_lahir;
                }

                public String getTgl_lahir() {
                    return tgl_lahir;
                }

                public void setTgl_lahir(String tgl_lahir) {
                    this.tgl_lahir = tgl_lahir;
                }

                public String getAlamat_ktp() {
                    return alamat_ktp;
                }

                public void setAlamat_ktp(String alamat_ktp) {
                    this.alamat_ktp = alamat_ktp;
                }

                public String getKecamatan() {
                    return kecamatan;
                }

                public void setKecamatan(String kecamatan) {
                    this.kecamatan = kecamatan;
                }

                public String getKelurahan() {
                    return kelurahan;
                }

                public void setKelurahan(String kelurahan) {
                    this.kelurahan = kelurahan;
                }

                public String getRt_rw() {
                    return rt_rw;
                }

                public void setRt_rw(String rt_rw) {
                    this.rt_rw = rt_rw;
                }

                public String getJenis_kelamin() {
                    return jenis_kelamin;
                }

                public void setJenis_kelamin(String jenis_kelamin) {
                    this.jenis_kelamin = jenis_kelamin;
                }

                public String getAgama() {
                    return agama;
                }

                public void setAgama(String agama) {
                    this.agama = agama;
                }
            }
        }
    }

    public static class Send {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public static class Data implements Serializable{
            private String message;
            private String percakapan_id;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getPercakapan_id() {
                return percakapan_id;
            }

            public void setPercakapan_id(String percakapan_id) {
                this.percakapan_id = percakapan_id;
            }
        }
    }

    public static class Read {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

        }
    }
}
