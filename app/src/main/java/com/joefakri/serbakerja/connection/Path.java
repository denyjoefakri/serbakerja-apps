/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.connection;

import com.joefakri.serbakerja.BuildConfig;

/**
 * Created by amitshekhar on 01/02/17.
 */

public final class Path {

    public static final String REGISTER = BuildConfig.BASE_URL
            + "/pengguna/register" + BuildConfig.KEY;

    public static final String LOGIN = BuildConfig.BASE_URL
            + "/pengguna/login" + BuildConfig.KEY;

    public static final String UPDATE_FCM = BuildConfig.BASE_URL
            + "/pengguna/update_gcm" + BuildConfig.KEY;

    public static final String CHANGE_PASSWORD = BuildConfig.BASE_URL
            + "/pengguna/changepassword" + BuildConfig.KEY;

    public static final String VERIFIKASI = BuildConfig.BASE_URL
            + "/pengguna/verifikasi" + BuildConfig.KEY;

    public static final String LIST_MESSAGE = BuildConfig.BASE_URL
            + "/percakapan/ListPesan/{id}" + BuildConfig.KEY;

    public static final String SEND_MESSAGE = BuildConfig.BASE_URL
            + "/percakapan/kirimpesan/" + BuildConfig.KEY;

    public static final String READ_MESSAGE = BuildConfig.BASE_URL
            + "/percakapan/setRead" + BuildConfig.KEY;

    public static final String GET_MESSAGE = BuildConfig.BASE_URL
            + "/percakapan/detailpesan/" + BuildConfig.KEY;

    public static final String GET_JOB_TYPE = BuildConfig.BASE_URL
            + "/pekerjaan/getjenispekerjaan" + BuildConfig.KEY;

    public static final String GET_CITY = BuildConfig.BASE_URL
            + "/wilayah/getkota" + BuildConfig.KEY;

    public static final String GET_DISTRICT = BuildConfig.BASE_URL
            + "/wilayah/getkecamatan" + BuildConfig.KEY;

    public static final String POST_RECURITER_COMPANY = BuildConfig.BASE_URL
            + "/pengguna/perusahaan/updateInfoDetail" + BuildConfig.KEY;

    public static final String POST_RECRUITER_WORK = BuildConfig.BASE_URL
            + "/pengguna/perusahaan/submitpekerjaan" + BuildConfig.KEY;

    public static final String POST_RECRUITER_INFO = BuildConfig.BASE_URL
            + "/pengguna/perusahaan/updatePerekrut" + BuildConfig.KEY;

    public static final String POST_CANDIDATE_INFO = BuildConfig.BASE_URL
            + "/pengguna/kandidat/updatepersonalinfo" + BuildConfig.KEY;

    public static final String POST_CANDIDATE_WORK = BuildConfig.BASE_URL
            + "/pengguna/kandidat/updateminatkerja" + BuildConfig.KEY;

    public static final String POST_CANDIDATE_EXPERIENCE = BuildConfig.BASE_URL
            + "/pengguna/kandidat/updatepengalamankerja" + BuildConfig.KEY;

    public static final String POST_CANDIDATE_EDUCATION = BuildConfig.BASE_URL
            + "/pengguna/kandidat/updatependidikan" + BuildConfig.KEY;

    public static final String POST_CANDIDATE_DOCUMENT = BuildConfig.BASE_URL
            + "/pengguna/kandidat/updatedokumen" + BuildConfig.KEY;

    public static final String DASHBOARD_RECRUTER = BuildConfig.BASE_URL
            + "/kandidat/getlistkandidat" + BuildConfig.KEY;

    public static final String DASHBOARD_RECRUTER_DETAIL = BuildConfig.BASE_URL
            + "/kandidat/GetDetailKandidat/{id}" + BuildConfig.KEY;

    public static final String DASHBOARD_RECRUTER_SEARCH = BuildConfig.BASE_URL
            + "/kandidat/searchkandidat/{keyword}" + BuildConfig.KEY;

    public static final String RECRUTER_PROFILE = BuildConfig.BASE_URL
            + "/perekrut/perusahaan/profile/{id}" + BuildConfig.KEY;

    public static final String CHANGE_RECRUTER_PROFILE = BuildConfig.BASE_URL
            + "/perekrut/perusahaan/profile/update/{id}" + BuildConfig.KEY;

    public static final String JOB_RECRUTER_PROFILE = BuildConfig.BASE_URL
            + "/pekerjaan/getlistpekerjaan/{id}" + BuildConfig.KEY;

    public static final String HISTORY = BuildConfig.BASE_URL
            + "/pengguna/riwayat/{id}" + BuildConfig.KEY;

    public static final String CLOSED_JOB = BuildConfig.BASE_URL
            + "/pekerjaan/updatestatus" + BuildConfig.KEY;

    public static final String APPLICANT = BuildConfig.BASE_URL
            + "/pekerjaan/pelamar/getlistpelamar/{id}" + BuildConfig.KEY;

    public static final String UPDATE_APPLICANT = BuildConfig.BASE_URL
            + "/pekerjaan/pelamar/updatestatus" + BuildConfig.KEY;

    public static final String VERIFY = BuildConfig.BASE_URL
            + "/pekerjaan/pelamar/getlistverifikasi/{id}" + BuildConfig.KEY;

    public static final String PROFILE_RECRUTER = BuildConfig.BASE_URL
            + "/perekrut/profile/{id}" + BuildConfig.KEY;

    public static final String PROFILE_RECRUTER_UPDATE = BuildConfig.BASE_URL
            + "/perekrut/update/{id}" + BuildConfig.KEY;

    public static final String DASHBOARD_CANDIDATE = BuildConfig.BASE_URL
            + "/pekerjaan/getallpekerjaan/{id}" + BuildConfig.KEY;

    public static final String DASHBOARD_CANDIDATE_DETAIL = BuildConfig.BASE_URL
            + "/pekerjaan/getdetailpekerjaan/{pengguna_id}/{id}" + BuildConfig.KEY;

    public static final String DASHBOARD_CANDIDATE_SEARCH = BuildConfig.BASE_URL
            + "/pekerjaan/searchpekerjaan/{id}/{keyword}" + BuildConfig.KEY;

    public static final String DASHBOARD_CANDIDATE_APPLY = BuildConfig.BASE_URL
            + "/pekerjaan/lamar/{id}" + BuildConfig.KEY;

    public static final String CANDIDATE_STATUS_APPLY = BuildConfig.BASE_URL
            + "/pekerjaan/pelamar/statuslamaran/{id}" + BuildConfig.KEY;

    public static final String UPDATE_RECRUTER_WORK = BuildConfig.BASE_URL
            + "/pekerjaan/pekerjaan/update/{id}" + BuildConfig.KEY;

    public static final String PROFILE_CANDIDATE = BuildConfig.BASE_URL
            + "/kandidat/profile/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE = BuildConfig.BASE_URL
            + "/kandidat/update/{id}" + BuildConfig.KEY;

    public static final String PROFILE_CANDIDATE_WORK = BuildConfig.BASE_URL
            + "/kandidat/minat/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE_WORK = BuildConfig.BASE_URL
            + "/kandidat/minat/update/{id}" + BuildConfig.KEY;

    public static final String PROFILE_CANDIDATE_EDUCATION = BuildConfig.BASE_URL
            + "/kandidat/pendidikan/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE_EDUCATION = BuildConfig.BASE_URL
            + "/kandidat/pendidikan/update/{id}" + BuildConfig.KEY;

    public static final String PROFILE_CANDIDATE_EXPERIENCE = BuildConfig.BASE_URL
            + "/kandidat/pengalaman/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE_EXPERIENCE = BuildConfig.BASE_URL
            + "/kandidat/pengalaman/update/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL = BuildConfig.BASE_URL
            + "/kandidat/pengalaman/detail/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL_INSERT = BuildConfig.BASE_URL
            + "/kandidat/pengalaman/detail/insert" + BuildConfig.KEY;

    public static final String PROFILE_CANDIDATE_DOCUMENT = BuildConfig.BASE_URL
            + "/kandidat/dokumen/{id}" + BuildConfig.KEY;

    public static final String UPDATE_PROFILE_CANDIDATE_DOCUMENT = BuildConfig.BASE_URL
            + "/kandidat/dokumen/update/{id}" + BuildConfig.KEY;

    public static final String SARAN = BuildConfig.BASE_URL
            + "/pengguna/kirimsaran" + BuildConfig.KEY;

    public static final String NOTIFICATION = BuildConfig.BASE_URL
            + "/notifications/listnotif/{id}" + BuildConfig.KEY;

    public static final String READ_NOTIFICATION = BuildConfig.BASE_URL
            + "/notifications/setread" + BuildConfig.KEY;

    public static final String DEACTIVE_CANDIDATE = BuildConfig.BASE_URL
            + "/pengguna/deactive" + BuildConfig.KEY;

    private Path() {
        // This class is not publicly instantiable
    }

}
