package com.joefakri.serbakerja.connection.request;

import android.util.Log;

import com.joefakri.serbakerja.data.model.WorkModel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deny on bandung.
 */

public class CandidateRequest {

    public static class PostInfo {

        private String pengguna_id;
        private String nama_ktp;
        private String tempat_lahir;
        private String tgl_lahir;
        private String alamat_ktp;
        private String rt_rw;
        private String kecamatan;
        private String kelurahan;
        private String jenis_kelamin;
        private String agama;

        public PostInfo(String pengguna_id, String nama_ktp, String tempat_lahir,
                        String tgl_lahir, String alamat_ktp, String rt_rw, String kecamatan,
                        String kelurahan, String jenis_kelamin, String agama) {
            this.pengguna_id = pengguna_id;
            this.nama_ktp = nama_ktp;
            this.tempat_lahir = tempat_lahir;
            this.tgl_lahir = tgl_lahir;
            this.alamat_ktp = alamat_ktp;
            this.rt_rw = rt_rw;
            this.kecamatan = kecamatan;
            this.kelurahan = kelurahan;
            this.jenis_kelamin = jenis_kelamin;
            this.agama = agama;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostInfo)) return false;

            PostInfo postInfo = (PostInfo) o;

            if (pengguna_id != null ? !pengguna_id.equals(postInfo.pengguna_id) : postInfo.pengguna_id != null)
                return false;
            if (nama_ktp != null ? !nama_ktp.equals(postInfo.nama_ktp) : postInfo.nama_ktp != null)
                return false;
            if (tempat_lahir != null ? !tempat_lahir.equals(postInfo.tempat_lahir) : postInfo.tempat_lahir != null)
                return false;
            if (tgl_lahir != null ? !tgl_lahir.equals(postInfo.tgl_lahir) : postInfo.tgl_lahir != null)
                return false;
            if (alamat_ktp != null ? !alamat_ktp.equals(postInfo.alamat_ktp) : postInfo.alamat_ktp != null)
                return false;
            if (rt_rw != null ? !rt_rw.equals(postInfo.rt_rw) : postInfo.rt_rw != null)
                return false;
            if (kecamatan != null ? !kecamatan.equals(postInfo.kecamatan) : postInfo.kecamatan != null)
                return false;
            if (kelurahan != null ? !kelurahan.equals(postInfo.kelurahan) : postInfo.kelurahan != null)
                return false;
            if (jenis_kelamin != null ? !jenis_kelamin.equals(postInfo.jenis_kelamin) : postInfo.jenis_kelamin != null)
                return false;
            return agama != null ? agama.equals(postInfo.agama) : postInfo.agama == null;

        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (nama_ktp != null ? nama_ktp.hashCode() : 0);
            result = 31 * result + (tempat_lahir != null ? tempat_lahir.hashCode() : 0);
            result = 31 * result + (tgl_lahir != null ? tgl_lahir.hashCode() : 0);
            result = 31 * result + (alamat_ktp != null ? alamat_ktp.hashCode() : 0);
            result = 31 * result + (rt_rw != null ? rt_rw.hashCode() : 0);
            result = 31 * result + (kecamatan != null ? kecamatan.hashCode() : 0);
            result = 31 * result + (kelurahan != null ? kelurahan.hashCode() : 0);
            result = 31 * result + (jenis_kelamin != null ? jenis_kelamin.hashCode() : 0);
            result = 31 * result + (agama != null ? agama.hashCode() : 0);
            return result;
        }
    }

    public static class PostWork {

        private HashMap<String, ArrayList<String>> map;
        private ArrayList<String> jenis_pekerjaan_id;
        private ArrayList<String> gaji_min;
        private ArrayList<String> gaji_max;
        private ArrayList<String> kota_id;
        private ArrayList<String> daerah;
        private ArrayList<String> waktu_kerja;
        private ArrayList<String> waktu_kerja_lainnya;
        private ArrayList<String> hari_kerja;

        public PostWork(ArrayList<WorkModel> workModels) {
            this.map = new HashMap<>();
            this.jenis_pekerjaan_id = new ArrayList<>();
            this.gaji_min = new ArrayList<>();
            this.gaji_max = new ArrayList<>();
            this.kota_id = new ArrayList<>();
            this.daerah = new ArrayList<>();
            this.waktu_kerja = new ArrayList<>();
            this.waktu_kerja_lainnya = new ArrayList<>();
            this.hari_kerja = new ArrayList<>();

            if (workModels != null) {
                for (int i = 0; i < workModels.size(); i++) {
                    this.jenis_pekerjaan_id.add(workModels.get(i).getWorkId());
                    this.gaji_min.add(workModels.get(i).getSalarymin());
                    this.gaji_max.add(workModels.get(i).getSalarymax());
                    this.kota_id.add(workModels.get(i).getCity_id());
                    this.daerah.add(workModels.get(i).getArea());
                    this.waktu_kerja.add(workModels.get(i).getWork_time_position());
                    this.waktu_kerja_lainnya.add(workModels.get(i).getWork_time_other());
                    this.hari_kerja.add(workModels.get(i).getWork_day());
                }

                if (workModels.size() > 0) {
                    this.map.put("jenis_pekerjaan_id[]", jenis_pekerjaan_id);
                    this.map.put("gaji_min[]", gaji_min);
                    this.map.put("gaji_max[]", gaji_max);
                    this.map.put("kota_id[]", kota_id);
                    this.map.put("daerah[]", daerah);
                    this.map.put("waktu_kerja[]", waktu_kerja);
                    this.map.put("waktu_kerja_lainnya[]", waktu_kerja_lainnya);
                    this.map.put("hari_kerja[]", hari_kerja);
                }
            }

        }

        public HashMap<String, String> getMap() {
            HashMap<String, String> result = new HashMap<>();
            for (String key : map.keySet()) {
                ArrayList<String> params = map.get(key);
                for (int i = 0; i < params.size(); i++) {
                    result.put(key.replace("[]", "["+i+"]"), params.get(i));
                }
            }
            for (HashMap.Entry entry : result.entrySet()) {
                Log.e("data request", entry.getKey() + ", " + entry.getValue());
            }

            if (!map.isEmpty()) return result;
            else return null;
        }

        public HashMap<String, ArrayList<String>> getMultipleMap(){
            return map;
        }
    }

    public static class PostExperience {

        private String pengguna_id;
        private String status_pekerjaan;
        private String status_pekerjaan_lainnya;
        private String gaji_min;
        private String gaji_max;

        public PostExperience(String pengguna_id, String status_pekerjaan, String status_pekerjaan_lainnya, String gaji_min, String gaji_max) {
            this.pengguna_id = pengguna_id;
            this.status_pekerjaan = status_pekerjaan;
            this.status_pekerjaan_lainnya = status_pekerjaan_lainnya;
            this.gaji_min = gaji_min;
            this.gaji_max = gaji_max;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostExperience)) return false;

            PostExperience that = (PostExperience) o;

            if (pengguna_id != null ? !pengguna_id.equals(that.pengguna_id) : that.pengguna_id != null)
                return false;
            if (status_pekerjaan != null ? !status_pekerjaan.equals(that.status_pekerjaan) : that.status_pekerjaan != null)
                return false;
            if (status_pekerjaan_lainnya != null ? !status_pekerjaan_lainnya.equals(that.status_pekerjaan_lainnya) : that.status_pekerjaan_lainnya != null)
                return false;
            if (gaji_min != null ? !gaji_min.equals(that.gaji_min) : that.gaji_min != null)
                return false;
            return gaji_max != null ? gaji_max.equals(that.gaji_max) : that.gaji_max == null;
        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (status_pekerjaan != null ? status_pekerjaan.hashCode() : 0);
            result = 31 * result + (status_pekerjaan_lainnya != null ? status_pekerjaan_lainnya.hashCode() : 0);
            result = 31 * result + (gaji_min != null ? gaji_min.hashCode() : 0);
            result = 31 * result + (gaji_max != null ? gaji_max.hashCode() : 0);
            return result;
        }
    }

    public static class Experience {

        private HashMap<String, ArrayList<String>> map;
        private ArrayList<String> company;
        private ArrayList<String> as;
        private ArrayList<String> _long;
        private ArrayList<String> period;

        public Experience(ArrayList<WorkModel> workModels) {
            this.map = new HashMap<>();
            this.company = new ArrayList<>();
            this.as = new ArrayList<>();
            this._long = new ArrayList<>();
            this.period = new ArrayList<>();

            if (workModels != null) {
                for (int i = 0; i < workModels.size(); i++) {
                    this.company.add(workModels.get(i).getCompany_name());
                    this.as.add(workModels.get(i).getWork_as());
                    this._long.add(workModels.get(i).getWork_long());
                    this.period.add(workModels.get(i).getPeriod());
                }

                if (workModels.size() > 0) {
                    this.map.put("posisi[]", as);
                    this.map.put("nama_perusahaan[]", company);
                    this.map.put("lama_bekerja[]", _long);
                    this.map.put("period[]", period);
                }
            }

        }

        public HashMap<String, String> getMap() {
            HashMap<String, String> result = new HashMap<>();
            for (String key : map.keySet()) {
                ArrayList<String> params = map.get(key);
                for (int i = 0; i < params.size(); i++) {
                    result.put(key.replace("[]", "["+i+"]"), params.get(i));
                }
            }

            if (!map.isEmpty()) return result;
            else return null;
        }

        public HashMap<String, ArrayList<String>> getMultipleMap(){
            return map;
        }
    }

    public static class PostEducation {

        private String pengguna_id;
        private String pend_terakhir;
        private String thn_pend_terakhir;
        private String sekolah_terakhir;
        private String kursus;

        public PostEducation(String pengguna_id, String pend_terakhir,
                             String thn_pend_terakhir, String sekolah_terakhir, String kursus) {
            this.pengguna_id = pengguna_id;
            this.pend_terakhir = pend_terakhir;
            this.thn_pend_terakhir = thn_pend_terakhir;
            this.sekolah_terakhir = sekolah_terakhir;
            this.kursus = kursus;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostEducation)) return false;

            PostEducation that = (PostEducation) o;

            if (pengguna_id != null ? !pengguna_id.equals(that.pengguna_id) : that.pengguna_id != null)
                return false;
            if (pend_terakhir != null ? !pend_terakhir.equals(that.pend_terakhir) : that.pend_terakhir != null)
                return false;
            if (thn_pend_terakhir != null ? !thn_pend_terakhir.equals(that.thn_pend_terakhir) : that.thn_pend_terakhir != null)
                return false;
            if (sekolah_terakhir != null ? !sekolah_terakhir.equals(that.sekolah_terakhir) : that.sekolah_terakhir != null)
                return false;
            return kursus != null ? kursus.equals(that.kursus) : that.kursus == null;

        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (pend_terakhir != null ? pend_terakhir.hashCode() : 0);
            result = 31 * result + (thn_pend_terakhir != null ? thn_pend_terakhir.hashCode() : 0);
            result = 31 * result + (sekolah_terakhir != null ? sekolah_terakhir.hashCode() : 0);
            result = 31 * result + (kursus != null ? kursus.hashCode() : 0);
            return result;
        }
    }

    public static class PostDocument {

        private String pengguna_id;

        public PostDocument(String pengguna_id) {
            this.pengguna_id = pengguna_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof PostDocument)) return false;

            PostDocument document = (PostDocument) o;

            return pengguna_id != null ? pengguna_id.equals(document.pengguna_id) : document.pengguna_id == null;

        }

        @Override
        public int hashCode() {
            return pengguna_id != null ? pengguna_id.hashCode() : 0;
        }
    }

    public static class PostDocumentFile {

        private HashMap<String, File> file;

        public PostDocumentFile(String ktp, String ijazah, String foto) {
            this.file = new HashMap<>();
            if (foto != null)
                this.file.put("foto", new File(foto));
            if (ijazah != null)
                this.file.put("ijazah", new File(ijazah));
            if (ktp != null)
                this.file.put("ktp", new File(ktp));
        }

        public HashMap<String, File> getFoto() {
            return file;
        }

    }

    public static class ChangeProfile {
        private String nama_ktp;
        private String tempat_lahir;
        private String tgl_lahir;
        private String alamat_ktp;
        private String kecamatan;
        private String kelurahan;
        private String jenis_kelamin;
        private String rt_rw;
        private String agama;

        public ChangeProfile(String nama_ktp, String tempat_lahir, String tgl_lahir,
                             String alamat_ktp, String kecamatan, String kelurahan, String jenis_kelamin,
                             String rt_rw, String agama) {
            this.nama_ktp = nama_ktp;
            this.tempat_lahir = tempat_lahir;
            this.tgl_lahir = tgl_lahir;
            this.alamat_ktp = alamat_ktp;
            this.kecamatan = kecamatan;
            this.kelurahan = kelurahan;
            this.jenis_kelamin = jenis_kelamin;
            this.rt_rw = rt_rw;
            this.agama = agama;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChangeProfile)) return false;

            ChangeProfile that = (ChangeProfile) o;

            if (nama_ktp != null ? !nama_ktp.equals(that.nama_ktp) : that.nama_ktp != null)
                return false;
            if (tempat_lahir != null ? !tempat_lahir.equals(that.tempat_lahir) : that.tempat_lahir != null)
                return false;
            if (tgl_lahir != null ? !tgl_lahir.equals(that.tgl_lahir) : that.tgl_lahir != null)
                return false;
            if (alamat_ktp != null ? !alamat_ktp.equals(that.alamat_ktp) : that.alamat_ktp != null)
                return false;
            if (kecamatan != null ? !kecamatan.equals(that.kecamatan) : that.kecamatan != null)
                return false;
            if (kelurahan != null ? !kelurahan.equals(that.kelurahan) : that.kelurahan != null)
                return false;
            if (jenis_kelamin != null ? !jenis_kelamin.equals(that.jenis_kelamin) : that.jenis_kelamin != null)
                return false;
            if (rt_rw != null ? !rt_rw.equals(that.rt_rw) : that.rt_rw != null) return false;
            return agama != null ? agama.equals(that.agama) : that.agama == null;
        }

        @Override
        public int hashCode() {
            int result = nama_ktp != null ? nama_ktp.hashCode() : 0;
            result = 31 * result + (tempat_lahir != null ? tempat_lahir.hashCode() : 0);
            result = 31 * result + (tgl_lahir != null ? tgl_lahir.hashCode() : 0);
            result = 31 * result + (alamat_ktp != null ? alamat_ktp.hashCode() : 0);
            result = 31 * result + (kecamatan != null ? kecamatan.hashCode() : 0);
            result = 31 * result + (kelurahan != null ? kelurahan.hashCode() : 0);
            result = 31 * result + (jenis_kelamin != null ? jenis_kelamin.hashCode() : 0);
            result = 31 * result + (rt_rw != null ? rt_rw.hashCode() : 0);
            result = 31 * result + (agama != null ? agama.hashCode() : 0);
            return result;
        }
    }

    public static class ChangeProfileFile {

        private HashMap<String, File> foto;

        public ChangeProfileFile(String foto) {
            this.foto = new HashMap<>();
            if (foto != null)
                this.foto.put("foto", new File(foto));
        }

        public HashMap<String, File> getFoto() {
            return foto;
        }

    }

    public static class ChangeProfileWork {
        private String jenis_pekerjaan_id;
        private String gaji_min;
        private String gaji_max;
        private String kota_id;
        private String daerah;
        private String waktu_kerja;
        private String waktu_kerja_lainnya;
        private String hari_kerja;
        private String pekerjaan_minat_id;

        public ChangeProfileWork(String jenis_pekerjaan_id, String gaji_min, String gaji_max,
                                 String kota_id, String daerah, String waktu_kerja, String waktu_kerja_lainnya,
                                 String hari_kerja, String pekerjaan_minat_id) {
            this.jenis_pekerjaan_id = jenis_pekerjaan_id;
            this.gaji_min = gaji_min;
            this.gaji_max = gaji_max;
            this.kota_id = kota_id;
            this.daerah = daerah;
            this.waktu_kerja = waktu_kerja;
            this.waktu_kerja_lainnya = waktu_kerja_lainnya;
            this.hari_kerja = hari_kerja;
            this.pekerjaan_minat_id = pekerjaan_minat_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChangeProfileWork)) return false;

            ChangeProfileWork that = (ChangeProfileWork) o;

            if (jenis_pekerjaan_id != null ? !jenis_pekerjaan_id.equals(that.jenis_pekerjaan_id) : that.jenis_pekerjaan_id != null)
                return false;
            if (gaji_min != null ? !gaji_min.equals(that.gaji_min) : that.gaji_min != null)
                return false;
            if (gaji_max != null ? !gaji_max.equals(that.gaji_max) : that.gaji_max != null)
                return false;
            if (kota_id != null ? !kota_id.equals(that.kota_id) : that.kota_id != null)
                return false;
            if (daerah != null ? !daerah.equals(that.daerah) : that.daerah != null) return false;
            if (waktu_kerja != null ? !waktu_kerja.equals(that.waktu_kerja) : that.waktu_kerja != null)
                return false;
            if (waktu_kerja_lainnya != null ? !waktu_kerja_lainnya.equals(that.waktu_kerja_lainnya) : that.waktu_kerja_lainnya != null)
                return false;
            if (hari_kerja != null ? !hari_kerja.equals(that.hari_kerja) : that.hari_kerja != null)
                return false;
            return pekerjaan_minat_id != null ? pekerjaan_minat_id.equals(that.pekerjaan_minat_id) : that.pekerjaan_minat_id == null;
        }

        @Override
        public int hashCode() {
            int result = jenis_pekerjaan_id != null ? jenis_pekerjaan_id.hashCode() : 0;
            result = 31 * result + (gaji_min != null ? gaji_min.hashCode() : 0);
            result = 31 * result + (gaji_max != null ? gaji_max.hashCode() : 0);
            result = 31 * result + (kota_id != null ? kota_id.hashCode() : 0);
            result = 31 * result + (daerah != null ? daerah.hashCode() : 0);
            result = 31 * result + (waktu_kerja != null ? waktu_kerja.hashCode() : 0);
            result = 31 * result + (waktu_kerja_lainnya != null ? waktu_kerja_lainnya.hashCode() : 0);
            result = 31 * result + (hari_kerja != null ? hari_kerja.hashCode() : 0);
            result = 31 * result + (pekerjaan_minat_id != null ? pekerjaan_minat_id.hashCode() : 0);
            return result;
        }

    }

    public static class ChangeProfileEducation {
        private String pend_terakhir;
        private String thn_pend_terakhir;
        private String sekolah_terakhir;
        private String kursus;

        public ChangeProfileEducation(String pend_terakhir, String thn_pend_terakhir,
                                      String sekolah_terakhir, String kursus) {
            this.pend_terakhir = pend_terakhir;
            this.thn_pend_terakhir = thn_pend_terakhir;
            this.sekolah_terakhir = sekolah_terakhir;
            this.kursus = kursus;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChangeProfileEducation)) return false;

            ChangeProfileEducation that = (ChangeProfileEducation) o;

            if (pend_terakhir != null ? !pend_terakhir.equals(that.pend_terakhir) : that.pend_terakhir != null)
                return false;
            if (thn_pend_terakhir != null ? !thn_pend_terakhir.equals(that.thn_pend_terakhir) : that.thn_pend_terakhir != null)
                return false;
            if (sekolah_terakhir != null ? !sekolah_terakhir.equals(that.sekolah_terakhir) : that.sekolah_terakhir != null)
                return false;
            return kursus != null ? kursus.equals(that.kursus) : that.kursus == null;
        }

        @Override
        public int hashCode() {
            int result = pend_terakhir != null ? pend_terakhir.hashCode() : 0;
            result = 31 * result + (thn_pend_terakhir != null ? thn_pend_terakhir.hashCode() : 0);
            result = 31 * result + (sekolah_terakhir != null ? sekolah_terakhir.hashCode() : 0);
            result = 31 * result + (kursus != null ? kursus.hashCode() : 0);
            return result;
        }
    }

    public static class ChangeProfileExperience {
        private String status_pekerjaan;
        private String status_pekerjaan_lainnya;
        private String gaji_min;
        private String gaji_max;
        private String posisi;
        private String lama_bekerja;
        private String period;
        private String nama_perusahaan;
        private String id_pengalaman_kerja;

        public ChangeProfileExperience( String gaji_min, String gaji_max, String... status_pekerjaan) {
            this.status_pekerjaan = status_pekerjaan[0];
            this.status_pekerjaan_lainnya = status_pekerjaan[1];
            this.gaji_min = gaji_min;
            this.gaji_max = gaji_max;
        }

        public ChangeProfileExperience(String posisi, String lama_bekerja, String period, String nama_perusahaan) {
            this.posisi = posisi;
            this.lama_bekerja = lama_bekerja;
            this.period = period;
            this.nama_perusahaan = nama_perusahaan;
        }

        public ChangeProfileExperience(String posisi, String lama_bekerja, String period, String nama_perusahaan, String id_pengalaman_kerja) {
            this.posisi = posisi;
            this.lama_bekerja = lama_bekerja;
            this.period = period;
            this.nama_perusahaan = nama_perusahaan;
            this.id_pengalaman_kerja = id_pengalaman_kerja;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ChangeProfileExperience)) return false;

            ChangeProfileExperience that = (ChangeProfileExperience) o;

            if (status_pekerjaan != null ? !status_pekerjaan.equals(that.status_pekerjaan) : that.status_pekerjaan != null)
                return false;
            if (status_pekerjaan_lainnya != null ? !status_pekerjaan_lainnya.equals(that.status_pekerjaan_lainnya) : that.status_pekerjaan_lainnya != null)
                return false;
            if (gaji_min != null ? !gaji_min.equals(that.gaji_min) : that.gaji_min != null)
                return false;
            if (gaji_max != null ? !gaji_max.equals(that.gaji_max) : that.gaji_max != null)
                return false;
            if (posisi != null ? !posisi.equals(that.posisi) : that.posisi != null) return false;
            if (lama_bekerja != null ? !lama_bekerja.equals(that.lama_bekerja) : that.lama_bekerja != null)
                return false;
            if (period != null ? !period.equals(that.period) : that.period != null) return false;
            if (nama_perusahaan != null ? !nama_perusahaan.equals(that.nama_perusahaan) : that.nama_perusahaan != null)
                return false;
            return id_pengalaman_kerja != null ? id_pengalaman_kerja.equals(that.id_pengalaman_kerja) : that.id_pengalaman_kerja == null;
        }

        @Override
        public int hashCode() {
            int result = status_pekerjaan != null ? status_pekerjaan.hashCode() : 0;
            result = 31 * result + (status_pekerjaan_lainnya != null ? status_pekerjaan_lainnya.hashCode() : 0);
            result = 31 * result + (gaji_min != null ? gaji_min.hashCode() : 0);
            result = 31 * result + (gaji_max != null ? gaji_max.hashCode() : 0);
            result = 31 * result + (posisi != null ? posisi.hashCode() : 0);
            result = 31 * result + (lama_bekerja != null ? lama_bekerja.hashCode() : 0);
            result = 31 * result + (period != null ? period.hashCode() : 0);
            result = 31 * result + (nama_perusahaan != null ? nama_perusahaan.hashCode() : 0);
            result = 31 * result + (id_pengalaman_kerja != null ? id_pengalaman_kerja.hashCode() : 0);
            return result;
        }
    }
}
