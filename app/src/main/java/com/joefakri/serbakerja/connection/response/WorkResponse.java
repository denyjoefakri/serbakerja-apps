package com.joefakri.serbakerja.connection.response;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class WorkResponse implements Serializable {

    public static class List implements Serializable{
        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public static class Data implements Serializable{

            public Data(boolean loading) {
                this.loading = loading;
            }

            private boolean loading;
            private String pekerjaan_id;
            private String pengguna_id;
            private String nama_pekerjaan;
            private String nama_perusahaan;
            private String logo_perusahaan;
            private String kota_id;
            private String kota;
            private String jenis_pekerjaan_id;
            private String jenis_pekerjaan;
            private String gaji_min;
            private String gaji_max;
            private String lokasi;
            private String alamat;
            private String waktu_kerja;
            private String waktu_kerja_lainnya;
            private String hari_kerja;
            private String jenis_kelamin;
            private String min_pengalaman;
            private String min_pendidikan;
            private String syarat_tambahan;
            private String lingkup_pekerjaan;
            private String expired_date;
            private String expired_status;
            private String status;
            private String lamaran_id;
            private String nama_ktp;
            private String tempat_lahir;
            private String tgl_lahir;
            private String alamat_ktp;
            private String kecamatan;
            private String kelurahan;
            private String rt_rw;
            private String agama;
            private String name;
            private String daerah;
            private String pend_terakhir;
            private String thn_pend_terakhir;
            private String sekolah_terakhir;
            private String foto_terbaru;
            private String status_lamaran;
            private String posisi;

            public boolean isLoading() {
                return loading;
            }

            public void setLoading(boolean loading) {
                this.loading = loading;
            }

            public String getPekerjaan_id() {
                return pekerjaan_id;
            }

            public void setPekerjaan_id(String pekerjaan_id) {
                this.pekerjaan_id = pekerjaan_id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_pekerjaan() {
                return nama_pekerjaan;
            }

            public void setNama_pekerjaan(String nama_pekerjaan) {
                this.nama_pekerjaan = nama_pekerjaan;
            }

            public String getNama_perusahaan() {
                return nama_perusahaan;
            }

            public void setNama_perusahaan(String nama_perusahaan) {
                this.nama_perusahaan = nama_perusahaan;
            }

            public String getLogo_perusahaan() {
                return logo_perusahaan;
            }

            public void setLogo_perusahaan(String logo_perusahaan) {
                this.logo_perusahaan = logo_perusahaan;
            }

            public String getKota_id() {
                return kota_id;
            }

            public void setKota_id(String kota_id) {
                this.kota_id = kota_id;
            }

            public String getKota() {
                return kota;
            }

            public void setKota(String kota) {
                this.kota = kota;
            }

            public String getJenis_pekerjaan_id() {
                return jenis_pekerjaan_id;
            }

            public void setJenis_pekerjaan_id(String jenis_pekerjaan_id) {
                this.jenis_pekerjaan_id = jenis_pekerjaan_id;
            }

            public String getJenis_pekerjaan() {
                return jenis_pekerjaan;
            }

            public void setJenis_pekerjaan(String jenis_pekerjaan) {
                this.jenis_pekerjaan = jenis_pekerjaan;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getLokasi() {
                return lokasi;
            }

            public void setLokasi(String lokasi) {
                this.lokasi = lokasi;
            }

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getWaktu_kerja() {
                return waktu_kerja;
            }

            public void setWaktu_kerja(String waktu_kerja) {
                this.waktu_kerja = waktu_kerja;
            }

            public String getWaktu_kerja_lainnya() {
                return waktu_kerja_lainnya;
            }

            public void setWaktu_kerja_lainnya(String waktu_kerja_lainnya) {
                this.waktu_kerja_lainnya = waktu_kerja_lainnya;
            }

            public String getHari_kerja() {
                return hari_kerja;
            }

            public void setHari_kerja(String hari_kerja) {
                this.hari_kerja = hari_kerja;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getMin_pengalaman() {
                return min_pengalaman;
            }

            public void setMin_pengalaman(String min_pengalaman) {
                this.min_pengalaman = min_pengalaman;
            }

            public String getMin_pendidikan() {
                return min_pendidikan;
            }

            public void setMin_pendidikan(String min_pendidikan) {
                this.min_pendidikan = min_pendidikan;
            }

            public String getSyarat_tambahan() {
                return syarat_tambahan;
            }

            public void setSyarat_tambahan(String syarat_tambahan) {
                this.syarat_tambahan = syarat_tambahan;
            }

            public String getLingkup_pekerjaan() {
                return lingkup_pekerjaan;
            }

            public void setLingkup_pekerjaan(String lingkup_pekerjaan) {
                this.lingkup_pekerjaan = lingkup_pekerjaan;
            }

            public String getExpired_date() {
                return expired_date;
            }

            public void setExpired_date(String expired_date) {
                this.expired_date = expired_date;
            }

            public String getExpired_status() {
                return expired_status;
            }

            public void setExpired_status(String expired_status) {
                this.expired_status = expired_status;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getLamaran_id() {
                return lamaran_id;
            }

            public void setLamaran_id(String lamaran_id) {
                this.lamaran_id = lamaran_id;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTempat_lahir() {
                return tempat_lahir;
            }

            public void setTempat_lahir(String tempat_lahir) {
                this.tempat_lahir = tempat_lahir;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getAlamat_ktp() {
                return alamat_ktp;
            }

            public void setAlamat_ktp(String alamat_ktp) {
                this.alamat_ktp = alamat_ktp;
            }

            public String getKecamatan() {
                return kecamatan;
            }

            public void setKecamatan(String kecamatan) {
                this.kecamatan = kecamatan;
            }

            public String getKelurahan() {
                return kelurahan;
            }

            public void setKelurahan(String kelurahan) {
                this.kelurahan = kelurahan;
            }

            public String getRt_rw() {
                return rt_rw;
            }

            public void setRt_rw(String rt_rw) {
                this.rt_rw = rt_rw;
            }

            public String getAgama() {
                return agama;
            }

            public void setAgama(String agama) {
                this.agama = agama;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDaerah() {
                return daerah;
            }

            public void setDaerah(String daerah) {
                this.daerah = daerah;
            }

            public String getPend_terakhir() {
                return pend_terakhir;
            }

            public void setPend_terakhir(String pend_terakhir) {
                this.pend_terakhir = pend_terakhir;
            }

            public String getThn_pend_terakhir() {
                return thn_pend_terakhir;
            }

            public void setThn_pend_terakhir(String thn_pend_terakhir) {
                this.thn_pend_terakhir = thn_pend_terakhir;
            }

            public String getSekolah_terakhir() {
                return sekolah_terakhir;
            }

            public void setSekolah_terakhir(String sekolah_terakhir) {
                this.sekolah_terakhir = sekolah_terakhir;
            }

            public String getFoto_terbaru() {
                return foto_terbaru;
            }

            public void setFoto_terbaru(String foto_terbaru) {
                this.foto_terbaru = foto_terbaru;
            }

            public String getStatus_lamaran() {
                return status_lamaran;
            }

            public void setStatus_lamaran(String status_lamaran) {
                this.status_lamaran = status_lamaran;
            }

            public String getPosisi() {
                return posisi;
            }

            public void setPosisi(String posisi) {
                this.posisi = posisi;
            }

        }
    }



    public static class Detail {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {
            private String id;
            private String pengguna_id;
            private String nama_perusahaan;
            private String jenis_pekerjaan;
            private String gaji_min;
            private String gaji_max;
            private String waktu_kerja;
            private String waktu_kerja_lainnya;
            private String hari_kerja;
            private String lokasi;
            private String alamat;
            private String kota;
            private String jenis_kelamin;
            private String min_pengalaman;
            private String min_pendidikan;
            private String syarat_tambahan;
            private String lingkup_pekerjaan;
            private String expired_date;
            private String logo;
            private String status_lamar;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_perusahaan() {
                return nama_perusahaan;
            }

            public void setNama_perusahaan(String nama_perusahaan) {
                this.nama_perusahaan = nama_perusahaan;
            }

            public String getJenis_pekerjaan() {
                return jenis_pekerjaan;
            }

            public void setJenis_pekerjaan(String jenis_pekerjaan) {
                this.jenis_pekerjaan = jenis_pekerjaan;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getWaktu_kerja() {
                return waktu_kerja;
            }

            public void setWaktu_kerja(String waktu_kerja) {
                this.waktu_kerja = waktu_kerja;
            }

            public String getWaktu_kerja_lainnya() {
                return waktu_kerja_lainnya;
            }

            public void setWaktu_kerja_lainnya(String waktu_kerja_lainnya) {
                this.waktu_kerja_lainnya = waktu_kerja_lainnya;
            }

            public String getHari_kerja() {
                return hari_kerja;
            }

            public void setHari_kerja(String hari_kerja) {
                this.hari_kerja = hari_kerja;
            }

            public String getLokasi() {
                return lokasi;
            }

            public void setLokasi(String lokasi) {
                this.lokasi = lokasi;
            }

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getKota() {
                return kota;
            }

            public void setKota(String kota) {
                this.kota = kota;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getMin_pengalaman() {
                return min_pengalaman;
            }

            public void setMin_pengalaman(String min_pengalaman) {
                this.min_pengalaman = min_pengalaman;
            }

            public String getMin_pendidikan() {
                return min_pendidikan;
            }

            public void setMin_pendidikan(String min_pendidikan) {
                this.min_pendidikan = min_pendidikan;
            }

            public String getSyarat_tambahan() {
                return syarat_tambahan;
            }

            public void setSyarat_tambahan(String syarat_tambahan) {
                this.syarat_tambahan = syarat_tambahan;
            }

            public String getLingkup_pekerjaan() {
                return lingkup_pekerjaan;
            }

            public void setLingkup_pekerjaan(String lingkup_pekerjaan) {
                this.lingkup_pekerjaan = lingkup_pekerjaan;
            }

            public String getExpired_date() {
                return expired_date;
            }

            public void setExpired_date(String expired_date) {
                this.expired_date = expired_date;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getStatus_lamar() {
                return status_lamar;
            }

            public void setStatus_lamar(String status_lamar) {
                this.status_lamar = status_lamar;
            }
        }
    }

    public static class CandidateWorkList implements Serializable{
        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public class Data implements Serializable{
            private String id;
            private String gaji_min;
            private String gaji_max;
            private String daerah;
            private String waktu_kerja;
            private String waktu_kerja_lainnya;
            private String hari_kerja;
            private String jenis_pekerjaan_id;
            private String jenis_pekerjaan;
            private ArrayList<Kota> kota;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getDaerah() {
                return daerah;
            }

            public void setDaerah(String daerah) {
                this.daerah = daerah;
            }

            public String getWaktu_kerja() {
                return waktu_kerja;
            }

            public void setWaktu_kerja(String waktu_kerja) {
                this.waktu_kerja = waktu_kerja;
            }

            public String getWaktu_kerja_lainnya() {
                return waktu_kerja_lainnya;
            }

            public void setWaktu_kerja_lainnya(String waktu_kerja_lainnya) {
                this.waktu_kerja_lainnya = waktu_kerja_lainnya;
            }

            public String getHari_kerja() {
                return hari_kerja;
            }

            public void setHari_kerja(String hari_kerja) {
                this.hari_kerja = hari_kerja;
            }

            public String getJenis_pekerjaan_id() {
                return jenis_pekerjaan_id;
            }

            public void setJenis_pekerjaan_id(String jenis_pekerjaan_id) {
                this.jenis_pekerjaan_id = jenis_pekerjaan_id;
            }

            public String getJenis_pekerjaan() {
                return jenis_pekerjaan;
            }

            public void setJenis_pekerjaan(String jenis_pekerjaan) {
                this.jenis_pekerjaan = jenis_pekerjaan;
            }

            public ArrayList<Kota> getKota() {
                return kota;
            }

            public void setKota(ArrayList<Kota> kota) {
                this.kota = kota;
            }

            public class Kota implements Serializable{
                private String id;
                private String name;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }
    }

    public static class Apply {
        private String status;
        private String message;
        private ArrayList<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ArrayList<Data> getData() {
            return data;
        }

        public void setData(ArrayList<Data> data) {
            this.data = data;
        }

        public static class Data implements Serializable {
            public Data(boolean loading) {
                this.loading = loading;
            }

            private boolean loading;
            private String id_pekerjaan;
            private String lamaran_id;
            private String nama_pekerjaan;
            private String nama_perusahaan;
            private String gaji_min;
            private String gaji_max;
            private String lokasi;
            private String alamat;
            private String nama_kota;
            private String status;
            private String logo;
            private String status_pekerjaan;
            private String expired_status;

            public boolean isLoading() {
                return loading;
            }

            public void setLoading(boolean loading) {
                this.loading = loading;
            }

            public String getId_pekerjaan() {
                return id_pekerjaan;
            }

            public String getLamaran_id() {
                return lamaran_id;
            }

            public void setLamaran_id(String lamaran_id) {
                this.lamaran_id = lamaran_id;
            }

            public String getNama_pekerjaan() {
                return nama_pekerjaan;
            }

            public void setNama_pekerjaan(String nama_pekerjaan) {
                this.nama_pekerjaan = nama_pekerjaan;
            }

            public String getNama_perusahaan() {
                return nama_perusahaan;
            }

            public void setNama_perusahaan(String nama_perusahaan) {
                this.nama_perusahaan = nama_perusahaan;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getLokasi() {
                return lokasi;
            }

            public void setLokasi(String lokasi) {
                this.lokasi = lokasi;
            }

            public String getAlamat() {
                return alamat;
            }

            public void setAlamat(String alamat) {
                this.alamat = alamat;
            }

            public String getNama_kota() {
                return nama_kota;
            }

            public void setNama_kota(String nama_kota) {
                this.nama_kota = nama_kota;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getStatus_pekerjaan() {
                return status_pekerjaan;
            }

            public void setStatus_pekerjaan(String status_pekerjaan) {
                this.status_pekerjaan = status_pekerjaan;
            }

            public String getExpired_status() {
                return expired_status;
            }

            public void setExpired_status(String expired_status) {
                this.expired_status = expired_status;
            }
        }
    }

    public static class ApplyStatus {
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public static class Data {

        }
    }
}
