package com.joefakri.serbakerja.connection.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deny on bandung.
 */

public class RecruiterResponse {

    public static class CompanyResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CompanyResponse that = (CompanyResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String pengguna_id;
            private String nama_perusahaan;
            private String alamat_perusahaan;
            private String kota_id;
            private String jumlah_karyawan;
            private String website;
            private String perusahaan_id;
            private String logo;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_perusahaan() {
                return nama_perusahaan;
            }

            public void setNama_perusahaan(String nama_perusahaan) {
                this.nama_perusahaan = nama_perusahaan;
            }

            public String getAlamat_perusahaan() {
                return alamat_perusahaan;
            }

            public void setAlamat_perusahaan(String alamat_perusahaan) {
                this.alamat_perusahaan = alamat_perusahaan;
            }

            public String getKota_id() {
                return kota_id;
            }

            public void setKota_id(String kota_id) {
                this.kota_id = kota_id;
            }

            public String getJumlah_karyawan() {
                return jumlah_karyawan;
            }

            public void setJumlah_karyawan(String jumlah_karyawan) {
                this.jumlah_karyawan = jumlah_karyawan;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }

            public String getPerusahaan_id() {
                return perusahaan_id;
            }

            public void setPerusahaan_id(String perusahaan_id) {
                this.perusahaan_id = perusahaan_id;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }

    public static class WorkResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CompanyResponse that = (CompanyResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String perusahaan_id;
            private String jenis_pekerjaan_id;
            private String gaji_min;
            private String gaji_max;
            private String lokasi;
            private String kota_id;
            private String waktu_kerja;
            private String hari_kerja;
            private String jenis_kelamin;
            private String min_pengalaman;
            private String min_pendidikan;
            private String syarat_tambahan;
            private String lingkup_pekerjaan;
            private String pekerjaan_id;

            public String getPerusahaan_id() {
                return perusahaan_id;
            }

            public void setPerusahaan_id(String perusahaan_id) {
                this.perusahaan_id = perusahaan_id;
            }

            public String getJenis_pekerjaan_id() {
                return jenis_pekerjaan_id;
            }

            public void setJenis_pekerjaan_id(String jenis_pekerjaan_id) {
                this.jenis_pekerjaan_id = jenis_pekerjaan_id;
            }

            public String getGaji_min() {
                return gaji_min;
            }

            public void setGaji_min(String gaji_min) {
                this.gaji_min = gaji_min;
            }

            public String getGaji_max() {
                return gaji_max;
            }

            public void setGaji_max(String gaji_max) {
                this.gaji_max = gaji_max;
            }

            public String getLokasi() {
                return lokasi;
            }

            public void setLokasi(String lokasi) {
                this.lokasi = lokasi;
            }

            public String getKota_id() {
                return kota_id;
            }

            public void setKota_id(String kota_id) {
                this.kota_id = kota_id;
            }

            public String getWaktu_kerja() {
                return waktu_kerja;
            }

            public void setWaktu_kerja(String waktu_kerja) {
                this.waktu_kerja = waktu_kerja;
            }

            public String getHari_kerja() {
                return hari_kerja;
            }

            public void setHari_kerja(String hari_kerja) {
                this.hari_kerja = hari_kerja;
            }

            public String getJenis_kelamin() {
                return jenis_kelamin;
            }

            public void setJenis_kelamin(String jenis_kelamin) {
                this.jenis_kelamin = jenis_kelamin;
            }

            public String getMin_pengalaman() {
                return min_pengalaman;
            }

            public void setMin_pengalaman(String min_pengalaman) {
                this.min_pengalaman = min_pengalaman;
            }

            public String getMin_pendidikan() {
                return min_pendidikan;
            }

            public void setMin_pendidikan(String min_pendidikan) {
                this.min_pendidikan = min_pendidikan;
            }

            public String getSyarat_tambahan() {
                return syarat_tambahan;
            }

            public void setSyarat_tambahan(String syarat_tambahan) {
                this.syarat_tambahan = syarat_tambahan;
            }

            public String getLingkup_pekerjaan() {
                return lingkup_pekerjaan;
            }

            public void setLingkup_pekerjaan(String lingkup_pekerjaan) {
                this.lingkup_pekerjaan = lingkup_pekerjaan;
            }

            public String getPekerjaan_id() {
                return pekerjaan_id;
            }

            public void setPekerjaan_id(String pekerjaan_id) {
                this.pekerjaan_id = pekerjaan_id;
            }
        }
    }

    public static class InfoResponse{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CompanyResponse that = (CompanyResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data {

            private String pengguna_id;
            private String nama_ktp;
            private String tempat_lahir;
            private String tgl_lahir;
            private String alamat_ktp;
            private String rt_rw;
            private String kelurahan;
            private String kecamatan_id;
            private String perekrut_id;

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTempat_lahir() {
                return tempat_lahir;
            }

            public void setTempat_lahir(String tempat_lahir) {
                this.tempat_lahir = tempat_lahir;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getAlamat_ktp() {
                return alamat_ktp;
            }

            public void setAlamat_ktp(String alamat_ktp) {
                this.alamat_ktp = alamat_ktp;
            }

            public String getRt_rw() {
                return rt_rw;
            }

            public void setRt_rw(String rt_rw) {
                this.rt_rw = rt_rw;
            }

            public String getKelurahan() {
                return kelurahan;
            }

            public void setKelurahan(String kelurahan) {
                this.kelurahan = kelurahan;
            }

            public String getKecamatan_id() {
                return kecamatan_id;
            }

            public void setKecamatan_id(String kecamatan_id) {
                this.kecamatan_id = kecamatan_id;
            }

            public String getPerekrut_id() {
                return perekrut_id;
            }

            public void setPerekrut_id(String perekrut_id) {
                this.perekrut_id = perekrut_id;
            }
        }
    }

    public static class CompanyProfile implements Serializable{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CompanyResponse that = (CompanyResponse) o;

            if (!status.equals(that.status)) return false;
            if (!message.equals(that.message)) return false;
            return data.equals(that.data);

        }

        @Override
        public int hashCode() {
            int result = status.hashCode();
            result = 31 * result + message.hashCode();
            result = 31 * result + data.hashCode();
            return result;
        }

        public static class Data implements Serializable{

            private String perusahaan_id;
            private String nama_perusahaan;
            private String alamat_perusahaan;
            private String kota_id;
            private String daerah;
            private String nama_kota;
            private String jumlah_karyawan;
            private String website;
            private String logo;

            public String getPerusahaan_id() {
                return perusahaan_id;
            }

            public void setPerusahaan_id(String perusahaan_id) {
                this.perusahaan_id = perusahaan_id;
            }

            public String getNama_perusahaan() {
                return nama_perusahaan;
            }

            public void setNama_perusahaan(String nama_perusahaan) {
                this.nama_perusahaan = nama_perusahaan;
            }

            public String getAlamat_perusahaan() {
                return alamat_perusahaan;
            }

            public void setAlamat_perusahaan(String alamat_perusahaan) {
                this.alamat_perusahaan = alamat_perusahaan;
            }

            public String getKota_id() {
                return kota_id;
            }

            public void setKota_id(String kota_id) {
                this.kota_id = kota_id;
            }

            public String getDaerah() {
                return daerah;
            }

            public void setDaerah(String daerah) {
                this.daerah = daerah;
            }

            public String getNama_kota() {
                return nama_kota;
            }

            public void setNama_kota(String nama_kota) {
                this.nama_kota = nama_kota;
            }

            public String getJumlah_karyawan() {
                return jumlah_karyawan;
            }

            public void setJumlah_karyawan(String jumlah_karyawan) {
                this.jumlah_karyawan = jumlah_karyawan;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }

    public static class ChangeCompany implements Serializable{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {
            private String message;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }

    }

    public static class ChangeProfile implements Serializable{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {
            private String message;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }

    }


    public static class Profile implements Serializable{
        private String status;
        private String message;
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data implements Serializable{
            private String id;
            private String perekrut_id;
            private String pengguna_id;
            private String nama_ktp;
            private String tempat_lahir;
            private String tgl_lahir;
            private String alamat_ktp;
            private String rt_rw;
            private String kelurahan;
            private String kecamatan;
            private String kecamatan_id;
            private String no_ktp;
            private String foto;
            private String foto_ktp;
            private String created_on;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPerekrut_id() {
                return perekrut_id;
            }

            public void setPerekrut_id(String perekrut_id) {
                this.perekrut_id = perekrut_id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getNama_ktp() {
                return nama_ktp;
            }

            public void setNama_ktp(String nama_ktp) {
                this.nama_ktp = nama_ktp;
            }

            public String getTempat_lahir() {
                return tempat_lahir;
            }

            public void setTempat_lahir(String tempat_lahir) {
                this.tempat_lahir = tempat_lahir;
            }

            public String getTgl_lahir() {
                return tgl_lahir;
            }

            public void setTgl_lahir(String tgl_lahir) {
                this.tgl_lahir = tgl_lahir;
            }

            public String getAlamat_ktp() {
                return alamat_ktp;
            }

            public void setAlamat_ktp(String alamat_ktp) {
                this.alamat_ktp = alamat_ktp;
            }

            public String getRt_rw() {
                return rt_rw;
            }

            public void setRt_rw(String rt_rw) {
                this.rt_rw = rt_rw;
            }

            public String getKelurahan() {
                return kelurahan;
            }

            public void setKelurahan(String kelurahan) {
                this.kelurahan = kelurahan;
            }

            public String getKecamatan() {
                return kecamatan;
            }

            public void setKecamatan(String kecamatan) {
                this.kecamatan = kecamatan;
            }

            public String getKecamatan_id() {
                return kecamatan_id;
            }

            public void setKecamatan_id(String kecamatan_id) {
                this.kecamatan_id = kecamatan_id;
            }

            public String getNo_ktp() {
                return no_ktp;
            }

            public void setNo_ktp(String no_ktp) {
                this.no_ktp = no_ktp;
            }

            public String getFoto() {
                return foto;
            }

            public void setFoto(String foto) {
                this.foto = foto;
            }

            public String getFoto_ktp() {
                return foto_ktp;
            }

            public void setFoto_ktp(String foto_ktp) {
                this.foto_ktp = foto_ktp;
            }

            public String getCreated_on() {
                return created_on;
            }

            public void setCreated_on(String created_on) {
                this.created_on = created_on;
            }
        }

    }
}
