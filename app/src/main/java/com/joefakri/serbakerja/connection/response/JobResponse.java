package com.joefakri.serbakerja.connection.response;

import java.util.List;

/**
 * Created by deny on bandung.
 */

public class JobResponse {

    private String status;
    private String message;
    private List<Data> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobResponse that = (JobResponse) o;

        if (!status.equals(that.status)) return false;
        if (!message.equals(that.message)) return false;
        return data.equals(that.data);

    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + data.hashCode();
        return result;
    }

    public static class Data {

        private String id;
        private String nama;

        public Data() {
        }

        public Data(String id, String nama) {
            this.id = id;
            this.nama = nama;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Data)) return false;

            Data data = (Data) o;

            if (!id.equals(data.id)) return false;
            return nama.equals(data.nama);

        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + nama.hashCode();
            return result;
        }
    }
}
