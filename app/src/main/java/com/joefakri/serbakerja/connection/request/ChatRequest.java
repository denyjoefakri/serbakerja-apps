package com.joefakri.serbakerja.connection.request;

/**
 * Created by deny on bandung.
 */

public class ChatRequest {


    public static class GetMessage {

        private String from_id;
        private String to_id;

        public GetMessage(String from_id, String to_id) {
            this.from_id = from_id;
            this.to_id = to_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof GetMessage)) return false;

            GetMessage that = (GetMessage) o;

            if (from_id != null ? !from_id.equals(that.from_id) : that.from_id != null)
                return false;
            return to_id != null ? to_id.equals(that.to_id) : that.to_id == null;
        }

        @Override
        public int hashCode() {
            int result = from_id != null ? from_id.hashCode() : 0;
            result = 31 * result + (to_id != null ? to_id.hashCode() : 0);
            return result;
        }
    }


    public static class SendMessage {

        private String from_id;
        private String to_id;
        private String pesan;

        public SendMessage(String from_id, String to_id, String pesan) {
            this.from_id = from_id;
            this.to_id = to_id;
            this.pesan = pesan;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof SendMessage)) return false;

            SendMessage that = (SendMessage) o;

            if (from_id != null ? !from_id.equals(that.from_id) : that.from_id != null)
                return false;
            if (to_id != null ? !to_id.equals(that.to_id) : that.to_id != null) return false;
            return pesan != null ? pesan.equals(that.pesan) : that.pesan == null;
        }

        @Override
        public int hashCode() {
            int result = from_id != null ? from_id.hashCode() : 0;
            result = 31 * result + (to_id != null ? to_id.hashCode() : 0);
            result = 31 * result + (pesan != null ? pesan.hashCode() : 0);
            return result;
        }
    }

    public static class ReadMessage {

        private String from_id;
        private String to_id;

        public ReadMessage(String from_id, String to_id) {
            this.from_id = from_id;
            this.to_id = to_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ReadMessage)) return false;

            ReadMessage that = (ReadMessage) o;

            if (from_id != null ? !from_id.equals(that.from_id) : that.from_id != null)
                return false;
            return to_id != null ? to_id.equals(that.to_id) : that.to_id == null;
        }

        @Override
        public int hashCode() {
            int result = from_id != null ? from_id.hashCode() : 0;
            result = 31 * result + (to_id != null ? to_id.hashCode() : 0);
            return result;
        }
    }


}
