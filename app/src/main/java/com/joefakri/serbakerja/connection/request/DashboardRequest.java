package com.joefakri.serbakerja.connection.request;

import android.util.Log;

import com.joefakri.serbakerja.connection.response.UserResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deny on bandung.
 */

public class DashboardRequest {

    public static class Recruter {

        private HashMap<String, ArrayList<String>> map;
        private ArrayList<String> jenis_pekerjaan_id;
        private ArrayList<String> kota_id;

        public Recruter(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> jenis_pekerjaan_id_param,
                        ArrayList<UserResponse.Login.Data.Kota> kota_id_param) {
            this.map = new HashMap<>();
            this.jenis_pekerjaan_id = new ArrayList<>();
            this.kota_id = new ArrayList<>();

            if (jenis_pekerjaan_id_param != null) {
                if (jenis_pekerjaan_id_param.size() > 0){
                    for (int i = 0; i < jenis_pekerjaan_id_param.size(); i++) {
                        this.jenis_pekerjaan_id.add(jenis_pekerjaan_id_param.get(i).getId());
                    }
                    this.map.put("jenis_pekerjaan_id[]", jenis_pekerjaan_id);
                }
            }

            if (kota_id_param != null) {
                if (kota_id_param.size() > 0) {
                    for (int i = 0; i < kota_id_param.size(); i++) {
                        this.kota_id.add(kota_id_param.get(i).getId());
                    }
                    this.map.put("kota_id[]", kota_id);
                }
            }

        }

        public HashMap<String, String> getMap() {
            HashMap<String, String> result = new HashMap<>();
            for (String key : map.keySet()) {
                ArrayList<String> params = map.get(key);
                for (int i = 0; i < params.size(); i++) {
                    result.put(key.replace("[]", "["+i+"]"), params.get(i));
                }
            }

            if (!map.isEmpty()) return result;
            else return null;
        }

        public HashMap<String, ArrayList<String>> getMultipleMap(){
            return map;
        }


    }

    public static class RecruterDetail {
        private String id;

        public RecruterDetail(String id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RecruterDetail)) return false;

            RecruterDetail that = (RecruterDetail) o;

            return id != null ? id.equals(that.id) : that.id == null;

        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }
    }

    public static class RecruterSearch {
        private String keyword;

        public RecruterSearch(String id) {
            this.keyword = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RecruterDetail)) return false;

            RecruterDetail that = (RecruterDetail) o;

            return keyword != null ? keyword.equals(that.id) : that.id == null;

        }

        @Override
        public int hashCode() {
            return keyword != null ? keyword.hashCode() : 0;
        }
    }

    public static class Candidate {

        private HashMap<String, ArrayList<String>> map;
        private ArrayList<String> jenis_pekerjaan_id;
        private ArrayList<String> kota_id;

        public Candidate(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> jenis_pekerjaan_id_param,
                        ArrayList<UserResponse.Login.Data.Kota> kota_id_param) {
            this.map = new HashMap<>();
            this.jenis_pekerjaan_id = new ArrayList<>();
            this.kota_id = new ArrayList<>();

            if (jenis_pekerjaan_id_param != null) {
                if (jenis_pekerjaan_id_param.size() > 0){
                    for (int i = 0; i < jenis_pekerjaan_id_param.size(); i++) {
                        this.jenis_pekerjaan_id.add(jenis_pekerjaan_id_param.get(i).getId());
                    }
                    this.map.put("jenis_pekerjaan_id[]", jenis_pekerjaan_id);
                }
            }

            if (kota_id_param != null) {
                if (kota_id_param.size() > 0){
                    for (int i = 0; i < kota_id_param.size(); i++) {
                        this.kota_id.add(kota_id_param.get(i).getId());
                    }
                    this.map.put("kota_id[]", kota_id);
                }
            }

        }

        public HashMap<String, String> getMap() {
            HashMap<String, String> result = new HashMap<>();
            for (String key : map.keySet()) {
                ArrayList<String> params = map.get(key);
                for (int i = 0; i < params.size(); i++) {
                    result.put(key.replace("[]", "["+i+"]"), params.get(i));
                }
            }

            if (!map.isEmpty()) return result;
            else return null;
        }

        public HashMap<String, ArrayList<String>> getMultipleMap(){
            return map;
        }

    }

    public static class CandidateDetail {
        private String id;

        public CandidateDetail(String id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CandidateDetail)) return false;

            CandidateDetail that = (CandidateDetail) o;

            return id != null ? id.equals(that.id) : that.id == null;

        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }
    }

    public static class CandidateSearch {
        private String keyword;

        public CandidateSearch(String id) {
            this.keyword = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CandidateSearch)) return false;

            CandidateSearch that = (CandidateSearch) o;

            return keyword != null ? keyword.equals(that.keyword) : that.keyword == null;

        }

        @Override
        public int hashCode() {
            return keyword != null ? keyword.hashCode() : 0;
        }
    }

    public static class CustomLocation{
        private String lat;
        private String lng;

        public CustomLocation(String lat, String lng) {
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CustomLocation)) return false;

            CustomLocation that = (CustomLocation) o;

            if (lat != null ? !lat.equals(that.lat) : that.lat != null) return false;
            return lng != null ? lng.equals(that.lng) : that.lng == null;
        }

        @Override
        public int hashCode() {
            int result = lat != null ? lat.hashCode() : 0;
            result = 31 * result + (lng != null ? lng.hashCode() : 0);
            return result;
        }
    }
}
