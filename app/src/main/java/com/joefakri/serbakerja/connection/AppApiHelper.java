/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.connection;

import android.util.Log;

import com.androidnetworking.common.Priority;
import com.joefakri.serbakerja.connection.request.AreaRequest;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.ChatRequest;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.request.WorkRequest;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.rx2androidnetworking.Rx2ANRequest;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by janisharali on 28/01/17.
 */

@Singleton
public class AppApiHelper implements Helper {

    private Header header;

    @Inject
    public AppApiHelper(Header apiheader) {
        header = apiheader;
    }

    @Override
    public Header getApiHeader() {
        return header;
    }

    @Override
    public Observable<JSONObject> register(UserRequest.Register request) {
        return Rx2AndroidNetworking.post(Path.REGISTER)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> login(UserRequest.Login request) {
        return Rx2AndroidNetworking.post(Path.LOGIN)
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> fcm(UserRequest.FCM request) {
        return Rx2AndroidNetworking.post(Path.UPDATE_FCM)
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> changePassword(UserRequest.Password request) {
        return Rx2AndroidNetworking.post(Path.CHANGE_PASSWORD)
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> verifikasi(UserRequest.Verification verification) {
        return Rx2AndroidNetworking.post(Path.VERIFIKASI)
                .addBodyParameter(verification)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> saran(UserRequest.Saran request) {
        return Rx2AndroidNetworking.post(Path.SARAN)
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> listChat(UserRequest.Id id, String offset) {
        return Rx2AndroidNetworking.get(Path.LIST_MESSAGE)
                .addPathParameter(id)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> chatSend(ChatRequest.SendMessage request, String TAG) {
        return Rx2AndroidNetworking.post(Path.SEND_MESSAGE)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .setTag(TAG)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> chatRead(ChatRequest.ReadMessage request) {
        return Rx2AndroidNetworking.post(Path.READ_MESSAGE)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> getChat(ChatRequest.GetMessage request) {
        return Rx2AndroidNetworking.post(Path.GET_MESSAGE)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> getJenisPekerjaan() {
        return Rx2AndroidNetworking.get(Path.GET_JOB_TYPE)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> getCity() {
        return Rx2AndroidNetworking.get(Path.GET_CITY)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> getDistrict(AreaRequest.District district) {
        return Rx2AndroidNetworking.get(Path.GET_DISTRICT)
                .addQueryParameter(district)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postRCompany(RecruiterRequest.PostCompany post_company, File file) {
        return Rx2AndroidNetworking.upload(Path.POST_RECURITER_COMPANY)
                .addMultipartParameter(post_company)
                .addMultipartFile("logo", file)
                .setPriority(Priority.HIGH)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postRWork(RecruiterRequest.PostWork post_work) {
        return Rx2AndroidNetworking.post(Path.POST_RECRUITER_WORK)
                .addBodyParameter(post_work)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postRInfo(RecruiterRequest.PostInfo post_info, RecruiterRequest.PostInfoFile post_info_file) {
        return Rx2AndroidNetworking.upload(Path.POST_RECRUITER_INFO)
                .addMultipartParameter(post_info)
                .addMultipartFile(post_info_file.getFoto())
                .setPriority(Priority.HIGH)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postCInfo(CandidateRequest.PostInfo post_info) {
        return Rx2AndroidNetworking.post(Path.POST_CANDIDATE_INFO)
                .addBodyParameter(post_info)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postCWork(CandidateRequest.PostDocument userid, CandidateRequest.PostWork postWork) {
        return Rx2AndroidNetworking.upload(Path.POST_CANDIDATE_WORK)
                .addMultipartParameter(userid)
                .addMultipartParameter(postWork.getMap())
                .setPriority(Priority.HIGH)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postCExperience(CandidateRequest.PostExperience postExperience, CandidateRequest.Experience experience) {
        if (experience.getMap() != null) {
            return Rx2AndroidNetworking.upload(Path.POST_CANDIDATE_EXPERIENCE)
                    .addMultipartParameter(postExperience)
                    .addMultipartParameter(experience.getMap())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getJSONObjectObservable();
        } else {
            return Rx2AndroidNetworking.post(Path.POST_CANDIDATE_EXPERIENCE)
                    .addBodyParameter(postExperience)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getJSONObjectObservable();
        }

    }

    @Override
    public Observable<JSONObject> postCEducation(CandidateRequest.PostEducation postEducation) {
        return Rx2AndroidNetworking.post(Path.POST_CANDIDATE_EDUCATION)
                .addBodyParameter(postEducation)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> postCDocument(CandidateRequest.PostDocument postDocument, CandidateRequest.PostDocumentFile file) {
        return Rx2AndroidNetworking.upload(Path.POST_CANDIDATE_DOCUMENT)
                .addMultipartParameter(postDocument)
                .addMultipartFile(file.getFoto())
                .setPriority(Priority.HIGH)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> dashboardPerekrut(DashboardRequest.Recruter recruter, DashboardRequest.CustomLocation location, String offset) {
        if (recruter != null) {
            Log.e("upload", "dashboardPerekrut");
            if (recruter.getMap() != null) {
                return Rx2AndroidNetworking.upload(Path.DASHBOARD_RECRUTER)
                        .addMultipartParameter(recruter.getMap())
                        .addQueryParameter("offset", offset)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getJSONObjectObservable();
            } else {
                Log.e("get 1", "dashboardPerekrut");
                return Rx2AndroidNetworking.get(Path.DASHBOARD_RECRUTER)
                        .addQueryParameter("offset", offset)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getJSONObjectObservable();
            }

        } if (location != null) {
            return Rx2AndroidNetworking.post(Path.DASHBOARD_RECRUTER)
                    .addBodyParameter(location)
                    .addQueryParameter("offset", offset)
                    .build()
                    .getJSONObjectObservable();
        } else {
            Log.e("get", "dashboardPerekrut");
            return Rx2AndroidNetworking.get(Path.DASHBOARD_RECRUTER)
                    .addQueryParameter("offset", offset)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getJSONObjectObservable();
        }
    }

    @Override
    public Observable<JSONObject> dashboardPerekrutDetail(DashboardRequest.RecruterDetail recruter) {
        return Rx2AndroidNetworking.get(Path.DASHBOARD_RECRUTER_DETAIL)
                .addPathParameter(recruter)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> dashboardPerekrutSearch(DashboardRequest.RecruterSearch recruter, String offset) {
        return Rx2AndroidNetworking.get(Path.DASHBOARD_RECRUTER_SEARCH)
                .addPathParameter(recruter)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> recruterProfile(RecruiterRequest.Profile recruter) {
        return Rx2AndroidNetworking.get(Path.RECRUTER_PROFILE)
                .addPathParameter(recruter)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> changeRecruterCompanyProfile(UserRequest.Id id,
                                                               RecruiterRequest.ChangeCompany changeProfile,
                                                               RecruiterRequest.ChangeCompanyFile file) {
        if (file != null) {
            return Rx2AndroidNetworking.upload(Path.CHANGE_RECRUTER_PROFILE)
                    .addPathParameter(id)
                    .addMultipartParameter(changeProfile)
                    .addMultipartFile(file.getFoto())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getJSONObjectObservable();
        } else {
            return Rx2AndroidNetworking.upload(Path.CHANGE_RECRUTER_PROFILE)
                    .addPathParameter(id)
                    .addMultipartParameter(changeProfile)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getJSONObjectObservable();
        }

    }

    @Override
    public Observable<JSONObject> jobRecruterProfile(UserRequest.Id id, String offset) {
        return Rx2AndroidNetworking.get(Path.JOB_RECRUTER_PROFILE)
                .addPathParameter(id)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> history(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.HISTORY)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> closeJob(WorkRequest.Status id) {
        return Rx2AndroidNetworking.post(Path.CLOSED_JOB)
                .addBodyParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> applicant(UserRequest.Id id, String offset) {
        return Rx2AndroidNetworking.get(Path.APPLICANT)
                .addPathParameter(id)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> update_status_applicant(UserRequest.Status status) {
        return Rx2AndroidNetworking.post(Path.UPDATE_APPLICANT)
                .addBodyParameter(status)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> verify(UserRequest.Id id, String offset) {
        return Rx2AndroidNetworking.get(Path.VERIFY)
                .addPathParameter(id)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> recruter(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.PROFILE_RECRUTER)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> changeRecruterProfile(UserRequest.Id id, RecruiterRequest.ChangeProfile profile,
                                                        RecruiterRequest.ChangeProfileFile profileFile) {
        if (profileFile != null) {
            return Rx2AndroidNetworking.upload(Path.PROFILE_RECRUTER_UPDATE)
                    .addMultipartParameter(profile)
                    .addMultipartFile(profileFile.getFoto())
                    .addPathParameter(id)
                    .build()
                    .getJSONObjectObservable();
        } else {
            return Rx2AndroidNetworking.upload(Path.PROFILE_RECRUTER_UPDATE)
                    .addMultipartParameter(profile)
                    .addPathParameter(id)
                    .build()
                    .getJSONObjectObservable();
        }

    }

    @Override
    public Observable<JSONObject> changeRecruterWork(UserRequest.Id id, RecruiterRequest.PostWork2 postWork) {
        return Rx2AndroidNetworking.upload(Path.UPDATE_RECRUTER_WORK)
                .addMultipartParameter(postWork)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }


    @Override
    public Observable<JSONObject> dashboardCandidate(UserRequest.Id id, DashboardRequest.Candidate candidate, DashboardRequest.CustomLocation location, String offset) {
        if (candidate != null) {
            if (candidate.getMap() != null) {
                return Rx2AndroidNetworking.upload(Path.DASHBOARD_CANDIDATE)
                        .addPathParameter(id)
                        .addQueryParameter("offset", offset)
                        .addMultipartParameter(candidate.getMap())
                        .setPriority(Priority.HIGH)
                        .build()
                        .getJSONObjectObservable();
            } else {
                return Rx2AndroidNetworking.get(Path.DASHBOARD_CANDIDATE)
                        .addPathParameter(id)
                        .addQueryParameter("offset", offset)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getJSONObjectObservable();
            }
        } else if (location != null) {
            return Rx2AndroidNetworking.post(Path.DASHBOARD_CANDIDATE)
                    .addPathParameter(id)
                    .addQueryParameter("offset", offset)
                    .addBodyParameter(location)
                    .build()
                    .getJSONObjectObservable();
        } else {
            return Rx2AndroidNetworking.get(Path.DASHBOARD_CANDIDATE)
                    .addPathParameter(id)
                    .addQueryParameter("offset", offset)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getJSONObjectObservable();
        }
    }

    @Override
    public Observable<JSONObject> dashboardCandidateDetail(UserRequest.Pengguna pengguna, DashboardRequest.CandidateDetail candidate) {
        return Rx2AndroidNetworking.get(Path.DASHBOARD_CANDIDATE_DETAIL)
                .addPathParameter(candidate)
                .addPathParameter(pengguna)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> dashboardCandidateSearch(UserRequest.Id id, DashboardRequest.CandidateSearch candidate, String offset) {
        return Rx2AndroidNetworking.get(Path.DASHBOARD_CANDIDATE_SEARCH)
                .addPathParameter(candidate)
                .addPathParameter(id)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> dashboardCandidateApply(UserRequest.Id id, UserRequest.Pengguna pengguna) {
        return Rx2AndroidNetworking.post(Path.DASHBOARD_CANDIDATE_APPLY)
                .addPathParameter(id)
                .addBodyParameter(pengguna)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> candidateStatusApply(UserRequest.Id id, String offset) {
        return Rx2AndroidNetworking.get(Path.CANDIDATE_STATUS_APPLY)
                .addPathParameter(id)
                .addQueryParameter("offset", offset)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileCandidate(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.PROFILE_CANDIDATE)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidate(UserRequest.Id id, CandidateRequest.ChangeProfile profile,
                                                         CandidateRequest.ChangeProfileFile profileFile) {
        if (profileFile.getFoto() != null) {
            return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE)
                    .addMultipartParameter(profile)
                    .addMultipartFile(profileFile.getFoto())
                    .addPathParameter(id)
                    .build()
                    .getJSONObjectObservable();
        } else {
            return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE)
                    .addMultipartParameter(profile)
                    .addPathParameter(id)
                    .build()
                    .getJSONObjectObservable();
        }

    }

    @Override
    public Observable<JSONObject> profileCandidateWork(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.PROFILE_CANDIDATE_WORK)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateWork(UserRequest.Id id, CandidateRequest.ChangeProfileWork profile) {
        return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE_WORK)
                .addMultipartParameter(profile)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileCandidateEducation(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.PROFILE_CANDIDATE_EDUCATION)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateEducation(UserRequest.Id id, CandidateRequest.ChangeProfileEducation profile) {
        return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE_EDUCATION)
                .addMultipartParameter(profile)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileCandidateDocument(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.PROFILE_CANDIDATE_DOCUMENT)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateDocument(UserRequest.Id id, CandidateRequest.PostDocumentFile profile) {
        return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE_DOCUMENT)
                .addMultipartFile(profile.getFoto())
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileCandidateExperience(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.PROFILE_CANDIDATE_EXPERIENCE)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateExperience(UserRequest.Id id, CandidateRequest.ChangeProfileExperience profile) {
        return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE)
                .addMultipartParameter(profile)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateExperienceDetail(UserRequest.Id id, CandidateRequest.ChangeProfileExperience profile) {
        return Rx2AndroidNetworking.upload(Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL)
                .addMultipartParameter(profile)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateExperienceDetailInsert(CandidateRequest.ChangeProfileExperience profile) {
        return Rx2AndroidNetworking.post(Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL_INSERT)
                .addBodyParameter(profile)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> notification(UserRequest.Id id) {
        return Rx2AndroidNetworking.get(Path.NOTIFICATION)
                .addPathParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> readNotification(UserRequest.Id id) {
        return Rx2AndroidNetworking.post(Path.READ_NOTIFICATION)
                .addBodyParameter(id)
                .build()
                .getJSONObjectObservable();
    }

    @Override
    public Observable<JSONObject> deactiveAccount(UserRequest.Pengguna id) {
        return Rx2AndroidNetworking.post(Path.DEACTIVE_CANDIDATE)
                .addBodyParameter(id)
                .build()
                .getJSONObjectObservable();
    }


}

