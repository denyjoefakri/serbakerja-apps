package com.joefakri.serbakerja.connection.request;

/**
 * Created by deny on bandung.
 */

public class AreaRequest {


    public static class District {

        private String kota_id;

        public District(String kota_id) {
            this.kota_id = kota_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof District)) return false;

            District district = (District) o;

            return kota_id != null ? kota_id.equals(district.kota_id) : district.kota_id == null;

        }

        @Override
        public int hashCode() {
            return kota_id != null ? kota_id.hashCode() : 0;
        }
    }

}
