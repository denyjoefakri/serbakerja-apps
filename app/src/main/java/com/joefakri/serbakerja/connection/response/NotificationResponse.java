package com.joefakri.serbakerja.connection.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by deny on bandung.
 */

public class NotificationResponse {

    public static class ListNotif implements Serializable {

        private String status;
        private String message;
        private List<Data> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        public class Data implements Serializable{

            private String id;
            private String pengguna_id;
            private String from_id;
            private String role;
            private String tipe;
            private String pesan;
            private String status;
            private String date;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPengguna_id() {
                return pengguna_id;
            }

            public void setPengguna_id(String pengguna_id) {
                this.pengguna_id = pengguna_id;
            }

            public String getFrom_id() {
                return from_id;
            }

            public void setFrom_id(String from_id) {
                this.from_id = from_id;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getTipe() {
                return tipe;
            }

            public void setTipe(String tipe) {
                this.tipe = tipe;
            }

            public String getPesan() {
                return pesan;
            }

            public void setPesan(String pesan) {
                this.pesan = pesan;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }
    }
}
