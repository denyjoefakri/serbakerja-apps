package com.joefakri.serbakerja.connection.request;

/**
 * Created by deny on bandung.
 */

public class UserRequest {

    public static class Id {
        private String id;

        public Id(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;

            Id id1 = (Id) o;

            return id != null ? id.equals(id1.id) : id1.id == null;
        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }
    }

    public static class Status {
        private String lamaran_id;
        private String status;

        public Status(String lamaran_id, String status) {
            this.lamaran_id = lamaran_id;
            this.status = status;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Status)) return false;

            Status status1 = (Status) o;

            if (lamaran_id != null ? !lamaran_id.equals(status1.lamaran_id) : status1.lamaran_id != null)
                return false;
            return status != null ? status.equals(status1.status) : status1.status == null;
        }

        @Override
        public int hashCode() {
            int result = lamaran_id != null ? lamaran_id.hashCode() : 0;
            result = 31 * result + (status != null ? status.hashCode() : 0);
            return result;
        }
    }

    public static class Register {

        private String nama;
        private String no_hp;
        private String email;
        private String password;

        public Register(String nama, String no_hp, String email, String password) {
            this.nama = nama;
            this.no_hp = no_hp;
            this.email = email;
            this.password = password;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Register)) return false;

            Register register = (Register) o;

            if (nama != null ? !nama.equals(register.nama) : register.nama != null) return false;
            if (no_hp != null ? !no_hp.equals(register.no_hp) : register.no_hp != null)
                return false;
            if (email != null ? !email.equals(register.email) : register.email != null)
                return false;
            return password != null ? password.equals(register.password) : register.password == null;

        }

        @Override
        public int hashCode() {
            int result = nama != null ? nama.hashCode() : 0;
            result = 31 * result + (no_hp != null ? no_hp.hashCode() : 0);
            result = 31 * result + (email != null ? email.hashCode() : 0);
            result = 31 * result + (password != null ? password.hashCode() : 0);
            return result;
        }
    }

    public static class Login {

        private String email;
        private String password;

        public Login(String email, String password) {
            this.email = email;
            this.password = password;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Login)) return false;

            Login login = (Login) o;

            if (email != null ? !email.equals(login.email) : login.email != null) return false;
            return password != null ? password.equals(login.password) : login.password == null;

        }

        @Override
        public int hashCode() {
            int result = email != null ? email.hashCode() : 0;
            result = 31 * result + (password != null ? password.hashCode() : 0);
            return result;
        }
    }

    public static class Password {
        private String pengguna_id;
        private String password;
        private String confirm_password;
        private String old_password;

        public Password(String pengguna_id, String old_password, String password, String confirm_password) {
            this.pengguna_id = pengguna_id;
            this.old_password = old_password;
            this.password = password;
            this.confirm_password = confirm_password;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Password)) return false;

            Password password1 = (Password) o;

            if (pengguna_id != null ? !pengguna_id.equals(password1.pengguna_id) : password1.pengguna_id != null)
                return false;
            if (password != null ? !password.equals(password1.password) : password1.password != null)
                return false;
            if (confirm_password != null ? !confirm_password.equals(password1.confirm_password) : password1.confirm_password != null)
                return false;
            return old_password != null ? old_password.equals(password1.old_password) : password1.old_password == null;
        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (password != null ? password.hashCode() : 0);
            result = 31 * result + (confirm_password != null ? confirm_password.hashCode() : 0);
            result = 31 * result + (old_password != null ? old_password.hashCode() : 0);
            return result;
        }
    }

    public static class Pengguna {
        private String pengguna_id;

        public Pengguna(String pengguna_id) {
            this.pengguna_id = pengguna_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Pengguna)) return false;

            Pengguna pengguna = (Pengguna) o;

            return pengguna_id != null ? pengguna_id.equals(pengguna.pengguna_id) : pengguna.pengguna_id == null;
        }

        @Override
        public int hashCode() {
            return pengguna_id != null ? pengguna_id.hashCode() : 0;
        }
    }


    public static class FCM {
        private String pengguna_id;
        private String gcm;

        public FCM(String pengguna_id, String gcm) {
            this.pengguna_id = pengguna_id;
            this.gcm = gcm;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof FCM)) return false;

            FCM fcm = (FCM) o;

            if (pengguna_id != null ? !pengguna_id.equals(fcm.pengguna_id) : fcm.pengguna_id != null)
                return false;
            return gcm != null ? gcm.equals(fcm.gcm) : fcm.gcm == null;
        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (gcm != null ? gcm.hashCode() : 0);
            return result;
        }
    }


    public static class Saran {
        private String pengguna_id;
        private String subjek;
        private String pesan;

        public Saran(String pengguna_id, String subjek, String pesan) {
            this.pengguna_id = pengguna_id;
            this.subjek = subjek;
            this.pesan = pesan;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Saran)) return false;

            Saran saran = (Saran) o;

            if (pengguna_id != null ? !pengguna_id.equals(saran.pengguna_id) : saran.pengguna_id != null)
                return false;
            if (subjek != null ? !subjek.equals(saran.subjek) : saran.subjek != null) return false;
            return pesan != null ? pesan.equals(saran.pesan) : saran.pesan == null;
        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (subjek != null ? subjek.hashCode() : 0);
            result = 31 * result + (pesan != null ? pesan.hashCode() : 0);
            return result;
        }
    }

    public static class Verification {
        private String pengguna_id;
        private String no_hp;

        public Verification(String pengguna_id, String no_hp) {
            this.pengguna_id = pengguna_id;
            this.no_hp = no_hp;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Verification)) return false;

            Verification that = (Verification) o;

            if (pengguna_id != null ? !pengguna_id.equals(that.pengguna_id) : that.pengguna_id != null)
                return false;
            return no_hp != null ? no_hp.equals(that.no_hp) : that.no_hp == null;
        }

        @Override
        public int hashCode() {
            int result = pengguna_id != null ? pengguna_id.hashCode() : 0;
            result = 31 * result + (no_hp != null ? no_hp.hashCode() : 0);
            return result;
        }
    }

}
