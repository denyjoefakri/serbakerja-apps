package com.joefakri.serbakerja.connection.response;

import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class HistoryResponse {

    private String status;
    private String message;
    private ArrayList<Data> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data {
        private String pengguna_id;
        private String title;
        private String activity;
        private String created_on;

        public String getPengguna_id() {
            return pengguna_id;
        }

        public void setPengguna_id(String pengguna_id) {
            this.pengguna_id = pengguna_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }
    }
}
