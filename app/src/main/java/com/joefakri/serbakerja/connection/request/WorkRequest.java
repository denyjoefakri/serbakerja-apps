package com.joefakri.serbakerja.connection.request;

/**
 * Created by deny on bandung.
 */

public class WorkRequest {

    public static class Status {

        private String pekerjaan_id;
        private String status;

        public Status(String pekerjaan_id, String status) {
            this.pekerjaan_id = pekerjaan_id;
            this.status = status;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Status)) return false;

            Status status1 = (Status) o;

            if (pekerjaan_id != null ? !pekerjaan_id.equals(status1.pekerjaan_id) : status1.pekerjaan_id != null)
                return false;
            return status != null ? status.equals(status1.status) : status1.status == null;
        }

        @Override
        public int hashCode() {
            int result = pekerjaan_id != null ? pekerjaan_id.hashCode() : 0;
            result = 31 * result + (status != null ? status.hashCode() : 0);
            return result;
        }
    }
}
