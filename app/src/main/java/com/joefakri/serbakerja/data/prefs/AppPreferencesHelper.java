
package com.joefakri.serbakerja.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.AppDataManager;
import com.joefakri.serbakerja.di.ApplicationContext;
import com.joefakri.serbakerja.di.PreferenceInfo;
import com.joefakri.serbakerja.utils.Constants;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_CURRENT_USER_PHONE = "PREF_KEY_CURRENT_USER_PHONE";
    private static final String PREF_KEY_CURRENT_USER_PASSWORD = "PREF_KEY_CURRENT_USER_PASSWORD";
    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_TYPE_USER = "PREF_KEY_TYPE_USER";
    private static final String PREF_KEY_USER_STATUS = "PREF_KEY_CURRENT_USER_STATUS";
    private static final String PREF_KEY_DEFAULT_CITY = "PREF_KEY_DEFAULT_CITY";
    private static final String PREF_KEY_DEFAULT_JOB = "PREF_KEY_DEFAULT_JOB";
    private static final String PREF_KEY_FCM = "PREF_KEY_FCM";
    private static final String PREF_KEY_DATE_TEMPORARY = "PREF_KEY_DATE_TEMPORARY";
    private static final String RECRUTER1 = "PREF_KEY_RECRUTER_1";
    private static final String RECRUTER2 = "PREF_KEY_RECRUTER_2";
    private static final String RECRUTER3 = "PREF_KEY_RECRUTER_3";
    private static final String CANDIDATE1 = "PREF_KEY_CANDIDATE_1";
    private static final String CANDIDATE2 = "PREF_KEY_CANDIDATE_2";
    private static final String CANDIDATE3 = "PREF_KEY_CANDIDATE_3";
    private static final String CANDIDATE4 = "PREF_KEY_CANDIDATE_4";
    private static final String CANDIDATE5 = "PREF_KEY_CANDIDATE_5";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getCurrentUserId() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_ID, null);
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID, userId).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, null);
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply();
    }

    @Override
    public String getCurrentUserPhone() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PHONE, null);
    }

    @Override
    public void setCurrentUserPhone(String phone) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PHONE, phone).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public String getCurrentUserPassword() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PASSWORD, null);
    }

    @Override
    public void setCurrentUserPassword(String password) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PASSWORD, password).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, null);
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                AppDataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(AppDataManager.LoggedInMode mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public String getTypeUser() {
        return mPrefs.getString(PREF_KEY_TYPE_USER, null);
    }

    @Override
    public void setTypeUser(String typeUser) {
        mPrefs.edit().putString(PREF_KEY_TYPE_USER, typeUser).apply();
    }

    @Override
    public String getUserStatus() {
        return mPrefs.getString(PREF_KEY_USER_STATUS, null);
    }

    @Override
    public void setUserStatus(String userStatus) {
        mPrefs.edit().putString(PREF_KEY_USER_STATUS, userStatus).apply();
    }

    @Override
    public String getFcmId() {
        return mPrefs.getString(PREF_KEY_FCM, null);
    }

    @Override
    public void setFcmId(String fcmId) {
        mPrefs.edit().putString(PREF_KEY_FCM, fcmId).apply();
    }

    @Override
    public ArrayList<UserResponse.Login.Data.Kota> getDefaultCity() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_KEY_DEFAULT_CITY, null);
        Type type = new TypeToken<ArrayList<UserResponse.Login.Data.Kota>>() {}.getType();
        ArrayList<UserResponse.Login.Data.Kota> data = gson.fromJson(json, type);
        return data;
    }

    @Override
    public void setDefaultCity(ArrayList<UserResponse.Login.Data.Kota> city_id) {
        Gson gson = new Gson();
        String json = gson.toJson(city_id);
        mPrefs.edit().putString(PREF_KEY_DEFAULT_CITY, json).apply();
    }


    @Override
    public ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> getDefaultJob() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_KEY_DEFAULT_JOB, null);
        Type type = new TypeToken<ArrayList<UserResponse.Login.Data.Jenis_pekerjaan>>() {}.getType();
        ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> data = gson.fromJson(json, type);
        return data;
    }

    @Override
    public void setDefaultJob(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> job_id) {
        Gson gson = new Gson();
        String json = gson.toJson(job_id);
        mPrefs.edit().putString(PREF_KEY_DEFAULT_JOB, json).apply();
    }

    @Override
    public String getDateTemporary() {
        return mPrefs.getString(PREF_KEY_DATE_TEMPORARY, "");
    }

    @Override
    public void setDateTemporary(String fcmId) {
        mPrefs.edit().putString(PREF_KEY_DATE_TEMPORARY, fcmId).apply();
    }

    @Override
    public String getRecruter1() {
        return mPrefs.getString(RECRUTER1, null);
    }

    @Override
    public void setRecruter1(String status) {
        mPrefs.edit().putString(RECRUTER1, status).apply();
    }

    @Override
    public String getRecruter2() {
        return mPrefs.getString(RECRUTER2, null);
    }

    @Override
    public void setRecruter2(String status) {
        mPrefs.edit().putString(RECRUTER2, status).apply();
    }

    @Override
    public String getRecruter3() {
        return mPrefs.getString(RECRUTER3, null);
    }

    @Override
    public void setRecruter3(String status) {
        mPrefs.edit().putString(RECRUTER3, status).apply();
    }

    @Override
    public String getCandidate1() {
        return mPrefs.getString(CANDIDATE1, null);
    }

    @Override
    public void setCandidate1(String status) {
        mPrefs.edit().putString(CANDIDATE1, status).apply();
    }

    @Override
    public String getCandidate2() {
        return mPrefs.getString(CANDIDATE2, null);
    }

    @Override
    public void setCandidate2(String status) {
        mPrefs.edit().putString(CANDIDATE2, status).apply();
    }

    @Override
    public String getCandidate3() {
        return mPrefs.getString(CANDIDATE3, null);
    }

    @Override
    public void setCandidate3(String status) {
        mPrefs.edit().putString(CANDIDATE3, status).apply();
    }

    @Override
    public String getCandidate4() {
        return mPrefs.getString(CANDIDATE4, null);
    }

    @Override
    public void setCandidate4(String status) {
        mPrefs.edit().putString(CANDIDATE4, status).apply();
    }

    @Override
    public String getCandidate5() {
        return mPrefs.getString(CANDIDATE5, null);
    }

    @Override
    public void setCandidate5(String status) {
        mPrefs.edit().putString(CANDIDATE5, status).apply();
    }

}
