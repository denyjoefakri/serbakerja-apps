
package com.joefakri.serbakerja.data;


import android.content.Context;

import com.joefakri.serbakerja.connection.Header;
import com.joefakri.serbakerja.connection.Helper;
import com.joefakri.serbakerja.connection.request.AreaRequest;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.ChatRequest;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.request.WorkRequest;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.prefs.PreferencesHelper;
import com.joefakri.serbakerja.di.ApplicationContext;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final Helper helper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          PreferencesHelper preferencesHelper,
                          Helper apihelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        helper = apihelper;
    }

    @Override
    public Header getApiHeader() {
        return helper.getApiHeader();
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        helper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public String getTypeUser() {
        return mPreferencesHelper.getTypeUser();
    }

    @Override
    public void setTypeUser(String typeUser) {
        mPreferencesHelper.setTypeUser(typeUser);
    }

    @Override
    public String getUserStatus() {
        return mPreferencesHelper.getUserStatus();
    }

    @Override
    public void setUserStatus(String userStatus) {
        mPreferencesHelper.setUserStatus(userStatus);
    }

    @Override
    public String getFcmId() {
        return mPreferencesHelper.getFcmId();
    }

    @Override
    public void setFcmId(String fcmId) {
        mPreferencesHelper.setFcmId(fcmId);
    }

    @Override
    public ArrayList<UserResponse.Login.Data.Kota> getDefaultCity() {
        return mPreferencesHelper.getDefaultCity();
    }

    @Override
    public void setDefaultCity(ArrayList<UserResponse.Login.Data.Kota> city_id) {
        mPreferencesHelper.setDefaultCity(city_id);
    }

    @Override
    public ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> getDefaultJob() {
        return mPreferencesHelper.getDefaultJob();
    }

    @Override
    public void setDefaultJob(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> job_id) {
        mPreferencesHelper.setDefaultJob(job_id);
    }

    @Override
    public String getDateTemporary() {
        return mPreferencesHelper.getDateTemporary();
    }

    @Override
    public void setDateTemporary(String fcmId) {
        mPreferencesHelper.setDateTemporary(fcmId);
    }

    @Override
    public String getRecruter1() {
        return mPreferencesHelper.getRecruter1();
    }

    @Override
    public void setRecruter1(String status) {
        mPreferencesHelper.setRecruter1(status);
    }

    @Override
    public String getRecruter2() {
        return mPreferencesHelper.getRecruter2();
    }

    @Override
    public void setRecruter2(String status) {
        mPreferencesHelper.setRecruter2(status);
    }

    @Override
    public String getRecruter3() {
        return mPreferencesHelper.getRecruter3();
    }

    @Override
    public void setRecruter3(String status) {
        mPreferencesHelper.setRecruter3(status);
    }

    @Override
    public String getCandidate1() {
        return mPreferencesHelper.getCandidate1();
    }

    @Override
    public void setCandidate1(String status) {
        mPreferencesHelper.setCandidate1(status);
    }

    @Override
    public String getCandidate2() {
        return mPreferencesHelper.getCandidate2();
    }

    @Override
    public void setCandidate2(String status) {
        mPreferencesHelper.setCandidate2(status);
    }

    @Override
    public String getCandidate3() {
        return mPreferencesHelper.getCandidate3();
    }

    @Override
    public void setCandidate3(String status) {
        mPreferencesHelper.setCandidate3(status);
    }

    @Override
    public String getCandidate4() {
        return mPreferencesHelper.getCandidate4();
    }

    @Override
    public void setCandidate4(String status) {
        mPreferencesHelper.setCandidate4(status);
    }

    @Override
    public String getCandidate5() {
        return mPreferencesHelper.getCandidate5();
    }

    @Override
    public void setCandidate5(String status) {
        mPreferencesHelper.setCandidate5(status);
    }


    @Override
    public void updateApiHeader(String userId, String accessToken) {
        //helper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        helper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public String getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(null, null, null, null, null, null, null);
        setTypeUser(null);
        setPreferenceNull();
        loginMode(AppDataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);
    }

    @Override
    public void setPreferenceNull() {
        setDefaultCity(null);
        setDefaultJob(null);
    }

    @Override
    public void updateUserInfo(String accessToken, String userId, String username, String phone, String email, String password, String profilePicPath) {
        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserName(username);
        setCurrentUserPhone(phone);
        setCurrentUserEmail(email);
        setCurrentUserPassword(password);
        setCurrentUserProfilePicUrl(profilePicPath);

        updateApiHeader(userId, accessToken);
    }

    @Override
    public void updateAdditionalUserInfo(String tipe_user, String status, ArrayList<UserResponse.Login.Data.Kota> city, ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> job) {
        setTypeUser(tipe_user);
        setUserStatus(status);
        setDefaultCity(city);
        setDefaultJob(job);
    }

    @Override
    public void updateRecruter(String status1, String status2, String status3) {
        setRecruter1(status1);
        setRecruter2(status2);
        setRecruter3(status3);
    }

    @Override
    public void updateCandidate(String status1, String status2, String status3, String status4, String status5) {
        setCandidate1(status1);
        setCandidate2(status2);
        setCandidate3(status3);
        setCandidate4(status4);
        setCandidate5(status5);
    }


    @Override
    public void loginMode(LoggedInMode loggedInMode) {
        setCurrentUserLoggedInMode(loggedInMode);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserPhone() {
        return mPreferencesHelper.getCurrentUserPhone();
    }

    @Override
    public void setCurrentUserPhone(String phone) {
        mPreferencesHelper.setCurrentUserPhone(phone);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserPassword() {
        return mPreferencesHelper.getCurrentUserPassword();
    }

    @Override
    public void setCurrentUserPassword(String password) {
        mPreferencesHelper.setCurrentUserPassword(password);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }


    /**
     * API
     * */

    @Override
    public Observable<JSONObject> register(UserRequest.Register request) {
        return helper.register(request);
    }

    @Override
    public Observable<JSONObject> login(UserRequest.Login request) {
        return helper.login(request);
    }

    @Override
    public Observable<JSONObject> fcm(UserRequest.FCM request) {
        return helper.fcm(request);
    }

    @Override
    public Observable<JSONObject> changePassword(UserRequest.Password request) {
        return helper.changePassword(request);
    }

    @Override
    public Observable<JSONObject> verifikasi(UserRequest.Verification verification) {
        return helper.verifikasi(verification);
    }

    @Override
    public Observable<JSONObject> saran(UserRequest.Saran request) {
        return helper.saran(request);
    }

    @Override
    public Observable<JSONObject> listChat(UserRequest.Id id, String offset) {
        return helper.listChat(id, offset);
    }


    @Override
    public Observable<JSONObject> chatSend(ChatRequest.SendMessage request, String TAG) {
        return helper.chatSend(request, TAG);
    }

    @Override
    public Observable<JSONObject> chatRead(ChatRequest.ReadMessage request) {
        return helper.chatRead(request);
    }

    @Override
    public Observable<JSONObject> getChat(ChatRequest.GetMessage request) {
        return helper.getChat(request);
    }

    @Override
    public Observable<JSONObject> getJenisPekerjaan() {
        return helper.getJenisPekerjaan();
    }

    @Override
    public Observable<JSONObject> getCity() {
        return helper.getCity();
    }

    @Override
    public Observable<JSONObject> getDistrict(AreaRequest.District district) {
        return helper.getDistrict(district);
    }

    @Override
    public Observable<JSONObject> postRCompany(RecruiterRequest.PostCompany post_company, File file) {
        return helper.postRCompany(post_company, file);
    }

    @Override
    public Observable<JSONObject> postRWork(RecruiterRequest.PostWork post_work) {
        return helper.postRWork(post_work);
    }

    @Override
    public Observable<JSONObject> postRInfo(RecruiterRequest.PostInfo post_info, RecruiterRequest.PostInfoFile file) {
        return helper.postRInfo(post_info, file);
    }

    @Override
    public Observable<JSONObject> postCInfo(CandidateRequest.PostInfo post_info) {
        return helper.postCInfo(post_info);
    }

    @Override
    public Observable<JSONObject> postCWork(CandidateRequest.PostDocument userid, CandidateRequest.PostWork postWork) {
        return helper.postCWork(userid, postWork);
    }

    @Override
    public Observable<JSONObject> postCExperience(CandidateRequest.PostExperience postExperience, CandidateRequest.Experience experience) {
        return helper.postCExperience(postExperience, experience);
    }

    @Override
    public Observable<JSONObject> postCEducation(CandidateRequest.PostEducation postEducation) {
        return helper.postCEducation(postEducation);
    }

    @Override
    public Observable<JSONObject> postCDocument(CandidateRequest.PostDocument postDocument, CandidateRequest.PostDocumentFile file) {
        return helper.postCDocument(postDocument, file);
    }

    @Override
    public Observable<JSONObject> dashboardPerekrut(DashboardRequest.Recruter recruter, DashboardRequest.CustomLocation location, String offset) {
        return helper.dashboardPerekrut(recruter, location, offset);
    }

    @Override
    public Observable<JSONObject> dashboardPerekrutDetail(DashboardRequest.RecruterDetail recruter) {
        return helper.dashboardPerekrutDetail(recruter);
    }

    @Override
    public Observable<JSONObject> dashboardPerekrutSearch(DashboardRequest.RecruterSearch recruter, String offset) {
        return helper.dashboardPerekrutSearch(recruter, offset);
    }

    @Override
    public Observable<JSONObject> recruterProfile(RecruiterRequest.Profile recruter) {
        return helper.recruterProfile(recruter);
    }

    @Override
    public Observable<JSONObject> changeRecruterCompanyProfile(UserRequest.Id id, RecruiterRequest.ChangeCompany changeProfile,
                                                               RecruiterRequest.ChangeCompanyFile file) {
        return helper.changeRecruterCompanyProfile(id, changeProfile, file);
    }

    @Override
    public Observable<JSONObject> jobRecruterProfile(UserRequest.Id id, String offset) {
        return helper.jobRecruterProfile(id, offset);
    }

    @Override
    public Observable<JSONObject> history(UserRequest.Id id) {
        return helper.history(id);
    }

    @Override
    public Observable<JSONObject> closeJob(WorkRequest.Status id) {
        return helper.closeJob(id);
    }

    @Override
    public Observable<JSONObject> applicant(UserRequest.Id id, String offset) {
        return helper.applicant(id, offset);
    }

    @Override
    public Observable<JSONObject> update_status_applicant(UserRequest.Status status) {
        return helper.update_status_applicant(status);
    }

    @Override
    public Observable<JSONObject> verify(UserRequest.Id id, String offset) {
        return helper.verify(id, offset);
    }

    @Override
    public Observable<JSONObject> recruter(UserRequest.Id id) {
        return helper.recruter(id);
    }

    @Override
    public Observable<JSONObject> changeRecruterProfile(UserRequest.Id id, RecruiterRequest.ChangeProfile profile, RecruiterRequest.ChangeProfileFile profileFile) {
        return helper.changeRecruterProfile(id, profile, profileFile);
    }

    @Override
    public Observable<JSONObject> changeRecruterWork(UserRequest.Id id, RecruiterRequest.PostWork2 postWork) {
        return helper.changeRecruterWork(id, postWork);
    }

    @Override
    public Observable<JSONObject> dashboardCandidate(UserRequest.Id id, DashboardRequest.Candidate candidate, DashboardRequest.CustomLocation location, String offset) {
        return helper.dashboardCandidate(id, candidate, location, offset);
    }

    @Override
    public Observable<JSONObject> dashboardCandidateDetail(UserRequest.Pengguna pengguna, DashboardRequest.CandidateDetail candidate) {
        return helper.dashboardCandidateDetail(pengguna, candidate);
    }

    @Override
    public Observable<JSONObject> dashboardCandidateSearch(UserRequest.Id id, DashboardRequest.CandidateSearch candidate, String offset) {
        return helper.dashboardCandidateSearch(id, candidate, offset);
    }

    @Override
    public Observable<JSONObject> dashboardCandidateApply(UserRequest.Id id, UserRequest.Pengguna pengguna) {
        return helper.dashboardCandidateApply(id, pengguna);
    }

    @Override
    public Observable<JSONObject> candidateStatusApply(UserRequest.Id id, String offset) {
        return helper.candidateStatusApply(id, offset);
    }

    @Override
    public Observable<JSONObject> profileCandidate(UserRequest.Id id) {
        return helper.profileCandidate(id);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidate(UserRequest.Id id, CandidateRequest.ChangeProfile profile, CandidateRequest.ChangeProfileFile profileFile) {
        return helper.profileUpdateCandidate(id, profile, profileFile);
    }

    @Override
    public Observable<JSONObject> profileCandidateWork(UserRequest.Id id) {
        return helper.profileCandidateWork(id);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateWork(UserRequest.Id id, CandidateRequest.ChangeProfileWork profile) {
        return helper.profileUpdateCandidateWork(id, profile);
    }

    @Override
    public Observable<JSONObject> profileCandidateEducation(UserRequest.Id id) {
        return helper.profileCandidateEducation(id);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateEducation(UserRequest.Id id, CandidateRequest.ChangeProfileEducation profile) {
        return helper.profileUpdateCandidateEducation(id, profile);
    }

    @Override
    public Observable<JSONObject> profileCandidateDocument(UserRequest.Id id) {
        return helper.profileCandidateDocument(id);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateDocument(UserRequest.Id id, CandidateRequest.PostDocumentFile profile) {
        return helper.profileUpdateCandidateDocument(id, profile);
    }

    @Override
    public Observable<JSONObject> profileCandidateExperience(UserRequest.Id id) {
        return helper.profileCandidateExperience(id);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateExperience(UserRequest.Id id, CandidateRequest.ChangeProfileExperience profile) {
        return helper.profileUpdateCandidateExperience(id, profile);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateExperienceDetail(UserRequest.Id id, CandidateRequest.ChangeProfileExperience profile) {
        return helper.profileUpdateCandidateExperienceDetail(id, profile);
    }

    @Override
    public Observable<JSONObject> profileUpdateCandidateExperienceDetailInsert(CandidateRequest.ChangeProfileExperience profile) {
        return helper.profileUpdateCandidateExperienceDetailInsert(profile);
    }

    @Override
    public Observable<JSONObject> notification(UserRequest.Id id) {
        return helper.notification(id);
    }

    @Override
    public Observable<JSONObject> readNotification(UserRequest.Id id) {
        return helper.readNotification(id);
    }

    @Override
    public Observable<JSONObject> deactiveAccount(UserRequest.Pengguna id) {
        return helper.deactiveAccount(id);
    }


}
