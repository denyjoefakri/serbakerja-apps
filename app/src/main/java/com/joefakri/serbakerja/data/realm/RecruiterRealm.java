package com.joefakri.serbakerja.data.realm;

import com.joefakri.serbakerja.utils.CommonUtils;


import io.realm.RealmObject;

/**
 * Created by deny on bandung.
 */

public class RecruiterRealm extends RealmObject {

    private int id;
    private int percent1 = 0;
    private int percent2 = 0;
    private int percent3 = 0;

    public int getPercent1() {
        return CommonUtils.percent(7, percent1);
    }

    public void setPercent1(int percent1) {
        this.percent1 = percent1;
    }

    public int getPercent2() {
        return CommonUtils.percent(13, percent2);
    }

    public void setPercent2(int percent2) {
        this.percent2 = percent2;
    }

    public int getPercent3() {
        return CommonUtils.percent(9, percent3);
    }

    public void setPercent3(int percent3) {
        this.percent3 = percent3;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
    * Company
    * */

    private String companyName;
    private String companyAddress;
    private String companyCity;
    private String companyCityId;
    private String companyArea;
    private String companyTotalEmployeCode;
    private String companyWebsite;
    private String companyLogo;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    public String getCompanyCityId() {
        return companyCityId;
    }

    public void setCompanyCityId(String companyCityId) {
        this.companyCityId = companyCityId;
    }

    public String getCompanyArea() {
        return companyArea;
    }

    public void setCompanyArea(String companyArea) {
        this.companyArea = companyArea;
    }

    public String getCompanyTotalEmployeCode() {
        return companyTotalEmployeCode;
    }

    public void setCompanyTotalEmployeCode(String companyTotalEmployeCode) {
        this.companyTotalEmployeCode = companyTotalEmployeCode;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }


    /**
     * Pekerjaan
     * */

    private String workTypeId;
    private String workType;
    private String workSallary;
    private String workArea;
    private String workCity;
    private String workCityId;
    private String workAddress;
    private String workDay;
    private String workTime;
    private String workTimeOption;
    private String workGender;
    private String workMinimalExperience;
    private String workEducation;
    private String workRequirement;
    private String workScope;
    private String workExpiredDate;

    public String getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(String workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkSallary() {
        return workSallary;
    }

    public void setWorkSallary(String workSallary) {
        this.workSallary = workSallary;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public String getWorkCityId() {
        return workCityId;
    }

    public void setWorkCityId(String workCityId) {
        this.workCityId = workCityId;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getWorkDay() {
        return workDay;
    }

    public void setWorkDay(String workDay) {
        this.workDay = workDay;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getWorkTimeOption() {
        return workTimeOption;
    }

    public void setWorkTimeOption(String workTimeOption) {
        this.workTimeOption = workTimeOption;
    }

    public String getWorkGender() {
        return workGender;
    }

    public void setWorkGender(String workGender) {
        this.workGender = workGender;
    }

    public String getWorkMinimalExperience() {
        return workMinimalExperience;
    }

    public void setWorkMinimalExperience(String workMinimalExperience) {
        this.workMinimalExperience = workMinimalExperience;
    }

    public String getWorkEducation() {
        return workEducation;
    }

    public void setWorkEducation(String workEducation) {
        this.workEducation = workEducation;
    }

    public String getWorkRequirement() {
        return workRequirement;
    }

    public void setWorkRequirement(String workRequirement) {
        this.workRequirement = workRequirement;
    }

    public String getWorkScope() {
        return workScope;
    }

    public void setWorkScope(String workScope) {
        this.workScope = workScope;
    }

    public String getWorkExpiredDate() {
        return workExpiredDate;
    }

    public void setWorkExpiredDate(String workExpiredDate) {
        this.workExpiredDate = workExpiredDate;
    }


    /**
     *
     * Person
     *
     * */

    private String personName;
    private String personBirthday;
    private String personBirthplace;
    private String personAddress;
    private String personDistrics;
    private String personVillage;
    private String personRtrw;
    private String personPhotoKTP;
    private String personPhoto;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonBirthday() {
        return personBirthday;
    }

    public void setPersonBirthday(String personBirthday) {
        this.personBirthday = personBirthday;
    }

    public String getPersonBirthplace() {
        return personBirthplace;
    }

    public void setPersonBirthplace(String personBirthplace) {
        this.personBirthplace = personBirthplace;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonDistrics() {
        return personDistrics;
    }

    public void setPersonDistrics(String personDistrics) {
        this.personDistrics = personDistrics;
    }

    public String getPersonVillage() {
        return personVillage;
    }

    public void setPersonVillage(String personVillage) {
        this.personVillage = personVillage;
    }

    public String getPersonRtrw() {
        return personRtrw;
    }

    public void setPersonRtrw(String personRtrw) {
        this.personRtrw = personRtrw;
    }

    public String getPersonPhotoKTP() {
        return personPhotoKTP;
    }

    public void setPersonPhotoKTP(String personPhotoKTP) {
        this.personPhotoKTP = personPhotoKTP;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }


}
