package com.joefakri.serbakerja.data.realm;

import io.realm.RealmObject;

/**
 * Created by deny on bandung.
 */

public class StateRealm extends RealmObject {

    private int id;
    private int state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
