package com.joefakri.serbakerja.data.realm;

import io.realm.RealmObject;

/**
 * Created by deny on bandung.
 */

public class ExperienceRealm extends RealmObject {

    private String workAs;
    private String workLongTime;
    private String workPeriod;
    private String companyName;

    public String getWorkAs() {
        return workAs;
    }

    public void setWorkAs(String workAs) {
        this.workAs = workAs;
    }

    public String getWorkLongTime() {
        return workLongTime;
    }

    public void setWorkLongTime(String workLongTime) {
        this.workLongTime = workLongTime;
    }

    public String getWorkPeriod() {
        return workPeriod;
    }

    public void setWorkPeriod(String workPeriod) {
        this.workPeriod = workPeriod;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
