package com.joefakri.serbakerja.data.realm;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by deny on bandung.
 */

public class WorkRealm extends RealmObject {
    private String workTypeId;
    private String workType;
    private String workTime;
    private String workTimeOther;
    private String workSallary;
    private String workArea;
    private String workDay;
    private String workCityId;
    private RealmList<CityRealm> CityRealms = new RealmList<>();

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getWorkTimeOther() {
        return workTimeOther;
    }

    public void setWorkTimeOther(String workTimeOther) {
        this.workTimeOther = workTimeOther;
    }

    public String getWorkSallary() {
        return workSallary;
    }

    public void setWorkSallary(String workSallary) {
        this.workSallary = workSallary;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getWorkDay() {
        return workDay;
    }

    public void setWorkDay(String workDay) {
        this.workDay = workDay;
    }

    public String getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(String workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getWorkCityId() {
        return workCityId;
    }

    public void setWorkCityId(String workCityId) {
        this.workCityId = workCityId;
    }

    public RealmList<CityRealm> getCityRealms() {
        return CityRealms;
    }

    public void setCityRealms(RealmList<CityRealm> CityRealms) {
        this.CityRealms = CityRealms;
    }

    
}
