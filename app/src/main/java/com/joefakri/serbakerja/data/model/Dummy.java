package com.joefakri.serbakerja.data.model;

import com.joefakri.serbakerja.R;

import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class Dummy {

    public static ArrayList<CandidateModel> getCandidate(){
        ArrayList<CandidateModel> candidateModels = new ArrayList<>();

        CandidateModel candidateModel = new CandidateModel();
        candidateModel.setName("Siti Nurhamidah");
        candidateModel.setAge("35 tahun");
        candidateModel.setJob("Asisten Rumah tangga");
        candidateModel.setSalary("Rp. 2.000.000/Bulan");
        candidateModels.add(candidateModel);

        candidateModel = new CandidateModel();
        candidateModel.setName("Budi gunawan");
        candidateModel.setAge("28 tahun");
        candidateModel.setJob("Penjaga keamanan");
        candidateModel.setSalary("Rp. 4.000.000/Bulan");
        candidateModels.add(candidateModel);

        candidateModel = new CandidateModel();
        candidateModel.setName("Budi gunawan");
        candidateModel.setAge("28 tahun");
        candidateModel.setJob("Penjaga keamanan");
        candidateModel.setSalary("Rp. 4.000.000/Bulan");
        candidateModels.add(candidateModel);

        candidateModel = new CandidateModel();
        candidateModel.setName("Budi gunawan");
        candidateModel.setAge("28 tahun");
        candidateModel.setJob("Penjaga keamanan");
        candidateModel.setSalary("Rp. 4.000.000/Bulan");
        candidateModels.add(candidateModel);

        candidateModel = new CandidateModel();
        candidateModel.setName("Budi gunawan");
        candidateModel.setAge("28 tahun");
        candidateModel.setJob("Penjaga keamanan");
        candidateModel.setSalary("Rp. 4.000.000/Bulan");
        candidateModels.add(candidateModel);

        return candidateModels;
    }

    public static ArrayList<String> job(){
        ArrayList<String> job = new ArrayList<>();
        job.add("Asisten Rumah tangga");
        job.add("Penjaga keamanan");
        return job;
    }

    public static ArrayList<String> location(){
        ArrayList<String> location = new ArrayList<>();
        location.add("Custom Location");
        location.add("Bandung");
        location.add("Jakarta");
        return location;
    }

    public static ArrayList<ChatModel> getListMessage(){
        ArrayList<ChatModel> chatModels = new ArrayList<>();

        ChatModel chatModel = new ChatModel();
        chatModel.setCompany_name("PT Test");
        chatModel.setLocation("Pluit, jakarta utara");
        chatModel.setSallary("Rp. 3.000.000");
        chatModel.setName("Budi gunawan");
        chatModel.setMessage("This new message");
        chatModel.setJob("Asisten Rumah Tangga");
        chatModel.setAge("12 tahun");
        chatModel.setTime("13.00");
        chatModel.setUnread("12");
        chatModels.add(chatModel);

        chatModel = new ChatModel();
        chatModel.setCompany_name("PT Test");
        chatModel.setLocation("Pluit, jakarta utara");
        chatModel.setSallary("Rp. 3.000.000");
        chatModel.setName("Siti Nurhamidah");
        chatModel.setMessage("This new message");
        chatModel.setJob("Asisten Rumah Tangga");
        chatModel.setAge("12 tahun");
        chatModel.setTime("09.00");
        chatModel.setUnread("2");
        chatModels.add(chatModel);

        return chatModels;
    }

    public static ArrayList<ChatModel> getDetailMessage(){
        ArrayList<ChatModel> chatModels = new ArrayList<>();

        ChatModel chatModel = new ChatModel();
        chatModel.setId("1");
        chatModel.setName("Budi gunawan");
        chatModel.setMessage("This new message");
        chatModel.setTime_stamp("13.00");
        chatModels.add(chatModel);

        chatModel = new ChatModel();
        chatModel.setId("2");
        chatModel.setName("Siti Nurhamidah");
        chatModel.setMessage("This new message");
        chatModel.setTime_stamp("13.03");
        chatModels.add(chatModel);

        chatModel = new ChatModel();
        chatModel.setId("2");
        chatModel.setName("Siti Nurhamidah");
        chatModel.setMessage("This new message");
        chatModel.setTime_stamp("13.04");
        chatModels.add(chatModel);

        chatModel = new ChatModel();
        chatModel.setId("1");
        chatModel.setName("Budi gunawan");
        chatModel.setMessage("This new message");
        chatModel.setTime_stamp("13.06");
        chatModels.add(chatModel);

        chatModel = new ChatModel();
        chatModel.setId("2");
        chatModel.setName("Siti Nurhamidah");
        chatModel.setMessage("This new message");
        chatModel.setTime_stamp("13.07");
        chatModels.add(chatModel);

        return chatModels;
    }

    public static CandidateModel candidateModel(){
        CandidateModel candidateModel = new CandidateModel();
        candidateModel.setName("Siti Nurhamidah");
        candidateModel.setAge("35 tahun");
        candidateModel.setGender("Perempuan");
        candidateModel.setBirthday("Bandung, 19 sep 1992");
        candidateModel.setBirthplace("Bandung");
        candidateModel.setAddress("Jln Melati, no. 25");
        candidateModel.setRtrw("02 / 04");
        candidateModel.setVillage("Kampung boro");
        candidateModel.setDistrics("Boro - boro");
        candidateModel.setReligion("Islam");
        candidateModel.setPhone("0853154367890");
        candidateModel.setJob("Asisten rumah tangga");
        candidateModel.setSalary("Rp. 3.000.000");
        candidateModel.setSalary("Solo");
        candidateModel.setSalary("Kemanggi");
        candidateModel.setDay_work("Senin - sabtu");
        candidateModel.setTime_work("Penuh waktu");
        candidateModel.setWork_status("Belum pernah bekerja");
        candidateModel.setWork_last_sallary("< 1 jt");
        candidateModel.setEducation_graduated("2007");
        candidateModel.setEducation_last("S1");
        candidateModel.setEducation_name_last_school("Sekolah manajemen");
        candidateModel.setEducation_name_courses("Primagama");
        candidateModel.setPhoto_ktp("https://crazygays.files.wordpress.com/2014/11/e-ktp-online.png");
        candidateModel.setPhoto_sim("https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/SIM_A.jpg/300px-SIM_A.jpg");
        candidateModel.setPhoto_ijazah("https://rahmadridwanmanajemen.files.wordpress.com/2015/04/ijazah.jpg");
        candidateModel.setPhoto_person("https://www.jawapos.com/uploads/news/2017/06/24/tak-mudik-presiden-jokowi-lebaran-di-jakarta_m_140106.jpeg");
        return candidateModel;
    }

    public static ProfileModel profileModel(){
        ProfileModel profileModel = new ProfileModel();
        profileModel.setCompany_name("PT. Tridaya");
        profileModel.setCompany_address("Jl. Bukit dago selatan, no. 25A");
        profileModel.setCompany_city("Bandung");
        profileModel.setCompany_total_employeer("30 Karyawan");
        profileModel.setCompany_website("www.test.com");
        profileModel.setCompany_owner_name("Brian");
        profileModel.setCompany_owner_birthday("Jakarta, 17 agustus 1995");
        profileModel.setCompany_rtrw("3 / 5");
        return profileModel;
    }

    public static ArrayList<WorkModel> workModels(){
        ArrayList<WorkModel> workModels = new ArrayList<>();
        WorkModel workModel = new WorkModel();
        workModel.setWork("Asisten rumah tangga");
        workModel.setSalary("1jt - 2jt");
        workModel.setCity("Bandung");
        workModel.setArea("Dago");
        workModel.setGender("Laki & Laki");
        workModel.setAddress("Jlan sentosa");
        workModel.setRequirement("Belum menikah");
        workModel.setWork_time("Penuh waktu (fulltime)");
        workModel.setWork_day("Senin - sabtu");
        workModel.setWork_minimal_experience("1 - 3 tahun");
        workModel.setWork_education("S1");
        workModel.setWork_scope("Wanita");
        workModel.setCompany_name("PT Indah");
        workModels.add(workModel);

        workModel = new WorkModel();
        workModel.setWork("Security");
        workModel.setSalary("1jt - 2jt");
        workModel.setCity("Bandung");
        workModel.setArea("Dago");
        workModel.setGender("Perempuan");
        workModel.setAddress("Jlan sentosa");
        workModel.setRequirement("Belum menikah");
        workModel.setWork_time("Penuh waktu (fulltime)");
        workModel.setWork_day("Senin - sabtu");
        workModel.setWork_minimal_experience("1 - 3 tahun");
        workModel.setWork_education("S1");
        workModel.setWork_scope("Laki-laki");
        workModel.setCompany_name("PT Indah");
        workModels.add(workModel);

        workModel = new WorkModel();
        workModel.setWork("Asisten rumah tangga");
        workModel.setSalary("1jt - 2jt");
        workModel.setCity("Bandung");
        workModel.setArea("Dago");
        workModel.setGender("Laki & Laki");
        workModel.setAddress("Jlan sentosa");
        workModel.setRequirement("Belum menikah");
        workModel.setWork_time("Penuh waktu (fulltime)");
        workModel.setWork_day("Senin - sabtu");
        workModel.setWork_minimal_experience("1 - 3 tahun");
        workModel.setWork_education("S1");
        workModel.setWork_scope("Wanita");
        workModel.setCompany_name("PT Indah");
        workModels.add(workModel);

        workModel = new WorkModel();
        workModel.setWork("Security");
        workModel.setSalary("1jt - 2jt");
        workModel.setCity("Bandung");
        workModel.setArea("Dago");
        workModel.setGender("Laki & Laki");
        workModel.setAddress("Jlan sentosa");
        workModel.setRequirement("Belum menikah");
        workModel.setWork_time("Penuh waktu (fulltime)");
        workModel.setWork_day("Senin - sabtu");
        workModel.setWork_minimal_experience("1 - 3 tahun");
        workModel.setWork_education("S1");
        workModel.setWork_scope("Laki-laki");
        workModel.setCompany_name("PT Indah");
        workModels.add(workModel);

        return workModels;
    }

    public static ArrayList<StatusModel> getStatus(){
        ArrayList<StatusModel> statusModels = new ArrayList<>();
        StatusModel statusModel = new StatusModel();
        statusModel.setName("Jokowi");
        statusModel.setAge("55 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("5 tahun");
        statusModel.setLast_education("SMA");
        statusModel.setJob("Asisten rumah tangga");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Menunggu jawaban");
        statusModel.setImage("http://assets.kompas.com/data/photo/2016/05/11/1239136IMG-20160511-WA006780x390.jpg");
        statusModels.add(statusModel);

        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Menunggu jawaban");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);

        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);

        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);


        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);


        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);


        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);


        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);

        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);

        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);

        statusModel = new StatusModel();
        statusModel.setName("Prabowo");
        statusModel.setAge("51 tahun");
        statusModel.setWork_time("Penuh waktu");
        statusModel.setWork_experience("1 tahun");
        statusModel.setLast_education("S1");
        statusModel.setJob("Security");
        statusModel.setAddress("Bandung");
        statusModel.setCompany("PT Indah jaya");
        statusModel.setSallary("Rp. 3.000.000");
        statusModel.setStatus("Ditolak");
        statusModel.setImage("https://pbs.twimg.com/profile_images/789441486211330048/_G_v5D5H.jpg");
        statusModels.add(statusModel);

        return statusModels;
    }

    public static ArrayList<HistoryModel> getHistory(){
        ArrayList<HistoryModel> historyModels = new ArrayList<>();

        HistoryModel historyModel = new HistoryModel();
        historyModel.setCategory("Pekerjaan");
        historyModel.setMessage("Anda sudah memasang pekerjaan \"Asisten Rumah tangga\"");
        historyModel.setDate("20 Juni 2017");
        historyModel.setImg(R.drawable.ic_work);
        historyModels.add(historyModel);

        historyModel = new HistoryModel();
        historyModel.setCategory("Pekerjaan");
        historyModel.setMessage("Anda sudah memasang pekerjaan \"Asisten Rumah tangga\"");
        historyModel.setDate("20 Juni 2017");
        historyModel.setImg(R.drawable.ic_work);
        historyModels.add(historyModel);

        historyModel = new HistoryModel();
        historyModel.setCategory("Pesan");
        historyModel.setMessage("Chatting anda dengan \"Siti Nurhamidah (30 tahun)\"");
        historyModel.setDate("20 Juni 2017");
        historyModel.setImg(R.drawable.ic_chat_vector);
        historyModels.add(historyModel);

        historyModel = new HistoryModel();
        historyModel.setCategory("Pekerjaan");
        historyModel.setMessage("Anda sudah memasang pekerjaan \"Asisten Rumah tangga\"");
        historyModel.setDate("20 Juni 2017");
        historyModel.setImg(R.drawable.ic_work);
        historyModels.add(historyModel);

        historyModel = new HistoryModel();
        historyModel.setCategory("Pesan");
        historyModel.setMessage("Chatting anda dengan \"Siti Nurhamidah (30 tahun)\"");
        historyModel.setDate("20 Juni 2017");
        historyModel.setImg(R.drawable.ic_chat_vector);
        historyModels.add(historyModel);

        historyModel = new HistoryModel();
        historyModel.setCategory("Pesan");
        historyModel.setMessage("Chatting anda dengan \"Siti Nurhamidah (30 tahun)\"");
        historyModel.setDate("20 Juni 2017");
        historyModel.setImg(R.drawable.ic_chat_vector);
        historyModels.add(historyModel);

        return historyModels;
    }



}
