
package com.joefakri.serbakerja.data;


import com.joefakri.serbakerja.connection.Helper;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.prefs.PreferencesHelper;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by janisharali on 27/01/17.
 */

public interface DataManager extends PreferencesHelper, Helper {

    void updateApiHeader(String userId, String accessToken);

    void setUserAsLoggedOut();

    void setPreferenceNull();

    void updateUserInfo(
            String accessToken,
            String userId,
            String username,
            String phone,
            String email,
            String password,
            String profilePicPath);

    void updateAdditionalUserInfo(String tipe_user,
                                  String status, ArrayList<UserResponse.Login.Data.Kota> city,
                                  ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> job);

    void updateRecruter(
            String status1,
            String status2,
            String status3);

    void updateCandidate(
            String status1,
            String status2,
            String status3,
            String status4,
            String status5);

    void loginMode(LoggedInMode loggedInMode);

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_REGISTER_SERVER(4);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

}
