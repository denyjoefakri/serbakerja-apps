package com.joefakri.serbakerja.data.model;

/**
 * Created by deny on bandung.
 * on 3/24/18 at 3:52 AM
 */

public class HeaderModel {

    private String job;
    private String city;
    private String custom_location;
    private String total;
    private String query;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCustom_location() {
        return custom_location;
    }

    public void setCustom_location(String custom_location) {
        this.custom_location = custom_location;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
