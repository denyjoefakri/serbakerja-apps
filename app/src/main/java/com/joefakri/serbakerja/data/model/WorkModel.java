package com.joefakri.serbakerja.data.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class WorkModel implements Serializable{

    private String id;
    private String work_experience;
    private String work_as;
    private String work_long;
    private String company_name;
    private String period;
    private String work;
    private String workId;
    private String salary;
    private String salarymin;
    private String salarymax;
    private String city;
    private String city_id;
    private String area;
    private String gender;
    private String address;
    private String requirement;
    private String work_time;
    private String work_time_position;
    private String work_time_other;
    private String work_day;
    private String work_minimal_experience;
    private String work_education;
    private String work_scope;
    private ArrayList<City> citys = new ArrayList<>();
    private boolean data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWork_experience() {
        return work_experience;
    }

    public void setWork_experience(String work_experience) {
        this.work_experience = work_experience;
    }

    public String getWork_as() {
        return work_as;
    }

    public void setWork_as(String work_as) {
        this.work_as = work_as;
    }

    public String getWork_long() {
        return work_long;
    }

    public void setWork_long(String work_long) {
        this.work_long = work_long;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getSalarymin() {
        return salarymin;
    }

    public void setSalarymin(String salarymin) {
        this.salarymin = salarymin;
    }

    public String getSalarymax() {
        return salarymax;
    }

    public void setSalarymax(String salarymax) {
        this.salarymax = salarymax;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getWork_time() {
        return work_time;
    }

    public void setWork_time(String work_time) {
        this.work_time = work_time;
    }

    public String getWork_time_position() {
        return work_time_position;
    }

    public void setWork_time_position(String work_time_position) {
        this.work_time_position = work_time_position;
    }

    public String getWork_time_other() {
        return work_time_other;
    }

    public void setWork_time_other(String work_time_other) {
        this.work_time_other = work_time_other;
    }

    public String getWork_day() {
        return work_day;
    }

    public void setWork_day(String work_day) {
        this.work_day = work_day;
    }

    public String getWork_minimal_experience() {
        return work_minimal_experience;
    }

    public void setWork_minimal_experience(String work_minimal_experience) {
        this.work_minimal_experience = work_minimal_experience;
    }

    public String getWork_education() {
        return work_education;
    }

    public void setWork_education(String work_education) {
        this.work_education = work_education;
    }

    public String getWork_scope() {
        return work_scope;
    }

    public void setWork_scope(String work_scope) {
        this.work_scope = work_scope;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public ArrayList<City> getCitys() {
        return citys;
    }

    public void setCitys(ArrayList<City> citys) {
        this.citys = citys;
    }

    public class City implements Serializable{
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }
}
