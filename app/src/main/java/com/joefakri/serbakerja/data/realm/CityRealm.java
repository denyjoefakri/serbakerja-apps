package com.joefakri.serbakerja.data.realm;

import io.realm.RealmObject;

/**
 * Created by deny on bandung.
 */

public class CityRealm extends RealmObject{

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
