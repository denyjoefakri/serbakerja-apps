package com.joefakri.serbakerja.data.model;

import java.io.Serializable;

/**
 * Created by deny on bandung.
 */

public class ReceiverMessageModel implements Serializable {
    private String percakapan_id;
    private String from_id;
    private String to_id;
    private String message;
    private String date;
    private String status_message;
    private String name;
    private String foto;

    public String getPercakapan_id() {
        return percakapan_id;
    }

    public void setPercakapan_id(String percakapan_id) {
        this.percakapan_id = percakapan_id;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
