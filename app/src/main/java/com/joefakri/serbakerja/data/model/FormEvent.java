package com.joefakri.serbakerja.data.model;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class FormEvent {
    private String company_name = "";
    private String company_address = "";
    private String company_city = "";
    private String company_city_id = "";
    private String company_area = "";
    private String company_employee = "";
    private String company_website = "";
    private File company_logo;
    private String work = "";
    private String work_id = "";
    private String sallary = "";
    private String city = "";
    private String city_id = "";
    private String area = "";
    private String work_address = "";
    private String day = "";
    private String time = "";
    private String time_other = "";
    private String gender = "";
    private String experience = "";
    private String education = "";
    private String requirements = "";
    private String expired_date = "";
    private String scope = "";
    private String name = "";
    private String birtplace = "";
    private String birtdate = "";
    private String address = "";
    private String rtrw = "";
    private String village = "";
    private String district = "";
    private String religion = "";
    private String phone = "";
    private String status = "";
    private String statusOptional = "";
    private String last_sallary = "";
    private int last_sallaryPosition = 0;
    private String education_last = "";
    private String education_graduated = "";
    private String education_name_last_school = "";
    private String education_name_courses = "";
    private ArrayList<WorkModel> workModels = new ArrayList<>();
    private ArrayList<WorkModel> workExperience = new ArrayList<>();
    private File ktp;
    private File sim;
    private File ijazah;
    private File user;

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCompany_city() {
        return company_city;
    }

    public void setCompany_city(String company_city) {
        this.company_city = company_city;
    }

    public String getCompany_city_id() {
        return company_city_id;
    }

    public void setCompany_city_id(String company_city_id) {
        this.company_city_id = company_city_id;
    }

    public String getCompany_area() {
        return company_area;
    }

    public void setCompany_area(String company_area) {
        this.company_area = company_area;
    }

    public String getCompany_employee() {
        return company_employee;
    }

    public void setCompany_employee(String company_employee) {
        this.company_employee = company_employee;
    }

    public String getCompany_website() {
        return company_website;
    }

    public void setCompany_website(String company_website) {
        this.company_website = company_website;
    }

    public File getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(File company_logo) {
        this.company_logo = company_logo;
    }

    public String getWork_id() {
        return work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String job) {
        this.work = job;
    }

    public String getSallary() {
        return sallary;
    }

    public void setSallary(String sallary) {
        this.sallary = sallary;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getWork_address() {
        return work_address;
    }

    public void setWork_address(String work_address) {
        this.work_address = work_address;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime_other() {
        return time_other;
    }

    public void setTime_other(String time_other) {
        this.time_other = time_other;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirtplace() {
        return birtplace;
    }

    public void setBirtplace(String birtplace) {
        this.birtplace = birtplace;
    }

    public String getBirtdate() {
        return birtdate;
    }

    public void setBirtdate(String birtdate) {
        this.birtdate = birtdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRtrw() {
        return rtrw;
    }

    public void setRtrw(String rtrw) {
        this.rtrw = rtrw;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusOptional() {
        return statusOptional;
    }

    public void setStatusOptional(String statusOptional) {
        this.statusOptional = statusOptional;
    }

    public String getLast_sallary() {
        return last_sallary;
    }

    public void setLast_sallary(String last_sallary) {
        this.last_sallary = last_sallary;
    }

    public int getLast_sallaryPosition() {
        return last_sallaryPosition;
    }

    public void setLast_sallaryPosition(int last_sallaryPosition) {
        this.last_sallaryPosition = last_sallaryPosition;
    }

    public File getKtp() {
        return ktp;
    }

    public void setKtp(File ktp) {
        this.ktp = ktp;
    }

    public File getUser() {
        return user;
    }

    public void setUser(File user) {
        this.user = user;
    }

    public ArrayList<WorkModel> getWorkModels() {
        return workModels;
    }

    public void setWorkModels(ArrayList<WorkModel> workModels) {
        this.workModels = workModels;
    }

    public ArrayList<WorkModel> getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(ArrayList<WorkModel> workExperience) {
        this.workExperience = workExperience;
    }

    public String getEducation_last() {
        return education_last;
    }

    public void setEducation_last(String education_last) {
        this.education_last = education_last;
    }

    public String getEducation_graduated() {
        return education_graduated;
    }

    public void setEducation_graduated(String education_graduated) {
        this.education_graduated = education_graduated;
    }

    public String getEducation_name_last_school() {
        return education_name_last_school;
    }

    public void setEducation_name_last_school(String education_name_last_school) {
        this.education_name_last_school = education_name_last_school;
    }

    public String getEducation_name_courses() {
        return education_name_courses;
    }

    public void setEducation_name_courses(String education_name_courses) {
        this.education_name_courses = education_name_courses;
    }

    public File getSim() {
        return sim;
    }

    public void setSim(File sim) {
        this.sim = sim;
    }

    public File getIjazah() {
        return ijazah;
    }

    public void setIjazah(File ijazah) {
        this.ijazah = ijazah;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }
}
