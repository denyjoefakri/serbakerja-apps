package com.joefakri.serbakerja.data.prefs;

import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.AppDataManager;

import java.util.ArrayList;

/**
 * Created by janisharali on 27/01/17.
 */

public interface PreferencesHelper {

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(AppDataManager.LoggedInMode mode);

    String getCurrentUserId();

    void setCurrentUserId(String userId);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserPhone();

    void setCurrentUserPhone(String phone);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    String getCurrentUserPassword();

    void setCurrentUserPassword(String password);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getTypeUser();

    void setTypeUser(String typeUser);

    String getUserStatus();

    void setUserStatus(String userStatus);

    String getFcmId();

    void setFcmId(String fcmId);

    ArrayList<UserResponse.Login.Data.Kota> getDefaultCity();

    void setDefaultCity(ArrayList<UserResponse.Login.Data.Kota> city_id);

    ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> getDefaultJob();

    void setDefaultJob(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> job_id);

    String getDateTemporary();

    void setDateTemporary(String fcmId);

    String getRecruter1();

    void setRecruter1(String status);

    String getRecruter2();

    void setRecruter2(String status);

    String getRecruter3();

    void setRecruter3(String status);

    String getCandidate1();

    void setCandidate1(String status);

    String getCandidate2();

    void setCandidate2(String status);

    String getCandidate3();

    void setCandidate3(String status);

    String getCandidate4();

    void setCandidate4(String status);

    String getCandidate5();

    void setCandidate5(String status);

}
