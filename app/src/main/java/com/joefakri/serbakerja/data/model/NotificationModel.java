package com.joefakri.serbakerja.data.model;

import java.io.Serializable;

/**
 * Created by deny on bandung.
 */

public class NotificationModel implements Serializable {

    private String status;
    private String from_id;
    private String id;
    private String role;
    private String tipe;
    private String pesan;
    private String pengguna_id;
    private String from_pengguna;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getPengguna_id() {
        return pengguna_id;
    }

    public void setPengguna_id(String pengguna_id) {
        this.pengguna_id = pengguna_id;
    }

    public String getFrom_pengguna() {
        return from_pengguna;
    }

    public void setFrom_pengguna(String from_pengguna) {
        this.from_pengguna = from_pengguna;
    }
}
