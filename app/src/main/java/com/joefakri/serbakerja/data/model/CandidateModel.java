package com.joefakri.serbakerja.data.model;

import java.io.Serializable;

/**
 * Created by deny on bandung.
 */

public class CandidateModel implements Serializable {

    private String name;
    private String age;
    private String gender;
    private String birthplace;
    private String birthday;
    private String address;
    private String rtrw;
    private String village;
    private String districs;
    private String religion;
    private String phone;
    private String job;
    private String salary;
    private String city;
    private String area;
    private String time_work;
    private String day_work;
    private String work_status;
    private String work_last_sallary;
    private String education_graduated;
    private String education_last;
    private String education_name_last_school;
    private String education_name_courses;
    private String photo_ktp;
    private String photo_sim;
    private String photo_ijazah;
    private String photo_person;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTime_work() {
        return time_work;
    }

    public void setTime_work(String time_work) {
        this.time_work = time_work;
    }

    public String getDay_work() {
        return day_work;
    }

    public void setDay_work(String day_work) {
        this.day_work = day_work;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRtrw() {
        return rtrw;
    }

    public void setRtrw(String rtrw) {
        this.rtrw = rtrw;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrics() {
        return districs;
    }

    public void setDistrics(String districs) {
        this.districs = districs;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getWork_status() {
        return work_status;
    }

    public void setWork_status(String work_status) {
        this.work_status = work_status;
    }

    public String getWork_last_sallary() {
        return work_last_sallary;
    }

    public void setWork_last_sallary(String work_last_sallary) {
        this.work_last_sallary = work_last_sallary;
    }

    public String getEducation_graduated() {
        return education_graduated;
    }

    public void setEducation_graduated(String education_graduated) {
        this.education_graduated = education_graduated;
    }

    public String getEducation_last() {
        return education_last;
    }

    public void setEducation_last(String education_last) {
        this.education_last = education_last;
    }

    public String getEducation_name_last_school() {
        return education_name_last_school;
    }

    public void setEducation_name_last_school(String education_name_last_school) {
        this.education_name_last_school = education_name_last_school;
    }

    public String getEducation_name_courses() {
        return education_name_courses;
    }

    public void setEducation_name_courses(String education_name_courses) {
        this.education_name_courses = education_name_courses;
    }

    public String getPhoto_ktp() {
        return photo_ktp;
    }

    public void setPhoto_ktp(String photo_ktp) {
        this.photo_ktp = photo_ktp;
    }

    public String getPhoto_sim() {
        return photo_sim;
    }

    public void setPhoto_sim(String photo_sim) {
        this.photo_sim = photo_sim;
    }

    public String getPhoto_ijazah() {
        return photo_ijazah;
    }

    public void setPhoto_ijazah(String photo_ijazah) {
        this.photo_ijazah = photo_ijazah;
    }

    public String getPhoto_person() {
        return photo_person;
    }

    public void setPhoto_person(String photo_person) {
        this.photo_person = photo_person;
    }
}
