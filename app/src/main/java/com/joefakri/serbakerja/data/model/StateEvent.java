/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.joefakri.serbakerja.data.model;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public class StateEvent {

    private String Basestate = "";
    private String state;
    private String state_form;
    private String status = "";
    private int value = 0;
    private int total = 0;
    private int type = 0;

    public StateEvent(String state, String state_form) {
        this.state = state;
        this.state_form = state_form;
    }


    public StateEvent() {
    }


    public String getBasestate() {
        return Basestate;
    }

    public void setBasestate(String basestate) {
        Basestate = basestate;
    }

    public StateEvent(String status) {
        this.status = status;
    }

    public StateEvent(int type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState_form() {
        return state_form;
    }

    public void setState_form(String state_form) {
        this.state_form = state_form;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
