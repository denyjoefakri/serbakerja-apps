package com.joefakri.serbakerja.data.realm;

import com.joefakri.serbakerja.utils.CommonUtils;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by deny on bandung.
 */

public class CandidateRealm extends RealmObject {

    private int id;
    private int percent1 = 0;
    private int percent2 = 0;
    private int percent3 = 0;
    private int percent4 = 0;
    private int percent5 = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPercent1() {
        return CommonUtils.percent(9, percent1);
    }

    public void setPercent1(int percent1) {
        this.percent1 = percent1;
    }

    public int getPercent2() {
        return CommonUtils.percent(1, percent2);
    }

    public void setPercent2(int percent2) {
        this.percent2 = percent2;
    }

    public int getPercent3() {
        return CommonUtils.percent(3, percent3);
    }

    public void setPercent3(int percent3) {
        this.percent3 = percent3;
    }

    public int getPercent4() {
        return CommonUtils.percent(4, percent4);
    }

    public void setPercent4(int percent4) {
        this.percent4 = percent4;
    }

    public int getPercent5() {
        return CommonUtils.percent(3, percent5);
    }

    public void setPercent5(int percent5) {
        this.percent5 = percent5;
    }

    private String personName;
    private String personBirthday;
    private String personBirthplace;
    private String personAddress;
    private String personDistrics;
    private String personVillage;
    private String personRtrw;
    private String personGender;
    private String personReligion;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonBirthday() {
        return personBirthday;
    }

    public void setPersonBirthday(String personBirthday) {
        this.personBirthday = personBirthday;
    }

    public String getPersonBirthplace() {
        return personBirthplace;
    }

    public void setPersonBirthplace(String personBirthplace) {
        this.personBirthplace = personBirthplace;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonDistrics() {
        return personDistrics;
    }

    public void setPersonDistrics(String personDistrics) {
        this.personDistrics = personDistrics;
    }

    public String getPersonVillage() {
        return personVillage;
    }

    public void setPersonVillage(String personVillage) {
        this.personVillage = personVillage;
    }

    public String getPersonRtrw() {
        return personRtrw;
    }

    public void setPersonRtrw(String personRtrw) {
        this.personRtrw = personRtrw;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }

    public String getPersonReligion() {
        return personReligion;
    }

    public void setPersonReligion(String personReligion) {
        this.personReligion = personReligion;
    }

    private RealmList<WorkRealm> WorkRealms = new RealmList<>();

    public RealmList<WorkRealm> getWorkRealms() {
        return WorkRealms;
    }

    public void setWorkRealms(RealmList<WorkRealm> WorkRealms) {
        this.WorkRealms = WorkRealms;
    }
    

    /**
     * Experience
     * */

    private String workStatus;
    private String workStatusOptional;
    private String workLastSallary;
    private RealmList<ExperienceRealm> experienceRealms = new RealmList<>();

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getWorkStatusOptional() {
        return workStatusOptional;
    }

    public void setWorkStatusOptional(String workStatusOptional) {
        this.workStatusOptional = workStatusOptional;
    }

    public String getWorkLastSallary() {
        return workLastSallary;
    }

    public void setWorkLastSallary(String workLastSallary) {
        this.workLastSallary = workLastSallary;
    }

    public RealmList<ExperienceRealm> getExperienceRealms() {
        return experienceRealms;
    }

    public void setExperienceRealms(RealmList<ExperienceRealm> experienceRealms) {
        this.experienceRealms = experienceRealms;
    }

    /**
     * Education
     * */

    private String educationLast;
    private String educationGraduated;
    private String educationNameLastSchool;
    private String educationNameCourse;

    public String getEducationLast() {
        return educationLast;
    }

    public void setEducationLast(String educationLast) {
        this.educationLast = educationLast;
    }

    public String getEducationGraduated() {
        return educationGraduated;
    }

    public void setEducationGraduated(String educationGraduated) {
        this.educationGraduated = educationGraduated;
    }

    public String getEducationNameLastSchool() {
        return educationNameLastSchool;
    }

    public void setEducationNameLastSchool(String educationNameLastSchool) {
        this.educationNameLastSchool = educationNameLastSchool;
    }

    public String getEducationNameCourse() {
        return educationNameCourse;
    }

    public void setEducationNameCourse(String educationNameCourse) {
        this.educationNameCourse = educationNameCourse;
    }


    /**
     * Document
     * */

    private String DocPhotoKTP;
    private String DocPhotoIjazah;
    private String DocPhoto;

    public String getDocPhotoKTP() {
        return DocPhotoKTP;
    }

    public void setDocPhotoKTP(String docPhotoKTP) {
        DocPhotoKTP = docPhotoKTP;
    }

    public String getDocPhotoIjazah() {
        return DocPhotoIjazah;
    }

    public void setDocPhotoIjazah(String docPhotoIjazah) {
        DocPhotoIjazah = docPhotoIjazah;
    }

    public String getDocPhoto() {
        return DocPhoto;
    }

    public void setDocPhoto(String docPhoto) {
        DocPhoto = docPhoto;
    }
}
