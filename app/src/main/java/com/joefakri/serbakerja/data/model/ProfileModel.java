package com.joefakri.serbakerja.data.model;

/**
 * Created by deny on bandung.
 */

public class ProfileModel {

    private String company_name;
    private String company_address;
    private String company_city;
    private String company_total_employeer;
    private String company_website;
    private String company_owner_name;
    private String company_owner_birthday;
    private String company_rtrw;

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCompany_city() {
        return company_city;
    }

    public void setCompany_city(String company_city) {
        this.company_city = company_city;
    }

    public String getCompany_total_employeer() {
        return company_total_employeer;
    }

    public void setCompany_total_employeer(String company_total_employeer) {
        this.company_total_employeer = company_total_employeer;
    }

    public String getCompany_website() {
        return company_website;
    }

    public void setCompany_website(String company_website) {
        this.company_website = company_website;
    }

    public String getCompany_owner_name() {
        return company_owner_name;
    }

    public void setCompany_owner_name(String company_owner_name) {
        this.company_owner_name = company_owner_name;
    }

    public String getCompany_owner_birthday() {
        return company_owner_birthday;
    }

    public void setCompany_owner_birthday(String company_owner_birthday) {
        this.company_owner_birthday = company_owner_birthday;
    }

    public String getCompany_rtrw() {
        return company_rtrw;
    }

    public void setCompany_rtrw(String company_rtrw) {
        this.company_rtrw = company_rtrw;
    }
}
