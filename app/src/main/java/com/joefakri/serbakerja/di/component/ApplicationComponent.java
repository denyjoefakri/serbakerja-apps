

package com.joefakri.serbakerja.di.component;

import android.app.Application;
import android.content.Context;

import com.joefakri.serbakerja.MyApp;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.di.ApplicationContext;
import com.joefakri.serbakerja.di.module.ApplicationModule;
import com.joefakri.serbakerja.service.SyncService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyApp app);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}