
package com.joefakri.serbakerja.di.component;


import com.joefakri.serbakerja.di.PerActivity;
import com.joefakri.serbakerja.di.module.ActivityModule;
import com.joefakri.serbakerja.ui.bio.ChooseRoleActivity;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.ui.chat.ChatFragment;
import com.joefakri.serbakerja.ui.city.DataCityActivity;
import com.joefakri.serbakerja.ui.form.input_work.InputWorkActivity;
import com.joefakri.serbakerja.ui.form.person.FormPersonAddressFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkAreaAddressFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkCandidateFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkDayTimeFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkExpiredDateFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkTypeSallaryFragment;
import com.joefakri.serbakerja.ui.main.MainActivity;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyAddressFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyCityFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyLogoFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyNameFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyTotalEmployeeFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyWebsiteFragment;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoIjazahFragment;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoKTPFragment;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoPersonFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationGraduatedFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationLastFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationNameCoursesFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationNameLastSchoolFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.form.person.FormPersonBirthdayFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonDistrictsFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonGenderFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonNameFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonPhotoFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonPhotoKTPFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonReligionFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonRtrwFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonVillageFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkAreaFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkCustomRequirement;
import com.joefakri.serbakerja.ui.form.work.FormWorkDayFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkEducationFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkExperienceFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkGenderFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkLastSallaryFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkMinimalExperienceFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkSallaryFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkScopeFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkStatusFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkTimeFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkTypeFragment;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardFragment2;
import com.joefakri.serbakerja.ui.notification.NotificationActivity;
import com.joefakri.serbakerja.ui.notification.detail.NotificationDetailCSActivity;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainActivity;
import com.joefakri.serbakerja.ui.profile.candidate.document.CandidateDocumentFragment;
import com.joefakri.serbakerja.ui.profile.candidate.document_edit.EditDocumentActivity;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationFragment;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationActivity;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceFragment;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceActivity;
import com.joefakri.serbakerja.ui.profile.candidate.profile.CandidateProfileFragment;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkFragment;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkActivity;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfileFragment;
import com.joefakri.serbakerja.ui.profile.recruiter.profile.ProfileFragment;
import com.joefakri.serbakerja.ui.profile.recruiter.profile_edit.EditProfileActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostFragment;
import com.joefakri.serbakerja.ui.profile.candidate.profile_edit.EditCandidateProfileActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileActivity;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainActivity;
import com.joefakri.serbakerja.ui.recruters.DetailRecrutersActivity;
import com.joefakri.serbakerja.ui.register.RegisterActivity;
import com.joefakri.serbakerja.ui.setting.SettingActivity;
import com.joefakri.serbakerja.ui.setting.contactus.ContactUsActivity;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackActivity;
import com.joefakri.serbakerja.ui.setting.manageaccount.ManageAccountActivity;
import com.joefakri.serbakerja.ui.status.candidate.StatusCandidateFragment;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterFragment;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterFragmentApplication;
import com.joefakri.serbakerja.ui.status.recruiter.verify.StatusRecruiterFragmentVerify;
import com.joefakri.serbakerja.ui.verify.VerifyActivity;
import com.joefakri.serbakerja.ui.work.DetailWorkActivity;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;

import dagger.Component;

/**
 * Created by janisharali on 27/01/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {


    void inject(LoginActivity activity);
    void inject(RegisterActivity activity);
    void inject(VerifyActivity activity);
    void inject(FormOptionalActivity activity);
    void inject(MainActivity activity);
    void inject(ChatRoomActivity activity);
    void inject(DetailCandidateActivity activity);
    void inject(RecruiterMainActivity activity);
    void inject(CandidateMainActivity activity);
    void inject(EditCompanyProfileActivity activity);
    void inject(EditCandidateProfileActivity activity);
    void inject(EditPasswordActivity activity);
    void inject(EditWorkPostActivity activity);
    void inject(NotificationActivity activity);
    void inject(DetailWorkActivity activity);
    void inject(InputWorkActivity activity);
    void inject(DataWorkActivity activity);
    void inject(DataCityActivity activity);
    void inject(SettingActivity activity);
    void inject(CompanyHistoryActivity activity);
    void inject(EditProfileActivity activity);
    void inject(EditWorkActivity activity);
    void inject(EditExperienceActivity activity);
    void inject(EditEducationActivity activity);
    void inject(EditDocumentActivity activity);
    void inject(ContactUsActivity activity);
    void inject(FeedbackActivity activity);
    void inject(DetailRecrutersActivity activity);
    void inject(ChooseRoleActivity activity);
    void inject(FormActivity activity);
    void inject(ManageAccountActivity activity);
    void inject(NotificationDetailCSActivity activity);

    void inject(ChatFragment fragment);
    void inject(DashboardFragment fragment);
    void inject(DashboardFragment2 fragment);
    void inject(StatusRecruiterFragment fragment);
    void inject(StatusCandidateFragment fragment);
    void inject(StatusRecruiterFragmentApplication fragment);
    void inject(StatusRecruiterFragmentVerify fragment);
    void inject(CandidateProfileFragment fragment);
    void inject(CompanyProfileFragment fragment);
    void inject(CompanyWorkPostFragment fragment);
    void inject(ProfileFragment fragment);
    void inject(CandidateWorkFragment fragment);
    void inject(CandidateExperienceFragment fragment);
    void inject(CandidateEducationFragment fragment);
    void inject(CandidateDocumentFragment fragment);

    void inject(FormCompanyAddressFragment fragment);
    void inject(FormCompanyCityFragment fragment);
    void inject(FormCompanyLogoFragment fragment);
    void inject(FormCompanyNameFragment fragment);
    void inject(FormCompanyTotalEmployeeFragment fragment);
    void inject(FormCompanyWebsiteFragment fragment);
    void inject(FormDocPhotoIjazahFragment fragment);
    void inject(FormDocPhotoKTPFragment fragment);
    void inject(FormDocPhotoPersonFragment fragment);
    void inject(FormEducationGraduatedFragment fragment);
    void inject(FormEducationLastFragment fragment);
    void inject(FormEducationNameCoursesFragment fragment);
    void inject(FormEducationNameLastSchoolFragment fragment);
    void inject(FormPersonBirthdayFragment fragment);
    void inject(FormPersonDistrictsFragment fragment);
    void inject(FormPersonGenderFragment fragment);
    void inject(FormPersonNameFragment fragment);
    void inject(FormPersonPhotoFragment fragment);
    void inject(FormPersonPhotoKTPFragment fragment);
    void inject(FormPersonReligionFragment fragment);
    void inject(FormPersonRtrwFragment fragment);
    void inject(FormPersonVillageFragment fragment);
    void inject(FormPersonAddressFragment fragment);
    void inject(FormWorkAreaFragment fragment);
    void inject(FormWorkCandidateFragment fragment);
    void inject(FormWorkAreaAddressFragment fragment);
    void inject(FormWorkCustomRequirement fragment);
    void inject(FormWorkDayFragment fragment);
    void inject(FormWorkDayTimeFragment fragment);
    void inject(FormWorkEducationFragment fragment);
    void inject(FormWorkExperienceFragment fragment);
    void inject(FormWorkGenderFragment fragment);
    void inject(FormWorkLastSallaryFragment fragment);
    void inject(FormWorkMinimalExperienceFragment fragment);
    void inject(FormWorkSallaryFragment fragment);
    void inject(FormWorkScopeFragment fragment);
    void inject(FormWorkStatusFragment fragment);
    void inject(FormWorkTimeFragment fragment);
    void inject(FormWorkTypeFragment fragment);
    void inject(FormWorkTypeSallaryFragment fragment);
    void inject(FormWorkExpiredDateFragment fragment);


}
