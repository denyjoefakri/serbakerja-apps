
package com.joefakri.serbakerja.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.joefakri.serbakerja.adapter.ChatMessageAdapter;
import com.joefakri.serbakerja.di.ActivityContext;
import com.joefakri.serbakerja.ui.bio.ChooseRoleMvpPresenter;
import com.joefakri.serbakerja.ui.bio.ChooseRolePresenter;
import com.joefakri.serbakerja.ui.bio.ChooseRoleView;
import com.joefakri.serbakerja.ui.bio.bioform.FormMvpPresenter;
import com.joefakri.serbakerja.ui.bio.bioform.FormPresenter;
import com.joefakri.serbakerja.ui.bio.bioform.FormView;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateMvpPresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidatePresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateView;
import com.joefakri.serbakerja.ui.chat.ChatMvpPresenter;
import com.joefakri.serbakerja.ui.chat.ChatPresenter;
import com.joefakri.serbakerja.ui.chat.ChatView;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomMvpPresenter;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomPresenter;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomView;
import com.joefakri.serbakerja.ui.city.DataCityMvpPresenter;
import com.joefakri.serbakerja.ui.city.DataCityPresenter;
import com.joefakri.serbakerja.ui.city.DataCityView;
import com.joefakri.serbakerja.ui.form.FormOptionalMvpPresenter;
import com.joefakri.serbakerja.ui.form.FormOptionalPresenter;
import com.joefakri.serbakerja.ui.form.FormOptionalView;
import com.joefakri.serbakerja.ui.form.company.FormCompanyMvpPresenter;
import com.joefakri.serbakerja.ui.form.company.FormCompanyPresenter;
import com.joefakri.serbakerja.ui.form.company.FormCompanyView;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoMvpPresenter;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoPresenter;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoView;
import com.joefakri.serbakerja.ui.form.education.FormEducationMvpPresenter;
import com.joefakri.serbakerja.ui.form.education.FormEducationPresenter;
import com.joefakri.serbakerja.ui.form.education.FormEducationView;
import com.joefakri.serbakerja.ui.form.input_work.InputWorkMvpPresenter;
import com.joefakri.serbakerja.ui.form.input_work.InputWorkPresenter;
import com.joefakri.serbakerja.ui.form.input_work.InputWorkView;
import com.joefakri.serbakerja.ui.form.person.FormPersonMvpPresenter;
import com.joefakri.serbakerja.ui.form.person.FormPersonPresenter;
import com.joefakri.serbakerja.ui.form.person.FormPersonView;
import com.joefakri.serbakerja.ui.form.work.FormWorkMvpPresenter;
import com.joefakri.serbakerja.ui.form.work.FormWorkPresenter;
import com.joefakri.serbakerja.ui.form.work.FormWorkView;
import com.joefakri.serbakerja.ui.login.LoginMvpPresenter;
import com.joefakri.serbakerja.ui.login.LoginPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;
import com.joefakri.serbakerja.ui.main.MainMvpPresenter;
import com.joefakri.serbakerja.ui.main.MainPresenter;
import com.joefakri.serbakerja.ui.main.MainView;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardMvpPresenter;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardPresenter;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardView;
import com.joefakri.serbakerja.ui.notification.NotificationMvpPresenter;
import com.joefakri.serbakerja.ui.notification.NotificationPresenter;
import com.joefakri.serbakerja.ui.notification.NotificationView;
import com.joefakri.serbakerja.ui.notification.detail.NotificationDetailMvpPresenter;
import com.joefakri.serbakerja.ui.notification.detail.NotificationDetailPresenter;
import com.joefakri.serbakerja.ui.notification.detail.NotificationDetailView;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainView;
import com.joefakri.serbakerja.ui.profile.candidate.document.CandidateDocumentMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.document.CandidateDocumentPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.document.CandidateDocumentView;
import com.joefakri.serbakerja.ui.profile.candidate.document_edit.EditDocumentMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.document_edit.EditDocumentPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.document_edit.EditDocumentView;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationView;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationView;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperiencePresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceView;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperiencePresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceView;
import com.joefakri.serbakerja.ui.profile.candidate.profile.CandidateProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.profile.CandidateProfilePresenter;
import com.joefakri.serbakerja.ui.profile.candidate.profile.CandidateProfileView;
import com.joefakri.serbakerja.ui.profile.candidate.profile_edit.EditCandidateProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.profile_edit.EditCandidateProfilePresenter;
import com.joefakri.serbakerja.ui.profile.candidate.profile_edit.EditCandidateProfileView;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkView;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkView;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryMvpPresenter;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryPresenter;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryView;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordMvpPresenter;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordPresenter;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordView;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainView;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfilePresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfileView;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfilePresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileView;
import com.joefakri.serbakerja.ui.profile.recruiter.profile.ProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.profile.ProfilePresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.profile.ProfileView;
import com.joefakri.serbakerja.ui.profile.recruiter.profile_edit.EditProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.profile_edit.EditProfilePresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.profile_edit.EditProfileView;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostView;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostView;
import com.joefakri.serbakerja.ui.recruters.DetailRecrutersMvpPresenter;
import com.joefakri.serbakerja.ui.recruters.DetailRecrutersPresenter;
import com.joefakri.serbakerja.ui.recruters.DetailRecrutersView;
import com.joefakri.serbakerja.ui.register.RegisterMvpPresenter;
import com.joefakri.serbakerja.ui.register.RegisterPresenter;
import com.joefakri.serbakerja.ui.register.RegisterView;
import com.joefakri.serbakerja.ui.setting.SettingMvpPresenter;
import com.joefakri.serbakerja.ui.setting.SettingPresenter;
import com.joefakri.serbakerja.ui.setting.SettingView;
import com.joefakri.serbakerja.ui.setting.contactus.ContactUsMvpPresenter;
import com.joefakri.serbakerja.ui.setting.contactus.ContactUsPresenter;
import com.joefakri.serbakerja.ui.setting.contactus.ContactUsView;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackMvpPresenter;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackPresenter;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackView;
import com.joefakri.serbakerja.ui.setting.manageaccount.ManageAccountMvpPresenter;
import com.joefakri.serbakerja.ui.setting.manageaccount.ManageAccountPresenter;
import com.joefakri.serbakerja.ui.setting.manageaccount.ManageAccountView;
import com.joefakri.serbakerja.ui.status.candidate.StatusCandidateMvpPresenter;
import com.joefakri.serbakerja.ui.status.candidate.StatusCandidatePresenter;
import com.joefakri.serbakerja.ui.status.candidate.StatusCandidateView;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterMvpPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterView;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationMvpPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationView;
import com.joefakri.serbakerja.ui.status.recruiter.verify.StatusRecruiterVerifyMvpPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.verify.StatusRecruiterVerifyPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.verify.StatusRecruiterVerifyView;
import com.joefakri.serbakerja.ui.verify.VerifyMvpPresenter;
import com.joefakri.serbakerja.ui.verify.VerifyPresenter;
import com.joefakri.serbakerja.ui.verify.VerifyView;
import com.joefakri.serbakerja.ui.work.DetailWorkMvpPresenter;
import com.joefakri.serbakerja.ui.work.DetailWorkPresenter;
import com.joefakri.serbakerja.ui.work.DetailWorkView;
import com.joefakri.serbakerja.ui.work.data.DataWorkMvpPresenter;
import com.joefakri.serbakerja.ui.work.data.DataWorkPresenter;
import com.joefakri.serbakerja.ui.work.data.DataWorkView;
import com.joefakri.serbakerja.utils.rx.AppSchedulerProvider;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    /** CANDIDATE */

    @Provides
    EditCandidateProfileMvpPresenter<EditCandidateProfileView> provideEditCandidateProfileMvpPresenter(
            EditCandidateProfilePresenter<EditCandidateProfileView> presenter){
        return presenter;
    }

    @Provides
    CandidateProfileMvpPresenter<CandidateProfileView> provideCandidateProfileMvpPresenter(
            CandidateProfilePresenter<CandidateProfileView> presenter){
        return presenter;
    }

    @Provides
    CandidateWorkMvpPresenter<CandidateWorkView> provideCandidateWorkMvpPresenter(
            CandidateWorkPresenter<CandidateWorkView> presenter){
        return presenter;
    }

    @Provides
    EditWorkMvpPresenter<EditWorkView> provideEditWorkMvpPresenter(
            EditWorkPresenter<EditWorkView> presenter){
        return presenter;
    }

    @Provides
    CandidateExperienceMvpPresenter<CandidateExperienceView> provideCandidateExperienceMvpPresenter(
            CandidateExperiencePresenter<CandidateExperienceView> presenter){
        return presenter;
    }

    @Provides
    EditExperienceMvpPresenter<EditExperienceView> provideEditExperienceMvpPresenter(
            EditExperiencePresenter<EditExperienceView> presenter){
        return presenter;
    }

    @Provides
    CandidateEducationMvpPresenter<CandidateEducationView> provideCandidateEducationMvpPresenter(
            CandidateEducationPresenter<CandidateEducationView> presenter){
        return presenter;
    }

    @Provides
    EditEducationMvpPresenter<EditEducationView> provideEditEducationMvpPresenter(
            EditEducationPresenter<EditEducationView> presenter){
        return presenter;
    }

    @Provides
    CandidateDocumentMvpPresenter<CandidateDocumentView> provideCandidateDocumentMvpPresenter(
            CandidateDocumentPresenter<CandidateDocumentView> presenter){
        return presenter;
    }

    @Provides
    EditDocumentMvpPresenter<EditDocumentView> provideEditDocumentMvpPresenter(
            EditDocumentPresenter<EditDocumentView> presenter){
        return presenter;
    }

    @Provides
    CandidateMainMvpPresenter<CandidateMainView> provideCandidateMainMvpPresenter(
            CandidateMainPresenter<CandidateMainView> presenter){
        return presenter;
    }

    /** HISTORY */

    @Provides
    CompanyHistoryMvpPresenter<CompanyHistoryView> provideCompanyHistoryMvpPresenter(
            CompanyHistoryPresenter<CompanyHistoryView> presenter){
        return presenter;
    }

    /** PASSWORD */

    @Provides
    EditPasswordMvpPresenter<EditPasswordView> provideEditPasswordMvpPresenter(
            EditPasswordPresenter<EditPasswordView> presenter){
        return presenter;
    }

    /** RECRUITER */

    @Provides
    CompanyProfileMvpPresenter<CompanyProfileView> provideCompanyProfileMvpPresenter(
            CompanyProfilePresenter<CompanyProfileView> presenter){
        return presenter;
    }

    @Provides
    EditCompanyProfileMvpPresenter<EditCompanyProfileView> provideEditCompanyProfileMvpPresenter(
            EditCompanyProfilePresenter<EditCompanyProfileView> presenter){
        return presenter;
    }

    @Provides
    CompanyWorkPostMvpPresenter<CompanyWorkPostView> provideCompanyWorkPostMvpPresenter(
            CompanyWorkPostPresenter<CompanyWorkPostView> presenter){
        return presenter;
    }

    @Provides
    EditWorkPostMvpPresenter<EditWorkPostView> provideEditWorkPostMvpPresenter(
            EditWorkPostPresenter<EditWorkPostView> presenter){
        return presenter;
    }

    @Provides
    RecruiterMainMvpPresenter<RecruiterMainView> provideRecruiterMainMvpPresenter(
            RecruiterMainPresenter<RecruiterMainView> presenter){
        return presenter;
    }

    /** LOGIN  & REGISTER */

    @Provides
    LoginMvpPresenter<LoginView> provideLoginMvpPresenter(
            LoginPresenter<LoginView> presenter){
        return presenter;
    }

    @Provides
    RegisterMvpPresenter<RegisterView> provideRegisterMvpPresenter(
            RegisterPresenter<RegisterView> presenter){
        return presenter;
    }

    /** STATUS CANDIDATE */

    @Provides
    StatusCandidateMvpPresenter<StatusCandidateView> provideStatusCandidateMvpPresenter(
            StatusCandidatePresenter<StatusCandidateView> presenter){
        return presenter;
    }

    /** STATUS RECRUITER */

    @Provides
    StatusRecruiterMvpPresenter<StatusRecruiterView> provideStatusRecruiterMvpPresenter(
            StatusRecruiterPresenter<StatusRecruiterView> presenter){
        return presenter;
    }

    @Provides
    StatusRecruiterApplicationMvpPresenter<StatusRecruiterApplicationView> provideStatusRecruiterApplicationMvpPresenter(
            StatusRecruiterApplicationPresenter<StatusRecruiterApplicationView> presenter){
        return presenter;
    }

    @Provides
    StatusRecruiterVerifyMvpPresenter<StatusRecruiterVerifyView> provideStatusRecruiterVerifyMvpPresenter(
            StatusRecruiterVerifyPresenter<StatusRecruiterVerifyView> presenter){
        return presenter;
    }

    /** VERIFY */

    @Provides
    VerifyMvpPresenter<VerifyView> provideVerifyMvpPresenter(
            VerifyPresenter<VerifyView> presenter){
        return presenter;
    }

    /** WORK */

    @Provides
    DetailWorkMvpPresenter<DetailWorkView> provideDetailWorkMvpPresenter(
            DetailWorkPresenter<DetailWorkView> presenter){
        return presenter;
    }

    /** NOTIFICATION */

    @Provides
    NotificationMvpPresenter<NotificationView> provideNotificationMvpPresenter(
            NotificationPresenter<NotificationView> presenter){
        return presenter;
    }

    /** DASHBOARD */

    @Provides
    DashboardMvpPresenter<DashboardView> provideDashboardMvpPresenter(
            DashboardPresenter<DashboardView> presenter){
        return presenter;
    }

    @Provides
    MainMvpPresenter<MainView> provideMainMvpPresenter(
            MainPresenter<MainView> presenter){
        return presenter;
    }

    /** DETAIL CANDIDATE */

    @Provides
    DetailCandidateMvpPresenter<DetailCandidateView> provideDetailCandidateMvpPresenter(
            DetailCandidatePresenter<DetailCandidateView> presenter) {
        return presenter;
    }

    /** CHAT */

    @Provides
    ChatMvpPresenter<ChatView> provideChatMvpPresenter(
            ChatPresenter<ChatView> presenter) {
        return presenter;
    }

    @Provides
    ChatRoomMvpPresenter<ChatRoomView> provideChatRoomMvpPresenter(
            ChatRoomPresenter<ChatRoomView> presenter) {
        return presenter;
    }

    @Provides
    ChatMessageAdapter provideChatMessageAdapter() {
        return new ChatMessageAdapter(mActivity);
    }

    /** FORM */

    @Provides
    FormOptionalMvpPresenter<FormOptionalView> provideFormOptionalMvpPresenter(
            FormOptionalPresenter<FormOptionalView> presenter) {
        return presenter;
    }

    @Provides
    FormCompanyMvpPresenter<FormCompanyView> provideFormCompanyMvpPresenter(
            FormCompanyPresenter<FormCompanyView> presenter) {
        return presenter;
    }

    @Provides
    FormDocPhotoMvpPresenter<FormDocPhotoView> provideFormDocPhotoMvpPresenter(
            FormDocPhotoPresenter<FormDocPhotoView> presenter) {
        return presenter;
    }

    @Provides
    FormEducationMvpPresenter<FormEducationView> provideFormEducationMvpPresenter(
            FormEducationPresenter<FormEducationView> presenter) {
        return presenter;
    }

    @Provides
    FormPersonMvpPresenter<FormPersonView> provideFormPersonMvpPresenter(
            FormPersonPresenter<FormPersonView> presenter) {
        return presenter;
    }

    @Provides
    FormWorkMvpPresenter<FormWorkView> provideFormWorkMvpPresenter(
            FormWorkPresenter<FormWorkView> presenter) {
        return presenter;
    }

    @Provides
    InputWorkMvpPresenter<InputWorkView> provideInputWorkMvpPresenter(
            InputWorkPresenter<InputWorkView> presenter) {
        return presenter;
    }

    @Provides
    DataWorkMvpPresenter<DataWorkView> provideDataWorkMvpPresenter(
            DataWorkPresenter<DataWorkView> presenter) {
        return presenter;
    }

    @Provides
    DataCityMvpPresenter<DataCityView> provideDataCityMvpPresenter(
            DataCityPresenter<DataCityView> presenter) {
        return presenter;
    }

    @Provides
    SettingMvpPresenter<SettingView> provideSettingMvpPresenter(
            SettingPresenter<SettingView> presenter) {
        return presenter;
    }

    @Provides
    ProfileMvpPresenter<ProfileView> provideDPCMvpPresenter(
            ProfilePresenter<ProfileView> presenter) {
        return presenter;
    }

    @Provides
    EditProfileMvpPresenter<EditProfileView> provideEditDPCMvpPresenter(
            EditProfilePresenter<EditProfileView> presenter) {
        return presenter;
    }

    @Provides
    ContactUsMvpPresenter<ContactUsView> provideContactUsMvpPresenter(
            ContactUsPresenter<ContactUsView> presenter) {
        return presenter;
    }

    @Provides
    FeedbackMvpPresenter<FeedbackView> provideFeedbackMvpPresenter(
            FeedbackPresenter<FeedbackView> presenter) {
        return presenter;
    }

    @Provides
    DetailRecrutersMvpPresenter<DetailRecrutersView> provideDetailRecrutersMvpPresenter(
            DetailRecrutersPresenter<DetailRecrutersView> presenter) {
        return presenter;
    }

    @Provides
    ChooseRoleMvpPresenter<ChooseRoleView> provideChooseRoleMvpPresenter(
            ChooseRolePresenter<ChooseRoleView> presenter) {
        return presenter;
    }

    @Provides
    FormMvpPresenter<FormView> provideFormMvpPresenter(
            FormPresenter<FormView> presenter) {
        return presenter;
    }

    @Provides
    ManageAccountMvpPresenter<ManageAccountView> provideManageAccountMvpPresenter(
            ManageAccountPresenter<ManageAccountView> presenter) {
        return presenter;
    }

    @Provides
    NotificationDetailMvpPresenter<NotificationDetailView> provideNotificationDetailMvpPresenter(
            NotificationDetailPresenter<NotificationDetailView> presenter) {
        return presenter;
    }

}


