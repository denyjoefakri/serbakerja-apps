package com.joefakri.serbakerja.di.module;

import android.app.Application;
import android.content.Context;

import com.joefakri.serbakerja.BuildConfig;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.AppApiHelper;
import com.joefakri.serbakerja.connection.Header;
import com.joefakri.serbakerja.connection.Helper;
import com.joefakri.serbakerja.data.AppDataManager;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.prefs.AppPreferencesHelper;
import com.joefakri.serbakerja.data.prefs.PreferencesHelper;
import com.joefakri.serbakerja.di.ApiInfo;
import com.joefakri.serbakerja.di.ApplicationContext;
import com.joefakri.serbakerja.di.PreferenceInfo;
import com.joefakri.serbakerja.utils.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.KEY;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return Constants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    Helper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    Header.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey, PreferencesHelper preferencesHelper) {
        return new Header.ProtectedApiHeader(
                apiKey,
                preferencesHelper.getCurrentUserId(),
                preferencesHelper.getAccessToken());
    }

    @Provides
    @Singleton
    CalligraphyConfig provideCalligraphyDefaultConfig() {
        return new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build();
    }
}
