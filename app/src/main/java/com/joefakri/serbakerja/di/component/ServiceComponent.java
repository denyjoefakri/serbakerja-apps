
package com.joefakri.serbakerja.di.component;


import com.joefakri.serbakerja.di.PerService;
import com.joefakri.serbakerja.di.module.ServiceModule;
import com.joefakri.serbakerja.service.MyFirebaseInstanceIDService;
import com.joefakri.serbakerja.service.SyncService;

import dagger.Component;

/**
 * Created by janisharali on 01/02/17.
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);
    void inject(MyFirebaseInstanceIDService service);

}
