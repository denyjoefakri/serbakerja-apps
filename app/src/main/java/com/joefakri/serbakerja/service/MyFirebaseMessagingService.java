/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.joefakri.serbakerja.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.NotificationModel;
import com.joefakri.serbakerja.data.model.ReceiverMessageModel;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.ui.register.RegisterActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());


            String notification = remoteMessage.getData().get("notification");
            if (notification.equals("0")){
                String type = remoteMessage.getData().get("type");
                if (type.equals("message")){
                    ReceiverMessageModel model = new ReceiverMessageModel();
                    model.setPercakapan_id(remoteMessage.getData().get("percakapan_id"));
                    model.setFrom_id(remoteMessage.getData().get("from_id"));
                    model.setTo_id(remoteMessage.getData().get("to_id"));
                    model.setMessage(remoteMessage.getData().get("pesan"));
                    model.setDate(remoteMessage.getData().get("created_on"));
                    model.setName(remoteMessage.getData().get("nama"));
                    model.setFoto(remoteMessage.getData().get("foto"));
                    sendBroadcastIntent(model, type);
                    activeActivity(model);

                } else {
                    ReceiverMessageModel model = new ReceiverMessageModel();
                    model.setStatus_message(remoteMessage.getData().get("status_message"));
                    model.setPercakapan_id(remoteMessage.getData().get("percakapan_id"));
                    sendBroadcastIntent(model, type);
                }
            } else {
                NotificationModel model = new NotificationModel();
                model.setFrom_id(remoteMessage.getData().get("from_id"));
                model.setRole(remoteMessage.getData().get("role"));
                model.setTipe(remoteMessage.getData().get("tipe"));
                model.setPesan(remoteMessage.getData().get("pesan"));
                model.setPengguna_id(remoteMessage.getData().get("pengguna_id"));
                model.setFrom_pengguna(remoteMessage.getData().get("from_pengguna"));
                model.setStatus(remoteMessage.getData().get("status"));
                sendNotificationNotif(model);
            }



        }

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }

    private void activeActivity(ReceiverMessageModel model){
        try {

            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> alltasks = am.getRunningTasks(1);

            for (ActivityManager.RunningTaskInfo aTask : alltasks) {
                String packageName = "com.joefakri.serbakerja.ui.chat.detail";
                Log.e("Activity", aTask.topActivity.getClassName());

                if (!aTask.topActivity.getClassName().equals(packageName + ".ChatRoomActivity")) {
                    sendNotificationChat(model);
                }

            }

        } catch (Throwable t) {
            Log.i(TAG, "Throwable caught: " + t.getMessage(), t);
        }
    }

    private void scheduleJob() {
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }


    private void sendNotificationChat(ReceiverMessageModel model) {
        Intent intent = new Intent(this, ChatRoomActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("target", model.getFrom_id());
        intent.putExtra("name", model.getName());
        intent.putExtra("image", model.getFoto());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification).setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_serba_kerja_splash))
                        /*.setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(getBitmapfromUrl(url)))*/
                .setContentTitle(getString(R.string.app_name))
                .setContentText(model.getName()+ " : " + model.getMessage())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                        .setPriority(Notification.VISIBILITY_PRIVATE)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(11, notificationBuilder.build());
    }

    private void sendNotificationNotif(NotificationModel model) {

        Intent intent = new Intent(this, RegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("target", model.getFrom_id());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notification).setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_serba_kerja_splash))
                        /*.setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(getBitmapfromUrl(url)))*/
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(model.getPesan())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setPriority(Notification.VISIBILITY_PRIVATE)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(22, notificationBuilder.build());
    }

    private void sendBroadcastIntent(ReceiverMessageModel model, String filter) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(filter);
        broadcastIntent.putExtra(TAG, model);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(broadcastIntent);
        sendBroadcast(broadcastIntent);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }
}
