package com.joefakri.serbakerja.utils;

import android.util.Log;

import com.androidnetworking.utils.ParseUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by deny on bandung.
 */

public class Logger {


    public static void printLogSuccess(String path, String message, String content) {
        Log.i(":", "==================== Success Response ====================");
        Log.i("Path", path);
        Log.i("Message", message);
        Log.i("Content", content);
    }

    public static void printLogBad(String path, String message, String content) {
        Log.e(":", "====================== Bad Response ======================");
        Log.e("Path", path);
        Log.e("Message", message);
        Log.e("Content", content);
    }

    public static void printLogCancel(int code, String path, String method, String message, String content) {
        Log.e(":", "==================== Request Canceled =====================");
        Log.e("Code", String.valueOf(code));
        Log.e("Path", path);
        Log.e("Method", method);
        Log.e("Message", message);
        Log.e("Content", content);
        Log.e(":", "==========================================================");
    }

    public static void printLogRequest(String path, Object string, HashMap<String, File> files) {
        Log.i(":", "********************** API Request **********************");
        Log.i("Path", path);
        HashMap<String, String> parameters = addParameter(string);
        if (parameters != null) {
            String printedLog = "";
            for (String key : parameters.keySet()) {
                if (printedLog.isEmpty()) printedLog = key + ":" + parameters.get(key);
                else printedLog = printedLog + "\n" + key + ":" + parameters.get(key);
            }
            Log.i("Parameter", printedLog);
        }

        if (files != null && !files.isEmpty()) Log.i("Files", files.toString());
        Log.i(":", "*********************************************************");
    }

    public static void printLogRequest(HashMap<String, ArrayList<String>> map) {
        Log.i(":", "********************** MultipleParam **********************");
        if (map != null){
            String printedLog = "";
            for (String key : map.keySet()) {
                ArrayList<String> params = map.get(key);
                for (int i = 0; i < params.size(); i++) {
                    if (printedLog.isEmpty()) printedLog = key + ":" + params.get(i);
                    else printedLog = printedLog + "\n" + key + ":" + params.get(i);
                }
            }
            Log.i("Parameter", printedLog);
        }
        Log.i(":", "*********************************************************");
    }

    public static HashMap<String, String> addParameter(Object object) {
        HashMap<String, String> addParameter = new HashMap<>();
        if (object != null) {
            addParameter.putAll(ParseUtil
                    .getParserFactory()
                    .getStringMap(object));
        }
        return addParameter;
    }

}
