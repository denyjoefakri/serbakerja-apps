package com.joefakri.serbakerja.utils;

/**
 * Created by dev_deny on 3/16/17.
 */
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.joefakri.serbakerja.R;

public class TintableTextView extends TextView {

    private ColorStateList tint;

    public TintableTextView(Context context) {
        super(context);
    }

    public TintableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public TintableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(
                attrs, R.styleable.TintableImageView, defStyle, 0);
        tint = a.getColorStateList(
                R.styleable.TintableImageView_tintSelector);
        a.recycle();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (tint != null && tint.isStateful()) {
            updateTintColor();
        }
    }

    /*public void setColorFilter(ColorStateList tint) {
        this.tint = tint;
        super.setTextColor(tint.getColorForState(getDrawableState(), 0));
    }*/

    private void updateTintColor() {
        int color = tint.getColorForState(getDrawableState(), 0);
        setTextColor(color);
    }

}
