package com.joefakri.serbakerja.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by deny on bandung.
 */

public class VerifyWatcher implements TextWatcher {

    EditText one;
    EditText two;

    public VerifyWatcher(EditText... one) {
        this.one = one[0];
        this.two = one[1];
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (one.length() > 0){
            two.requestFocus();
            return;
        }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
