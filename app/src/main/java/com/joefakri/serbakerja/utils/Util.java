package com.joefakri.serbakerja.utils;

import android.util.Log;

import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.autolabel.AutoLabelUI;

import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public class Util {

    public static String getMultiparam(AutoLabelUI param) {
        String xx = "";
        for (int i = 0; i < param.getLabels().size(); i++) {
            if (xx.length() == 0) xx = param.getLabels().get(i).getTag().toString();
            else xx = xx + ", " + param.getLabels().get(i).getTag().toString();
        }
        return xx;
    }

    public static String getCitys(ArrayList<WorkModel.City> city){
        String xx = "";
        for (int i = 0; i < city.size(); i++) {
            if (xx.length() == 0) xx = city.get(i).getId();
            else xx = xx + ", " + city.get(i).getId();
        }
        return xx;
    }

    public static String getCitysName(ArrayList<WorkModel.City> city){
        String xx = "";
        for (int i = 0; i < city.size(); i++) {
            if (xx.length() == 0) xx = city.get(i).getName();
            else xx = xx + ", " + city.get(i).getName();
        }
        return xx;
    }

    public static String getCitysNameCandidate(ArrayList<WorkResponse.CandidateWorkList.Data.Kota> city){
        String xx = "";
        for (int i = 0; i < city.size(); i++) {
            if (xx.length() == 0) xx = city.get(i).getName();
            else xx = xx + ", " + city.get(i).getName();
        }
        return xx;
    }

    public static String dashboardCity(ArrayList<UserResponse.Login.Data.Kota> city){
        String id = "";
        String citySelected = "";
        if (city != null){
            for (int i = 0; i < city.size(); i++) {
                if (!city.get(0).getId().equals(id)){
                    id = city.get(0).getId();
                    if (citySelected.length() == 0) citySelected = city.get(i).getName();
                    else if (!citySelected.contains(city.get(i).getName()))
                        citySelected = citySelected + ", " + city.get(i).getName();
                }
            }
        }
        return citySelected;
    }

    public static String dashboardJob(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> city){
        String id = "";
        String jobSelected = "";
        if (city != null) {
            for (int i = 0; i < city.size(); i++) {
                if (!city.get(i).getId().equals(id)){
                    id = city.get(i).getId();
                    if (jobSelected.length() == 0) jobSelected = city.get(i).getJenis_pekerjaan();
                    else if (!jobSelected.contains(city.get(i).getJenis_pekerjaan()))
                        jobSelected = jobSelected + ", " + city.get(i).getJenis_pekerjaan();
                }
            }
        }
        return jobSelected;
    }

    public static String merge(ArrayList<String> city){
        String xx = "";
        for (int i = 0; i < city.size(); i++) {
            if (xx.length() == 0) xx = city.get(i);
            else xx = xx + ", " + city.get(i);
        }
        return xx;
    }

    public static String checkNull(String input, String placeholder) {
        if (input != null) {
            if (input.equals("") || input.equals("null")) return placeholder;
            else {
                input = input.substring(0, 1).toUpperCase() + input.substring(1);
                return input;
            }
        } else {
            return placeholder;
        }
    }
}
