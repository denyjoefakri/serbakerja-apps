/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.utils;

import android.content.Context;
import android.util.Log;

import com.joefakri.serbakerja.R;

/**
 * Created by amitshekhar on 08/01/17.
 */

public final class Constants {

    public static final String STATUS_CODE_SUCCESS = "success";
    public static final String STATUS_CODE_FAILED = "failed";
    public static final String ROLE_PEREKRUT = "perekrut";
    public static final String ROLE_KANDIDAT = "kandidat";

    public static final String BASE_API_WHATSAPP = "https://api.whatsapp.com/send?phone=";
    public static final String API_WHATSAPP = "https://api.whatsapp.com/send?phone=+6281908283331";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final long NULL_INDEX = -1L;


    public static final String PREF_NAME = "serbakerja_pref";

    public static final String SEED_DATABASE_OPTIONS = "seed/options.json";
    public static final String SEED_DATABASE_QUESTIONS = "seed/questions.json";

    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String STATUS_waiting = "Menunggu Jawaban";
    public static final String STATUS_pending = "Pending";
    public static final String STATUS_reject = "Ditolak";
    public static final String STATUS_accept = "Diterima";

    private Constants() {
        // This utility class is not publicly instantiable
    }

    public static final int RC_CODE_PICKER = 2000;
    public static final int RC_CAMERA = 3000;


    public static final String ERROR_MESSAGE_500 = "Maaf, terjadi kesalahan pada server";

    public static String getStatus(String status) {
        if (status.equals("waiting")) return "Menunggu Jawaban";
        else if (status.equals("pending")) return "Pending";
        else if (status.equals("reject")) return "Ditolak";
        else return "Diterima";
    }

    public static String waktuKerjaValue(Context ctx, String s){
        String result = "";
        if (s != null) {
            if (s.contains("0")) result = ctx.getString(R.string.options_time_1);
            if (s.contains("1")) result = ctx.getString(R.string.options_time_2);
            if (s.contains("2")) result = ctx.getString(R.string.options_time_3);
        }
        return result;
    }

    public static String waktuKerjaPosition(Context ctx, String s){
        String result = "";
        if (s != null) {
            if (s.contains(ctx.getString(R.string.options_time_1))) result = "0";
            else if (s.contains(ctx.getString(R.string.options_time_2))) result = "1";
            else result = "2";
        }
        return result;
    }

    public static String waktuKerjaOther(String position, String s){
        String result = "";
        if (s != null){
            if (position.equals("2")) result = s;
            else result = "";
        }
        return result;
    }

    public static String gender(Context ctx, String s){
        String result = "";
        if (s != null){
            if (s.contains(ctx.getString(R.string.options_gender_1))) result = "0";
            if (s.contains(ctx.getString(R.string.options_gender_2))) result = "1";
            if (s.contains(ctx.getString(R.string.options_gender_3))) result = "2";
        }
        return result;
    }

    public static String genderValue(Context ctx, String s){
        String result = "";
        if (s != null){
            if (s.contains("0")) result = ctx.getString(R.string.options_gender_1);
            if (s.contains("1")) result = ctx.getString(R.string.options_gender_2);
            if (s.contains("2")) result = ctx.getString(R.string.options_gender_3);
        }
        return result;
    }


    public static String minimal_experiencePosition(Context ctx, String s){
        String result = "";
        if (s != null) {
            if (s.contains(ctx.getString(R.string.options_experience_1))) result = "0";
            if (s.contains(ctx.getString(R.string.options_experience_2))) result = "1";
            if (s.contains(ctx.getString(R.string.options_experience_3))) result = "2";
            if (s.contains(ctx.getString(R.string.options_experience_4))) result = "3";
            if (s.contains(ctx.getString(R.string.options_experience_5))) result = "4";
        }
        return result;
    }

    public static String minimal_experience(Context ctx, String s){
        String result = "";
        if (s != null) {
            if (s.contains("0")) result = ctx.getString(R.string.options_experience_1);
            if (s.contains("1")) result = ctx.getString(R.string.options_experience_2);
            if (s.contains("2")) result = ctx.getString(R.string.options_experience_3);
            if (s.contains("3")) result = ctx.getString(R.string.options_experience_4);
            if (s.contains("4")) result = ctx.getString(R.string.options_experience_5);
        }
        return result;
    }

    public static String min_sallary(Context ctx, String s){
        String result = "";
        if (s != null) {
            if (s.contains(ctx.getString(R.string.options_sallary_1))) result = "0";
            if (s.contains(ctx.getString(R.string.options_sallary_2))) result = "1000000";
            if (s.contains(ctx.getString(R.string.options_sallary_3))) result = "2000000";
            if (s.contains(ctx.getString(R.string.options_sallary_4))) result = "3000000";
            if (s.contains(ctx.getString(R.string.options_sallary_5))) result = "5000000";
        }
        return result;
    }

    public static String max_sallary(Context ctx, String s){
        String result = "";
        if (s != null) {
            if (s.contains(ctx.getString(R.string.options_sallary_1))) result = "1000000";
            if (s.contains(ctx.getString(R.string.options_sallary_2))) result = "2000000";
            if (s.contains(ctx.getString(R.string.options_sallary_3))) result = "3000000";
            if (s.contains(ctx.getString(R.string.options_sallary_4))) result = "5000000";
            if (s.contains(ctx.getString(R.string.options_sallary_5))) result = "10000000";
        }
        return result;
    }

    public static String sallary(Context context, String min, String max){
        String result = "";
        if (min != null && max != null){
            if (min.equals("0") && max.equals("1000000")) result = context.getString(R.string.options_sallary_1);
            if (min.equals("1000000") && max.equals("2000000")) result = context.getString(R.string.options_sallary_2);
            if (min.equals("2000000") && max.equals("3000000")) result = context.getString(R.string.options_sallary_3);
            if (min.equals("3000000") && max.equals("5000000")) result = context.getString(R.string.options_sallary_4);
            if (min.equals("5000000") && max.equals("10000000")) result = context.getString(R.string.options_sallary_5);
        }
        return result;
    }

    public static String sallary(Context context, String code){
        String result = "";
        if (code != null){
            if (code.equals("0")) result = context.getString(R.string.options_sallary_1);
            if (code.equals("1")) result = context.getString(R.string.options_sallary_2);
            if (code.equals("2")) result = context.getString(R.string.options_sallary_3);
            if (code.equals("3")) result = context.getString(R.string.options_sallary_4);
            if (code.equals("4")) result = context.getString(R.string.options_sallary_5);
        }
        return result;
    }

    public static int sallaryPosition(Context context, String value){
        int result = 0;
        if (value != null) {
            if (value.equals(context.getString(R.string.options_sallary_1))) result = 0;
            if (value.equals(context.getString(R.string.options_sallary_2))) result = 1;
            if (value.equals(context.getString(R.string.options_sallary_3))) result = 2;
            if (value.equals(context.getString(R.string.options_sallary_4))) result = 3;
            if (value.equals(context.getString(R.string.options_sallary_5))) result = 4;
        }
        return result;
    }


    public static String workStatusPosition (Context ctx, String value){
        String result = "";
        if (value != null) {
            if (value.equals(ctx.getString(R.string.options_status_1))) result = "0";
            else if (value.equals(ctx.getString(R.string.options_status_2))) result = "1";
            else if (value.equals(ctx.getString(R.string.options_status_3))) result = "2";
            else if (value.equals(ctx.getString(R.string.options_status_4))) result = "3";
            else result = "";
        }
        return result;
    }

    public static String workStatusValue (Context ctx, String value){
        String result = "";
        if (value != null) {
            if (value.equals("0")) result = ctx.getString(R.string.options_status_1);
            if (value.equals("1")) result = ctx.getString(R.string.options_status_2);
            if (value.equals("2")) result = ctx.getString(R.string.options_status_3);
            if (value.equals("3")) result = ctx.getString(R.string.options_status_4);
        }
        return result;
    }

    public static String educationPosition (Context ctx, String value){
        String result = "";
        if (value != null) {
            if (value.equals(ctx.getString(R.string.options_education_1))) result = "0";
            else if (value.equals(ctx.getString(R.string.options_education_2))) result = "1";
            else if (value.equals(ctx.getString(R.string.options_education_3))) result = "2";
            else if (value.equals(ctx.getString(R.string.options_education_4))) result = "3";
            else if (value.equals(ctx.getString(R.string.options_education_5))) result = "4";
            else if (value.equals(ctx.getString(R.string.options_education_6))) result = "5";
        }
        return result;
    }

    public static String education(Context ctx, String value){
        String result = "";
        if (value != null) {
            if (value.equals("0")) result = ctx.getString(R.string.options_education_1);
            else if (value.equals("1")) result = ctx.getString(R.string.options_education_2);
            else if (value.equals("2")) result = ctx.getString(R.string.options_education_3);
            else if (value.equals("3")) result = ctx.getString(R.string.options_education_4);
            else if (value.equals("4")) result = ctx.getString(R.string.options_education_5);
            else if (value.equals("5")) result = ctx.getString(R.string.options_education_6);
        }
        return result;
    }

    public static String getTotalEmployee(Context ctx, String value){
        String result = "";
        if (value != null) {
            if (value.equals("0")) result = ctx.getString(R.string.options_total_employees_1);
            if (value.equals("1")) result = ctx.getString(R.string.options_total_employees_2);
            if (value.equals("2")) result = ctx.getString(R.string.options_total_employees_3);
            if (value.equals("3")) result = ctx.getString(R.string.options_total_employees_4);
        }
        return result;
    }

    public static String getPositionTotalEmployee(Context ctx, String value){
        String result = "";
        if (value != null) {
            if (value.equals(ctx.getString(R.string.options_total_employees_1))) result = "0";
            if (value.equals(ctx.getString(R.string.options_total_employees_2))) result = "1";
            if (value.equals(ctx.getString(R.string.options_total_employees_3))) result = "2";
            if (value.equals(ctx.getString(R.string.options_total_employees_4))) result = "3";
        }
        return result;
    }

    public static String getPeriod(String value){
        String result = "";
        if (value != null) {
            if (value.equals("0")) result = "Bulan";
            if (value.equals("1")) result = "Tahun";
        }
        return result;
    }

    public static String getPeriodCode(String value){
        String result = "";
        if (value != null) {
            if (value.equalsIgnoreCase("Bulan")) result = "0";
            if (value.equalsIgnoreCase("Tahun")) result = "1";
        }
        return result;
    }

}
