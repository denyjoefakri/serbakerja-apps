package com.joefakri.serbakerja.utils;

import android.view.View;
import android.widget.EditText;

/**
 * Created by deny on bandung.
 */

public class KeyEvent implements View.OnKeyListener {

    EditText one;
    EditText two;

    public KeyEvent(EditText... one) {
        this.one = one[0];
        this.two = one[1];
    }

    @Override
    public boolean onKey(View view, int i, android.view.KeyEvent keyEvent) {
        if (i == android.view.KeyEvent.KEYCODE_DEL){
            two.setText("");
            one.requestFocus();
            return true;
        }
        return false;
    }
}
