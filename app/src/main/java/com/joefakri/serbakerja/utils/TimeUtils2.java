package com.joefakri.serbakerja.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by deny on bandung.
 */

public class TimeUtils2 {

    public static boolean getExpiredDate(String expired) {
        Date nowDate = date(getCurrentDateString());
        Date end = date(expired);

        return nowDate.compareTo(end) > 0;
    }


    public static Date date(String datetime){
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String getCurrentDateString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
