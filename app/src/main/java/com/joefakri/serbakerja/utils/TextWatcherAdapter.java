package com.joefakri.serbakerja.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by dev_deny on 4/3/17.
 */

public class TextWatcherAdapter implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
