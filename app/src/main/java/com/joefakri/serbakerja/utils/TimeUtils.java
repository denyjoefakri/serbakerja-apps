package com.joefakri.serbakerja.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by deny on bandung.
 */

public class TimeUtils {

    public static String getTime(String time) {
        String year = "";
        String mount = "";
        String date = "";
        if (time != null) {
            year = time.substring(0, 4);
            mount = time.substring(5, 7);
            date = time.substring(8, 10);

            switch (mount){
                case "12":
                    mount = "Desember";
                    break;
                case "11":
                    mount = "November";
                    break;
                case "10":
                    mount = "Oktober";
                    break;
                case "9":
                    mount = "September";
                    break;
                case "8":
                    mount = "Agustus";
                    break;
                case "7":
                    mount = "Juli";
                    break;
                case "6":
                    mount = "Juni";
                    break;
                case "5":
                    mount = "Mei";
                    break;
                case "4":
                    mount = "April";
                    break;
                case "3":
                    mount = "Maret";
                    break;
                case "2":
                    mount = "Februari";
                    break;
                case "1":
                    mount = "Januari";
                    break;
            }
        }
        return date + "-" + mount + "-" + year;
    }

    public static String getTimeChat(String time, long currentTime) {
        String result = "";
        String year = "";
        String mount = "";
        String date = "";
        String clock = "";
        if (time != null) {
            year = time.substring(0, 4);
            mount = time.substring(5, 7);
            date = time.substring(8, 10);
            clock = time.substring(11, 16);

            switch (mount){
                case "12":
                    mount = "Desember";
                    break;
                case "11":
                    mount = "November";
                    break;
                case "10":
                    mount = "Oktober";
                    break;
                case "9":
                    mount = "September";
                    break;
                case "8":
                    mount = "Agustus";
                    break;
                case "7":
                    mount = "Juli";
                    break;
                case "6":
                    mount = "Juni";
                    break;
                case "5":
                    mount = "Mei";
                    break;
                case "4":
                    mount = "April";
                    break;
                case "3":
                    mount = "Maret";
                    break;
                case "2":
                    mount = "Februari";
                    break;
                case "1":
                    mount = "Januari";
                    break;
            }
        }

        long feedSec = getMilis(time) / 1000;
        long currentSec = currentTime / 1000;
        long resSec = currentSec - feedSec;

        if (resSec >= 0 && resSec < 86400) {
            result = clock;
        } else {
            result = date + "-" + mount + "-" + year;
        }

        return result;
    }

    public static String getDate(String time) {
        String year = time.substring(0, 4);
        String mount = time.substring(5, 7);
        String date = time.substring(8, 10);
        String clock = time.substring(11, 16);

        return date + "-"+mount+"-"+year;
    }

    public static String getClock(String time) {
        String clock = time.substring(11, 16);
        return clock;
    }

    public static boolean getVisibleTimeChat(String time) {
        String year = time.substring(0, 4);
        String mount = time.substring(5, 7);
        String date = time.substring(8, 10);
        String clock = time.substring(11, 16);

        long feedSec = getMilis(time) / 1000;
        long currentSec = getMilis(getCurrentDateTime()) / 1000;
        long resSec = currentSec - feedSec;

        if (resSec >= 0 && resSec < 86400) {
            return false;
        } else {
            return true;
        }

    }


    public static long getTwoMinute() {
        Date startTime = new Date();
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, 2);
        Date five = now.getTime();
        return five.getTime() - startTime.getTime();
    }


    public static Date date(String datetime){
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }


    public static String getAge(String date) {

        if (date != null) {

        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar dob = Calendar.getInstance();
        try {
             dob.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar today = Calendar.getInstance();

        int curYear = today.get(Calendar.YEAR);
        int dobYear = dob.get(Calendar.YEAR);

        int age = curYear - dobYear;


        int curMonth = today.get(Calendar.MONTH);
        int dobMonth = dob.get(Calendar.MONTH);
        if (dobMonth > curMonth) { // this year can't be counted!
            age--;
        } else if (dobMonth == curMonth) { // same month? check for day
            int curDay = today.get(Calendar.DAY_OF_MONTH);
            int dobDay = dob.get(Calendar.DAY_OF_MONTH);
            if (dobDay > curDay) { // this year can't be counted!
                age--;
            }
        }

        return String.valueOf(age);
    }

    public static String getFeedTime(long feedTime, long currentTime) {
        String result;
        long feedSec = feedTime / 1000;
        long currentSec = currentTime / 1000;
        long resSec = currentSec - feedSec;
        if (resSec >= 0 && resSec < 3600) {
            long minute = resSec / 60;
            result = minute + " min ago";
        } else if (resSec >= 3600 && resSec < 86400) {
            long hours = resSec / 3600;
            result = hours + " hours ago";
        } else if (resSec >= 86400 && resSec < 172800) {
            result = "Yesterday";
        } else if (resSec >= 86400 && resSec < 2592000) {
            long days = resSec / 86400;
            result = days + " days ago";
        } else {
            long month = resSec / 2592000;
            result = month + " month ago";
        }

        return result;
    }

    public static long getMilisDate(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(dateTime);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long getMilis(String dateTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = sdf.parse(dateTime);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getCurrentDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
