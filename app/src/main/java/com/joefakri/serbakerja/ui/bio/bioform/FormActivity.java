package com.joefakri.serbakerja.ui.bio.bioform;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.data.realm.StateRealm;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.bio.ChooseRoleMvpPresenter;
import com.joefakri.serbakerja.ui.bio.ChooseRoleView;
import com.joefakri.serbakerja.ui.widget.WrapContentHeightViewPager;
import com.levibostian.shutter_android.builder.ShutterResultListener;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;

public class FormActivity extends BaseActivity implements FormView{

    @Inject FormMvpPresenter<FormView> presenter;
    FormPagerAdapter pagerAdapter;
    
    @BindView(R.id.txt_title) TextView txt_title;
    @BindView(R.id.view_indicator) InkPageIndicator view_indicator;
    @BindView(R.id.view_pager_tab) public WrapContentHeightViewPager view_pager_tab;

    public ShutterResultListener shutterListener;
    Realm realm;
    private EventBus bus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_form, this);
        getActivityComponent().inject(this);

        presenter.onAttach(FormActivity.this);
        initComponent();

    }

    @Override
    protected void onResume() {
        super.onResume();
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void initComponent() {

        ViewPager.OnPageChangeListener pagerSynchronizer = getPagerSynchronizer();
        view_pager_tab.addOnPageChangeListener(pagerSynchronizer);
        view_pager_tab.setAllowedSwipeDirection(WrapContentHeightViewPager.SwipeDirection.left);
        view_pager_tab.setOffscreenPageLimit(0);

        if (getIntent().getStringExtra("form").equals("r1"))
            setupViewPager1_recruiters();
        else if (getIntent().getStringExtra("form").equals("r2"))
            setupViewPager2_recruiters();
        else if (getIntent().getStringExtra("form").equals("r3"))
            setupViewPager3_recruiters();
        else if (getIntent().getStringExtra("form").equals("c1"))
            setupViewPager1_candidate();
        else if (getIntent().getStringExtra("form").equals("c2")) {
            setupViewPager2_candidate();
            realm = Realm.getDefaultInstance();
            StateRealm state = realm.where(StateRealm.class).equalTo("id", 1).findFirst();
            realm.beginTransaction();
            if (state != null){
                state.setState(0);
            } else {
                state = realm.createObject(StateRealm.class);
                state.setId(1);
                state.setState(0);
            }
            realm.commitTransaction();
        }
        else if (getIntent().getStringExtra("form").equals("c3"))
            setupViewPager3_candidate();
        else if (getIntent().getStringExtra("form").equals("c4"))
            setupViewPager4_candidate();
        else if (getIntent().getStringExtra("form").equals("c5"))
            setupViewPager5_candidate();


    }

    public void setupViewPager1_recruiters() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 1);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager2_recruiters() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 2);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager3_recruiters() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 3);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager1_candidate() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 11);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager2_candidate() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 12);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager3_candidate() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 13);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager4_candidate() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 14);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager5_candidate() {
        pagerAdapter = new FormPagerAdapter(getSupportFragmentManager(), 15);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    private ViewPager.OnPageChangeListener getPagerSynchronizer() {
        return new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.e("position", ""+position);
                view_pager_tab.setCurrentItem(position, true);
                bus.postSticky(new StateEvent(String.valueOf(position),""));

                if (realm.isClosed()) realm = Realm.getDefaultInstance();
                StateRealm state = realm.where(StateRealm.class).equalTo("id", 1).findFirst();
                //if (!realm.isInTransaction()) realm.beginTransaction();
                realm.beginTransaction();
                if (state != null){
                    state.setState(position);
                } else {
                    state = realm.createObject(StateRealm.class);
                    state.setId(1);
                    state.setState(position);
                }
                realm.commitTransaction();
                realm.close();

            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (shutterListener != null) {
            if (!shutterListener.onActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        realm.close();
        if (pagerAdapter != null) {
            pagerAdapter.clearReference();
            pagerAdapter = null;
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){}

    @Override
    public void onBackPressed() {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        StateRealm state = realm.where(StateRealm.class).equalTo("id", 1).findFirst();
        if (state != null) {
            switch (state.getState()){
                case 0:
                    realm.beginTransaction();
                    state.deleteFromRealm();
                    realm.commitTransaction();
                    realm.close();
                    finish();
                    break;
                case 1:
                    view_pager_tab.setCurrentItem(0); break;
                case 2:
                    view_pager_tab.setCurrentItem(1); break;
                case 3:
                    view_pager_tab.setCurrentItem(2); break;
                case 4:
                    view_pager_tab.setCurrentItem(3); break;
                case 5:
                    view_pager_tab.setCurrentItem(4); break;
                case 6:
                    view_pager_tab.setCurrentItem(5); break;
                case 7:
                    view_pager_tab.setCurrentItem(6); break;
                case 8:
                    view_pager_tab.setCurrentItem(7); break;
                case 9:
                    view_pager_tab.setCurrentItem(8); break;
                case 10:
                    view_pager_tab.setCurrentItem(9); break;
                default:
                    super.onBackPressed(); break;
            }
        } else {
            super.onBackPressed();
        }



    }
}
