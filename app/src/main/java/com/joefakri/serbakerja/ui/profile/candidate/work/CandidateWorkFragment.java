package com.joefakri.serbakerja.ui.profile.candidate.work;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkCandidateProfileAdapter;
import com.joefakri.serbakerja.adapter.WorkPostAdapter;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostView;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostActivity;
import com.joefakri.serbakerja.utils.Logger;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class CandidateWorkFragment extends BaseFragment implements CandidateWorkView{

    @BindView(R.id.rv_work) RecyclerView rv_work;
    @BindView(R.id.fab_add) FloatingActionButton fab_add;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.view_data) LinearLayout view_data;
    @BindView(R.id.tv_message) TextView tv_message;
    WorkCandidateProfileAdapter workPostAdapter;

    @Inject CandidateWorkMvpPresenter<CandidateWorkView> mPresenter;
    @Inject LinearLayoutManager mLayoutManager;
    @Inject DataManager dataManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_candidate_work, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View view) {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work.setLayoutManager(mLayoutManager);
        rv_work.setHasFixedSize(true);
        rv_work.setNestedScrollingEnabled(false);
        rv_work.setAdapter(workPostAdapter = new WorkCandidateProfileAdapter(getActivity()));
        workPostAdapter.setOnclickListener((model, position) -> {
            Intent intent = new Intent(getActivity(), EditWorkActivity.class);
            intent.putExtra("model", model);
            startActivity(intent);
        });

        fab_add.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), EditWorkActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewWork();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultWorkView(ArrayList<WorkResponse.CandidateWorkList.Data> data) {
        if (data.isEmpty()){
            view_empty.setVisibility(View.VISIBLE);
            view_data.setVisibility(View.GONE);
            tv_message.setText(getString(R.string.txt_work_empty));
        } else {
            view_empty.setVisibility(View.GONE);
            view_data.setVisibility(View.VISIBLE);
            workPostAdapter.update(data);

            ArrayList<UserResponse.Login.Data.Kota> citys = new ArrayList<>();
            ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> works = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                UserResponse.Login.Data.Jenis_pekerjaan work = new UserResponse.Login.Data.Jenis_pekerjaan();
                work.setId(data.get(i).getJenis_pekerjaan_id());
                work.setJenis_pekerjaan(data.get(i).getJenis_pekerjaan());
                for (int j = 0; j < data.get(i).getKota().size(); j++) {
                    UserResponse.Login.Data.Kota city = new UserResponse.Login.Data.Kota();
                    city.setId(data.get(i).getKota().get(j).getId());
                    city.setName(data.get(i).getKota().get(j).getName());
                    citys.add(city);
                }
                works.add(work);
            }

            dataManager.setPreferenceNull();
            dataManager.setDefaultCity(citys);
            dataManager.setDefaultJob(works);

        }
    }
}
