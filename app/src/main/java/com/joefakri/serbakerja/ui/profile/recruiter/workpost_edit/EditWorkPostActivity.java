package com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class EditWorkPostActivity extends BaseActivity implements EditWorkPostView, DatePickerDialog.OnDateSetListener{

    @Inject
    EditWorkPostMvpPresenter<EditWorkPostView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_work) EditText txt_work;
    @BindView(R.id.txt_work_city) AutoCompleteTextView txt_work_city;
    @BindView(R.id.txt_work_area) EditText txt_work_area;
    @BindView(R.id.txt_work_address) EditText txt_work_address;
    @BindView(R.id.txt_work_day) EditText txt_work_day;
    @BindView(R.id.txt_work_custom_requirement) EditText txt_work_custom_requirement;
    @BindView(R.id.txt_work_scope) EditText txt_work_scope;
    @BindView(R.id.txt_work_expired_date) EditText txt_work_expired_date;
    @BindView(R.id.txt_options_time_3) EditText txt_options_time_3;
    @BindView(R.id.rd_gender) RadioGroup rd_gender;
    @BindView(R.id.rd_group_sallary) RadioGroup rd_group_sallary;
    @BindView(R.id.rd_group_time) RadioGroup rd_group_time;
    @BindView(R.id.rd_group_experience) RadioGroup rd_group_experience;
    @BindView(R.id.rd_group_education) RadioGroup rd_group_education;
    @BindView(R.id.btn_save) Button btn_save;

    ArrayList<String> citys;
    ArrayList<String> ids;
    private String city_id = "";
    private String city = "";
    private String id_job = "";
    private String job = "";
    DatePickerDialog dpd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_work_post, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_work));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        WorkResponse.List.Data workModel = (WorkResponse.List.Data) getIntent().getSerializableExtra("model");
        presenter.onViewCity();

        txt_work.setOnFocusChangeListener((view, b) -> {
            if (b){
                Intent intent = new Intent(EditWorkPostActivity.this, DataWorkActivity.class);
                intent.putExtra("models", new ArrayList<WorkModel>());
                intent.putExtra("dashboard", false);
                startActivityForResult(intent, 0);
            }
        });

        if (workModel != null){

            if (workModel.getJenis_pekerjaan() != null) txt_work.setText(workModel.getJenis_pekerjaan());
            txt_work.setEnabled(false);
            if (workModel.getKota() != null) txt_work_city.setText(workModel.getKota());
            if (workModel.getLokasi() != null) txt_work_area.setText(workModel.getLokasi());
            if (workModel.getHari_kerja() != null) txt_work_day.setText(workModel.getHari_kerja());
            if (workModel.getLingkup_pekerjaan() != null) txt_work_scope.setText(workModel.getLingkup_pekerjaan());
            if (workModel.getAlamat() != null) txt_work_address.setText(workModel.getAlamat());
            if (workModel.getSyarat_tambahan() != null) txt_work_custom_requirement.setText(workModel.getSyarat_tambahan());
            if (workModel.getKota() != null) city = workModel.getKota();
            if (workModel.getKota_id() != null) city_id = workModel.getKota_id();
            if (workModel.getJenis_pekerjaan() != null) job = workModel.getJenis_pekerjaan();
            if (workModel.getJenis_pekerjaan_id() != null) id_job = workModel.getJenis_pekerjaan_id();

            String sallaryValue = Constants.sallary(this, workModel.getGaji_min(), workModel.getGaji_max());
            if (!sallaryValue.isEmpty()){
                int sallaryPosition = Constants.sallaryPosition(this, sallaryValue);
                ((RadioButton)rd_group_sallary.getChildAt(sallaryPosition)).setChecked(true);
            }

            if (workModel.getWaktu_kerja() != null){
                ((RadioButton)rd_group_time.getChildAt(Integer.parseInt(workModel.getWaktu_kerja()))).setChecked(true);
            }

            if (workModel.getWaktu_kerja().equals("2")) {
                txt_options_time_3.setVisibility(View.VISIBLE);
                txt_options_time_3.setText(workModel.getWaktu_kerja_lainnya());
            }

            if (workModel.getJenis_kelamin() != null)
                ((RadioButton)rd_gender.getChildAt(Integer.parseInt(workModel.getJenis_kelamin()))).setChecked(true);

            if (workModel.getMin_pengalaman() != null)
                ((RadioButton)rd_group_experience.getChildAt(Integer.parseInt(workModel.getMin_pengalaman()))).setChecked(true);

            //String positionEducation = Constants.educationPosition(this, workModel.getMin_pendidikan());
            if (workModel.getMin_pendidikan() != null)
                ((RadioButton)rd_group_education.getChildAt(Integer.parseInt(workModel.getMin_pendidikan()))).setChecked(true);

            txt_work_expired_date.setText(TimeUtils.getTime(workModel.getExpired_date()));
            txt_work_expired_date.setEnabled(false);

        }

        rd_group_sallary.setOnCheckedChangeListener((radioGroup, i) -> {
            txt_work.clearFocus();
        });


        rd_group_time.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton radioButton = findViewById(i);
            if (radioButton.getText().equals(getString(R.string.options_time_3))) {
                txt_options_time_3.setVisibility(View.VISIBLE);
                txt_options_time_3.setText("");
            }
            else {
                txt_options_time_3.setVisibility(View.GONE);
            }
        });

        btn_save.setOnClickListener(view -> {

            if (txt_work.getText().toString().isEmpty() || !txt_work.getText().toString().equals(job)) {
                onError("Pilih jenis pekerjaan yang tersedia");
                return;
            }

            if (rd_group_sallary.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu gaji yang tersedia");
                return;
            }

            if (city_id.isEmpty() && txt_work_city.getText().toString().isEmpty()
                    || !txt_work_city.getText().toString().equals(city)) {
                onError("Pilih Kota / Kabupaten yang tersedia");
                return;
            }

            if (txt_work_area.getText().toString().isEmpty()) {
                onError("Daerah tidak boleh kosong");
                return;
            }

            if (txt_work_address.getText().toString().isEmpty()) {
                onError("Alamat lengkap tidak boleh kosong");
                return;
            }

            if (txt_work_day.getText().toString().isEmpty()) {
                onError("Hari kerja tidak boleh kosong");
                return;
            }

            if (rd_group_time.getCheckedRadioButtonId() == -1){
                onError("Pilih salah waktu bekerja yang tersedia");
                return;
            }

            if (rd_gender.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu jenis kelamin yang tersedia");
                return;
            }

            if (rd_group_experience.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu minimal pengalaman yang tersedia");
                return;
            }

            if (rd_group_education.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu minimal pendidikan yang tersedia");
                return;
            }

            if (txt_work_scope.getText().toString().isEmpty()){
                onError("Lingkup pekerjaan tidak boleh kosong");
                return;
            }

            if (txt_work_expired_date.getText().toString().isEmpty() ) {
                onError("Tanggal kadaluarsa postingan pekerjaan tidak boleh kosong");
                return;
            }

            String work_time = Constants.waktuKerjaPosition(this, getTime());
            String work_time_other = Constants.waktuKerjaOther(work_time, txt_options_time_3.getText().toString());
            String gender = Constants.gender(this, getGender());
            String minimal = Constants.minimal_experiencePosition(this, getExperience());
            String min_sallary = Constants.min_sallary(this, getSallary());
            String max_sallary = Constants.max_sallary(this, getSallary());
            String education = Constants.educationPosition(this, getEducation());

            if (workModel == null){
                presenter.onPostRecruterWork(id_job, min_sallary, max_sallary,
                        txt_work_expired_date.getText().toString(), txt_work_address.getText().toString(),
                        txt_work_area.getText().toString(), city_id, work_time, work_time_other,
                        txt_work_day.getText().toString(), gender, minimal, education,
                        txt_work_custom_requirement.getText().toString(), txt_work_scope.getText().toString());
            } else {
                presenter.onPostEditWork(id_job, min_sallary, max_sallary,
                        txt_work_expired_date.getText().toString(), txt_work_address.getText().toString(),
                        txt_work_area.getText().toString(), city_id, work_time, work_time_other,
                        txt_work_day.getText().toString(), gender, minimal, education,
                        txt_work_custom_requirement.getText().toString(), txt_work_scope.getText().toString(), workModel.getPekerjaan_id());
            }


        });

        txt_work_expired_date.setOnFocusChangeListener((view, b) -> {
            if (b){
                Calendar now = Calendar.getInstance();
                dpd = DatePickerDialog.newInstance(this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, 0);
                dpd.setMinDate(c);
                dpd.show(getFragmentManager(), "DatepickerdialogBirthday");
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
            }
        });

    }

    private String getTime(){
        int selectedId= rd_group_time.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    private String getGender(){
        int selectedId= rd_gender.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    private String getExperience(){
        int selectedId= rd_group_experience.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    private String getEducation(){
        int selectedId= rd_group_education.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    private String getSallary(){
        int selectedId= rd_group_sallary.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            id_job = data.getStringExtra("id");
            job = data.getStringExtra("job");
            txt_work.setText(job);
        }
    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {
        citys = new ArrayList<>();
        ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, citys);
        txt_work_city.setAdapter(adapter);
        txt_work_city.setOnItemClickListener((adapterView, view, i, l) -> {
            city = adapter.getItem(i).toString();
            city_id = ids.get(citys.indexOf(city));
            Log.e("job", ids.get(citys.indexOf(city)) + " | " + city);
        });
    }


    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {
        finish();
    }

    @Override
    public void onResultEditRecruterWork() {
        finish();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String mount = String.valueOf(monthOfYear + 1);
        String day = String.valueOf(dayOfMonth);

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
        try {
            calendar.setTime(dateFormat.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mount.length() <= 1 &&  day.length() <= 1){
            txt_work_expired_date.setText("0"+dayOfMonth + "-" +"0"+(monthOfYear+1)+"-" + year);
        }else if (day.length() <= 1){
            txt_work_expired_date.setText("0"+dayOfMonth +"-"+(monthOfYear+1)+"-"+ year);
        }else if (mount.length() <= 1){
            txt_work_expired_date.setText(dayOfMonth+"-"+"0"+(monthOfYear+1)+"-"+ year);
        }else {
            txt_work_expired_date.setText(dayOfMonth + "-" + (monthOfYear + 1)+"-"+ year);
        }

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        if (format.format(date).equals(txt_work_expired_date.getText().toString())) {
            onError("Tidak bisa pilih tanggal hari ini");
            txt_work_expired_date.setText("");
        }
        txt_work_expired_date.clearFocus();
    }
}
