/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.notification;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.ChatRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.connection.response.NotificationResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.main.MainMvpPresenter;
import com.joefakri.serbakerja.ui.main.MainView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class NotificationPresenter<V extends NotificationView> extends BasePresenter<V>
        implements NotificationMvpPresenter<V> {

    @Inject
    public NotificationPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getSerbakerjaView().showLoading();
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());
        Logger.printLogRequest(Path.NOTIFICATION, id, null);

        getCompositeDisposable().add(getDataManager()
                .notification(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.NOTIFICATION);
                    Gson gson = new Gson();

                    NotificationResponse.ListNotif response = gson.fromJson(jsonObject.toString(), NotificationResponse.ListNotif.class);
                    if (response != null && response.getData() != null) {
                        getSerbakerjaView().onResult(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onRead(String id, String type) {
        getSerbakerjaView().showLoading();
        UserRequest.Id idNotif = new UserRequest.Id(id);
        Logger.printLogRequest(Path.READ_NOTIFICATION, idNotif, null);

        getCompositeDisposable().add(getDataManager()
                .readNotification(idNotif)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.READ_NOTIFICATION);
                    Gson gson = new Gson();

                    ChatResponse.Read chatResponse = gson.fromJson(jsonObject.toString(), ChatResponse.Read.class);
                    if (chatResponse != null && chatResponse.getStatus().equals("success")) {
                        getSerbakerjaView().onResultReadNotification(type);
                    } else {
                        getSerbakerjaView().onError(chatResponse.getMessage());
                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
