/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form.person;

import android.Manifest;
import android.app.Activity;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.AreaRequest;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class FormPersonPresenter<V extends FormPersonView> extends BasePresenter<V>
        implements FormPersonMvpPresenter<V>{

    private Realm realm;

    @Inject
    public FormPersonPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void readCameraGallery(Activity activity, Fragment fragment, View v) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (hasPermission) {
            openImageChooser(activity, v);
        } else {
            EasyPermissions.requestPermissions(fragment, activity.getString(R.string.message_permission_camera), 0,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void openImageChooser(Activity activity, View v) {
        PopupMenu popupMenu = new PopupMenu(activity, v);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_camera) {
                takePhoto(activity);
            } else {
                openGallery(activity);
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    public void onViewCity() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCity()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(cityResponse -> {

                    LogResponse(cityResponse, Path.GET_CITY);
                    Gson gson = new Gson();
                    AreaResponse.CityResponse response = gson.fromJson(cityResponse.toString(), AreaResponse.CityResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCity(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onViewDistrict(String city_id) {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        getCompositeDisposable().add(getDataManager()
                .getDistrict(new AreaRequest.District(city_id))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(districtResponse -> {
                    LogResponse(districtResponse, Path.GET_DISTRICT);
                    Gson gson = new Gson();
                    AreaResponse.DistrictResponse response = gson.fromJson(districtResponse.toString(), AreaResponse.DistrictResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultDistrict(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostRecruterInfo() {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();

        RecruiterRequest.PostInfo info = new RecruiterRequest.PostInfo(getDataManager().getCurrentUserId(), recruiter.getPersonName(),
                recruiter.getPersonBirthplace(), recruiter.getPersonBirthday(), recruiter.getPersonAddress(),
                recruiter.getPersonRtrw(), recruiter.getPersonVillage(), recruiter.getPersonDistrics());
        RecruiterRequest.PostInfoFile infoFile = new RecruiterRequest.PostInfoFile(new File(recruiter.getPersonPhoto()),
                new File(recruiter.getPersonPhotoKTP()));

        Logger.printLogRequest(Path.POST_RECRUITER_INFO, info, infoFile.getFoto());
        getCompositeDisposable().add(getDataManager()
                .postRInfo(info, infoFile)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_RECRUITER_INFO);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    RecruiterResponse.InfoResponse response = gson.fromJson(jsonObject.toString(), RecruiterResponse.InfoResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultRecruterInfo(response.getData());
                        recruiter.setPercent3(9);
                    } else {
                        JSONObject object = new JSONObject(response.getMessage());
                        getSerbakerjaView().onError(object.getString("message"));
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostCandidateInfo() {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();

        CandidateRequest.PostInfo info = new CandidateRequest.PostInfo(getDataManager().getCurrentUserId(), candidateRealm.getPersonName(),
                candidateRealm.getPersonBirthplace(), candidateRealm.getPersonBirthday(), candidateRealm.getPersonAddress(),
                candidateRealm.getPersonRtrw(), candidateRealm.getPersonDistrics(), candidateRealm.getPersonVillage(),
                candidateRealm.getPersonGender(), candidateRealm.getPersonReligion());

        Logger.printLogRequest(Path.POST_CANDIDATE_INFO, info, null);
        getCompositeDisposable().add(getDataManager()
                .postCInfo(info)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_CANDIDATE_INFO);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    CandidateResponse.InfoResponse response = gson.fromJson(jsonObject.toString(), CandidateResponse.InfoResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCandidateInfo(response.getData());
                        candidateRealm.setPercent1(9);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void init() {
        realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        if (recruiter != null && candidate != null) getSerbakerjaView().initRecruter(recruiter, candidate);
    }

    @Override
    public void saveRecruterPersonName(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonName(data);
            recruiter.setPercent3(1);
        } else {
            recruiter = realm.createObject(RecruiterRealm.class);
            recruiter.setId(1);
            recruiter.setPersonName(data);
            recruiter.setPercent3(1);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonBirthday(String birthday, String birthplace) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonBirthday(birthday);
            recruiter.setPersonBirthplace(birthplace);
            recruiter.setPercent3(3);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonAddress(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonAddress(data);
            recruiter.setPercent3(4);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonDistricts(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonDistrics(data);
            recruiter.setPercent3(5);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonVillage(String code) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonVillage(code);
            recruiter.setPercent3(6);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonRtrw(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonRtrw(data);
            recruiter.setPercent3(7);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonPhotoKTP(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonPhotoKTP(data);
            recruiter.setPercent3(8);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterPersonPhoto(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setPersonPhoto(data);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidatePersonName(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonName(data);
            candidateRealm.setPercent1(1);
        } else {
            candidateRealm = realm.createObject(CandidateRealm.class);
            candidateRealm.setId(1);
            candidateRealm.setPersonName(data);
            candidateRealm.setPercent1(1);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidatePersonBirthday(String birthday, String birthplace) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonBirthday(birthday);
            candidateRealm.setPersonBirthplace(birthplace);
            candidateRealm.setPercent1(3);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidatePersonAddress(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonAddress(data);
            candidateRealm.setPercent1(4);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidatePersonDistricts(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonDistrics(data);
            candidateRealm.setPercent1(5);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidatePersonVillage(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonVillage(data);
            candidateRealm.setPercent1(6);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidatePersonRtrw(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonRtrw(data);
            candidateRealm.setPercent1(7);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidateGender(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonGender(data);
            candidateRealm.setPercent1(8);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidateReligion(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidateRealm != null){
            candidateRealm.setPersonReligion(data);
        }
        realm.commitTransaction();
        realm.close();
    }


    private void takePhoto(Activity activity){
        ShutterTakePhotoBuilder shutter = Shutter.Companion.with(activity)
                .takePhoto().usePrivateAppInternalStorage()
                .addPhotoToGallery();

        ((FormActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().takePhoto(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath());
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void openGallery(Activity activity){
        ShutterPickPhotoGalleryBuilder shutter = Shutter.Companion.with(activity)
                .getPhotoFromGallery().usePrivateAppInternalStorage();

        ((FormActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().openGallery(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath());
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private File compress(Activity activity, String file){
        File fileToSend;
        String nama = "";
        if (new File(file).getName().indexOf(".") > 0)
            nama = new File(file).getName().substring(0, new File(file).getName().lastIndexOf("."));

        String extension = new File(file).getAbsolutePath().substring(new File(file).getAbsolutePath().lastIndexOf("."));

        try {
            fileToSend = new Compressor(activity)
                    .setMaxWidth(1000)
                    .setMaxHeight(1000)
                    .setQuality(90)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(new File(file), nama + "_Serbakerja" + extension);
            Log.e("compress", fileToSend.getAbsolutePath());
            return fileToSend;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
