/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.chat.detail;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.ChatRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import java.sql.Time;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 25/05/17.
 */

public class ChatRoomPresenter<V extends ChatRoomView> extends BasePresenter<V>
        implements ChatRoomMvpPresenter<V> {

    ArrayList<String> idMessages = new ArrayList<>();

    @Inject
    public ChatRoomPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(String targetId) {
        ChatRequest.GetMessage getMessage = new ChatRequest.GetMessage(getDataManager().getCurrentUserId(), targetId);
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.GET_MESSAGE, getMessage, null);

        getCompositeDisposable().add(getDataManager()
                .getChat(getMessage)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.GET_MESSAGE);
                    Gson gson = new Gson();

                    ChatResponse.Retrieve chatResponse = gson.fromJson(jsonObject.toString(), ChatResponse.Retrieve.class);
                    if (chatResponse != null && chatResponse.getData() != null) {
                        getSerbakerjaView().update(chatResponse.getData(), getDataManager().getCurrentUserId());
                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onRead(String targetId) {
        ChatRequest.ReadMessage readMessage = new ChatRequest.ReadMessage(getDataManager().getCurrentUserId(), targetId);
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.READ_MESSAGE, readMessage, null);

        getCompositeDisposable().add(getDataManager()
                .chatRead(readMessage)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.READ_MESSAGE);
                    Gson gson = new Gson();

                    ChatResponse.Read chatResponse = gson.fromJson(jsonObject.toString(), ChatResponse.Read.class);
                    if (chatResponse != null && chatResponse.getStatus().equals("success")) {

                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onSend(String targetId, String idMessagge, String message) {
        ChatRequest.SendMessage parameter = new ChatRequest.SendMessage(getDataManager().getCurrentUserId(),
                targetId, message);
        //getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.SEND_MESSAGE, parameter, null);

        idMessages.add(idMessagge);
        getCompositeDisposable().add(getDataManager()
                .chatSend(parameter, idMessagge)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.SEND_MESSAGE);
                    Gson gson = new Gson();

                    ChatResponse.Send send = gson.fromJson(jsonObject.toString(), ChatResponse.Send.class);
                    if (send != null && send.getData() != null) {
                        for (int i = 0; i < idMessages.size(); i++) {
                            getSerbakerjaView().onResultSend(send, getDataManager().getCurrentUserId(), idMessages.get(i));
                            idMessages.remove(i);
                        }
                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().onErrorSend(idMessagge);
                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void permissionCall(Activity activity, String phone) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CALL_PHONE);
        if (hasPermission) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+phone));
            activity.startActivity(callIntent);
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.message_permission_callphone), 10,
                    Manifest.permission.CALL_PHONE);
        }
    }
}
