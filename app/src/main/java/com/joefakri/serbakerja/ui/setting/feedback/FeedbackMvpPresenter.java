package com.joefakri.serbakerja.ui.setting.feedback;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.setting.SettingView;

/**
 * Created by deny on bandung.
 */

public interface FeedbackMvpPresenter<V extends FeedbackView> extends SerbakerjaPresenter<V> {

    void onInitFrom();

    void onSend(String subjek, String pesan);

}
