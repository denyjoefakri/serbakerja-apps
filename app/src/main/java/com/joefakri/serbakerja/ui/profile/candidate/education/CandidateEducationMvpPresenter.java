package com.joefakri.serbakerja.ui.profile.candidate.education;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceView;

/**
 * Created by deny on bandung.
 */

public interface CandidateEducationMvpPresenter<V extends CandidateEducationView> extends SerbakerjaPresenter<V> {

    void onViewEducation();

}
