/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.base;

/**
 * Created by janisharali on 27/01/17.
 */

import android.util.Log;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.ErrorResponse;
import com.joefakri.serbakerja.data.AppDataManager;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import io.reactivex.disposables.CompositeDisposable;

import static com.joefakri.serbakerja.utils.Constants.ERROR_MESSAGE_500;

public class BasePresenter<V extends SerbakerjaView> implements SerbakerjaPresenter<V> {

    private static final String TAG = "BasePresenter";

    private final DataManager mDataManager;
    private final SchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mCompositeDisposable;

    private V mSerbakerjaView;

    @Inject
    public BasePresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        this.mDataManager = dataManager;
        this.mSchedulerProvider = schedulerProvider;
        this.mCompositeDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V SerbakerjaView) {
        mSerbakerjaView = SerbakerjaView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.dispose();
        mSerbakerjaView = null;
    }

    public boolean isViewAttached() {
        return mSerbakerjaView != null;
    }

    public V getSerbakerjaView() {
        return mSerbakerjaView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new SerbakerjaViewNotAttachedException();
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    @Override
    public void handleApiError(ANError error) {

        if (error.getErrorCode() != 0) {
            Log.e("onPostCompany", "onErrorList errorCode : " + error.getErrorCode());
            Log.e("onPostCompany", "onErrorList errorBody : " + error.getErrorBody());
            Log.e("onPostCompany", "onErrorList errorDetail : " + error.getErrorDetail());
            Crashlytics.log(1, "handleApiError", error.getErrorBody() + "|||" + error.getErrorDetail());
        } else {
            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
            Log.e("onPostCompany", "onErrorList errorDetail : " + error.getErrorDetail());
        }

        if (error == null || error.getErrorBody() == null) {
            //Crashlytics.log(error.getErrorBody());
            getSerbakerjaView().onError(R.string.message_api_default_error);
            return;
        }

        if (error.getErrorCode() == Constants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.CONNECTION_ERROR)) {
            getSerbakerjaView().onError(R.string.message_connection_error);
            return;
        }

        if (error.getErrorCode() == Constants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.REQUEST_CANCELLED_ERROR)) {
            getSerbakerjaView().onError(R.string.message_api_retry_error);
            return;
        }

        final GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        try {
            ErrorResponse apiError = gson.fromJson(error.getErrorBody(), ErrorResponse.class);

            if (apiError == null || apiError.getMessage() == null) {
                Crashlytics.log(apiError.getMessage());
                getSerbakerjaView().onError(R.string.message_api_default_error);
                return;
            }

            switch (error.getErrorCode()) {
                case HttpsURLConnection.HTTP_UNAUTHORIZED:
                case HttpsURLConnection.HTTP_FORBIDDEN:
                    setUserAsLoggedOut();
                    getSerbakerjaView().openActivityOnTokenExpire();
                case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                case HttpsURLConnection.HTTP_NOT_FOUND:
                default:
                    getSerbakerjaView().onError(apiError.getMessage());
            }
        } catch (JsonSyntaxException | NullPointerException e) {
            Log.e(TAG, "handleApiError", e);
            Crashlytics.log(e.getMessage());
            getSerbakerjaView().onError(R.string.message_api_default_error);
        }
    }

    @Override
    public void LogResponse(JSONObject jsonObject, String path) {
        try {
            String status = jsonObject.getString("status");
            String message = jsonObject.getString("message");

            if (status.equals("success")) {
                Logger.printLogSuccess(path, message, jsonObject.toString());
            } else {
                Logger.printLogBad(path, message, jsonObject.toString());
            }
        } catch (JSONException e) {
            Logger.printLogBad(path, ERROR_MESSAGE_500, jsonObject.toString());
        }
    }

    @Override
    public void setUserAsLoggedOut() {
        getDataManager().setAccessToken(null);
    }

    public static class SerbakerjaViewNotAttachedException extends RuntimeException {
        public SerbakerjaViewNotAttachedException() {
            super("Please call Presenter.onAttach(SerbakerjaView) before" +
                    " requesting data to the Presenter");
        }
    }
}
