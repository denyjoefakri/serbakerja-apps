package com.joefakri.serbakerja.ui.profile.recruiter;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainView;

/**
 * Created by deny on bandung.
 */

public interface RecruiterMainMvpPresenter<V extends RecruiterMainView> extends SerbakerjaPresenter<V> {

}
