package com.joefakri.serbakerja.ui.profile.photo;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.ui.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class DetailPhotoActivity extends BaseActivity {

    @BindView(R.id.img_photo) SubsamplingScaleImageView img_photo;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_detail_photo, this);

        initComponent();
    }

    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_detail_image));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        img_photo.setZoomEnabled(true);
        Glide.with(this).load(getIntent().getStringExtra("image")).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                img_photo.setImage(ImageSource.bitmap(bitmap));
            }
        });
    }
}
