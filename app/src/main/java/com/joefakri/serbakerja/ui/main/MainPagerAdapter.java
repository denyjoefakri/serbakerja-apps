package com.joefakri.serbakerja.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.joefakri.serbakerja.ui.chat.ChatFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonAddressFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonBirthdayFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonDistrictsFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonNameFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonPhotoFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonPhotoKTPFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonRtrwFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonVillageFragment;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardFragment;
import com.joefakri.serbakerja.ui.status.candidate.StatusCandidateFragment;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deny on bandung.
 */

public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<>();

    private void tab(String type){
        clearReference();
        switch (type){
            case "kandidat":
                setupCandidate();
                break;
            case "perekrut":
                setupRecruter();
                break;
        }
    }

    private void setupCandidate(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new DashboardFragment());
        mFragmentList.add(new ChatFragment());
        mFragmentList.add(new StatusCandidateFragment());
    }

    private void setupRecruter(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new DashboardFragment());
        mFragmentList.add(new ChatFragment());
        mFragmentList.add(new StatusRecruiterFragment());
    }

    public MainPagerAdapter(FragmentManager manager, String type) {
        super(manager);
        this.tab(type);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    public void clearReference() {
        if (mFragmentList != null) {
            mFragmentList.clear();
        }
    }
}
