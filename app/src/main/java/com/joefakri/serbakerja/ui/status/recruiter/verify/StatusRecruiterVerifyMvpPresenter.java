package com.joefakri.serbakerja.ui.status.recruiter.verify;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationView;

/**
 * Created by deny on bandung.
 */

public interface StatusRecruiterVerifyMvpPresenter<V extends StatusRecruiterVerifyView> extends SerbakerjaPresenter<V> {

    void onViewApplicant(String offset);

    void onAcceptApplicant(String lamaran_id);

    void onRejectApplicant(String lamaran_id);

}
