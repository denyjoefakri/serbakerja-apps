package com.joefakri.serbakerja.ui.form;

import android.app.Activity;
import android.widget.SeekBar;
import android.widget.TextView;

import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;

/**
 * Created by deny on bandung.
 */

public interface FormOptionalMvpPresenter<V extends FormOptionalView> extends SerbakerjaPresenter<V> {

    void onViewPercent(StateEvent stateEvent);

    void onResetPercent(TextView... textViews);

    void onResetSeekbar(SeekBar... seekBars);

    void onLogin(Activity activity, String type);

}
