package com.joefakri.serbakerja.ui.profile.candidate.experience;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkView;

/**
 * Created by deny on bandung.
 */

public interface CandidateExperienceMvpPresenter<V extends CandidateExperienceView> extends SerbakerjaPresenter<V> {

    void onViewExperience();

}
