package com.joefakri.serbakerja.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.chat.ChatFragment;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardFragment;
import com.joefakri.serbakerja.ui.notification.NotificationActivity;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainActivity;
import com.joefakri.serbakerja.ui.setting.SettingActivity;
import com.joefakri.serbakerja.ui.status.candidate.StatusCandidateFragment;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterFragment;
import com.joefakri.serbakerja.utils.TintableImageView;
import com.joefakri.serbakerja.utils.TintableTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements MainView{

    @Inject MainMvpPresenter<MainView> presenter;
    @Inject DataManager dataManager;

    @BindView(R.id.tab_menu) TabLayout tab_menu;
    @BindView(R.id.viewpager_content) ViewPager viewpager_content;
    @BindView(R.id.btn_setting) ImageView btn_setting;
    @BindView(R.id.btn_notification) ImageView btn_notification;
    MainPagerAdapter adapter;

    String[] title = {"Dashboard", "Chat", "Status"};
    int[] image = {R.drawable.ic_dashboard, R.drawable.ic_chat, R.drawable.ic_status};
    private EventBus bus = EventBus.getDefault();
    StateEvent state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_main, this);
        getActivityComponent().inject(this);
        state = new StateEvent();

        presenter.onAttach(this);
        initComponent();

        /*if (dataManager.getCurrentUserId() != null)
            Crashlytics.setString("pengguna_id", dataManager.getCurrentUserId());*/
    }

    @Override
    protected void initComponent() {
        presenter.ViewTab();
        setupTabIcons();

        btn_setting.setOnClickListener(view -> {
            presenter.onClickProfile();
        });
        btn_notification.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, NotificationActivity.class)));

        presenter.updateFCM();

        StateEvent stateEvent = new StateEvent();
        stateEvent.setBasestate("0");
        bus.postSticky(stateEvent);

    }

    @Override
    public void onViewTab(String type) {
        adapter = new MainPagerAdapter(getSupportFragmentManager(), type);
        viewpager_content.setAdapter(adapter);
        viewpager_content.setOffscreenPageLimit(0);

        viewpager_content.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.e("positi", ""+position);
                StateEvent stateEvent = new StateEvent();
                //stateEvent.setType(0);
                stateEvent.setBasestate(String.valueOf(position));
                bus.postSticky(stateEvent);
            }
        });
    }

    private void setupTabIcons() {
        tab_menu.setupWithViewPager(viewpager_content);
        tab_menu.getTabAt(0).setCustomView(getTabview(0));
        tab_menu.getTabAt(1).setCustomView(getTabview(1));
        tab_menu.getTabAt(2).setCustomView(getTabview(2));
    }

    public View getTabview(int position){
        View v = LayoutInflater.from(this).inflate(R.layout.tab_view, null);
        TintableTextView tab_title = v.findViewById(R.id.tab_title);
        TintableImageView tab_image = v.findViewById(R.id.tab_image);
        tab_title.setText(title[position]);
        tab_image.setImageResource(image[position]);
        return v;
    }

    @Override
    public void onClickProfile(String session) {
        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
        intent.putExtra("role", session);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        adapter.getItem(0).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        adapter.getItem(0).onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.state = state;
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
        if (adapter != null) {
            adapter.clearReference();
            adapter = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (state.getStatus().equals("2")){
            state.setStatus("1");
            bus.postSticky(state);
        } else super.onBackPressed();
    }
}
