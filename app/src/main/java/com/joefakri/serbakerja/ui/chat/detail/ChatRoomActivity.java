package com.joefakri.serbakerja.ui.chat.detail;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.ChatMessageAdapter;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.ReceiverMessageModel;
import com.joefakri.serbakerja.service.MyFirebaseMessagingService;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.recruters.DetailRecrutersActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.EasyPermissions;

public class ChatRoomActivity extends BaseActivity implements ChatRoomView, ChatMessageAdapter.Listener, EasyPermissions.PermissionCallbacks{

    @Inject ChatRoomMvpPresenter<ChatRoomView> presenter;
    @Inject ChatMessageAdapter chatMessageAdapter;
    @Inject LinearLayoutManager mLayoutManager;
    @Inject DataManager mDataManager;
    private Parcelable recyclerViewState;

    @BindView(R.id.btn_profile) LinearLayout btn_profile;
    @BindView(R.id.img_user) CircleImageView img_user;
    @BindView(R.id.txt_name) TextView txt_name;
    @BindView(R.id.rv_chat_room) RecyclerView rv_chat_room;
    @BindView(R.id.ed_message) EditText ed_message;
    @BindView(R.id.btn_send) ImageButton btn_send;
    @BindView(R.id.btn_call) ImageView btn_call;

    BroadcastReceiver broadcastReceiver = getDataFirebase();
    String targetId = "";
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_chat_room, this);
        getActivityComponent().inject(this);

        presenter.onAttach(ChatRoomActivity.this);

        initComponent();
    }

    @Override
    protected void initComponent() {
        Glide.with(this).load(getIntent().getStringExtra("image")).into(img_user);
        txt_name.setText(getIntent().getStringExtra("name"));
        btn_profile.setOnClickListener(view -> {
            Intent intent;
            if (mDataManager.getTypeUser().equals("perekrut")){
                intent = new Intent(this, DetailCandidateActivity.class);
                intent.putExtra("id", getIntent().getStringExtra("target"));
                startActivity(intent);
            } else {
                intent = new Intent(this, DetailRecrutersActivity.class);
                intent.putExtra("id", getIntent().getStringExtra("target"));
                startActivity(intent);
            }
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_chat_room.setLayoutManager(mLayoutManager);
        rv_chat_room.setNestedScrollingEnabled(false);
        rv_chat_room.setItemAnimator(new DefaultItemAnimator());
        rv_chat_room.setAdapter(chatMessageAdapter = new ChatMessageAdapter(this));
        chatMessageAdapter.setOnclickListener(this);


        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.cancel(11);
        }

        recyclerViewState = rv_chat_room.getLayoutManager().onSaveInstanceState();
        rv_chat_room.getLayoutManager().onRestoreInstanceState(recyclerViewState);

        Log.e("target", getIntent().getStringExtra("target"));
        targetId = getIntent().getStringExtra("target");
        presenter.onViewPrepared(targetId);
        presenter.onRead(targetId);

        btn_send.setOnClickListener(view -> {
            if (!ed_message.getText().toString().isEmpty()){

                String idMessagge = String.valueOf(TimeUtils.getMilis(TimeUtils.getCurrentDateTime()) + id);
                id++;

                ChatResponse.Retrieve.Data data = new ChatResponse.Retrieve.Data();
                data.setFrom_id(mDataManager.getCurrentUserId());
                data.setId(idMessagge);
                data.setTo_id(targetId);
                data.setPesan(ed_message.getText().toString());
                data.setCreated_on(TimeUtils.getCurrentDateTime());
                data.setVisible(false);
                data.setStatus_sending(0);
                chatMessageAdapter.addChat(data, mDataManager.getCurrentUserId());
                rv_chat_room.smoothScrollToPosition(chatMessageAdapter.getItemCount() - 1);

                presenter.onSend(targetId, idMessagge, ed_message.getText().toString());
                ed_message.setText("");
            }
        });

        btn_call.setOnClickListener(view -> {
            presenter.permissionCall(this, getIntent().getStringExtra("phone"));
        });
    }

    @Override
    public void onClick(ChatResponse.Retrieve.Data chatModel) {

    }

    @Override
    public void update(ArrayList<ChatResponse.Retrieve.Data> dataList, String userId) {
        String temporaryTime = "";
        for (int i = 0; i < dataList.size(); i++) {
            if (temporaryTime.equals(TimeUtils.getDate(dataList.get(i).getCreated_on()))){
                dataList.get(i).setVisible(false);
            } else {
                temporaryTime = TimeUtils.getDate(dataList.get(i).getCreated_on());
                dataList.get(i).setVisible(true);
            }
        }
        chatMessageAdapter.update(dataList, userId);
        rv_chat_room.smoothScrollToPosition(chatMessageAdapter.getItemCount() - 1);
    }

    @Override
    public void onResultSend(ChatResponse.Send dataa, String userId, String idMessage) {
        if (dataa.getStatus().equals("success")) {
            chatMessageAdapter.getChat(idMessage).setStatus_sending(1);
            chatMessageAdapter.notifyDataSetChanged();
        } else chatMessageAdapter.remove(idMessage);

    }

    @Override
    public void onErrorSend(String idMessage) {
        chatMessageAdapter.remove(idMessage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter("message"));
        registerReceiver(broadcastReceiver, new IntentFilter("status"));
    }


    public BroadcastReceiver getDataFirebase() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ReceiverMessageModel model = (ReceiverMessageModel) intent.getSerializableExtra(MyFirebaseMessagingService.TAG);
                if (intent.getAction().equals("message")){
                    ChatResponse.Retrieve.Data data = new ChatResponse.Retrieve.Data();
                    data.setId(model.getPercakapan_id());
                    data.setFrom_id(model.getFrom_id());
                    data.setTo_id(model.getTo_id());
                    data.setPesan(model.getMessage());
                    data.setCreated_on(model.getDate());
                    data.setVisible(false);
                    chatMessageAdapter.addChat(data, mDataManager.getCurrentUserId());
                    rv_chat_room.smoothScrollToPosition(chatMessageAdapter.getItemCount() - 1);
                    presenter.onRead(targetId);
                } else {
                    /*chatMessageAdapter.getChat(model.getPercakapan_id()).setStatus_sending(Integer.parseInt(model.getStatus_message()));
                    chatMessageAdapter.notifyDataSetChanged();*/
                }
            }
        };
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == 10){
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+getIntent().getStringExtra("phone")));
            startActivity(callIntent);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_callphone);
            dialog.setOnMessageClosed(() -> {
                if (requestCode == 10) {
                    presenter.permissionCall(this, getIntent().getStringExtra("phone"));
                }
            });
        }
    }

    @OnClick(R.id.btn_back)
    public void back(){
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


}
