package com.joefakri.serbakerja.ui.form.company;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ImagePickerConfig;
import com.esafirm.imagepicker.features.camera.CameraModule;
import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.esafirm.imagepicker.features.camera.ImmediateCameraModule;
import com.esafirm.imagepicker.features.camera.OnImageReadyListener;
import com.esafirm.imagepicker.model.Image;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;
import static com.joefakri.serbakerja.utils.Constants.RC_CAMERA;
import static com.joefakri.serbakerja.utils.Constants.RC_CODE_PICKER;

/**
 * Created by deny on bandung.
 */

public class FormCompanyLogoFragment extends BaseFragment implements FormCompanyView, EasyPermissions.PermissionCallbacks{

    @Inject FormCompanyMvpPresenter<FormCompanyView> mPresenter;
    @BindView(R.id.btn_upload) Button btn_upload;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.img_photo) ImageView img_photo;

    private String fileToSend;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_company_logo, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view2 -> {

            if (fileToSend == null){
                onError("Foto tidak boleh kosong");
                return;
            }

            MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_1_perekrut);
            dialog.setOnMessageClosed(() -> {
                mPresenter.onPostCompany();
            });


        });

        btn_upload.setOnClickListener(view2 -> {
            mPresenter.readCameraGallery(getActivity(), this, view2);
        });


    }


    @Override
    public void init(RecruiterRealm recruiterRealm) {
        if (recruiterRealm.getCompanyLogo() != null) {
            Glide.with(this).load(recruiterRealm.getCompanyLogo()).into(img_photo);
            fileToSend = recruiterRealm.getCompanyLogo();
            btn_next.setEnabled(true);
        } else
            btn_next.setEnabled(false);
    }

    @Override
    public void takePhoto(String file) {
        fileToSend = file;
        Log.e("path", fileToSend);
        Glide.with(this).load(fileToSend).into(img_photo);
        mPresenter.saveCompanyLogo(fileToSend);
        btn_next.setEnabled(true);
    }

    @Override
    public void openGallery(String file) {
        fileToSend = file;
        Log.e("path", fileToSend);
        Glide.with(this).load(fileToSend).into(img_photo);
        mPresenter.saveCompanyLogo(fileToSend);
        btn_next.setEnabled(true);
    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultCompany(RecruiterResponse.CompanyResponse.Data companyResponses) {
        getActivity().finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        mPresenter.openImageChooser(getActivity(), btn_upload);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                mPresenter.readCameraGallery(getActivity(), this, btn_upload);
            });
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }


}
