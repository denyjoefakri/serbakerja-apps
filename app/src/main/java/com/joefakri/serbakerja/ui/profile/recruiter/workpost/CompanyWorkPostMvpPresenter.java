package com.joefakri.serbakerja.ui.profile.recruiter.workpost;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfileView;

/**
 * Created by deny on bandung.
 */

public interface CompanyWorkPostMvpPresenter<V extends CompanyWorkPostView> extends SerbakerjaPresenter<V> {

    void onViewWork(String offset);

}
