package com.joefakri.serbakerja.ui.profile.password;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileView;

/**
 * Created by deny on bandung.
 */

public interface EditPasswordMvpPresenter<V extends EditPasswordView> extends SerbakerjaPresenter<V> {

    void onEditPassword(String... s);

}
