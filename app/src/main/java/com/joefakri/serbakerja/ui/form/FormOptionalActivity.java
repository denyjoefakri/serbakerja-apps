package com.joefakri.serbakerja.ui.form;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.form.work.FormWorkTypeSallaryFragment;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.ui.widget.WrapContentHeightViewPager;
import com.levibostian.shutter_android.builder.ShutterResultListener;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;

public class FormOptionalActivity extends BaseActivity implements FormOptionalView{

    @Inject FormOptionalMvpPresenter<FormOptionalView> mPresenter;
    FormOptionalPagerAdapter pagerAdapter;

    @BindView(R.id.btn_candidate) LinearLayout btn_candidate;
    @BindView(R.id.btn_recruiters) LinearLayout btn_recruiters;
    @BindView(R.id.optional_form_kandidat) LinearLayout optional_form_kandidat;
    @BindView(R.id.optional_form_perekrut) LinearLayout optional_form_perekrut;
    @BindView(R.id.view_form) LinearLayout view_form;
    @BindView(R.id.candidate_checked) ImageView candidate_checked;
    @BindView(R.id.recruiters_checked) ImageView recruiters_checked;
    @BindView(R.id.txt_choose_form) TextView txt_choose_form;
    @BindView(R.id.btn_form_1_perekrut) View btn_form_1_perekrut;
    @BindView(R.id.sb_form_1_perekrut) SeekBar sb_form_1_perekrut;
    @BindView(R.id.btn_form_2_perekrut) View btn_form_2_perekrut;
    @BindView(R.id.sb_form_2_perekrut) SeekBar sb_form_2_perekrut;
    @BindView(R.id.btn_form_3_perekrut) View btn_form_3_perekrut;
    @BindView(R.id.sb_form_3_perekrut) SeekBar sb_form_3_perekrut;
    @BindView(R.id.btn_form_1_candidate) View btn_form_1_candidate;
    @BindView(R.id.sb_form_1_candidate) SeekBar sb_form_1_candidate;
    @BindView(R.id.btn_form_2_candidate) View btn_form_2_candidate;
    @BindView(R.id.sb_form_2_candidate) SeekBar sb_form_2_candidate;
    @BindView(R.id.btn_form_3_candidate) View btn_form_3_candidate;
    @BindView(R.id.sb_form_3_candidate) SeekBar sb_form_3_candidate;
    @BindView(R.id.btn_form_4_candidate) View btn_form_4_candidate;
    @BindView(R.id.sb_form_4_candidate) SeekBar sb_form_4_candidate;
    @BindView(R.id.btn_form_5_candidate) View btn_form_5_candidate;
    @BindView(R.id.sb_form_5_candidate) SeekBar sb_form_5_candidate;
    @BindView(R.id.txt_percent_form_1_candidate) TextView txt_percent_form_1_candidate;
    @BindView(R.id.txt_percent_form_2_candidate) TextView txt_percent_form_2_candidate;
    @BindView(R.id.txt_percent_form_3_candidate) TextView txt_percent_form_3_candidate;
    @BindView(R.id.txt_percent_form_4_candidate) TextView txt_percent_form_4_candidate;
    @BindView(R.id.txt_percent_form_5_candidate) TextView txt_percent_form_5_candidate;
    @BindView(R.id.txt_percent_form_1_perekrut) TextView txt_percent_form_1_perekrut;
    @BindView(R.id.txt_percent_form_2_perekrut) TextView txt_percent_form_2_perekrut;
    @BindView(R.id.txt_percent_form_3_perekrut) TextView txt_percent_form_3_perekrut;
    @BindView(R.id.txt_title) TextView txt_title;
    @BindView(R.id.view_indicator) InkPageIndicator view_indicator;
    @BindView(R.id.view_pager_tab) public WrapContentHeightViewPager view_pager_tab;

    private EventBus bus = EventBus.getDefault();
    private String state = "";
    private String stateForm = "";
    public ShutterResultListener shutterListener;
    int percentForm = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_optional_form, this);
        getActivityComponent().inject(this);

        mPresenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        btn_candidate.setOnClickListener(view -> {
            if (percentForm == 0){
                clickTab(0);
                txt_choose_form.setVisibility(View.GONE);
                optional_form_perekrut.setVisibility(View.GONE);
                optional_form_kandidat.setVisibility(View.VISIBLE);
                view_form.setVisibility(View.GONE);
                bus.postSticky(new StateEvent("c0",""));
            } else {

                MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_tab);
                dialog.setOnMessageClosed(() -> {
                    percentForm = 0;
                    clickTab(0);
                    txt_choose_form.setVisibility(View.GONE);
                    optional_form_perekrut.setVisibility(View.GONE);
                    optional_form_kandidat.setVisibility(View.VISIBLE);
                    view_form.setVisibility(View.GONE);
                    bus.postSticky(new StateEvent("c0",""));
                });


            }
        });

        btn_recruiters.setOnClickListener(view -> {
            if (percentForm == 0){
                clickTab(1);
                txt_choose_form.setVisibility(View.GONE);
                optional_form_kandidat.setVisibility(View.GONE);
                optional_form_perekrut.setVisibility(View.VISIBLE);
                view_form.setVisibility(View.GONE);
                bus.postSticky(new StateEvent("r0",""));
            } else {
                MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_tab);
                dialog.setOnMessageClosed(() -> {
                    percentForm = 0;
                    clickTab(1);
                    txt_choose_form.setVisibility(View.GONE);
                    optional_form_kandidat.setVisibility(View.GONE);
                    optional_form_perekrut.setVisibility(View.VISIBLE);
                    view_form.setVisibility(View.GONE);
                    bus.postSticky(new StateEvent("r0",""));
                });

            }

        });

        sb_form_1_perekrut.setEnabled(false);
        btn_form_1_perekrut.setOnClickListener(view -> {
            optional_form_perekrut.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_1_perekrut));
            bus.postSticky(new StateEvent("r1",""));
            setupViewPager1_recruiters();
        });

        sb_form_2_perekrut.setEnabled(false);
        btn_form_2_perekrut.setOnClickListener(view -> {
            optional_form_perekrut.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_2_perekrut));
            bus.postSticky(new StateEvent("r1",""));
            setupViewPager2_recruiters();
        });

        sb_form_3_perekrut.setEnabled(false);
        btn_form_3_perekrut.setOnClickListener(view -> {
            optional_form_perekrut.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_3_perekrut));
            bus.postSticky(new StateEvent("r1",""));
            setupViewPager3_recruiters();
        });

        sb_form_1_candidate.setEnabled(false);
        btn_form_1_candidate.setOnClickListener(view -> {
            optional_form_kandidat.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_1_kandidat));
            bus.postSticky(new StateEvent("c1",""));
            setupViewPager1_candidate();
        });

        sb_form_2_candidate.setEnabled(false);
        btn_form_2_candidate.setOnClickListener(view -> {
            optional_form_kandidat.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_2_kandidat));
            bus.postSticky(new StateEvent("c1",""));
            setupViewPager2_candidate();
        });

        sb_form_3_candidate.setEnabled(false);
        btn_form_3_candidate.setOnClickListener(view -> {
            optional_form_kandidat.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_3_kandidat));
            bus.postSticky(new StateEvent("c1",""));
            setupViewPager3_candidate();
        });


        sb_form_4_candidate.setEnabled(false);
        btn_form_4_candidate.setOnClickListener(view -> {
            optional_form_kandidat.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_4_kandidat));
            bus.postSticky(new StateEvent("c1",""));
            setupViewPager4_candidate();
        });

        sb_form_5_candidate.setEnabled(false);
        btn_form_5_candidate.setOnClickListener(view -> {
            optional_form_kandidat.setVisibility(View.GONE);
            view_form.setVisibility(View.VISIBLE);
            txt_title.setText(getString(R.string.txt_form_5_kandidat));
            bus.postSticky(new StateEvent("c1",""));
            setupViewPager5_candidate();
        });

        ViewPager.OnPageChangeListener pagerSynchronizer = getPagerSynchronizer();
        view_pager_tab.addOnPageChangeListener(pagerSynchronizer);
        view_pager_tab.setAllowedSwipeDirection(WrapContentHeightViewPager.SwipeDirection.left);

    }

    public void setupViewPager1_recruiters() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 1);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager2_recruiters() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 2);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager3_recruiters() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 3);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager1_candidate() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 11);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager2_candidate() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 12);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager3_candidate() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 13);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager4_candidate() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 14);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    public void setupViewPager5_candidate() {
        pagerAdapter = new FormOptionalPagerAdapter(getSupportFragmentManager(), 15);
        view_pager_tab.setAdapter(pagerAdapter);
        view_indicator.setViewPager(view_pager_tab);
    }

    @Override
    public void setPercent(int type, int percent) {
        this.percentForm = percent;
        switch (type){
            case 1:
                txt_percent_form_1_perekrut.setText(String.valueOf(percent)+"%");
                sb_form_1_perekrut.setProgress(percent);
                if (percent == 100) {
                    btn_candidate.setEnabled(false);
                    btn_form_1_perekrut.setEnabled(false);
                }
                break;
            case 2:
                txt_percent_form_2_perekrut.setText(String.valueOf(percent)+"%");
                sb_form_2_perekrut.setProgress(percent);
                if (percent == 100) {
                    btn_candidate.setEnabled(false);
                    btn_form_2_perekrut.setEnabled(false);
                }
                break;
            case 3:
                txt_percent_form_3_perekrut.setText(String.valueOf(percent)+"%");
                sb_form_3_perekrut.setProgress(percent);
                if (percent == 100) {
                    btn_candidate.setEnabled(false);
                    btn_form_3_perekrut.setEnabled(false);
                }
                break;
            case 11:
                txt_percent_form_1_candidate.setText(String.valueOf(percent)+"%");
                sb_form_1_candidate.setProgress(percent);
                if (percent == 100) {
                    btn_recruiters.setEnabled(false);
                    btn_form_1_candidate.setEnabled(false);
                }
                break;
            case 22:
                txt_percent_form_2_candidate.setText(String.valueOf(percent)+"%");
                sb_form_2_candidate.setProgress(percent);
                if (percent == 100) {
                    btn_recruiters.setEnabled(false);
                    btn_form_2_candidate.setEnabled(false);
                }
                break;
            case 33:
                txt_percent_form_3_candidate.setText(String.valueOf(percent)+"%");
                sb_form_3_candidate.setProgress(percent);
                if (percent == 100) {
                    btn_recruiters.setEnabled(false);
                    btn_form_3_candidate.setEnabled(false);
                }
                break;
            case 44:
                txt_percent_form_4_candidate.setText(String.valueOf(percent)+"%");
                sb_form_4_candidate.setProgress(percent);
                if (percent == 100) {
                    btn_recruiters.setEnabled(false);
                    btn_form_4_candidate.setEnabled(false);
                }
                break;
            case 55:
                txt_percent_form_5_candidate.setText(String.valueOf(percent)+"%");
                sb_form_5_candidate.setProgress(percent);
                if (percent == 100) {
                    btn_recruiters.setEnabled(false);
                    btn_form_5_candidate.setEnabled(false);
                }
                break;
        }

        if (txt_percent_form_1_perekrut.getText().toString().equals("100%") &&
                txt_percent_form_2_perekrut.getText().toString().equals("100%") &&
                txt_percent_form_3_perekrut.getText().toString().equals("100%")){
            mPresenter.onLogin(this, "perekrut");
        }

        if (txt_percent_form_1_candidate.getText().toString().equals("100%") &&
                txt_percent_form_2_candidate.getText().toString().equals("100%") &&
                txt_percent_form_3_candidate.getText().toString().equals("100%") &&
                txt_percent_form_4_candidate.getText().toString().equals("100%") &&
                txt_percent_form_5_candidate.getText().toString().equals("100%")){
            mPresenter.onLogin(this, "kandidat");
        }
    }


    private void clickTab(int position){
        bus.postSticky(new FormEvent());
        mPresenter.onResetPercent(txt_percent_form_1_perekrut, txt_percent_form_2_perekrut, txt_percent_form_3_perekrut,
                txt_percent_form_1_candidate, txt_percent_form_2_candidate, txt_percent_form_3_candidate,
                txt_percent_form_4_candidate, txt_percent_form_5_candidate);
        mPresenter.onResetSeekbar(sb_form_1_perekrut, sb_form_2_perekrut, sb_form_3_perekrut,
                sb_form_1_candidate, sb_form_2_candidate, sb_form_3_candidate, sb_form_4_candidate, sb_form_5_candidate);
        Drawable leftSelectedBackgroud = ContextCompat.getDrawable(this, R.drawable.bg_button_tab_left_selected);
        Drawable leftUnSelectedBackgroud = ContextCompat.getDrawable(this, R.drawable.bg_button_tab_left);
        Drawable rightSelectedBackgroud = ContextCompat.getDrawable(this, R.drawable.bg_button_tab_right_selected);
        Drawable rightUnSelectedBackgroud = ContextCompat.getDrawable(this, R.drawable.bg_button_tab_right);

        recruiters_checked.setVisibility(View.GONE);
        btn_recruiters.setBackground(rightUnSelectedBackgroud);
        candidate_checked.setVisibility(View.GONE);
        btn_candidate.setBackground(leftUnSelectedBackgroud);

        if (position == 0){
            btn_candidate.setBackground(leftSelectedBackgroud);
            candidate_checked.setVisibility(View.VISIBLE);
            btn_candidate.setEnabled(false);
            btn_recruiters.setEnabled(true);
        } else if (position == 1){
            btn_recruiters.setBackground(rightSelectedBackgroud);
            recruiters_checked.setVisibility(View.VISIBLE);
            btn_candidate.setEnabled(true);
            btn_recruiters.setEnabled(false);
        }
    }

    private ViewPager.OnPageChangeListener getPagerSynchronizer() {
        return new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.e("position", ""+position);
                view_pager_tab.setCurrentItem(position, true);
                if (position == 0) bus.postSticky(new StateEvent(state, ""));
                else bus.postSticky(new StateEvent(state, String.valueOf(position)));
            }
        };
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (shutterListener != null) {
            if (!shutterListener.onActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            //pagerAdapter.getItem(0).onActivityResult(requestCode, resultCode, data);
        }
        //super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.state = state.getState();
        this.stateForm = state.getState_form();
        mPresenter.onViewPercent(state);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onRecruiterEvent(FormEvent state){

    }

    @Override
    protected void onDestroy() {
        clearPager();
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!stateForm.isEmpty()){
            switch (stateForm){
                case "1": view_pager_tab.setCurrentItem(0); stateForm = ""; break;
                case "2": view_pager_tab.setCurrentItem(1); break;
                case "3": view_pager_tab.setCurrentItem(2); break;
                case "4": view_pager_tab.setCurrentItem(3); break;
                case "5": view_pager_tab.setCurrentItem(4); break;
                case "6": view_pager_tab.setCurrentItem(5); break;
                case "7": view_pager_tab.setCurrentItem(6); break;
                case "8": view_pager_tab.setCurrentItem(7); break;
                case "9": view_pager_tab.setCurrentItem(8); break;
                case "10": view_pager_tab.setCurrentItem(9); break;
                case "11": view_pager_tab.setCurrentItem(10); break;
                case "12": view_pager_tab.setCurrentItem(11); break;
            }
        } else {

            /*if (txt_choose_form.getVisibility() == View.VISIBLE){
                showConfirmMessage(R.string.message_save_bio).setOnMessageClosed(super::onBackPressed);
            }*/

            if (state.equals("c0") && !state_first_candidate() || state.equals("r0") && !state_first_recruter()){
                percentForm = 0;
                txt_choose_form.setVisibility(View.VISIBLE);
                optional_form_perekrut.setVisibility(View.GONE);
                optional_form_kandidat.setVisibility(View.GONE);
                btn_recruiters.setEnabled(true);
                btn_candidate.setEnabled(true);
                clickTab(2);
                state = "";
            } else if (state.equals("c1") && stateForm.equals("")){
                last_form_candidate();
            } else if (state.equals("r1") && stateForm.equals("")){
                last_form_recruiter();
            } else {
                MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_4_kandidat);
                dialog.setOnMessageClosed(super::onBackPressed);

            }
        }
    }

    private boolean state_first_candidate(){
        if (state.equals("c0") && txt_percent_form_1_candidate.getText().toString().equals("100%")){
            return true;
        } else if (state.equals("c0") && txt_percent_form_2_candidate.getText().toString().equals("100%")){
            return true;
        } else if (state.equals("c0") && txt_percent_form_3_candidate.getText().toString().equals("100%")){
            return true;
        } else if (state.equals("c0") && txt_percent_form_4_candidate.getText().toString().equals("100%")){
            return true;
        } else if (state.equals("c0") && txt_percent_form_5_candidate.getText().toString().equals("100%")){
            return true;
        } else {
            return false;
        }
    }

    private boolean state_first_recruter(){
        if (state.equals("r0") && txt_percent_form_1_perekrut.getText().toString().equals("100%")){
            return true;
        } else if (state.equals("r0") && txt_percent_form_2_perekrut.getText().toString().equals("100%")){
            return true;
        } else if (state.equals("r0") && txt_percent_form_3_perekrut.getText().toString().equals("100%")){
            return true;
        } else {
            return false;
        }
    }

    public void last_form_candidate(){
        clearPager();
        optional_form_kandidat.setVisibility(View.VISIBLE);
        optional_form_perekrut.setVisibility(View.GONE);
        view_form.setVisibility(View.GONE);
        bus.postSticky(new StateEvent("c0",""));
    }

    public void last_form_recruiter(){
        clearPager();
        optional_form_kandidat.setVisibility(View.GONE);
        optional_form_perekrut.setVisibility(View.VISIBLE);
        view_form.setVisibility(View.GONE);
        bus.postSticky(new StateEvent("r0",""));
    }

    private void clearPager(){
        if (pagerAdapter != null) {
            pagerAdapter.clearReference();
            pagerAdapter = null;
        }
    }
}
