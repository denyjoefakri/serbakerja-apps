package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkEducationFragment extends BaseFragment implements FormWorkView {

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.rd_education) RadioGroup rd_education;
    RadioButton rd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_education, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {

            if (rd_education.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu minimal pendidikan yang tersedia");
                return;
            }

            mPresenter.saveRecruterWorkEducation(Constants.educationPosition(getActivity(), get()));
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(6);
        });
    }

    private String get(){
        int selectedId= rd_education.getCheckedRadioButtonId();
        rd = getActivity().findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm.getWorkEducation() != null)
            ((RadioButton)rd_education.getChildAt(Integer.parseInt(recruiterRealm.getWorkEducation()))).setChecked(true);
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }
}
