package com.joefakri.serbakerja.ui.form.company;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormCompanyTotalEmployeeFragment extends BaseFragment implements FormCompanyView{

    @Inject FormCompanyMvpPresenter<FormCompanyView> mPresenter;
    @BindView(R.id.rd_employee) RadioGroup rd_employee;
    RadioButton rd;
    @BindView(R.id.btn_next) Button btn_next;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_company_total_employees, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {

            if (rd_employee.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu jumlah karyawan yang tersedia");
                return;
            }

            mPresenter.saveCompanyTotalEmployee(Constants.getPositionTotalEmployee(getActivity(), get()));
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(4);
        });

    }

    private String get(){
        int selectedId= rd_employee.getCheckedRadioButtonId();
        rd = getActivity().findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm) {
        if (recruiterRealm.getCompanyTotalEmployeCode() != null)
            ((RadioButton)rd_employee.getChildAt(Integer.parseInt(recruiterRealm.getCompanyTotalEmployeCode()))).setChecked(true);
    }

    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultCompany(RecruiterResponse.CompanyResponse.Data companyResponses) {

    }

}
