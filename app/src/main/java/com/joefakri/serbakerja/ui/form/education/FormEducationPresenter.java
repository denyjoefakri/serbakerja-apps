/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form.education;

import android.app.Activity;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardMvpPresenter;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardView;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;

/**
 * Created by janisharali on 27/01/17.
 */

public class FormEducationPresenter<V extends FormEducationView> extends BasePresenter<V>
        implements FormEducationMvpPresenter<V> {

    private Realm realm;

    @Inject
    public FormEducationPresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onPostCandidateEducation(Activity activity) {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        CandidateRequest.PostEducation education = new CandidateRequest.PostEducation(getDataManager().getCurrentUserId(),
                candidate.getEducationGraduated(), candidate.getEducationLast(),
                candidate.getEducationNameLastSchool(), candidate.getEducationNameCourse());

        Logger.printLogRequest(Path.POST_CANDIDATE_EDUCATION, education, null);
        getCompositeDisposable().add(getDataManager()
                .postCEducation(education)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_CANDIDATE_EDUCATION);
                    Gson gson = new Gson();
                    CandidateResponse.EducationResponse response = gson.fromJson(jsonObject.toString(), CandidateResponse.EducationResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCandidateEducation(response.getData());
                        candidate.setPercent4(4);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void init() {
        realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        if (candidateRealm != null) getSerbakerjaView().init(candidateRealm);
    }

    @Override
    public void saveEducationLast(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setEducationLast(data);
            candidate.setPercent4(1);
        } else {
            candidate = realm.createObject(CandidateRealm.class);
            candidate.setId(1);
            candidate.setEducationLast(data);
            candidate.setPercent4(1);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveEducationGraduated(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setEducationGraduated(data);
            candidate.setPercent4(2);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveEducationNameCourses(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setEducationNameCourse(data);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveEducationNameLastSchool(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setEducationNameLastSchool(data);
            candidate.setPercent4(3);
        }
        realm.commitTransaction();
        realm.close();
    }
}
