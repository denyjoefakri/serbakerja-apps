package com.joefakri.serbakerja.ui.form.person;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormPersonNameFragment extends BaseFragment implements FormPersonView{

    @Inject FormPersonMvpPresenter<FormPersonView> mPresenter;
    @Inject DataManager dataManager;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_person_name) EditText txt_person_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_person_name, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (txt_person_name.getText().toString().isEmpty() ) {
                onError("Nama tidak boleh kosong");
                return;
            }

            if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)) {
                mPresenter.saveRecruterPersonName(txt_person_name.getText().toString());
            } else mPresenter.saveCandidatePersonName(txt_person_name.getText().toString());
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(1);
        });

        txt_person_name.setText(dataManager.getCurrentUserName());
    }

    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void onResultCity(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultDistrict(List<AreaResponse.DistrictResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterInfo(RecruiterResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void onResultCandidateInfo(CandidateResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void initRecruter(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm != null){
            if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)){
                if (recruiterRealm.getPersonName() != null)
                    txt_person_name.setText(recruiterRealm.getPersonName());
            }
        }

        if (candidateRealm != null) {
            if (dataManager.getTypeUser().equals(Constants.ROLE_KANDIDAT)){
                if (candidateRealm.getPersonName() != null)
                    txt_person_name.setText(candidateRealm.getPersonName());
            }
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
