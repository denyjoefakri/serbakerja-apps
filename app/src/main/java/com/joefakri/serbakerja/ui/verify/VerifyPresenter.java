/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.verify;

import android.app.Activity;
import android.os.CountDownTimer;

import com.androidnetworking.error.ANError;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class VerifyPresenter<V extends VerifyView> extends BasePresenter<V>
        implements VerifyMvpPresenter<V> {

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    private FirebaseAuth mAuth;

    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private CountDownTimer timer;

    @Inject
    public VerifyPresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void initPhoneAuth(Activity activity) {

        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                mVerificationInProgress = false;
                signInWithPhoneAuthCredential(activity, credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                mVerificationInProgress = false;
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    getSerbakerjaView().showMessageDialog(R.string.message_invalid_phone_number);
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    getSerbakerjaView().showMessageDialog(R.string.message_quota_exceeded);
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                getSerbakerjaView().hideLoading();
                getSerbakerjaView().onCodeSent(verificationId, token);
            }
        };
    }

    @Override
    public void startPhoneNumberVerification(Activity activity, String phoneNumber) {
        getSerbakerjaView().showLoading();
        mAuth.signOut();
        initPhoneAuth(activity);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 120, TimeUnit.SECONDS, activity, mCallbacks);
        mVerificationInProgress = true;
    }

    private void signInWithPhoneAuthCredential(Activity activity, PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        getSerbakerjaView().signinSuccess(task.getResult().getUser());
                    } else {
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            getSerbakerjaView().showMessageDialog(R.string.message_invalid_code);
                        }
                    }
                });
    }

    @Override
    public void verifyPhoneNumberWithCode(Activity activity, String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(activity, credential);
    }

    @Override
    public void resendVerificationCode(Activity activity, String phoneNumber, PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 120, TimeUnit.SECONDS, activity, mCallbacks, token);
    }

    @Override
    public void countdown() {
        timer = new CountDownTimer(TimeUtils.getTwoMinute(), 1000) {
            public void onTick(long millisUntilFinished) {
                long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);

                millisUntilFinished -= TimeUnit.DAYS.toMillis(days);
                long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);
                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                String min = String.valueOf(minutes);
                String sec = String.valueOf(seconds);
                if (min.length() == 1) min = "0" + min;
                if (sec.length() == 1) sec = "0" + sec;

                getSerbakerjaView().onCount(minutes, seconds);

            }

            public void onFinish() {
                getSerbakerjaView().finishCountdown();
            }
        }.start();
    }

    @Override
    public void cancelCountdown() {
        if (timer != null) timer.cancel();
    }

    @Override
    public void updateStatusVerification(String phone_number) {
        getSerbakerjaView().showLoading();

        UserRequest.Verification verification = new UserRequest.Verification(getDataManager().getCurrentUserId(), phone_number);
        Logger.printLogRequest(Path.VERIFIKASI, verification, null);

        getCompositeDisposable().add(getDataManager()
                .verifikasi(verification)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.VERIFIKASI);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    UserResponse.Register response = gson.fromJson(jsonObject.toString(), UserResponse.Register.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultUpdateStatus();
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().hideLoading();
                    }

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
