package com.joefakri.serbakerja.ui.profile.candidate.work_edit;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostView;

/**
 * Created by deny on bandung.
 */

public interface EditWorkMvpPresenter<V extends EditWorkView> extends SerbakerjaPresenter<V> {


    void onViewCity();

    void onEditWork(String... s);

    void onInsertWork(String... s);

}
