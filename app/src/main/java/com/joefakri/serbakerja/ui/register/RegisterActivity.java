package com.joefakri.serbakerja.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.bio.ChooseRoleActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.form.input_work.InputWorkActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.login.LoginMvpPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;
import com.joefakri.serbakerja.ui.main.MainActivity;
import com.joefakri.serbakerja.ui.setting.TNCActivity;
import com.joefakri.serbakerja.ui.verify.VerifyActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.ValidationUtil;

import javax.inject.Inject;

import butterknife.BindView;

public class RegisterActivity extends BaseActivity implements RegisterView{

    @Inject RegisterMvpPresenter<RegisterView> presenter;

    @BindView(R.id.view_splash) RelativeLayout view_splash;
    @BindView(R.id.view_login) RelativeLayout view_login;
    @BindView(R.id.btn_login) TextView btn_login;
    @BindView(R.id.btn_tnc) TextView btn_tnc;
    @BindView(R.id.btn_register) Button btn_register;
    @BindView(R.id.txt_name) EditText txt_name;
    @BindView(R.id.txt_phone_area) EditText txt_phone_area;
    @BindView(R.id.txt_phone) EditText txt_phone;
    @BindView(R.id.txt_email) EditText txt_email;
    @BindView(R.id.txt_password) EditText txt_password;
    String phone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_register, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {

        presenter.StartOpen(this, view_splash, view_login);
        presenter.initPhoneAuth(this);

        btn_login.setOnClickListener(view -> {
            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        });

        btn_tnc.setOnClickListener(view -> {
            startActivity(new Intent(RegisterActivity.this, TNCActivity.class));
        });


        btn_register.setOnClickListener(view -> {
            phone = txt_phone_area.getText().toString() + txt_phone.getText().toString();
            if (TextUtils.isEmpty(txt_phone.getText().toString())) {
                txt_phone.setError("Nomor HP tidak boleh kosong");
                return;
            }
            /*if (!txt_phone.getText().toString().substring(0, 1).equals("8")){
                txt_phone.setError("Invalid phone number");
                return;
            }*/

            String name = txt_name.getText().toString();
            if (name.isEmpty()){
                txt_name.setError("Nama tidak boleh kosong");
                return;
            }

            String email = txt_email.getText().toString();
            /*if (!ValidationUtil.isValidEmail(email)){
                txt_email.setError("Email tidak valid");
                return;
            }*/

            String password = txt_password.getText().toString();
            if (password.isEmpty()){
                txt_password.setError("Kata sandi tidak boleh kosong");
                return;
            }

            Log.e("phone", phone);
            presenter.onRegister(name, phone.replace(" ", "").replace("+620", "+62"), email, password);


            /*finish();
            startActivity(new Intent(RegisterActivity.this, FormOptionalActivity.class));*/

        });


    }

    @Override
    public void openMainActivity() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
        Intent intent = new Intent(RegisterActivity.this, VerifyActivity.class);
        intent.putExtra("verification_id", verificationId);
        intent.putExtra("phone", phone.replace(" ", "").replace("+620", "+62"));
        intent.putExtra("token", token);
        startActivity(intent);
        finish();
    }

    @Override
    public void signinSuccess(FirebaseUser user) {
        presenter.updateStatusVerification(phone);
    }


    @Override
    public void registerResponse(UserResponse.Register.Data data) {
        presenter.startPhoneNumberVerification(this, phone.replace(" ", "").replace("+620", "+62"));
        /*finish();
        startActivity(new Intent(RegisterActivity.this, FormOptionalActivity.class));*/
    }

    @Override
    public void onResultUpdateStatus() {
        Log.e("this", "signinSuccess");
        finish();
        Intent i = new Intent(RegisterActivity.this, ChooseRoleActivity.class);
        i.putExtra("from", 0);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }
}
