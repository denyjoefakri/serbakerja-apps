package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkDayFragment extends BaseFragment implements FormWorkView {

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_work_day) EditText txt_work_day;

    private EventBus bus = EventBus.getDefault();
    StateEvent stateEvent;
    FormEvent recruiterEvent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_day, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        btn_next.setOnClickListener(view -> {
            if (txt_work_day.getText().toString().isEmpty()) {
                onError("Hari kerja tidak boleh kosong");
                return;
            }


            /*String min_sallary = Constants.min_sallary(recruiterEvent.getSallary());
            String max_sallary = Constants.max_sallary(recruiterEvent.getSallary());
            String work_time = Constants.waktu_kerja(recruiterEvent.getTime());

            mPresenter.onPostCandidateWork(recruiterEvent.getWork_id(),
                    min_sallary, max_sallary, recruiterEvent.getCity_id(), recruiterEvent.getArea(),
                    work_time, txt_work_day.getText().toString());*/

        });
    }

    private void setEvent(int value){
        stateEvent.setValue(value);
        stateEvent.setTotal(6);
        stateEvent.setType(22);
        recruiterEvent.setDay(txt_work_day.getText().toString());
        bus.postSticky(stateEvent);
        bus.postSticky(recruiterEvent);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.stateEvent = state;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onRecruiterEvent(FormEvent state){
        this.recruiterEvent = state;
        txt_work_day.setText(recruiterEvent.getDay());
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {

    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {
        setEvent(6);
        ((FormOptionalActivity)getActivity()).last_form_candidate();
    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }
}
