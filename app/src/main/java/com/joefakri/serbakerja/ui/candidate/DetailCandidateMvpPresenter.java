package com.joefakri.serbakerja.ui.candidate;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.chat.ChatView;

/**
 * Created by deny on bandung.
 */

public interface DetailCandidateMvpPresenter<V extends DetailCandidateView> extends SerbakerjaPresenter<V> {

    //void onContact(Activity activity, View v, String phone);
    void permissionCall(Activity activity, String phone);
    void permissionSMS(Activity activity, String phone);
    void whatsapp(Activity activity, String phone);
    void onViewDetail(String candidate_id);

}
