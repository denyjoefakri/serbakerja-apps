package com.joefakri.serbakerja.ui.form.input_work;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.login.LoginMvpPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;
import com.joefakri.serbakerja.ui.register.RegisterActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;
import com.joefakri.serbakerja.utils.autolabel.AutoLabelUI;
import com.joefakri.serbakerja.utils.autolabel.Label;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import pub.devrel.easypermissions.EasyPermissions;

public class InputWorkActivity extends BaseActivity implements InputWorkView, EasyPermissions.PermissionCallbacks{

    @Inject InputWorkMvpPresenter<InputWorkView> presenter;
    @BindView(R.id.txt_work_type) EditText txt_work_type;
    @BindView(R.id.rd_sallary) RadioGroup rd_sallary;
    @BindView(R.id.txt_city) AutoCompleteTextView txt_city;
    @BindView(R.id.lbl_city) AutoLabelUI lbl_city;
    @BindView(R.id.txt_area) EditText txt_area;
    @BindView(R.id.rd_work_time) RadioGroup rd_work_time;
    @BindView(R.id.txt_options_time_3) EditText txt_options_time_3;
    @BindView(R.id.txt_work_day) EditText txt_work_day;
    @BindView(R.id.btn_contact_us) Button btn_contact_us;
    @BindView(R.id.btn_next) Button btn_next;

    WorkModel workModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_candidate_work_input, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();

    }

    @Override
    protected void initComponent() {
        presenter.initCustomTab(this);
        presenter.onViewCity();

        txt_work_type.setOnFocusChangeListener((view, b) -> {
            if (b){
                Intent intent = new Intent(InputWorkActivity.this, DataWorkActivity.class);
                intent.putExtra("models", getIntent().getSerializableExtra("models"));
                intent.putExtra("dashboard", false);
                startActivityForResult(intent, 0);
            }
        });

        btn_contact_us.setOnClickListener(view -> {
            presenter.readPhone(this, view);
        });

        lbl_city.setOnLabelClickListener(labelClicked -> {
            multipleLabelListener(lbl_city, labelClicked);
        });

        rd_work_time.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton radioButton = findViewById(i);
            if (radioButton.getText().equals(getString(R.string.options_time_3))) {
                txt_options_time_3.setVisibility(View.VISIBLE);
                txt_options_time_3.setText("");
            }
            else {
                txt_options_time_3.setVisibility(View.GONE);
            }
        });

        if (getIntent().getBooleanExtra("edit", false)){
            workModel = (WorkModel) getIntent().getSerializableExtra("model");
            txt_work_type.setText(workModel.getWork());
            txt_work_day.setText(workModel.getWork_day());
            txt_area.setText(workModel.getArea());

            for (WorkModel.City model : workModel.getCitys()){
                lbl_city.addLabelSelected(model.getName(), model.getId());
            }
            int positionSallary = Constants.sallaryPosition(this, workModel.getSalary());
            ((RadioButton)rd_sallary.getChildAt(positionSallary)).setChecked(true);
            String positionTime = Constants.waktuKerjaPosition(this, workModel.getWork_time());
            ((RadioButton)rd_work_time.getChildAt(Integer.parseInt(positionTime))).setChecked(true);

            if (Integer.parseInt(positionTime) == 2) txt_options_time_3.setText(workModel.getWork_time());

        } else {
            workModel = new WorkModel();
        }

        rd_sallary.setOnCheckedChangeListener((radioGroup, i) -> {
            txt_work_type.clearFocus();
        });

        btn_next.setOnClickListener(view -> {

            if (txt_work_type.getText().toString().isEmpty()){
                onError("Pekerjaan tidak boleh kosong");
                return;
            }

            if (rd_work_time.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu waktu bekerja yang tersedia");
                return;
            }

            if (rd_sallary.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu gaji yang tersedia");
                return;
            }

            if (lbl_city.getLabels().size() == 0){
                onError("Kota / Kabupaten tidak boleh kosong");
                return;
            }

            if (txt_area.getText().toString().isEmpty()){
                onError("Daerah tidak boleh kosong");
                return;
            }

            if (txt_work_day.getText().toString().isEmpty()){
                onError("Hari kerja tidak boleh kosong");
                return;
            }

            setData();
            finish();
        });
    }

    private void setData(){
        workModel.setData(true);
        workModel.setWork_day(txt_work_day.getText().toString());
        workModel.setArea(txt_area.getText().toString());
        workModel.setSalary(getSallary());

        String min_sallary = Constants.min_sallary(this, getSallary());
        String max_sallary = Constants.max_sallary(this, getSallary());
        workModel.setSalarymin(min_sallary);
        workModel.setSalarymax(max_sallary);

        String work_time = Constants.waktuKerjaPosition(this, getTime());
        String work_time_other = Constants.waktuKerjaOther(work_time, getTime());
        workModel.setWork_time_position(work_time);
        workModel.setWork_time_other(work_time_other);
        workModel.setWork_time(getTime());

        workModel.getCitys().clear();
        for (int i = 0; i < lbl_city.getLabels().size(); i++) {
            WorkModel.City city = new WorkModel().new City();
            city.setName(lbl_city.getLabels().get(i).getText());
            city.setId(lbl_city.getLabels().get(i).getTag().toString());
            workModel.getCitys().add(city);
        }
        workModel.setCity_id(Util.getCitys(workModel.getCitys()));
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            String jobId = data.getStringExtra("id");
            String jobName = data.getStringExtra("job");
            workModel.setWorkId(jobId);
            workModel.setWork(jobName);
            txt_work_type.setText(jobName);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        presenter.contactusOption(this, btn_contact_us);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_callphone);
            dialog.setOnMessageClosed(() -> {
                presenter.readPhone(this, btn_contact_us);
            });
        }
    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {
        ArrayList<String> citys = new ArrayList<>();
        ArrayList<String> ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, citys);
        txt_city.setAdapter(adapter);
        txt_city.setOnItemClickListener((adapterView, view, i, l) -> {
            String city = adapter.getItem(i).toString();
            String id = ids.get(citys.indexOf(city));

            if (!lbl_city.isLabelAvailable(city)) {
                onError("Tidak bisa memasukkan kota yang sama");
                txt_city.setText("");
            } else if (!city.isEmpty()){
                lbl_city.addLabelSelected(city, id);
                txt_city.setText("");
            }
        });
    }

    private void multipleLabelListener(AutoLabelUI labelView, Label label) {
        if (label.getBackgroundResource() == R.drawable.bg_item_solid_lightgrey) {
            labelView.selectLabel(label.getText(), (String) label.getTag());
        } else {
            labelView.unselectLabel(label.getText(), (String) label.getTag());
            labelView.removeLabel((String) label.getTag());
        }
    }

    private String getSallary(){
        int selectedId= rd_sallary.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    private String getTime(){
        int selectedId= rd_work_time.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        String result = "";
        if (!rd.getText().toString().equals(getString(R.string.options_time_3))){
            result = rd.getText().toString();
        } else {
            result = txt_options_time_3.getText().toString();
        }
        return result;
    }

    @Override
    public void finish() {
        Intent intentResult = new Intent();
        intentResult.putExtra("model", workModel);
        setResult(11, intentResult);
        super.finish();
    }
}
