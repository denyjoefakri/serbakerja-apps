/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.candidate.experience_edit;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class EditExperiencePresenter<V extends EditExperienceView> extends BasePresenter<V>
        implements EditExperienceMvpPresenter<V> {

    @Inject
    public EditExperiencePresenter(DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onPostEditExperience(String... strings) {
        CandidateRequest.ChangeProfileExperience profile = new CandidateRequest.ChangeProfileExperience(strings[1], strings[2],
                strings[3], strings[4]);
        UserRequest.Id id = new UserRequest.Id(strings[0]);

        getSerbakerjaView().showLoading();

        Log.i("id", strings[0]);
        Logger.printLogRequest(Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL, profile, null);

        getCompositeDisposable().add(getDataManager()
                .profileUpdateCandidateExperienceDetail(id, profile)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL);
                    Gson gson = new Gson();
                    CandidateResponse.ExperienceMan response = gson.fromJson(jsonObject.toString(), CandidateResponse.ExperienceMan.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultExperienceEdit(response);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostInsertExperience(WorkModel workModel, String... strings) {
        CandidateRequest.ChangeProfileExperience profile = new CandidateRequest.ChangeProfileExperience(strings[0], strings[1], strings[2],
                strings[3], strings[4]);

        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL_INSERT, profile, null);

        getCompositeDisposable().add(getDataManager()
                .profileUpdateCandidateExperienceDetailInsert(profile)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE_DETAIL_INSERT);
                    Gson gson = new Gson();
                    CandidateResponse.ExperienceMan response = gson.fromJson(jsonObject.toString(), CandidateResponse.ExperienceMan.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultExperienceInsert(response, workModel);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostAllExperience(String... strings) {
        String[] status = {strings[2], strings[3]};
        CandidateRequest.ChangeProfileExperience profile = new CandidateRequest.ChangeProfileExperience(strings[0], strings[1], status);
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());

        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE, profile, null);

        getCompositeDisposable().add(getDataManager()
                .profileUpdateCandidateExperience(id, profile)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_PROFILE_CANDIDATE_EXPERIENCE);
                    Gson gson = new Gson();
                    CandidateResponse.ChangeProfile response = gson.fromJson(jsonObject.toString(), CandidateResponse.ChangeProfile.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultAllExperience(response);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
