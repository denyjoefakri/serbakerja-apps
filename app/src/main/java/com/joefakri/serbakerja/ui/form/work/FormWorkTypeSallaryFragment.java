package com.joefakri.serbakerja.ui.form.work;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class FormWorkTypeSallaryFragment extends BaseFragment implements FormWorkView, EasyPermissions.PermissionCallbacks{

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.ti_type) TextInputLayout ti_type;
    @BindView(R.id.txt_job_type) EditText txt_work_type;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.btn_contact_us) Button btn_contact_us;
    @BindView(R.id.rd_sallary) RadioGroup rd_sallary;
    RadioButton rd;

    private String id = "";
    private String job = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_type_sallary, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.initCustomTab(getActivity());
        mPresenter.init();
        txt_work_type.setOnFocusChangeListener((view, b) -> {
            if (b){
                Intent intent = new Intent(getActivity(), DataWorkActivity.class);
                intent.putExtra("models", new ArrayList<WorkModel>());
                intent.putExtra("dashboard", false);
                startActivityForResult(intent, 0);
            }
        });

        btn_next.setOnClickListener(view -> {

            if (txt_work_type.getText().toString().isEmpty() || !txt_work_type.getText().toString().equals(job)) {
                onError("Pilih jenis pekerjaan yang tersedia");
                return;
            }

            if (rd_sallary.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu gaji yang tersedia");
                return;
            }

            mPresenter.saveRecruterWorkTypeSallary(id, job,
                    String.valueOf(Constants.sallaryPosition(getActivity(), get())));
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(1);
        });

        btn_contact_us.setOnClickListener(view -> {
            mPresenter.readPhone(getActivity(), this, view);
        });
    }

    private String get(){
        int selectedId= rd_sallary.getCheckedRadioButtonId();
        rd = getActivity().findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm != null){
            job = recruiterRealm.getWorkType();
            txt_work_type.setText(recruiterRealm.getWorkType());
            id = recruiterRealm.getWorkTypeId();
            if (recruiterRealm.getWorkSallary() != null)
                ((RadioButton)rd_sallary.getChildAt(Integer.parseInt(recruiterRealm.getWorkSallary()))).setChecked(true);
        }
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            id = data.getStringExtra("id");
            job = data.getStringExtra("job");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        txt_work_type.setText(job);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        mPresenter.contactusOption(getActivity(), btn_contact_us);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_callphone);
            dialog.setOnMessageClosed(() -> {
                mPresenter.readPhone(getActivity(), this, btn_contact_us);
            });
        }
    }
}
