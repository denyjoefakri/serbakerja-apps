package com.joefakri.serbakerja.ui.form.person;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormPersonReligionFragment extends BaseFragment implements FormPersonView{

    @Inject FormPersonMvpPresenter<FormPersonView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_person_religion) EditText txt_person_religion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_person_religion, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (txt_person_religion.getText().toString().isEmpty()){
                onError("Agama tidak boleh kosong");
                return;
            }

            MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_1_kandidat);
            dialog.setOnMessageClosed(() -> {
                mPresenter.saveCandidateReligion(txt_person_religion.getText().toString());
                mPresenter.onPostCandidateInfo();
            });

        });
    }

    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void onResultCity(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultDistrict(List<AreaResponse.DistrictResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterInfo(RecruiterResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void onResultCandidateInfo(CandidateResponse.InfoResponse.Data dataList) {
        getActivity().finish();
        /*setEvent(9);
        ((FormOptionalActivity)getActivity()).last_form_candidate();*/
    }

    @Override
    public void initRecruter(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (candidateRealm.getPersonReligion() != null)
            txt_person_religion.setText(candidateRealm.getPersonReligion());
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
