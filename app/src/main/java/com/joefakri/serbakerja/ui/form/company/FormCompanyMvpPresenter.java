package com.joefakri.serbakerja.ui.form.company;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.form.FormOptionalView;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface FormCompanyMvpPresenter<V extends FormCompanyView> extends SerbakerjaPresenter<V> {

    void readCameraGallery(Activity activity, Fragment fragment, View v);

    void openImageChooser(Activity activity, View v);

    void onViewCity();

    void onPostCompany();

    void init();

    void saveCompanyName(String data);
    void saveCompanyAddress(String data);
    void saveCompanyCity(String id, String data);
    void saveCompanyArea(String data);
    void saveCompanyTotalEmployee(String code);
    void saveCompanyWebsite(String data);
    void saveCompanyLogo(String data);
}
