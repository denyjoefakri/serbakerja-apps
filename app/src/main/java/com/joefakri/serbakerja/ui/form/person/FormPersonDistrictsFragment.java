package com.joefakri.serbakerja.ui.form.person;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormPersonDistrictsFragment extends BaseFragment implements FormPersonView{

    @Inject FormPersonMvpPresenter<FormPersonView> mPresenter;
    @Inject DataManager dataManager;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_person_districts) AutoCompleteTextView txt_person_districts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_person_districts, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (txt_person_districts.getText().toString().isEmpty()) {
                onError("Kecamatan tidak boleh kosong");
                return;
            }

            if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT))
                mPresenter.saveRecruterPersonDistricts(txt_person_districts.getText().toString());
            else mPresenter.saveCandidatePersonDistricts(txt_person_districts.getText().toString());

            txt_person_districts.dismissDropDown();
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(4);
        });

    }

    @Override
    public void onResultDistrict(List<AreaResponse.DistrictResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterInfo(RecruiterResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void onResultCandidateInfo(CandidateResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void initRecruter(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm != null)
            if (recruiterRealm.getPersonDistrics() != null) {
                if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT))
                    txt_person_districts.setText(recruiterRealm.getPersonDistrics());
        }

        if (candidateRealm != null)
            if (candidateRealm.getPersonDistrics() != null)
                if (dataManager.getTypeUser().equals(Constants.ROLE_KANDIDAT))
                    txt_person_districts.setText(candidateRealm.getPersonDistrics());
    }

    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void onResultCity(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
