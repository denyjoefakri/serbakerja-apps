package com.joefakri.serbakerja.ui.notification;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.NotificationAdapter;
import com.joefakri.serbakerja.connection.response.NotificationResponse;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.main.MainMvpPresenter;
import com.joefakri.serbakerja.ui.main.MainView;
import com.joefakri.serbakerja.ui.notification.detail.NotificationDetailCSActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class NotificationActivity extends BaseActivity implements NotificationView, NotificationAdapter.Listener{

    @Inject NotificationMvpPresenter<NotificationView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.rv_notification) RecyclerView rv_notification;
    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_notification, this);
        getActivityComponent().inject(this);


        toolbar.setTitle(getString(R.string.txt_notification));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        rv_notification.setLayoutManager(new LinearLayoutManager(this));
        rv_notification.setHasFixedSize(true);
        rv_notification.setNestedScrollingEnabled(false);
        rv_notification.setAdapter(notificationAdapter = new NotificationAdapter(this));
        notificationAdapter.setOnclickListener(this);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.cancel(22);
        }

        presenter.onViewPrepared();

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResult(List<NotificationResponse.ListNotif.Data> listNotifs) {
        notificationAdapter.update(listNotifs);
    }

    @Override
    public void onResultReadNotification(String type) {
        presenter.onViewPrepared();
        if (type.equals("cs")) {
            Intent i = new Intent(NotificationActivity.this, NotificationDetailCSActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onClick(NotificationResponse.ListNotif.Data notificationModel) {
        presenter.onRead(notificationModel.getId(), notificationModel.getTipe());
    }
}
