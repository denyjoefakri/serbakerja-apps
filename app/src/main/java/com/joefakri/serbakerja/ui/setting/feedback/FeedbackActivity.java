package com.joefakri.serbakerja.ui.setting.feedback;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.MessageResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainActivity;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryActivity;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainActivity;
import com.joefakri.serbakerja.ui.setting.SettingMvpPresenter;
import com.joefakri.serbakerja.ui.setting.SettingView;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class FeedbackActivity extends BaseActivity implements FeedbackView{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_from) TextView txt_from;
    @BindView(R.id.ed_subject) EditText ed_subject;
    @BindView(R.id.ed_feedback) EditText ed_feedback;
    @BindView(R.id.btn_send) Button btn_send;
    @Inject FeedbackMvpPresenter<FeedbackView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_feedback, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();
    }
    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_feedback));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onInitFrom();

        btn_send.setOnClickListener(view -> {
            presenter.onSend(ed_subject.getText().toString(), ed_feedback.getText().toString());
        });

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onFrom(String phone, String name) {
        txt_from.setText(name + " ("+phone+")");
    }

    @Override
    public void onResult(MessageResponse messageResponse) {
        finish();
    }

}
