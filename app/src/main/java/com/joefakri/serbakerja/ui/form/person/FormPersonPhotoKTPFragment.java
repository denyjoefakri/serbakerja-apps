package com.joefakri.serbakerja.ui.form.person;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class FormPersonPhotoKTPFragment extends BaseFragment implements FormPersonView, EasyPermissions.PermissionCallbacks{

    @Inject FormPersonMvpPresenter<FormPersonView> mPresenter;
    @BindView(R.id.btn_get_photo) Button btn_get_photo;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.img_photo) ImageView img_photo;

    private String fileToSend;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_person_photo_ktp, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view2 -> {
            if (fileToSend == null) {
                onError("Foto KTP tidak boleh kosong");
                return;
            }

            mPresenter.saveRecruterPersonPhotoKTP(fileToSend);
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(7);
        });

        btn_get_photo.setOnClickListener(view2 -> {
            mPresenter.readCameraGallery(getActivity(), this, btn_get_photo);
        });
    }

    @Override
    public void openGallery(String file) {
        fileToSend = file;
        Log.e("fileToSend", fileToSend);
        Glide.with(this).load(fileToSend).into(img_photo);
    }

    @Override
    public void takePhoto(String file) {
        fileToSend = file;
        Log.e("fileToSend", fileToSend);
        Glide.with(this).load(fileToSend).into(img_photo);
    }



    @Override
    public void onResultCity(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultDistrict(List<AreaResponse.DistrictResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterInfo(RecruiterResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void onResultCandidateInfo(CandidateResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void initRecruter(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm.getPersonPhotoKTP() != null) {
            fileToSend = recruiterRealm.getPersonPhotoKTP();
            Glide.with(this).load(recruiterRealm.getPersonPhotoKTP()).into(img_photo);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        mPresenter.openImageChooser(getActivity(), btn_get_photo);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                mPresenter.readCameraGallery(getActivity(), this, btn_get_photo);
            });
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
