package com.joefakri.serbakerja.ui.recruters;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateView;

/**
 * Created by deny on bandung.
 */

public interface DetailRecrutersMvpPresenter<V extends DetailRecrutersView> extends SerbakerjaPresenter<V> {

    void onViewDetail(String recruters_id);

}
