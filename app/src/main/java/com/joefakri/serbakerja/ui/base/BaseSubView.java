/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by janisharali on 27/01/17.
 */

public abstract class BaseSubView extends ViewGroup implements SubSerbakerjaView {

    private SerbakerjaView mSerbakerjaView;

    public BaseSubView(Context context) {
        super(context);
    }

    public BaseSubView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseSubView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public BaseSubView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void attachParentMvpView(SerbakerjaView serbakerjaView) {
        mSerbakerjaView = serbakerjaView;
    }

    @Override
    public void showLoading() {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.showLoading();
        }
    }

    @Override
    public void hideLoading() {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.hideLoading();
        }
    }

    @Override
    public void onError(@StringRes int resId) {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.onError(resId);
        }
    }

    @Override
    public void onError(String message) {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.onError(message);
        }
    }

    @Override
    public void showMessage(String message) {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.showMessage(message);
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.showMessage(resId);
        }
    }

    @Override
    public void hideKeyboard() {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.hideKeyboard();
        }
    }

    @Override
    public boolean isNetworkConnected() {
        if (mSerbakerjaView != null) {
            return mSerbakerjaView.isNetworkConnected();
        }
        return false;
    }

    @Override
    public void openActivityOnTokenExpire() {
        if (mSerbakerjaView != null) {
            mSerbakerjaView.openActivityOnTokenExpire();
        }
    }

    protected abstract void bindViewsAndSetOnClickListeners();

    protected abstract void setUp();
}
