package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.ui.widget.WorkExperienceDialog;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkExperienceFragment extends BaseFragment implements FormWorkView, WorkExperienceAdapter.Listener{

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.rv_work_experience) RecyclerView rv_work_experience;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.btn_add) LinearLayout btn_add;

    WorkExperienceAdapter adapter;
    ArrayList<WorkModel> models;

    @Inject LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_experience, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {


        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        models = new ArrayList<>();
        rv_work_experience.setLayoutManager(mLayoutManager);
        rv_work_experience.setHasFixedSize(true);
        rv_work_experience.setNestedScrollingEnabled(false);
        rv_work_experience.setAdapter(adapter = new WorkExperienceAdapter(getActivity()));
        adapter.setOnclickListener(this);
        mPresenter.init();

        btn_next.setOnClickListener(view -> {
            MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_3_kandidat);
            dialog.setOnMessageClosed(() -> {
                mPresenter.onPostCandidateExperience(getActivity());
            });

        });

        btn_add.setOnClickListener(view -> {
            workExperienceDialog(false, new WorkModel()).setOnListener((txt_work_company_name,
                                                                        txt_work_as, txt_work_long, perio) -> {

                if (txt_work_company_name.isEmpty()|txt_work_as.isEmpty()|txt_work_long.isEmpty()|perio.isEmpty()){
                    onError("Form tidak boleh kosong");
                } else {
                    WorkModel experienceWorkModel = new WorkModel();
                    experienceWorkModel.setCompany_name(txt_work_company_name);
                    experienceWorkModel.setWork_as(txt_work_as);
                    experienceWorkModel.setWork_long(txt_work_long);
                    experienceWorkModel.setPeriod(perio);
                    mPresenter.saveCandidateExperienceWork(experienceWorkModel);

                    models.add(experienceWorkModel);
                    adapter.update(models);
                }

            });
        });

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {
        getActivity().finish();
        /*setEvent(3);
        ((FormOptionalActivity)getActivity()).last_form_candidate();*/
    }

    public WorkExperienceDialog workExperienceDialog (boolean edit, WorkModel model) {
        WorkExperienceDialog messageDialog = new WorkExperienceDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean("edit", edit);
        bundle.putSerializable("model", model);
        messageDialog.setArguments(bundle);
        messageDialog.show(getFragmentManager(), "messageDialog");
        return messageDialog;
    }

    @Override
    public void onClick(WorkModel model, int position) {
        workExperienceDialog(true, model).setOnListener((txt_work_company_name,
                                                         txt_work_as, txt_work_long, perio) -> {

            WorkModel experienceWorkModel = models.get(position);
            experienceWorkModel.setCompany_name(txt_work_company_name);
            experienceWorkModel.setWork_as(txt_work_as);
            experienceWorkModel.setWork_long(txt_work_long);
            experienceWorkModel.setPeriod(perio);

            adapter.update(models);

        });

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (candidateRealm != null) {
            if (candidateRealm.getExperienceRealms().size() > 0){
                for (int i = 0; i < candidateRealm.getExperienceRealms().size(); i++) {
                    WorkModel experienceWorkModel = new WorkModel();
                    experienceWorkModel.setCompany_name(candidateRealm.getExperienceRealms().get(i).getCompanyName());
                    experienceWorkModel.setWork_as(candidateRealm.getExperienceRealms().get(i).getWorkAs());
                    experienceWorkModel.setWork_long(candidateRealm.getExperienceRealms().get(i).getWorkLongTime());
                    experienceWorkModel.setPeriod(candidateRealm.getExperienceRealms().get(i).getWorkPeriod());
                    models.add(experienceWorkModel);
                }

                adapter.update(models);
            }
        }
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }


}
