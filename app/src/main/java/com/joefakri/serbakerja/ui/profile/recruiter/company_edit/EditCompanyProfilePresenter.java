/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.recruiter.company_edit;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class EditCompanyProfilePresenter<V extends EditCompanyProfileView> extends BasePresenter<V>
        implements EditCompanyProfileMvpPresenter<V> {

    @Inject
    public EditCompanyProfilePresenter(DataManager dataManager,
                                       SchedulerProvider schedulerProvider,
                                       CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onEdit(File file, String... strings) {
        getSerbakerjaView().showLoading();
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());
        RecruiterRequest.ChangeCompany profile = new RecruiterRequest.ChangeCompany(strings[0],
                strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]);
        RecruiterRequest.ChangeCompanyFile profileFile = new RecruiterRequest.ChangeCompanyFile(file);

        Logger.printLogRequest(Path.CHANGE_RECRUTER_PROFILE, profile, profileFile.getFoto());

        getCompositeDisposable().add(getDataManager()
                .changeRecruterCompanyProfile(id, profile, profileFile)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.CHANGE_RECRUTER_PROFILE);
                    Gson gson = new Gson();
                    RecruiterResponse.ChangeCompany response = gson.fromJson(jsonObject.toString(), RecruiterResponse.ChangeCompany.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultEdit(response);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onViewCity() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCity()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(cityResponse -> {

                    LogResponse(cityResponse, Path.GET_CITY);
                    Gson gson = new Gson();
                    AreaResponse.CityResponse response = gson.fromJson(cityResponse.toString(), AreaResponse.CityResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onViewCity(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();


                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
