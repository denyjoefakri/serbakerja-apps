package com.joefakri.serbakerja.ui.profile.candidate.document;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationView;

/**
 * Created by deny on bandung.
 */

public interface CandidateDocumentMvpPresenter<V extends CandidateDocumentView> extends SerbakerjaPresenter<V> {

    void onViewDocument();

}
