package com.joefakri.serbakerja.ui.form.company;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormCompanyWebsiteFragment extends BaseFragment implements FormCompanyView{

    @Inject FormCompanyMvpPresenter<FormCompanyView> mPresenter;
    @BindView(R.id.txt_website) EditText txt_website;
    @BindView(R.id.btn_next) Button btn_next;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_company_website, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (!txt_website.getText().toString().isEmpty()) {
                mPresenter.saveCompanyWebsite(txt_website.getText().toString());
            }

            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(5);
        });
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm) {
        if (recruiterRealm.getCompanyWebsite() != null)
            txt_website.setText(recruiterRealm.getCompanyWebsite());
    }

    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultCompany(RecruiterResponse.CompanyResponse.Data companyResponses) {

    }

}
