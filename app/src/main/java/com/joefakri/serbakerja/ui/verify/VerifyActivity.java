package com.joefakri.serbakerja.ui.verify;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthProvider;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.bio.ChooseRoleActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.register.RegisterActivity;
import com.joefakri.serbakerja.ui.register.RegisterMvpPresenter;
import com.joefakri.serbakerja.ui.register.RegisterView;
import com.joefakri.serbakerja.utils.KeyEvent;
import com.joefakri.serbakerja.utils.VerifyWatcher;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerifyActivity extends BaseActivity implements VerifyView{

    @Inject VerifyMvpPresenter<VerifyView> presenter;

    @BindView(R.id.btn_edit) ImageButton btn_edit;
    @BindView(R.id.txt_countdown) TextView txt_countdown;
    @BindView(R.id.txt_phone_number) EditText txt_phone_number;
    @BindView(R.id.ed_OTP) PinEntryEditText ed_OTP;
    @BindView(R.id.btn_verify) Button btn_verify;
    @BindView(R.id.btn_resend) Button btn_resend;

    String verification_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_sms_verify, this);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        presenter.initPhoneAuth(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        txt_phone_number.setText(getIntent().getStringExtra("phone"));
        presenter.countdown();

        verification_id = getIntent().getStringExtra("verification_id");

        btn_edit.setOnClickListener(view -> {
            txt_phone_number.setEnabled(true);
            txt_phone_number.requestFocus();
            txt_phone_number.setSelection(txt_phone_number.getText().toString().length());
        });


        btn_verify.setOnClickListener(view -> {
            Log.e("otp", ed_OTP.getText().toString());
            String code = ed_OTP.getText().toString();

            if (TextUtils.isEmpty(code)) {
                onError("Kode tidak boleh kosong");
                return;
            }

            presenter.verifyPhoneNumberWithCode(this, verification_id, code);

        });

        btn_resend.setOnClickListener(view -> {
            if (TextUtils.isEmpty(txt_phone_number.getText().toString())) {
                txt_phone_number.setError("Nomor HP tidak boleh kosong");
                return;
            }

            if (getIntent().getStringExtra("phone").equals(txt_phone_number.getText().toString())){
                PhoneAuthProvider.ForceResendingToken token = (PhoneAuthProvider.ForceResendingToken) getIntent().getExtras().get("token");
                presenter.resendVerificationCode(this, txt_phone_number.getText().toString(), token);
            } else {
                presenter.startPhoneNumberVerification(this, txt_phone_number.getText().toString());
            }

        });

    }

    @Override
    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
        Log.e("this", "onCodeSent");
        verification_id = verificationId;
        presenter.cancelCountdown();
        presenter.countdown();
        txt_countdown.setTextSize(30f);
        ed_OTP.setText("");
    }

    @Override
    public void signinSuccess(FirebaseUser user) {
        Log.e("this", "signinSuccess");
        presenter.updateStatusVerification(txt_phone_number.getText().toString());
    }

    @Override
    public void onCount(long minutes, long seconds) {
        if (minutes < 1) {
            txt_countdown.setTextColor(Color.RED);
        } else {
            txt_countdown.setTextColor(ContextCompat.getColor(this, R.color.green400));
        }

        String min = String.valueOf(minutes);
        String sec = String.valueOf(seconds);
        if (min.length() == 1) min = "0" + min;
        if (sec.length() == 1) sec = "0" + sec;
        txt_countdown.setText(min + ":" + sec);
    }

    @Override
    public void finishCountdown() {
        txt_countdown.setTextSize(15f);
        txt_countdown.setText(getString(R.string.message_time_has_run_out));
    }

    @Override
    public void onResultUpdateStatus() {
        finish();
        Intent i = new Intent(VerifyActivity.this, ChooseRoleActivity.class);
        i.putExtra("from", 0);
        startActivity(i);
    }


    @Override
    protected void onDestroy() {
        presenter.cancelCountdown();
        presenter.onDetach();
        super.onDestroy();
    }
}
