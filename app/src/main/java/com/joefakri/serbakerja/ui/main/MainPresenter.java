/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.main;

import com.androidnetworking.error.ANError;
import com.google.android.gms.maps.MapView;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.chat.ChatMvpPresenter;
import com.joefakri.serbakerja.ui.chat.ChatView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class MainPresenter<V extends MainView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter(DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onClickProfile() {
        getSerbakerjaView().onClickProfile(getDataManager().getTypeUser());
    }

    @Override
    public void ViewTab() {
        getSerbakerjaView().onViewTab(getDataManager().getTypeUser());
    }

    @Override
    public void updateFCM() {
        UserRequest.FCM request = new UserRequest.FCM(getDataManager().getCurrentUserId(), getDataManager().getFcmId());
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.UPDATE_FCM, request, null);

        getCompositeDisposable().add(getDataManager()
                .fcm(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_FCM);
                    Gson gson = new Gson();

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
