
package com.joefakri.serbakerja.ui.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.joefakri.serbakerja.MyApp;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.di.component.DaggerActivityComponent;
import com.joefakri.serbakerja.di.module.ActivityModule;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.CommonUtils;
import com.joefakri.serbakerja.utils.NetworkUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by janisharali on 27/01/17.
 */

public abstract class BaseActivity extends AppCompatActivity
        implements SerbakerjaView, BaseFragment.Callback{

    private ProgressDialog mProgressDialog;
    private MessageDialog messageDialog;
    private ActivityComponent mActivityComponent;
    private Unbinder mUnBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((MyApp) getApplication()).getComponent())
                .build();


    }

    protected abstract void initComponent();

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    @Override
    public void openActivityOnTokenExpire() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }


    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar(getString(R.string.message_some_error));
        }
    }

    @Override
    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.message_some_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }

    @Override
    public MessageDialog showConfirmMessage(@StringRes int resId) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        MessageDialog dialog = new MessageDialog();
        Bundle bundle = new Bundle();
        bundle.putString("message", getString(resId));
        dialog.setArguments(bundle);
        //dialog.show(ft, "messageDialog");
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
        return dialog;
    }

    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void showMessageDialog(String message) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        messageDialog = new MessageDialog();
        messageDialog.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putCharSequence("message", message);
        messageDialog.setArguments(bundle);
        //messageDialog.show(ft,"");
        ft.add(messageDialog, "messageDialog");
        ft.commitAllowingStateLoss();
        messageDialog.setOnMessageClosed(this::onClosedMessageDialog);
    }

    @Override
    public void showMessageDialog(@StringRes int resId) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        messageDialog = new MessageDialog();
        messageDialog.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putCharSequence("message", getString(resId));
        messageDialog.setArguments(bundle);
        //messageDialog.show(ft,"");
        ft.add(messageDialog, "messageDialog");
        ft.commitAllowingStateLoss();
        messageDialog.setOnMessageClosed(this::onClosedMessageDialog);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }

    @Override
    public void onClosedMessageDialog() {

    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    protected void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
            Log.e("mUnBinder", "unbind");
        }
        super.onDestroy();
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    public void setLayout(@LayoutRes int layout, Activity activity) {
        setContentView(layout);
        mUnBinder = ButterKnife.bind(activity);
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.red900));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }



}
