/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit;

import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.ui.base.SerbakerjaView;

import java.util.List;

/**
 * Created by janisharali on 25/05/17.
 */

public interface EditWorkPostView extends SerbakerjaView {

    void city(List<AreaResponse.CityResponse.Data> dataList);

    void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse);

    void onResultEditRecruterWork();

}
