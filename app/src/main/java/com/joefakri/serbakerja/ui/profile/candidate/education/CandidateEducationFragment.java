package com.joefakri.serbakerja.ui.profile.candidate.education;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationActivity;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceView;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class CandidateEducationFragment extends BaseFragment implements CandidateEducationView{

    @BindView(R.id.txt_education_graduated) TextView txt_education_graduated;
    @BindView(R.id.txt_education_name_last_school) TextView txt_education_name_last_school;
    @BindView(R.id.txt_education_name_courses) TextView txt_education_name_courses;
    @BindView(R.id.txt_education_last) TextView txt_education_last;
    @BindView(R.id.btn_edit) LinearLayout btn_edit;

    @Inject CandidateEducationMvpPresenter<CandidateEducationView> mPresenter;
    CandidateResponse.Education.Data data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_candidate_education, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View view) {

        btn_edit.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), EditEducationActivity.class);
            intent.putExtra("model", data);
            startActivity(intent);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewEducation();
        btn_edit.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultEducationView(CandidateResponse.Education.Data data) {
        btn_edit.setEnabled(true);
        this.data = data;
        txt_education_graduated.setText(data.getThn_pend_terakhir());
        txt_education_name_last_school.setText(data.getSekolah_terakhir());
        txt_education_name_courses.setText(data.getKursus());
        txt_education_last.setText(Constants.education(getActivity(), data.getPend_terakhir()));
    }
}
