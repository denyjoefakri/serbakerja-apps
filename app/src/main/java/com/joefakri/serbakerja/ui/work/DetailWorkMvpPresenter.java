package com.joefakri.serbakerja.ui.work;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.verify.VerifyView;

/**
 * Created by deny on bandung.
 */

public interface DetailWorkMvpPresenter<V extends DetailWorkView> extends SerbakerjaPresenter<V> {

    void onApply(String id);

    void onViewDetail(String candidate_id);

}
