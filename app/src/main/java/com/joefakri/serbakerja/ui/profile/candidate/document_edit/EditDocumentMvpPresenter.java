package com.joefakri.serbakerja.ui.profile.candidate.document_edit;

import android.app.Activity;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationView;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface EditDocumentMvpPresenter<V extends EditDocumentView> extends SerbakerjaPresenter<V> {

    void readCameraGallery(Activity activity, View v, int requestCode);

    void openImageChooser(Activity activity, View v, int requestCode);

    void onUpdate(String foto, String ijazah, String ktp);

}
