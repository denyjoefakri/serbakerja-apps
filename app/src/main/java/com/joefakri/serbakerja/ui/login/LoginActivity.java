package com.joefakri.serbakerja.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthProvider;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.bio.ChooseRoleActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.main.MainActivity;
import com.joefakri.serbakerja.ui.main.MainMvpPresenter;
import com.joefakri.serbakerja.ui.main.MainView;
import com.joefakri.serbakerja.ui.register.RegisterActivity;
import com.joefakri.serbakerja.ui.verify.VerifyActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

public class LoginActivity extends BaseActivity implements LoginView, EasyPermissions.PermissionCallbacks{

    @Inject LoginMvpPresenter<LoginView> presenter;
    @BindView(R.id.txt_email_or_phone) EditText txt_email_or_phone;
    @BindView(R.id.txt_password) EditText txt_password;
    @BindView(R.id.btn_login) Button btn_login;
    @BindView(R.id.btn_contact) TextView btn_contact;
    @BindView(R.id.btn_register) TextView btn_register;

    String no_hp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_login, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();

    }

    @Override
    protected void initComponent() {
        presenter.initPhoneAuth(this);
        presenter.initCustomTab(this);
        btn_login.setOnClickListener(view -> {

            if (txt_email_or_phone.getText().toString().isEmpty()) {
                onError("Nomor HP tidak boleh kosong");
                return;
            }

            if (txt_password.getText().toString().isEmpty()) {
                onError("Kata sandi tidak boleh kosong");
                return;
            }

            presenter.onRegister(txt_email_or_phone.getText().toString(), txt_password.getText().toString());

        });

        btn_contact.setOnClickListener(view -> {
            presenter.readPhone(this, view);
        });

        btn_register.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        });
    }

    @Override
    protected void onDestroy() {
        presenter.unRegisterListener();
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResultLogin(UserResponse.Login.Data login) {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResultLoginNotVerified(String phone_number) {
        no_hp = phone_number;
        presenter.startPhoneNumberVerification(this, phone_number);
    }

    @Override
    public void onResultLoginNotRegistered(String state) {
        showMessage("Anda belum melengkapi biodata");
        Intent i = new Intent(LoginActivity.this, ChooseRoleActivity.class);
        i.putExtra("state", state);
        i.putExtra("from", 1);
        startActivity(i);
        finish();
    }

    @Override
    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
        Intent intent = new Intent(LoginActivity.this, VerifyActivity.class);
        intent.putExtra("verification_id", verificationId);
        intent.putExtra("token", token);
        intent.putExtra("phone", no_hp);
        startActivity(intent);
        finish();
    }

    @Override
    public void signinSuccess(FirebaseUser user) {
        Log.e("log", "signinSuccess");
        presenter.updateStatusVerification(txt_email_or_phone.getText().toString());
    }

    @Override
    public void onResultUpdateStatus() {
        finish();
        Intent i = new Intent(LoginActivity.this, ChooseRoleActivity.class);
        i.putExtra("from", 0);
        startActivity(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        presenter.contactusOption(this, btn_contact);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_callphone);
            dialog.setOnMessageClosed(() -> {
                presenter.readPhone(this, btn_contact);
            });
        }
    }
}
