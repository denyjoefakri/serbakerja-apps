package com.joefakri.serbakerja.ui.profile.candidate.work_edit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;
import com.joefakri.serbakerja.utils.autolabel.AutoLabelUI;
import com.joefakri.serbakerja.utils.autolabel.Label;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class EditWorkActivity extends BaseActivity implements EditWorkView{

    @Inject
    EditWorkMvpPresenter<EditWorkView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_work) EditText txt_work;
    @BindView(R.id.lbl_city) AutoLabelUI lbl_city;
    @BindView(R.id.txt_work_city) AutoCompleteTextView txt_work_city;
    @BindView(R.id.txt_work_area) EditText txt_work_area;
    @BindView(R.id.txt_work_day) EditText txt_work_day;
    @BindView(R.id.txt_options_time_3) EditText txt_options_time_3;
    @BindView(R.id.rd_group_sallary) RadioGroup rd_group_sallary;
    @BindView(R.id.rd_group_time) RadioGroup rd_group_time;
    @BindView(R.id.btn_save) Button btn_save;

    ArrayList<String> citys;
    ArrayList<String> ids;
    private String city_id = "";
    private String city = "";
    private String id_job = "";
    private String job = "";
    DatePickerDialog dpd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_candidate_work, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_work));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        WorkResponse.CandidateWorkList.Data workModel = (WorkResponse.CandidateWorkList.Data) getIntent().getSerializableExtra("model");
        presenter.onViewCity();

        txt_work.setOnFocusChangeListener((view, b) -> {
            if (b){
                Intent intent = new Intent(EditWorkActivity.this, DataWorkActivity.class);
                intent.putExtra("models", new ArrayList<WorkModel>());
                intent.putExtra("dashboard", false);
                startActivityForResult(intent, 0);
            }
        });

        if (workModel != null){

            if (workModel.getJenis_pekerjaan() != null) txt_work.setText(workModel.getJenis_pekerjaan());
            if (workModel.getDaerah() != null) txt_work_area.setText(workModel.getDaerah());
            if (workModel.getHari_kerja() != null) txt_work_day.setText(workModel.getHari_kerja());
            if (workModel.getJenis_pekerjaan_id() != null) id_job = workModel.getJenis_pekerjaan_id();

            for (WorkResponse.CandidateWorkList.Data.Kota model : workModel.getKota()){
                lbl_city.addLabelSelected(model.getName(), model.getId());
            }

            String sallaryValue = Constants.sallary(this, workModel.getGaji_min(), workModel.getGaji_max());
            if (!sallaryValue.isEmpty()) {
                int sallaryPosition = Constants.sallaryPosition(this, sallaryValue);
                ((RadioButton)rd_group_sallary.getChildAt(sallaryPosition)).setChecked(true);
            }

            if (workModel.getWaktu_kerja() != null){
                ((RadioButton)rd_group_time.getChildAt(Integer.parseInt(workModel.getWaktu_kerja()))).setChecked(true);
            }

            if (workModel.getWaktu_kerja().equals("2")) {
                txt_options_time_3.setVisibility(View.VISIBLE);
                txt_options_time_3.setText(workModel.getWaktu_kerja_lainnya());
            }

        }

        rd_group_sallary.setOnCheckedChangeListener((radioGroup, i) -> {
            txt_work.clearFocus();
        });

        lbl_city.setOnLabelClickListener(labelClicked -> {
            multipleLabelListener(lbl_city, labelClicked);
        });

        rd_group_time.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton radioButton = findViewById(i);
            if (radioButton.getText().equals(getString(R.string.options_time_3))) {
                txt_options_time_3.setVisibility(View.VISIBLE);
                txt_options_time_3.setText("");
            }
            else {
                txt_options_time_3.setVisibility(View.GONE);
            }
        });

        btn_save.setOnClickListener(view -> {

            if (id_job.isEmpty()){
                onError("Pekerjaan tidak boleh kosong");
                return;
            }

            if (rd_group_sallary.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu gaji yang tersedia");
                return;
            }

            if (rd_group_time.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu waktu bekerja yang tersedia");
                return;
            }

            if (lbl_city.getLabels().size() == 0){
                onError("Kota / Kabupaten tidak boleh kosong");
                return;
            }

            if (txt_work_area.getText().toString().isEmpty()){
                onError("Daerah tidak boleh kosong");
                return;
            }

            if (txt_work_day.getText().toString().isEmpty()){
                onError("Hari kerja tidak boleh kosong");
                return;
            }

            String work_time = Constants.waktuKerjaPosition(this, getTime());
            String work_time_other = Constants.waktuKerjaOther(work_time, getTime());
            String min_sallary = Constants.min_sallary(this, getSallary());
            String max_sallary = Constants.max_sallary(this, getSallary());

            ArrayList<WorkModel.City> cities = new ArrayList<>();
            for (int i = 0; i < lbl_city.getLabels().size(); i++) {
                WorkModel.City city = new WorkModel().new City();
                city.setName(lbl_city.getLabels().get(i).getText());
                city.setId(lbl_city.getLabels().get(i).getTag().toString());
                cities.add(city);
            }
            String citys = Util.getCitys(cities);

            if (workModel != null){
                presenter.onEditWork(id_job, min_sallary, max_sallary,
                        citys, txt_work_area.getText().toString(), work_time, work_time_other,
                        txt_work_day.getText().toString(), workModel.getId());
            } else {
                if (id_job.isEmpty()|rd_group_sallary.getCheckedRadioButtonId() == -1|rd_group_time.getCheckedRadioButtonId() == -1
                        |cities.size() == 0|txt_work_area.getText().toString().isEmpty()|txt_work_day.getText().toString().isEmpty()){
                    onError("Tidak boleh ada form yang kosong");
                } else {
                    presenter.onInsertWork(id_job, min_sallary, max_sallary,
                            citys, txt_work_area.getText().toString(), work_time, work_time_other,
                            txt_work_day.getText().toString());
                }

            }


        });

    }

    private String getTime(){
        int selectedId= rd_group_time.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        String result = "";
        if (rd != null){
            if (!rd.getText().toString().equals(getString(R.string.options_time_3))){
                result = rd.getText().toString();
            } else {
                result = txt_options_time_3.getText().toString();
            }
        }
        return result;
    }

    private String getSallary(){
        int selectedId= rd_group_sallary.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        if (rd != null) return rd.getText().toString();
        else return "";
    }

    private void multipleLabelListener(AutoLabelUI labelView, Label label) {
        if (label.getBackgroundResource() == R.drawable.bg_item_solid_lightgrey) {
            labelView.selectLabel(label.getText(), (String) label.getTag());
        } else {
            labelView.unselectLabel(label.getText(), (String) label.getTag());
            labelView.removeLabel((String) label.getTag());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            id_job = data.getStringExtra("id");
            job = data.getStringExtra("job");
            txt_work.setText(job);
        }
    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {
        citys = new ArrayList<>();
        ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, citys);
        txt_work_city.setAdapter(adapter);
        txt_work_city.setOnItemClickListener((adapterView, view, i, l) -> {
            city = adapter.getItem(i).toString();
            city_id = ids.get(citys.indexOf(city));
            Log.e("job", ids.get(citys.indexOf(city)) + " | " + city);
            if (!lbl_city.isLabelAvailable(city)) {
                onError("Tidak bisa memasukkan kota yang sama");
                txt_work_city.setText("");
            } else if (!city.isEmpty()){
                lbl_city.addLabelSelected(city, city_id);
                txt_work_city.setText("");
            }
        });
    }

    @Override
    public void onResultEditWork(CandidateResponse.ChangeProfile.Data workResponse) {
        finish();
    }

    @Override
    public void onResultInsertWork() {
        finish();
    }

}
