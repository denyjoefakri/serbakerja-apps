package com.joefakri.serbakerja.ui.form.doc;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.form.company.FormCompanyView;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface FormDocPhotoMvpPresenter<V extends FormDocPhotoView> extends SerbakerjaPresenter<V> {

    void init();

    void readCameraGallery(Activity activity, Fragment fragment, View v);

    void openImageChooser(Activity activity, View v);

    void onPostCandidateDocument();

    void saveDocKTP(String data);
    void saveDocIjazah(String data);
    void saveDocPhoto(String data);

}
