package com.joefakri.serbakerja.ui.form.education;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormEducationNameCoursesFragment extends BaseFragment implements FormEducationView{

    @Inject FormEducationMvpPresenter<FormEducationView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_education_name_courses) EditText txt_education_name_courses;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_education_name_courses, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_4_kandidat);
            dialog.setOnMessageClosed(() -> {
                mPresenter.saveEducationNameCourses(txt_education_name_courses.getText().toString());
                mPresenter.onPostCandidateEducation(getActivity());
            });

        });
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultCandidateEducation(CandidateResponse.EducationResponse.Data dataList) {
        getActivity().finish();
        /*setEvent(4);
        ((FormOptionalActivity)getActivity()).last_form_candidate();*/
    }

    @Override
    public void init(CandidateRealm candidateRealm) {
        if (candidateRealm.getEducationNameCourse() != null){
            txt_education_name_courses.setText(candidateRealm.getEducationNameCourse());
        }
    }
}
