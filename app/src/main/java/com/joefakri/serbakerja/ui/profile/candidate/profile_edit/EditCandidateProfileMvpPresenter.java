package com.joefakri.serbakerja.ui.profile.candidate.profile_edit;

import android.app.Activity;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface EditCandidateProfileMvpPresenter<V extends EditCandidateProfileView> extends SerbakerjaPresenter<V> {

    void readCameraGallery(Activity activity, View v);

    void openImageChooser(Activity activity, View v);

    void onUpdate(String photo, String... strings);
}
