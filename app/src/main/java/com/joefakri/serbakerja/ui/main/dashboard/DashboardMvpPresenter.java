package com.joefakri.serbakerja.ui.main.dashboard;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.main.MainView;

/**
 * Created by deny on bandung.
 */

public interface DashboardMvpPresenter<V extends DashboardView> extends SerbakerjaPresenter<V> {

    void onDefaultJob();

    void onDefaultCity();

    void onViewList(String offset);

    void onViewListCityOrJob(String city, String job, String offset);

    void onViewSearch(String keyword, String offset);

    void onApply(String id);

    void onCustomLocation(String lat, String lng, String offset);

}
