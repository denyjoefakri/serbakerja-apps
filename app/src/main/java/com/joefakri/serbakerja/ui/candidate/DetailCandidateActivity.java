package com.joefakri.serbakerja.ui.candidate;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExpandAdapter;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.ui.profile.photo.DetailPhotoActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.EasyPermissions;

public class DetailCandidateActivity extends BaseActivity implements DetailCandidateView, EasyPermissions.PermissionCallbacks{

    @Inject DetailCandidateMvpPresenter<DetailCandidateView> presenter;

    @BindView(R.id.btn_contact_toolbar) TextView btn_contact_toolbar;
    @BindView(R.id.txt_title_toolbar) TextView txt_title_toolbar;
    @BindView(R.id.btn_back) ImageButton btn_back;

    @BindView(R.id.txt_name_header) TextView txt_name_header;
    @BindView(R.id.txt_age_header) TextView txt_age_header;
    @BindView(R.id.txt_gender_header) TextView txt_gender_header;
    @BindView(R.id.img_user) CircleImageView img_user;

    @BindView(R.id.rv_work_expand) RecyclerView rv_work_expand;

    @BindView(R.id.txt_work_status) TextView txt_work_status;
    @BindView(R.id.txt_work_last_sallary) TextView txt_work_last_sallary;
    @BindView(R.id.rv_work_experience) RecyclerView rv_work_experience;

    @BindView(R.id.txt_education_graduated) TextView txt_education_graduated;
    @BindView(R.id.txt_education_last) TextView txt_education_last;
    @BindView(R.id.txt_education_name_last_school) TextView txt_education_name_last_school;
    @BindView(R.id.txt_education_name_courses) TextView txt_education_name_courses;
    @BindView(R.id.txt_no_hp) TextView txt_no_hp;
    @BindView(R.id.btn_contact) Button btn_contact;

    @Inject LinearLayoutManager mLayoutManager;
    @Inject LinearLayoutManager mLayoutManager2;
    WorkExperienceAdapter experienceAdapter;
    WorkExpandAdapter expandAdapter;

    CandidateResponse.DetailInfo.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_detail_candidate, this);
        getActivityComponent().inject(this);

        presenter.onAttach(DetailCandidateActivity.this);
        initComponent();

    }

    @Override
    protected void initComponent() {
        txt_title_toolbar.setText(getString(R.string.txt_detail_candidate));
        btn_back.setOnClickListener(view -> {
            finish();
        });
        btn_contact_toolbar.setOnClickListener(view -> {
            PopupMenu popupMenu = new PopupMenu(this, view);
            popupMenu.getMenuInflater().inflate(R.menu.option_contact_candidate, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                if (item.getItemId() == R.id.option_call) {
                    presenter.permissionCall(this, txt_no_hp.getText().toString());
                    return true;
                } else if (item.getItemId() == R.id.option_sms){
                    presenter.permissionSMS(this, txt_no_hp.getText().toString());
                    return true;
                } else if (item.getItemId() == R.id.option_chat) {
                    Intent intent = new Intent(this, ChatRoomActivity.class);
                    intent.putExtra("target", data.getPengguna_id());
                    intent.putExtra("image", data.getFoto_terbaru());
                    intent.putExtra("name", data.getNama_ktp());
                    startActivity(intent);
                    return true;
                } else if (item.getItemId() == R.id.option_wa) {
                    presenter.whatsapp(this, txt_no_hp.getText().toString());
                    return true;
                }
                return false;
            });
            popupMenu.show();
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work_experience.setLayoutManager(mLayoutManager);
        rv_work_experience.setHasFixedSize(true);
        rv_work_experience.setNestedScrollingEnabled(false);
        rv_work_experience.setAdapter(experienceAdapter = new WorkExperienceAdapter(this));

        mLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work_expand.setLayoutManager(mLayoutManager2);
        rv_work_expand.setHasFixedSize(true);
        rv_work_expand.setNestedScrollingEnabled(false);
        rv_work_expand.setAdapter(expandAdapter = new WorkExpandAdapter(this));


        presenter.onViewDetail(getIntent().getStringExtra("id"));


    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResultViewDetail(CandidateResponse.DetailInfo.Data detailInfo) {
        data = detailInfo;
        Glide.with(this).load(detailInfo.getFoto_terbaru()).into(img_user);
        txt_name_header.setText(detailInfo.getNama_ktp());
        txt_age_header.setText(TimeUtils.getAge(detailInfo.getTgl_lahir()) + " Tahun");
        txt_gender_header.setText(Constants.genderValue(this, detailInfo.getJenis_kelamin()));
        txt_no_hp.setText(detailInfo.getNo_hp());
        btn_contact.setEnabled(true);

        ArrayList<WorkModel> workModels1 = new ArrayList<>();
        for (int i = 0; i < detailInfo.getMinat().size(); i++) {
            WorkModel workModel = new WorkModel();
            workModel.setWork(detailInfo.getMinat().get(i).getJenis_pekerjaan());
            workModel.setSalary(Constants.sallary(this, detailInfo.getMinat().get(i).getGaji_min(), detailInfo.getMinat().get(i).getGaji_max()));
            workModel.setWork_time(detailInfo.getMinat().get(i).getWaktu_kerja());
            workModel.setWork_time_other(detailInfo.getMinat().get(i).getWaktu_kerja_lainnya());
            workModel.setWork_day(detailInfo.getMinat().get(i).getHari_kerja());
            workModel.setArea(detailInfo.getMinat().get(i).getDaerah());

            ArrayList<WorkModel.City> cities = new ArrayList<>();
            for (int j = 0; j < detailInfo.getMinat().get(i).getKota().size(); j++) {
                WorkModel.City city = new WorkModel().new City();
                city.setId(detailInfo.getMinat().get(i).getKota().get(j).getId());
                city.setName(detailInfo.getMinat().get(i).getKota().get(j).getName());
                cities.add(city);
            }
            workModel.setCitys(cities);
            workModels1.add(workModel);
        }
        expandAdapter.update(workModels1);

        if (detailInfo.getStatus_pekerjaan_terakhir().equals("3")){
            txt_work_status.setText(detailInfo.getStatus_pekerjaan_terakhir_lainnya());
        } else {
            txt_work_status.setText(Constants.workStatusValue(this, detailInfo.getStatus_pekerjaan_terakhir()));
        }
        txt_work_last_sallary.setText(Constants.sallary(this, detailInfo.getGaji_min_terakhir(), detailInfo.getGaji_max_terakhir()));
        ArrayList<WorkModel> workModels2 = new ArrayList<>();
        for (int i = 0; i < detailInfo.getPengalaman().size(); i++) {
            WorkModel workModel = new WorkModel();
            workModel.setWork_experience(detailInfo.getPengalaman().get(i).getPengalaman_kerja());
            workModel.setCompany_name(detailInfo.getPengalaman().get(i).getNama_perusahaan());
            workModel.setWork_long(detailInfo.getPengalaman().get(i).getLama_bekerja() + " " + Constants.getPeriod(detailInfo.getPengalaman().get(i).getPeriod()));
            workModel.setWork_as(detailInfo.getPengalaman().get(i).getPosisi());
            workModels2.add(workModel);
        }
        experienceAdapter.update(workModels2);

        txt_education_graduated.setText(detailInfo.getThn_pend_terakhir());
        txt_education_last.setText(Constants.education(this, detailInfo.getPend_terakhir()));
        txt_education_name_last_school.setText(detailInfo.getSekolah_terakhir());
        txt_education_name_courses.setText(detailInfo.getKursus());

        img_user.setOnClickListener(view -> {
            Intent intent = new Intent(DetailCandidateActivity.this, DetailPhotoActivity.class);
            intent.putExtra("image", detailInfo.getFoto_terbaru());
            startActivity(intent);
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == 10){
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+txt_no_hp.getText().toString()));
            startActivity(callIntent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.fromParts("sms", txt_no_hp.getText().toString(), null)));
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_callphone);
            dialog.setOnMessageClosed(() -> {
                if (requestCode == 10) {
                    presenter.permissionCall(this, txt_no_hp.getText().toString());
                } else {
                    presenter.permissionSMS(this, txt_no_hp.getText().toString());
                }
            });
        }
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.option_call) {
            presenter.permissionCall(this, txt_no_hp.getText().toString());
            return true;
        } else if (id == R.id.option_sms){
            presenter.permissionSMS(this, txt_no_hp.getText().toString());
            return true;
        } else if (id == R.id.option_chat) {
            Intent intent = new Intent(this, ChatRoomActivity.class);
            intent.putExtra("target", data.getPengguna_id());
            intent.putExtra("image", data.getFoto_terbaru());
            intent.putExtra("name", data.getNama_ktp());
            startActivity(intent);
            return true;
        } else if (id == R.id.option_wa) {
            presenter.whatsapp(this, txt_no_hp.getText().toString());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

}
