package com.joefakri.serbakerja.ui.profile.password;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.data.model.ProfileModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class EditPasswordActivity extends BaseActivity implements EditPasswordView{

    @Inject EditPasswordMvpPresenter<EditPasswordView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_password_old) EditText txt_password_old;
    @BindView(R.id.txt_password_new) EditText txt_password_new;
    @BindView(R.id.txt_password_new_confirm) EditText txt_password_new_confirm;
    @BindView(R.id.btn_save) Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_password, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_password));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {

        btn_save.setOnClickListener(view -> {
            if (txt_password_old.getText().toString().isEmpty()) {
                onError("Kata sandi lama tidak boleh kosong");
                return;
            }

            if (txt_password_new.getText().toString().isEmpty()) {
                onError("Kata sandi baru tidak boleh kosong");
                return;
            }

            if (txt_password_new_confirm.getText().toString().isEmpty()) {
                onError("Konfirmasi kata sandi baru tidak boleh kosong");
                return;
            }

            if (!txt_password_new_confirm.getText().toString().equals(txt_password_new.getText().toString())){
                onError("Konfirmasi kata sandi baru tidak sama");
                return;
            }

            presenter.onEditPassword(txt_password_old.getText().toString(), txt_password_new.getText().toString(),
                    txt_password_new_confirm.getText().toString());

        });
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResultChangePassword() {
        finish();
    }

}
