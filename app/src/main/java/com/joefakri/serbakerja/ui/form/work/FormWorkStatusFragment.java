package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkStatusFragment extends BaseFragment implements FormWorkView{

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.rd_status) RadioGroup rd_status;
    RadioButton rd;
    @BindView(R.id.txt_options_time_4) EditText txt_options_time_4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_status, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (rd_status.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu status pekerjaan yang tersedia");
                return;
            }

            mPresenter.saveCandidateExperienceStatus(Constants.workStatusPosition(getActivity(), get()), txt_options_time_4.getText().toString());
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(1);
        });

        rd_status.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton radioButton = v.findViewById(i);
            if (radioButton.getText().equals(getString(R.string.options_status_4))) {
                txt_options_time_4.setVisibility(View.VISIBLE);
                txt_options_time_4.setText("");
            }
            else {
                txt_options_time_4.setVisibility(View.GONE);
                txt_options_time_4.setText("");
            }
        });
    }

    private String get(){
        int selectedId= rd_status.getCheckedRadioButtonId();
        rd = getActivity().findViewById(selectedId);
        return rd.getText().toString();
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (candidateRealm != null){
            if (candidateRealm.getWorkStatus() != null){
                ((RadioButton)rd_status.getChildAt(Integer.parseInt(candidateRealm.getWorkStatus()))).setChecked(true);
                if (candidateRealm.getWorkStatus().equals("3")) {
                    txt_options_time_4.setVisibility(View.VISIBLE);
                    txt_options_time_4.setText(candidateRealm.getWorkStatusOptional());
                }
            }

        }
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }
}
