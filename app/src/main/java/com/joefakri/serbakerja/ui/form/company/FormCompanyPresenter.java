/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form.company;

import android.Manifest;
import android.app.Activity;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.google.gson.Gson;
import com.joefakri.serbakerja.BuildConfig;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class FormCompanyPresenter<V extends FormCompanyView> extends BasePresenter<V>
        implements FormCompanyMvpPresenter<V>{

    private Realm realm;

    @Inject
    public FormCompanyPresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void init() {
        realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        if (recruiter != null) getSerbakerjaView().init(recruiter);
    }

    @Override
    public void saveCompanyName(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setCompanyName(data);
            recruiter.setPercent1(1);
        } else {
            recruiter = realm.createObject(RecruiterRealm.class);
            recruiter.setId(1);
            recruiter.setCompanyName(data);
            recruiter.setPercent1(1);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCompanyAddress(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setCompanyAddress(data);
            recruiter.setPercent1(2);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCompanyCity(String id, String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null) {
            recruiter.setCompanyCity(data);
            recruiter.setCompanyCityId(id);
            recruiter.setPercent1(3);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCompanyArea(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null) {
            recruiter.setCompanyArea(data);
            recruiter.setPercent1(4);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCompanyTotalEmployee(String code) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null) {
            recruiter.setCompanyTotalEmployeCode(code);
            recruiter.setPercent1(5);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCompanyWebsite(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null) {
            recruiter.setCompanyWebsite(data);
            recruiter.setPercent1(6);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCompanyLogo(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null) {
            recruiter.setCompanyLogo(data);
        }
        realm.commitTransaction();
        realm.close();
    }


    @Override
    public void readCameraGallery(Activity activity, Fragment fragment, View v) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (hasPermission) {
            openImageChooser(activity, v);
        } else {
            EasyPermissions.requestPermissions(fragment, activity.getString(R.string.message_permission_camera), 0,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void openImageChooser(Activity activity, View v) {
        PopupMenu popupMenu = new PopupMenu(activity, v);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_camera) {
                takePhoto(activity);
            } else {
                openGallery(activity);
            }
            return false;
        });
        popupMenu.show();
    }

    private void takePhoto(Activity activity){
        ShutterTakePhotoBuilder shutter = Shutter.Companion.with(activity)
                .takePhoto().usePrivateAppInternalStorage()
                .addPhotoToGallery();

        ((FormActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().takePhoto(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath());
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void openGallery(Activity activity){
        ShutterPickPhotoGalleryBuilder shutter = Shutter.Companion.with(activity)
                .getPhotoFromGallery().usePrivateAppInternalStorage();

        ((FormActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().openGallery(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath());
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }


    private File compress(Activity activity, String file){
        File fileToSend;
        String nama = "";
        if (new File(file).getName().indexOf(".") > 0)
            nama = new File(file).getName().substring(0, new File(file).getName().lastIndexOf("."));

        String extension = new File(file).getAbsolutePath().substring(new File(file).getAbsolutePath().lastIndexOf("."));

        try {
            fileToSend = new Compressor(activity)
                    .setMaxWidth(1000)
                    .setMaxHeight(1000)
                    .setQuality(90)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(new File(file), nama + "_Serbakerja" + extension);
            Log.e("compress", fileToSend.getAbsolutePath());
            return fileToSend;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onViewCity() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCity()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(cityResponse -> {

                    LogResponse(cityResponse, Path.GET_CITY);
                    Gson gson = new Gson();
                    AreaResponse.CityResponse response = gson.fromJson(cityResponse.toString(), AreaResponse.CityResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().city(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostCompany() {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        getCompositeDisposable().add(getDataManager()
                .postRCompany(new RecruiterRequest.PostCompany(getDataManager().getCurrentUserId(), recruiter.getCompanyName(),
                        recruiter.getCompanyAddress(), recruiter.getCompanyCityId(), recruiter.getCompanyArea(),
                        recruiter.getCompanyTotalEmployeCode(), recruiter.getCompanyWebsite()), new File(recruiter.getCompanyLogo()))

                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(companyResponse -> {
                    LogResponse(companyResponse, Path.POST_RECURITER_COMPANY);
                    JSONObject data = companyResponse.getJSONObject("data");
                    Gson gson = new Gson();
                    RecruiterResponse.CompanyResponse response = gson.fromJson(companyResponse.toString(), RecruiterResponse.CompanyResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCompany(response.getData());
                        recruiter.setPercent1(7);
                    } else {
                        JSONObject object = new JSONObject(response.getMessage());
                        Log.e("error", object.getString("message"));
                        getSerbakerjaView().onError(object.getString("message"));
                        //getSerbakerjaView().onErrorList(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }
                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }


}
