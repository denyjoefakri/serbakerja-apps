package com.joefakri.serbakerja.ui.form.input_work;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;

/**
 * Created by deny on bandung.
 */

public interface InputWorkMvpPresenter<V extends InputWorkView> extends SerbakerjaPresenter<V> {

    void contactusOption(Activity activity, View v);

    void readPhone(Activity activity, View v);

    void initCustomTab(Activity activity);

    void onViewCity();

}
