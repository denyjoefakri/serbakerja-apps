package com.joefakri.serbakerja.ui.profile.candidate.profile_edit;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterResultListener;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class EditCandidateProfileActivity extends BaseActivity implements EditCandidateProfileView, EasyPermissions.PermissionCallbacks,
        DatePickerDialog.OnDateSetListener{

    @Inject EditCandidateProfileMvpPresenter<EditCandidateProfileView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_user) CircleImageView img_user;
    @BindView(R.id.txt_name_header) TextView txt_name_header;
    @BindView(R.id.txt_name) EditText txt_name;
    @BindView(R.id.txt_birthplace) EditText txt_birthplace;
    @BindView(R.id.txt_birthday) EditText txt_birthday;
    @BindView(R.id.txt_address) EditText txt_address;
    @BindView(R.id.txt_rtrw) EditText txt_rtrw;
    @BindView(R.id.txt_village) EditText txt_village;
    @BindView(R.id.txt_district) EditText txt_district;
    @BindView(R.id.txt_religion) EditText txt_religion;
    @BindView(R.id.txt_phone) EditText txt_phone;
    @BindView(R.id.btn_change_photo) ImageButton btn_change_photo;
    @BindView(R.id.btn_save) Button btn_save;
    @BindView(R.id.rd_gender) RadioGroup rd_gender;
    DatePickerDialog dpd;

    private String fileToSend;
    public ShutterResultListener shutterListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_candidate_profile, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_profile));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {

        CandidateResponse.CandidateProfile.Data data = (CandidateResponse.CandidateProfile.Data) getIntent().getSerializableExtra("model");

        if (data != null) {
            if (data.getFoto_terbaru() != null) Glide.with(this).load(data.getFoto_terbaru()).into(img_user);
            if (data.getNama_ktp() != null) txt_name_header.setText(data.getNama_ktp());
            if (data.getNama_ktp() != null) txt_name.setText(data.getNama_ktp());
            if (data.getTempat_lahir() != null) txt_birthplace.setText(data.getTempat_lahir());
            if (data.getTgl_lahir() != null) txt_birthday.setText(data.getTgl_lahir());
            if (data.getAlamat_ktp() != null) txt_address.setText(data.getAlamat_ktp());
            if (data.getRt_rw() != null) txt_rtrw.setText(data.getRt_rw());
            if (data.getKelurahan() != null) txt_village.setText(data.getKelurahan());
            if (data.getKecamatan() != null) txt_district.setText(data.getKecamatan());
            if (data.getJenis_kelamin() != null) ((RadioButton)rd_gender.getChildAt(Integer.parseInt(data.getJenis_kelamin()))).setChecked(true);
            if (data.getAgama() != null) txt_religion.setText(data.getAgama());
            if (data.getNo_hp() != null) txt_phone.setText(data.getNo_hp());
        }

        txt_birthday.setOnFocusChangeListener((view, b) -> {
            if (b){
                Calendar now = Calendar.getInstance();
                dpd = DatePickerDialog.newInstance(this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "DatepickerdialogBirthday");
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_change_photo.setOnClickListener(view -> presenter.readCameraGallery(this, view));

        btn_save.setOnClickListener(view -> {
            if (txt_name.getText().toString().isEmpty() ) {
                onError("Nama tidak boleh kosong");
                return;
            } else if (txt_birthplace.getText().toString().isEmpty() ) {
                onError("Tempat lahir tidak boleh kosong");
                return;
            } else if (txt_birthday.getText().toString().isEmpty() ) {
                onError("Tanggal lahir tidak boleh kosong");
                return;
            } else if (txt_address.getText().toString().isEmpty()) {
                onError("Alamat tidak boleh kosong");
                return;
            } else if (txt_district.getText().toString().isEmpty()) {
                onError("Kecamatan tidak boleh kosong");
                return;
            } else if (txt_village.getText().toString().isEmpty()) {
                onError("Kelurahan / Desa tidak boleh kosong");
                return;
            } else if (txt_rtrw.getText().toString().isEmpty() ) {
                onError("RT / RW tidak boleh kosong");
                return;
            } else if (rd_gender.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu jenis kelamin yang tersedia");
                return;
            } else if (txt_religion.getText().toString().isEmpty()){
                onError("Agama tidak boleh kosong");
                return;
            }

            String gender = Constants.gender(this, getGender());
            presenter.onUpdate(fileToSend, txt_name.getText().toString(), txt_birthplace.getText().toString(),
                    txt_birthday.getText().toString(), txt_address.getText().toString(),
                    txt_district.getText().toString(), txt_village.getText().toString(),
                    gender, txt_rtrw.getText().toString(), txt_religion.getText().toString());
        });
    }

    private String getGender(){
        int selectedId= rd_gender.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String mount = String.valueOf(monthOfYear + 1);
        String day = String.valueOf(dayOfMonth);

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
        try {
            calendar.setTime(dateFormat.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mount.length() <= 1 &&  day.length() <= 1){
            txt_birthday.setText("0"+dayOfMonth + "-" +"0"+(monthOfYear+1)+"-" + year);
        }else if (day.length() <= 1){
            txt_birthday.setText("0"+dayOfMonth +"-"+(monthOfYear+1)+"-"+ year);
        }else if (mount.length() <= 1){
            txt_birthday.setText(dayOfMonth+"-"+"0"+(monthOfYear+1)+"-"+ year);
        }else {
            txt_birthday.setText(dayOfMonth + "-" + (monthOfYear + 1)+"-"+ year);
        }
    }

    @Override
    public void takePhoto(String file) {
        Glide.with(this).load(file).into(img_user);
        fileToSend = file;
    }

    @Override
    public void openGallery(String file) {
        Glide.with(this).load(file).into(img_user);
        fileToSend = file;
    }

    @Override
    public void onResult(CandidateResponse.ChangeProfile changeProfileData) {
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!shutterListener.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        presenter.openImageChooser(this, btn_change_photo);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                presenter.readCameraGallery(this, btn_change_photo);
            });
        }
    }
}
