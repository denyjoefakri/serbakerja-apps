package com.joefakri.serbakerja.ui.bio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExpandAdapter;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.CityRealm;
import com.joefakri.serbakerja.data.realm.ExperienceRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.data.realm.StateRealm;
import com.joefakri.serbakerja.data.realm.WorkRealm;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateMvpPresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateView;
import com.joefakri.serbakerja.ui.profile.photo.DetailPhotoActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.joefakri.serbakerja.utils.autosize.AutofitTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmResults;

public class ChooseRoleActivity extends BaseActivity implements ChooseRoleView{

    @Inject ChooseRoleMvpPresenter<ChooseRoleView> presenter;
    @Inject DataManager dataManager;

    @BindView(R.id.view_role) LinearLayout view_role;
    @BindView(R.id.btn_candidate) LinearLayout btn_candidate;
    @BindView(R.id.btn_recruiters) LinearLayout btn_recruiters;
    @BindView(R.id.txt_candidate) AutofitTextView txt_candidate;
    @BindView(R.id.txt_reruiters) AutofitTextView txt_reruiters;
    @BindView(R.id.img_candidate) ImageView img_candidate;
    @BindView(R.id.img_recruiters) ImageView img_recruiters;
    @BindView(R.id.optional_form_kandidat) LinearLayout optional_form_kandidat;
    @BindView(R.id.optional_form_perekrut) LinearLayout optional_form_perekrut;

    @BindView(R.id.btn_form_1_perekrut) View btn_form_1_perekrut;
    @BindView(R.id.sb_form_1_perekrut) SeekBar sb_form_1_perekrut;
    @BindView(R.id.btn_form_2_perekrut) View btn_form_2_perekrut;
    @BindView(R.id.sb_form_2_perekrut) SeekBar sb_form_2_perekrut;
    @BindView(R.id.btn_form_3_perekrut) View btn_form_3_perekrut;
    @BindView(R.id.sb_form_3_perekrut) SeekBar sb_form_3_perekrut;
    @BindView(R.id.btn_form_1_candidate) View btn_form_1_candidate;
    @BindView(R.id.sb_form_1_candidate) SeekBar sb_form_1_candidate;
    @BindView(R.id.btn_form_2_candidate) View btn_form_2_candidate;
    @BindView(R.id.sb_form_2_candidate) SeekBar sb_form_2_candidate;
    @BindView(R.id.btn_form_3_candidate) View btn_form_3_candidate;
    @BindView(R.id.sb_form_3_candidate) SeekBar sb_form_3_candidate;
    @BindView(R.id.btn_form_4_candidate) View btn_form_4_candidate;
    @BindView(R.id.sb_form_4_candidate) SeekBar sb_form_4_candidate;
    @BindView(R.id.btn_form_5_candidate) View btn_form_5_candidate;
    @BindView(R.id.sb_form_5_candidate) SeekBar sb_form_5_candidate;
    @BindView(R.id.txt_percent_form_1_candidate) TextView txt_percent_form_1_candidate;
    @BindView(R.id.txt_percent_form_2_candidate) TextView txt_percent_form_2_candidate;
    @BindView(R.id.txt_percent_form_3_candidate) TextView txt_percent_form_3_candidate;
    @BindView(R.id.txt_percent_form_4_candidate) TextView txt_percent_form_4_candidate;
    @BindView(R.id.txt_percent_form_5_candidate) TextView txt_percent_form_5_candidate;
    @BindView(R.id.txt_percent_form_1_perekrut) TextView txt_percent_form_1_perekrut;
    @BindView(R.id.txt_percent_form_2_perekrut) TextView txt_percent_form_2_perekrut;
    @BindView(R.id.txt_percent_form_3_perekrut) TextView txt_percent_form_3_perekrut;

    int state = 0;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_choose_role, this);
        getActivityComponent().inject(this);

        presenter.onAttach(ChooseRoleActivity.this);
        initComponent();

    }

    @Override
    protected void initComponent() {

        if (getIntent().getStringExtra("state") != null){
            if (getIntent().getStringExtra("state").equals("1")) {
                view_role.setVisibility(View.GONE);
                optional_form_kandidat.setVisibility(View.GONE);
                optional_form_perekrut.setVisibility(View.VISIBLE);
                presenter.initPercent(getIntent().getIntExtra("from", 0));
            } else if (getIntent().getStringExtra("state").equals("2")){
                view_role.setVisibility(View.GONE);
                optional_form_kandidat.setVisibility(View.VISIBLE);
                optional_form_perekrut.setVisibility(View.GONE);
                presenter.initPercent(getIntent().getIntExtra("from", 0));
            }
        }

        btn_candidate.setOnClickListener(view -> {
            dataManager.setTypeUser(Constants.ROLE_KANDIDAT);
            view_role.setVisibility(View.GONE);
            optional_form_kandidat.setVisibility(View.VISIBLE);
            optional_form_perekrut.setVisibility(View.GONE);
            presenter.initPercent(getIntent().getIntExtra("from", 0));
        });

        btn_recruiters.setOnClickListener(view -> {
            dataManager.setTypeUser(Constants.ROLE_PEREKRUT);
            view_role.setVisibility(View.GONE);
            optional_form_kandidat.setVisibility(View.GONE);
            optional_form_perekrut.setVisibility(View.VISIBLE);
            presenter.initPercent(getIntent().getIntExtra("from", 0));
        });

        sb_form_1_perekrut.setEnabled(false);
        btn_form_1_perekrut.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "r1");
            startActivity(i);
        });

        sb_form_2_perekrut.setEnabled(false);
        btn_form_2_perekrut.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "r2");
            startActivity(i);
        });

        sb_form_3_perekrut.setEnabled(false);
        btn_form_3_perekrut.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "r3");
            startActivity(i);
        });

        sb_form_1_candidate.setEnabled(false);
        btn_form_1_candidate.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "c1");
            startActivity(i);
        });

        sb_form_2_candidate.setEnabled(false);
        btn_form_2_candidate.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "c2");
            startActivity(i);
        });

        sb_form_3_candidate.setEnabled(false);
        btn_form_3_candidate.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "c3");
            startActivity(i);
        });

        sb_form_4_candidate.setEnabled(false);
        btn_form_4_candidate.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "c4");
            startActivity(i);
        });

        sb_form_5_candidate.setEnabled(false);
        btn_form_5_candidate.setOnClickListener(view -> {
            Intent i = new Intent(ChooseRoleActivity.this, FormActivity.class);
            i.putExtra("form", "c5");
            startActivity(i);
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        realm = Realm.getDefaultInstance();
        presenter.initPercent(getIntent().getIntExtra("from", 0));
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        presenter.close();
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onRecruter(String status1, String status2, String status3, String db1, String db2, String db3) {
        if (status1.equals("1")){
            txt_percent_form_1_perekrut.setText("100%");
            sb_form_1_perekrut.setProgress(100);
        } else if (!db1.isEmpty()){
            txt_percent_form_1_perekrut.setText(db1+"%");
            sb_form_1_perekrut.setProgress(Integer.valueOf(db1));
        } else {
            txt_percent_form_1_perekrut.setText("");
            sb_form_1_perekrut.setProgress(0);
        }

        if (txt_percent_form_1_perekrut.getText().toString().equals("100%")) {
            btn_form_1_perekrut.setEnabled(false);
            disableBtnCandidate();
        } else btn_form_1_perekrut.setEnabled(true);

        if (status2.equals("1")){
            txt_percent_form_2_perekrut.setText("100%");
            sb_form_2_perekrut.setProgress(100);
        } else if (!db2.isEmpty()){
            txt_percent_form_2_perekrut.setText(db2+"%");
            sb_form_2_perekrut.setProgress(Integer.valueOf(db2));
        } else {
            txt_percent_form_2_perekrut.setText("");
            sb_form_2_perekrut.setProgress(0);
        }

        if (txt_percent_form_2_perekrut.getText().toString().equals("100%")) {
            btn_form_2_perekrut.setEnabled(false);
            disableBtnCandidate();
        } else btn_form_2_perekrut.setEnabled(true);

        if (status3.equals("1")){
            txt_percent_form_3_perekrut.setText("100%");
            sb_form_3_perekrut.setProgress(100);
        } else if (!db3.isEmpty()){
            txt_percent_form_3_perekrut.setText(db3+"%");
            sb_form_3_perekrut.setProgress(Integer.valueOf(db3));
        } else {
            txt_percent_form_3_perekrut.setText("");
            sb_form_3_perekrut.setProgress(0);
        }

        if (txt_percent_form_3_perekrut.getText().toString().equals("100%")) {
            btn_form_3_perekrut.setEnabled(false);
            disableBtnCandidate();
        } else btn_form_3_perekrut.setEnabled(true);

        if (dataManager.getTypeUser() != null) {
            if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)) {
                if (!txt_percent_form_1_perekrut.getText().toString().isEmpty()
                        || !txt_percent_form_2_perekrut.getText().toString().isEmpty()
                        || !txt_percent_form_3_perekrut.getText().toString().isEmpty()) state = 2;
                else state = 1;
            }
        }

        if (txt_percent_form_1_perekrut.getText().toString().equals("100%")
                && txt_percent_form_2_perekrut.getText().toString().equals("100%")
                && txt_percent_form_3_perekrut.getText().toString().equals("100%")){
            presenter.onLogin(this, dataManager.getTypeUser());
        }
    }

    @Override
    public void onCandidate(String status1, String status2, String status3, String status4,
                            String status5, String db1, String db2, String db3, String db4, String db5) {
        if (status1.equals("1")){
            txt_percent_form_1_candidate.setText("100%");
            sb_form_1_candidate.setProgress(100);
        } else if (!db1.isEmpty()){
            txt_percent_form_1_candidate.setText(db1+"%");
            sb_form_1_candidate.setProgress(Integer.valueOf(db1));
        } else {
            txt_percent_form_1_candidate.setText("");
            sb_form_1_candidate.setProgress(0);
        }

        if (txt_percent_form_1_candidate.getText().toString().equals("100%")) {
            btn_form_1_candidate.setEnabled(false);
            disableBtnRecruter();
        } else btn_form_1_candidate.setEnabled(true);

        if (status2.equals("1")){
            txt_percent_form_2_candidate.setText("100%");
            sb_form_2_candidate.setProgress(100);
        } else if (!db2.isEmpty()){
            txt_percent_form_2_candidate.setText(db2+"%");
            sb_form_2_candidate.setProgress(Integer.valueOf(db2));
        } else {
            txt_percent_form_2_candidate.setText("");
            sb_form_2_candidate.setProgress(0);
        }

        if (txt_percent_form_2_candidate.getText().toString().equals("100%")) {
            btn_form_2_candidate.setEnabled(false);
            disableBtnRecruter();
        } else btn_form_2_candidate.setEnabled(true);

        if (status3.equals("1")){
            txt_percent_form_3_candidate.setText("100%");
            sb_form_3_candidate.setProgress(100);
        } else if (!db3.isEmpty()){
            txt_percent_form_3_candidate.setText(db3+"%");
            sb_form_3_candidate.setProgress(Integer.valueOf(db3));
        } else {
            txt_percent_form_3_candidate.setText("");
            sb_form_3_candidate.setProgress(0);
        }

        if (txt_percent_form_3_candidate.getText().toString().equals("100%")) {
            btn_form_3_candidate.setEnabled(false);
            disableBtnRecruter();
        } else btn_form_3_candidate.setEnabled(true);

        if (status4.equals("1")){
            txt_percent_form_4_candidate.setText("100%");
            sb_form_4_candidate.setProgress(100);
        } else if (!db4.isEmpty()){
            txt_percent_form_4_candidate.setText(db4+"%");
            sb_form_4_candidate.setProgress(Integer.valueOf(db4));
        } else {
            txt_percent_form_4_candidate.setText("");
            sb_form_4_candidate.setProgress(0);
        }

        if (txt_percent_form_4_candidate.getText().toString().equals("100%")) {
            btn_form_4_candidate.setEnabled(false);
            disableBtnRecruter();
        } else btn_form_4_candidate.setEnabled(true);

        if (status5.equals("1")){
            txt_percent_form_5_candidate.setText("100%");
            sb_form_5_candidate.setProgress(100);
        } else if (!db5.isEmpty()){
            txt_percent_form_5_candidate.setText(db5+"%");
            sb_form_5_candidate.setProgress(Integer.valueOf(db5));
        } else {
            txt_percent_form_5_candidate.setText("");
            sb_form_5_candidate.setProgress(0);
        }

        if (txt_percent_form_5_candidate.getText().toString().equals("100%")) {
            btn_form_5_candidate.setEnabled(false);
            disableBtnRecruter();
        } else btn_form_5_candidate.setEnabled(true);

        if (dataManager.getTypeUser() != null) {
            if (dataManager.getTypeUser().equals(Constants.ROLE_KANDIDAT)) {
                if (!txt_percent_form_1_candidate.getText().toString().isEmpty()
                        || !txt_percent_form_2_candidate.getText().toString().isEmpty()
                        || !txt_percent_form_3_candidate.getText().toString().isEmpty()
                        || !txt_percent_form_4_candidate.getText().toString().isEmpty()
                        || !txt_percent_form_5_candidate.getText().toString().isEmpty()) state = 2;
                else state = 1;
            }
        }

        if (txt_percent_form_1_candidate.getText().toString().equals("100%")
                && txt_percent_form_2_candidate.getText().toString().equals("100%")
                && txt_percent_form_3_candidate.getText().toString().equals("100%")
                && txt_percent_form_4_candidate.getText().toString().equals("100%")
                && txt_percent_form_5_candidate.getText().toString().equals("100%")){
            presenter.onLogin(this, dataManager.getTypeUser());
        }
    }

    private void disableBtnRecruter(){
        btn_recruiters.setEnabled(false);
        btn_recruiters.setBackgroundColor(ContextCompat.getColor(this, R.color.grey300));
        txt_reruiters.setTextColor(ContextCompat.getColor(this, R.color.grey500));
        img_recruiters.setColorFilter(R.color.grey500);
    }

    private void disableBtnCandidate(){
        btn_candidate.setEnabled(false);
        btn_candidate.setBackgroundColor(ContextCompat.getColor(this, R.color.grey300));
        txt_candidate.setTextColor(ContextCompat.getColor(this, R.color.grey500));
        img_candidate.setColorFilter(R.color.grey500);
    }

    @Override
    public void onBackPressed() {
        if (state == 2){
            /*showConfirmMessage(R.string.message_save_bio).setOnMessageClosed(() -> {
                realm.beginTransaction();
                if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)){
                    RealmResults<RecruiterRealm> realmR = realm.where(RecruiterRealm.class).findAll();
                    realmR.deleteAllFromRealm();
                } else {
                    RealmResults<CandidateRealm> realmC = realm.where(CandidateRealm.class).findAll();
                    RealmResults<CityRealm> realmCity = realm.where(CityRealm.class).findAll();
                    RealmResults<ExperienceRealm> realmExpe = realm.where(ExperienceRealm.class).findAll();
                    RealmResults<WorkRealm> realmWork = realm.where(WorkRealm.class).findAll();
                    realmC.deleteAllFromRealm();
                    realmCity.deleteAllFromRealm();
                    realmExpe.deleteAllFromRealm();
                    realmWork.deleteAllFromRealm();
                }
                presenter.initPercent(getIntent().getIntExtra("from", 0));
                realm.commitTransaction();
            });*/
            view_role.setVisibility(View.VISIBLE);
            optional_form_kandidat.setVisibility(View.GONE);
            optional_form_perekrut.setVisibility(View.GONE);
            state = 0;
        } else if (state == 1) {
            view_role.setVisibility(View.VISIBLE);
            optional_form_kandidat.setVisibility(View.GONE);
            optional_form_perekrut.setVisibility(View.GONE);
            dataManager.setTypeUser(null);
            state = 0;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void finish() {
        super.finish();
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<RecruiterRealm> realmR = realm.where(RecruiterRealm.class).findAll();
        RealmResults<CandidateRealm> realmC = realm.where(CandidateRealm.class).findAll();
        RealmResults<CityRealm> realmCity = realm.where(CityRealm.class).findAll();
        RealmResults<ExperienceRealm> realmExpe = realm.where(ExperienceRealm.class).findAll();
        RealmResults<WorkRealm> realmWork = realm.where(WorkRealm.class).findAll();
        RealmResults<StateRealm> stateRealms = realm.where(StateRealm.class).findAll();
        realmR.deleteAllFromRealm();
        realmC.deleteAllFromRealm();
        realmCity.deleteAllFromRealm();
        realmExpe.deleteAllFromRealm();
        realmWork.deleteAllFromRealm();
        stateRealms.deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }

}
