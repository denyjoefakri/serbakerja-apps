/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.recruiter.workpost;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company.CompanyProfileView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class CompanyWorkPostPresenter<V extends CompanyWorkPostView> extends BasePresenter<V>
        implements CompanyWorkPostMvpPresenter<V> {

    @Inject
    public CompanyWorkPostPresenter(DataManager dataManager,
                                    SchedulerProvider schedulerProvider,
                                    CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewWork(String offset) {
        getSerbakerjaView().showLoading();
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());

        Logger.printLogRequest(Path.JOB_RECRUTER_PROFILE, id, null);

        getCompositeDisposable().add(getDataManager()
                .jobRecruterProfile(id, offset)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.JOB_RECRUTER_PROFILE);
                    Gson gson = new Gson();
                    WorkResponse.List response = gson.fromJson(jsonObject.toString(), WorkResponse.List.class);

                    if (response != null) {
                        getSerbakerjaView().onResultProfileView(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().hideLoading();
                    }


                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
