package com.joefakri.serbakerja.ui.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainActivity;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryActivity;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainActivity;
import com.joefakri.serbakerja.ui.setting.contactus.ContactUsActivity;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackActivity;
import com.joefakri.serbakerja.ui.setting.manageaccount.ManageAccountActivity;
import com.joefakri.serbakerja.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class SettingActivity extends BaseActivity implements SettingView{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.btn_profile) LinearLayout btn_profile;
    @BindView(R.id.btn_history) LinearLayout btn_history;
    @BindView(R.id.btn_change_password) LinearLayout btn_change_password;
    @BindView(R.id.btn_manage_account) LinearLayout btn_manage_account;
    @BindView(R.id.btn_payment) LinearLayout btn_payment;
    @BindView(R.id.btn_contact_us) LinearLayout btn_contact_us;
    @BindView(R.id.btn_feedback) LinearLayout btn_feedback;
    @BindView(R.id.btn_about_us) LinearLayout btn_about_us;
    @BindView(R.id.btn_logout) LinearLayout btn_logout;
    @BindView(R.id.divider_manage_account) View divider_manage_account;
    @Inject SettingMvpPresenter<SettingView> presenter;
    @Inject DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_setting, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();
    }
    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_setting));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        btn_profile.setOnClickListener(view -> {
            if (getIntent().getStringExtra("role").equals("perekrut")){
                startActivity(new Intent(SettingActivity.this, RecruiterMainActivity.class));
            } else startActivity(new Intent(SettingActivity.this, CandidateMainActivity.class));
        });

        btn_history.setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this, CompanyHistoryActivity.class));
        });

        btn_change_password.setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this, EditPasswordActivity.class));
        });

        btn_manage_account.setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this,  ManageAccountActivity.class));
        });

        btn_payment.setOnClickListener( view -> {});

        btn_contact_us.setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this, ContactUsActivity.class));
        });

        btn_feedback.setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this, FeedbackActivity.class));
        });

        btn_about_us.setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this, AppInfoActivity.class));
        });

        btn_logout.setOnClickListener(view -> {
            presenter.onLogout();
        });

        if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)){
            divider_manage_account.setVisibility(View.GONE);
            btn_manage_account.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onLogout() {
        Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
