package com.joefakri.serbakerja.ui.status.candidate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.StatusCandidateAdapter;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.model.StatusModel;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.register.RegisterMvpPresenter;
import com.joefakri.serbakerja.ui.register.RegisterView;
import com.joefakri.serbakerja.ui.work.DetailWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TextWatcherAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class StatusCandidateFragment extends BaseFragment implements StatusCandidateView, StatusCandidateAdapter.Listener{

    @BindView(R.id.nv_main) NestedScrollView nv_main;
    @BindView(R.id.rv_status) RecyclerView rv_status;
    @BindView(R.id.ed_query) EditText ed_query;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.tv_message) TextView tv_message;
    StatusCandidateAdapter statusAdapter;
    private EventBus bus = EventBus.getDefault();

    ArrayList<WorkResponse.Apply.Data> models;
    ArrayList<WorkResponse.Apply.Data> modelsSearch;

    @Inject StatusCandidateMvpPresenter<StatusCandidateView> presenter;
    @Inject LinearLayoutManager mLayoutManager;

    private int currentPage = 0;
    private boolean notLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status_candidate, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
        }
        return view;
    }


    @Override
    protected void initComponent(View view) {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_status.setLayoutManager(mLayoutManager);
        rv_status.setHasFixedSize(true);
        rv_status.setNestedScrollingEnabled(false);
        rv_status.setAdapter(statusAdapter = new StatusCandidateAdapter(getActivity()));
        //statusAdapter.update(Dummy.getStatus());

        statusAdapter.setOnclickListener(this);
        ed_query.setEnabled(false);

        ed_query.addTextChangedListener(watcherAdapter);

        nv_main.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v1, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v1.getChildAt(v1.getChildCount() - 1) != null) {
                if ((scrollY >= (v1.getChildAt(v1.getChildCount() - 1).getMeasuredHeight() - v1.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!notLoading){
                        notLoading = true;
                        currentPage += 20;
                        presenter.onViewStatus(String.valueOf(currentPage));
                        statusAdapter.addLoading();
                    }
                }
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getBasestate().equals("2")) {
            presenter.onViewStatus("0");
        }
    }


    TextWatcherAdapter watcherAdapter = new TextWatcherAdapter(){
        @Override
        public void afterTextChanged(Editable s) {
            super.afterTextChanged(s);
            search(ed_query.getText().toString());
        }
    };

    private void search(String keyword) {
        if (keyword == null || keyword.equals("")) {
            statusAdapter.update(models);
        } else {
            modelsSearch = new ArrayList<>();
            for (int i = 0; i < models.size(); i++) {
                WorkResponse.Apply.Data model = models.get(i);
                if (model.getNama_pekerjaan().toLowerCase().contains(keyword.toLowerCase())
                        || model.getNama_perusahaan().toLowerCase().contains(keyword.toLowerCase())
                        || model.getAlamat().toLowerCase().contains(keyword.toLowerCase())
                        || model.getGaji_min().toLowerCase().contains(keyword.toLowerCase())
                        || model.getGaji_max().toLowerCase().contains(keyword.toLowerCase())) {
                    modelsSearch.add(model);
                }
            }

            statusAdapter.update(modelsSearch);
        }
    }


    @Override
    public void onDestroyView() {
        presenter.onDetach();
        rv_status.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onResultStatus(ArrayList<WorkResponse.Apply.Data> data) {
        models = new ArrayList<>();
        if (data.isEmpty()) {
            if (statusAdapter.getItemCount() == 0){
                view_empty.setVisibility(View.VISIBLE);
                rv_status.setVisibility(View.GONE);
                tv_message.setText(getString(R.string.txt_work_empty));
            }
            if (notLoading) statusAdapter.removeLoading();
        } else {
            models = data;
            view_empty.setVisibility(View.GONE);
            rv_status.setVisibility(View.VISIBLE);
            ed_query.setEnabled(true);
            //statusAdapter.update(models);
            if (notLoading) {
                statusAdapter.removeLoading();
                statusAdapter.updateAll(data);
                notLoading = false;
            } else {
                statusAdapter.update(data);
            }
        }

    }

    @Override
    public void onErrorResult() {
        statusAdapter.update(new ArrayList<>());
    }

    @Override
    public void onJobClick(WorkResponse.Apply.Data model) {
        Intent intent = new Intent(getActivity(), DetailWorkActivity.class);
        intent.putExtra("id", model.getId_pekerjaan());
        startActivity(intent);
    }
}
