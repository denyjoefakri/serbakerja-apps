package com.joefakri.serbakerja.ui.profile.recruiter.company_edit;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.profile.photo.DetailPhotoActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterResultListener;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class EditCompanyProfileActivity extends BaseActivity implements EditCompanyProfileView, EasyPermissions.PermissionCallbacks{

    @Inject EditCompanyProfileMvpPresenter<EditCompanyProfileView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_company) CircleImageView img_company;
    @BindView(R.id.txt_company_name_header) TextView txt_company_name_header;
    @BindView(R.id.txt_company_name) EditText txt_company_name;
    @BindView(R.id.txt_company_address) EditText txt_company_address;
    @BindView(R.id.txt_company_city) AutoCompleteTextView txt_company_city;
    @BindView(R.id.txt_company_area) EditText txt_company_area;
    @BindView(R.id.rd_employee) RadioGroup rd_employee;
    RadioButton rd;
    @BindView(R.id.txt_company_website) EditText txt_company_website;
    @BindView(R.id.btn_change_photo) ImageButton btn_change_photo;
    @BindView(R.id.btn_save) Button btn_save;

    private File fileToSend ;
    public ShutterResultListener shutterListener;
    String city_id = "", city = "";
    RecruiterResponse.CompanyProfile.Data companyProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_company_profile, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_profile));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        showLoading();
        companyProfile = (RecruiterResponse.CompanyProfile.Data) getIntent().getSerializableExtra("model");

        if (companyProfile != null) {
            Glide.with(this).load(companyProfile.getLogo()).into(img_company);
            if (companyProfile.getNama_perusahaan() != null) txt_company_name_header.setText(companyProfile.getNama_perusahaan());
            if (companyProfile.getNama_perusahaan() != null) txt_company_name.setText(companyProfile.getNama_perusahaan());
            if (companyProfile.getAlamat_perusahaan() != null) txt_company_address.setText(companyProfile.getAlamat_perusahaan());
            if (companyProfile.getDaerah() != null) txt_company_area.setText(companyProfile.getDaerah());
            if (companyProfile.getNama_kota() != null) txt_company_city.setText(companyProfile.getNama_kota());
            if (companyProfile.getNama_kota() != null) city = companyProfile.getNama_kota();
            txt_company_city.dismissDropDown();
            if (companyProfile.getJumlah_karyawan() != null) ((RadioButton)rd_employee.getChildAt(Integer.parseInt(companyProfile.getJumlah_karyawan()))).setChecked(true);
            if (companyProfile.getWebsite() != null) txt_company_website.setText(companyProfile.getWebsite());
            if (companyProfile.getKota_id() != null) city_id = companyProfile.getKota_id();
        }

        img_company.setOnClickListener(view -> {
            Intent intent = new Intent(EditCompanyProfileActivity.this, DetailPhotoActivity.class);
            intent.putExtra("image", companyProfile.getLogo());
            startActivity(intent);
        });

        btn_change_photo.setOnClickListener(view -> readCameraGallery());

        btn_save.setOnClickListener(view -> {
            if (txt_company_name.getText().toString().isEmpty()) {
                onError("Nama perusahaan tidak boleh kosong");
                return;
            }

            if (txt_company_address.getText().toString().isEmpty()) {
                onError("Alamat perusahaan tidak boleh kosong");
                return;
            }

            if (txt_company_city.getText().toString().isEmpty() || !txt_company_city.getText().toString().equals(city)){
                onError("Pilih Kota / Kabupaten yang tersedia");
                return;
            }

            if (txt_company_area.getText().toString().isEmpty()) {
                onError("Daerah tidak boleh kosong");
                return;
            }

            if (rd_employee.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu jumlah karyawan yang tersedia");
                return;
            }

            if (companyProfile.getLogo().isEmpty()){
                onError("Foto tidak boleh kosong");
                return;
            }

            String id = Constants.getPositionTotalEmployee(this, getEmployee());
            presenter.onEdit(fileToSend, companyProfile.getPerusahaan_id(), txt_company_name.getText().toString(),
                    txt_company_address.getText().toString(), city_id, txt_company_area.getText().toString(),
                    id, txt_company_website.getText().toString());
        });

        presenter.onViewCity();
    }

    private String getEmployee(){
        int selectedId= rd_employee.getCheckedRadioButtonId();
        rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    public void onResultEdit(RecruiterResponse.ChangeCompany profile) {
        finish();
    }

    @Override
    public void onViewCity(List<AreaResponse.CityResponse.Data> dataList) {
        ArrayList<String> citys = new ArrayList<>();
        ArrayList<String> ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
            if (txt_company_city.getText().toString().equals(dataList.get(i).getName())){
                city_id = dataList.get(i).getId();
            }
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, citys);
        txt_company_city.setAdapter(adapter);
        txt_company_city.setOnItemClickListener((adapterView, view, i, l) -> {
            city = adapter.getItem(i).toString();
            String id = ids.get(citys.indexOf(city));
            Log.e("job", ids.get(citys.indexOf(city)) + " | " + city);
            city_id = ids.get(citys.indexOf(city));
        });

    }


    public void openImageChooser(Activity activity, View anchorView) {
        PopupMenu popupMenu = new PopupMenu(activity, anchorView);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_camera) {
                takePhoto();
            } else {
                openGallery();
            }
            return false;
        });
        popupMenu.show();
    }

    private void takePhoto(){
        ShutterTakePhotoBuilder shutter = Shutter.Companion.with(this)
                .takePhoto().usePrivateAppInternalStorage()
                .addPhotoToGallery();

        shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                if (shutterResult.getAbsoluteFilePath() != null) {
                    fileToSend = new File(shutterResult.getAbsoluteFilePath());
                    onResultPhoto(new File(shutterResult.getAbsoluteFilePath()));
                }
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void openGallery(){
        ShutterPickPhotoGalleryBuilder shutter = Shutter.Companion.with(this)
                .getPhotoFromGallery().usePrivateAppInternalStorage();

        shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                if (shutterResult.getAbsoluteFilePath() != null) {
                    fileToSend = new File(shutterResult.getAbsoluteFilePath());
                    onResultPhoto(new File(shutterResult.getAbsoluteFilePath()));
                }
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void readCameraGallery(){
        boolean hasPermission = EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasPermission) {
            openImageChooser(this, btn_change_photo);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.message_permission_camera), 0,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!shutterListener.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void onResultPhoto(File file){
        String nama = "";
        if (file.getName().indexOf(".") > 0)
            nama = file.getName().substring(0, file.getName().lastIndexOf("."));

        String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));

        try {
            fileToSend = new Compressor(this)
                    .setMaxWidth(1000)
                    .setMaxHeight(1000)
                    .setQuality(90)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(file, nama + "_Serbakerja" + extension);
            Log.e("compress", fileToSend.getAbsolutePath());
            companyProfile.setLogo(fileToSend.getAbsolutePath());
            Glide.with(this).load(fileToSend).into(img_company);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        openImageChooser(this, btn_change_photo);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                readCameraGallery();
            });
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

}
