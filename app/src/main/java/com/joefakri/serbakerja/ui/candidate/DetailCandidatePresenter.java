/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.candidate;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.chat.ChatMvpPresenter;
import com.joefakri.serbakerja.ui.chat.ChatView;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class DetailCandidatePresenter<V extends DetailCandidateView> extends BasePresenter<V>
        implements DetailCandidateMvpPresenter<V> {

    private CustomTabsClient mCustomTabsClient;
    private CustomTabsSession mCustomTabsSession;
    private CustomTabsServiceConnection mCustomTabsServiceConnection;
    private CustomTabsIntent mCustomTabsIntent;

    @Inject
    public DetailCandidatePresenter(DataManager dataManager,
                                    SchedulerProvider schedulerProvider,
                                    CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }



    public void initCustomTab(Activity activity) {
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                mCustomTabsClient= customTabsClient;
                mCustomTabsClient.warmup(0L);
                mCustomTabsSession = mCustomTabsClient.newSession(null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mCustomTabsClient= null;
            }
        };

        CustomTabsClient.bindCustomTabsService(activity, "com.android.chrome", mCustomTabsServiceConnection);

        mCustomTabsIntent = new CustomTabsIntent.Builder(mCustomTabsSession)
                .setToolbarColor(Color.parseColor("#2a2a2a"))
                .setSecondaryToolbarColor(Color.parseColor("#628D97"))
                .setShowTitle(true)
                .build();
    }


    @Override
    public void permissionCall(Activity activity, String phone) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CALL_PHONE);
        if (hasPermission) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+phone));
            activity.startActivity(callIntent);
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.message_permission_callphone), 10,
                    Manifest.permission.CALL_PHONE);
        }
    }

    @Override
    public void permissionSMS(Activity activity, String phone) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity,
                Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS);
        if (hasPermission) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone, null)));
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.message_permission_callphone), 20,
                    Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS);
        }
    }

    @Override
    public void whatsapp(Activity activity, String phone) {
        initCustomTab(activity);
        mCustomTabsIntent.launchUrl(activity, Uri.parse(Constants.BASE_API_WHATSAPP + phone) );
    }

    @Override
    public void onViewDetail(String candidate_id) {
        getSerbakerjaView().showLoading();

        DashboardRequest.RecruterDetail recruter = new DashboardRequest.RecruterDetail(candidate_id);
        Logger.printLogRequest(Path.DASHBOARD_RECRUTER_DETAIL, recruter, null);

        getCompositeDisposable().add(getDataManager()
                .dashboardPerekrutDetail(recruter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {

                    LogResponse(jsonObject, Path.DASHBOARD_RECRUTER_DETAIL);
                    Gson gson = new Gson();
                    CandidateResponse.DetailInfo response = gson.fromJson(jsonObject.toString(), CandidateResponse.DetailInfo.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultViewDetail(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
