package com.joefakri.serbakerja.ui.status.recruiter;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterFragmentApplication;
import com.joefakri.serbakerja.ui.status.recruiter.verify.StatusRecruiterFragmentVerify;
import com.joefakri.serbakerja.utils.CustomViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class StatusRecruiterFragment extends BaseFragment implements StatusRecruiterView{

    @Inject StatusRecruiterMvpPresenter<StatusRecruiterView> presenter;

    @BindView(R.id.btn_tab_application) TextView btn_tab_application;
    @BindView(R.id.btn_tab_verify) TextView btn_tab_verify;
    @BindView(R.id.vp_status_recruiter) CustomViewPager vp_status_recruiter;

    PagerAdapter pagerAdapter;
    private EventBus bus = EventBus.getDefault();
    StateEvent state;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status_recruiter, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
        }
        return view;
    }

    View.OnClickListener tabClickListener = v -> {
        if (v.getId() == R.id.btn_tab_application) {
            btn_tab_application.setBackgroundResource(R.drawable.bg_tab_select_left);
            btn_tab_application.setTextColor(Color.WHITE);
            btn_tab_verify.setBackgroundResource(R.drawable.bg_tab_unselect_right);
            btn_tab_verify.setTextColor(Color.BLACK);
            vp_status_recruiter.setCurrentItem(0);
            StateEvent stateEvent = new StateEvent();
            stateEvent.setType(0);
            bus.postSticky(stateEvent);
            //fab_add.setVisibility(View.VISIBLE);
        }else if (v.getId() == R.id.btn_tab_verify) {
            btn_tab_application.setBackgroundResource(R.drawable.bg_tab_unselect_left);
            btn_tab_application.setTextColor(Color.BLACK);
            btn_tab_verify.setBackgroundResource(R.drawable.bg_tab_select_right);
            btn_tab_verify.setTextColor(Color.WHITE);
            vp_status_recruiter.setCurrentItem(1);
            //fab_add.setVisibility(View.GONE);

            StateEvent stateEvent = new StateEvent();
            stateEvent.setType(1);
            bus.postSticky(stateEvent);
        }
    };

    @Override
    protected void initComponent(View view) {
        pagerAdapter = new PagerAdapter(getChildFragmentManager());
        btn_tab_application.setOnClickListener(tabClickListener);
        btn_tab_verify.setOnClickListener(tabClickListener);
    }

    class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return new StatusRecruiterFragmentApplication();
            else
                return new StatusRecruiterFragmentVerify();
        }

        @Override
        public int getCount() {
            return 2;
        }


    }

    @Override
    public void onDestroyView() {
        presenter.onDetach();
        vp_status_recruiter.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.state = state;
        if (state.getBasestate().equals("2")) {
            vp_status_recruiter.setAdapter(pagerAdapter);
            btn_tab_application.performClick();
        }
    }

}
