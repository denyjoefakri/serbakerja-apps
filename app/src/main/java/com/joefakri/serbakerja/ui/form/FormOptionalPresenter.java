/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form;

import android.app.Activity;
import android.content.Intent;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.login.LoginMvpPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;
import com.joefakri.serbakerja.ui.main.MainActivity;
import com.joefakri.serbakerja.utils.CommonUtils;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class FormOptionalPresenter<V extends FormOptionalView> extends BasePresenter<V>
        implements FormOptionalMvpPresenter<V> {

    @Inject
    public FormOptionalPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onViewPercent(StateEvent stateEvent) {
        if (stateEvent.getValue() != 0)
            getSerbakerjaView().setPercent(stateEvent.getType(), CommonUtils.percent(stateEvent.getTotal(), stateEvent.getValue()));
    }

    @Override
    public void onResetPercent(TextView... textViews) {
        textViews[0].setText("");
        textViews[1].setText("");
        textViews[2].setText("");
        textViews[3].setText("");
        textViews[4].setText("");
        textViews[5].setText("");
        textViews[6].setText("");
        textViews[7].setText("");
    }

    @Override
    public void onResetSeekbar(SeekBar... seekBars) {
        seekBars[0].setProgress(0);
        seekBars[1].setProgress(0);
        seekBars[2].setProgress(0);
        seekBars[3].setProgress(0);
        seekBars[4].setProgress(0);
        seekBars[5].setProgress(0);
        seekBars[6].setProgress(0);
        seekBars[7].setProgress(0);
    }

    @Override
    public void onLogin(Activity activity, String type) {

        UserRequest.Login login = new UserRequest.Login(getDataManager().getCurrentUserPhone(), getDataManager().getCurrentUserPassword());
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.LOGIN, login, null);

        getCompositeDisposable().add(getDataManager()
                .login(login)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.LOGIN);
                    Gson gson = new Gson();
                    UserResponse.Login response = gson.fromJson(jsonObject.toString(), UserResponse.Login.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getDataManager().setDefaultCity(null);
                        getDataManager().setDefaultJob(null);
                        getDataManager().updateUserInfo("", response.getData().getId(), response.getData().getNama(),
                                response.getData().getNo_hp(), response.getData().getEmail(), getDataManager().getCurrentUserPassword(), "");
                        getDataManager().updateAdditionalUserInfo(response.getData().getTipe_user(), response.getData().getStatus(),
                                response.getData().getKota(), response.getData().getJenis_pekerjaan());
                        getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);

                        resultLogin(activity, type);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private void resultLogin(Activity activity, String type){
        getDataManager().setTypeUser(type);
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
        getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);
    }

}
