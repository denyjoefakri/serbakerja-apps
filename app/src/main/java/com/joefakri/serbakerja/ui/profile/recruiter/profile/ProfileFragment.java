package com.joefakri.serbakerja.ui.profile.recruiter.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.recruiter.profile_edit.EditProfileActivity;
import com.joefakri.serbakerja.utils.TimeUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class ProfileFragment extends BaseFragment implements ProfileView {

    @Inject ProfileMvpPresenter<ProfileView> mPresenter;

    @BindView(R.id.btn_edit) LinearLayout btn_edit;
    @BindView(R.id.txt_name) TextView txt_name;
    @BindView(R.id.txt_birthday) TextView txt_birthday;
    @BindView(R.id.txt_birthplace) TextView txt_birthplace;
    @BindView(R.id.txt_address) TextView txt_address;
    @BindView(R.id.txt_district) TextView txt_district;
    @BindView(R.id.txt_village) TextView txt_village;
    @BindView(R.id.txt_rtrw) TextView txt_rtrw;
    @BindView(R.id.img_photo_ktp) ImageView img_photo_ktp;
    @BindView(R.id.img_photo) ImageView img_photo;

    RecruiterResponse.Profile.Data data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_profile, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        btn_edit.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
            intent.putExtra("model", data);
            startActivity(intent);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewProfile();
        btn_edit.setEnabled(false);
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultProfileView(RecruiterResponse.Profile.Data data) {
        btn_edit.setEnabled(true);
        this.data = data;
        txt_name.setText(data.getNama_ktp());
        txt_birthday.setText(TimeUtils.getTime(data.getTgl_lahir()));
        txt_birthplace.setText(data.getTempat_lahir());
        txt_address.setText(data.getAlamat_ktp());
        txt_district.setText(data.getKecamatan());
        txt_village.setText(data.getKelurahan());
        txt_rtrw.setText(data.getRt_rw());
        Glide.with(this).load(data.getFoto()).into(img_photo);
        Glide.with(this).load(data.getFoto_ktp()).into(img_photo_ktp);

        hideLoading();
    }

}
