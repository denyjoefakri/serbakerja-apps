package com.joefakri.serbakerja.ui.profile.candidate.experience_edit;

import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkView;

/**
 * Created by deny on bandung.
 */

public interface EditExperienceMvpPresenter<V extends EditExperienceView> extends SerbakerjaPresenter<V> {

    void onPostEditExperience( String... s);

    void onPostInsertExperience(WorkModel workModel, String... s);

    void onPostAllExperience(String... s);

}
