package com.joefakri.serbakerja.ui.work;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.profile.photo.DetailPhotoActivity;
import com.joefakri.serbakerja.ui.verify.VerifyMvpPresenter;
import com.joefakri.serbakerja.ui.verify.VerifyView;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailWorkActivity extends BaseActivity implements DetailWorkView{

    @Inject DetailWorkMvpPresenter<DetailWorkView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_company_name) TextView txt_company_name;
    @BindView(R.id.txt_location) TextView txt_location;
    @BindView(R.id.img_company) CircleImageView img_company;
    @BindView(R.id.btn_apply) Button btn_apply;

    @BindView(R.id.txt_jobs) TextView txt_jobs;
    @BindView(R.id.txt_salary) TextView txt_salary;
    @BindView(R.id.txt_time_work) TextView txt_time_work;
    @BindView(R.id.txt_day_work) TextView txt_day_work;
    @BindView(R.id.txt_address_work) TextView txt_address_work;
    @BindView(R.id.txt_gender_work) TextView txt_gender_work;
    @BindView(R.id.txt_education_work) TextView txt_education_work;
    @BindView(R.id.txt_experience_work) TextView txt_experience_work;
    @BindView(R.id.txt_requirement_work) TextView txt_requirement_work;
    @BindView(R.id.txt_scope_work) TextView txt_scope_work;
    @BindView(R.id.txt_work_expired_date) TextView txt_work_expired_date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_detail_work, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_detail_work));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();

    }

    @Override
    protected void initComponent() {
        presenter.onViewDetail(getIntent().getStringExtra("id"));
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResultViewDetail(WorkResponse.Detail.Data detailInfo) {
        Glide.with(this).load(detailInfo.getLogo()).into(img_company);
        txt_company_name.setText(detailInfo.getNama_perusahaan());
        txt_location.setText(detailInfo.getLokasi() + ", " + detailInfo.getKota());

        txt_jobs.setText(detailInfo.getJenis_pekerjaan());
        txt_salary.setText(Constants.sallary(this, detailInfo.getGaji_min(), detailInfo.getGaji_max()));
        if (!detailInfo.getWaktu_kerja().equals("2"))
            txt_time_work.setText(Constants.waktuKerjaValue(this, detailInfo.getWaktu_kerja()));
        else txt_time_work.setText(detailInfo.getWaktu_kerja_lainnya());

        txt_day_work.setText(detailInfo.getHari_kerja());
        txt_address_work.setText(detailInfo.getAlamat());
        txt_gender_work.setText(Constants.genderValue(this, detailInfo.getJenis_kelamin()));
        txt_education_work.setText(Constants.education(this, detailInfo.getMin_pendidikan()));
        txt_experience_work.setText(Constants.minimal_experience(this, detailInfo.getMin_pengalaman()));
        txt_requirement_work.setText(detailInfo.getSyarat_tambahan());
        txt_scope_work.setText(detailInfo.getLingkup_pekerjaan());
        txt_work_expired_date.setText(TimeUtils.getTime(detailInfo.getExpired_date()));

        if (!detailInfo.getStatus_lamar().equals("0")) {
            btn_apply.setEnabled(false);
            btn_apply.setBackgroundColor(ContextCompat.getColor(this, R.color.grey300));
            btn_apply.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            btn_apply.setText("Sudah Dilamar");
        }

        img_company.setOnClickListener(view -> {
            Intent intent = new Intent(DetailWorkActivity.this, DetailPhotoActivity.class);
            intent.putExtra("image", detailInfo.getLogo());
            startActivity(intent);
        });

        btn_apply.setOnClickListener(view -> {
            presenter.onApply(detailInfo.getId());
        });
    }

    @Override
    public void onResultApply() {
        btn_apply.setEnabled(false);
        btn_apply.setBackgroundColor(ContextCompat.getColor(this, R.color.grey300));
        btn_apply.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        btn_apply.setText("Sudah Dilamar");
        showMessage("Selamat! Anda telah berhasil melamar untuk pekerjaan ini");
    }

    @Override
    public void onErrorApply() {
        btn_apply.setEnabled(true);
        btn_apply.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_button_primary));
        btn_apply.setTextColor(ContextCompat.getColor(this, R.color.white));
    }


}
