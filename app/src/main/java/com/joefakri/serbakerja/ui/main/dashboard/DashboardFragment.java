package com.joefakri.serbakerja.ui.main.dashboard;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.DashboardCandidateAdapter;
import com.joefakri.serbakerja.adapter.DashboardWorkAdapter;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.ui.city.DataCityActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.ui.work.DetailWorkActivity;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class DashboardFragment extends BaseFragment implements DashboardView, DashboardCandidateAdapter.Listener, DashboardWorkAdapter.Listener,
        PlaceSelectionListener, EasyPermissions.PermissionCallbacks{

    @Inject DashboardMvpPresenter<DashboardView> mPresenter;

    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.sr_dashboard) SwipeRefreshLayout sr_dashboard;
    @BindView(R.id.nv_main) NestedScrollView nv_main;
    @BindView(R.id.rv_work_item) RecyclerView rv_work_item;
    @BindView(R.id.btn_filter) LinearLayout btn_filter;
    @BindView(R.id.view_filter) View view_filter;
    @BindView(R.id.view_custom_location) View view_custom_location;
    @BindView(R.id.txt_job) AutoCompleteTextView txt_job;
    @BindView(R.id.txt_city) AutoCompleteTextView txt_city;
    @BindView(R.id.img_location) ImageView img_location;
    @BindView(R.id.txt_total) TextView txt_total;
    @BindView(R.id.tv_message) TextView tv_message;
    @BindView(R.id.ed_query) EditText ed_query;
    @BindView(R.id.btn_oke) Button btn_oke;
    @BindView(R.id.txt_custom_location) TextView txt_custom_location;
    final int REQUEST_SELECT_PLACE = 1001;
    DashboardCandidateAdapter candidateAdapter;
    DashboardWorkAdapter workAdapter;

    private EventBus bus = EventBus.getDefault();

    @Inject LinearLayoutManager mLayoutManager;
    @Inject DataManager dataManager;

    String job_id = "";
    String city_id = "";
    String lat = "";
    String lng = "";
    String query = "";

    private int typeLoad = 0; // 0 = onViewList, 2 = onViewListCityOrJob, 3 = onCustomLocation, 4 = search
    private int currentPage = 0;
    private boolean notLoading = false;
    private boolean first = true;
    private int totalPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work_item.setLayoutManager(mLayoutManager);
        rv_work_item.setHasFixedSize(true);
        rv_work_item.setNestedScrollingEnabled(false);

        candidateAdapter = new DashboardCandidateAdapter(getActivity());
        workAdapter = new DashboardWorkAdapter(getActivity());
        nv_main.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v1, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v1.getChildAt(v1.getChildCount() - 1) != null) {
                if ((scrollY >= (v1.getChildAt(v1.getChildCount() - 1).getMeasuredHeight() - v1.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!notLoading){
                        notLoading = true;
                        currentPage += 20;
                        first = false;
                        if (typeLoad == 0) {
                            mPresenter.onViewList(String.valueOf(currentPage));
                        } else if (typeLoad == 2){
                            mPresenter.onViewListCityOrJob(city_id, job_id, String.valueOf(currentPage));
                        } else if (typeLoad == 3) {
                            mPresenter.onCustomLocation(lat, lng, String.valueOf(currentPage));
                        } else if (typeLoad == 4){
                            mPresenter.onViewSearch(query, String.valueOf(currentPage));
                        }

                        if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)) candidateAdapter.addLoading();
                        else workAdapter.addLoading();
                    }
                }
            }
        });

        sr_dashboard.setOnRefreshListener(() -> {
            if (dataManager.getTypeUser().equals(Constants.ROLE_KANDIDAT)) {
                if (workAdapter != null)
                    workAdapter.update(new ArrayList<>());
            }
            else {
                if (candidateAdapter != null)
                    candidateAdapter.update(new ArrayList<>());
            }
            typeLoad = 0;
            currentPage = 0;
            notLoading = false;
            first = true;
            mPresenter.onDefaultCity();
            mPresenter.onDefaultJob();
            mPresenter.onViewList("0");
            txt_custom_location.setText("");
            view_custom_location.setVisibility(View.GONE);
        });

        mPresenter.onDefaultCity();
        mPresenter.onDefaultJob();

        txt_job.setOnFocusChangeListener((view, b) -> {
            if (b){
                Intent intent = new Intent(getActivity(), DataWorkActivity.class);
                intent.putExtra("models", new ArrayList<WorkModel>());
                intent.putExtra("dashboard", true);
                startActivityForResult(intent, 11);
            }
        });

        txt_city.setOnFocusChangeListener((view, b) -> {
            if (b){
                Intent intent = new Intent(getActivity(), DataCityActivity.class);
                startActivityForResult(intent, 22);
            }
        });

        btn_filter.setOnClickListener(view -> {
            if (view_filter.getVisibility() == View.VISIBLE) collapse(view_filter);
            else expand(view_filter);
        });

        btn_oke.setOnClickListener(view -> {
            if (TextUtils.isEmpty(ed_query.getText().toString())) {
                ed_query.setError("Cannot be empty");
                return;
            }

            typeLoad = 4;
            notLoading = false;
            first = true;
            query = ed_query.getText().toString();
            mPresenter.onViewSearch(query, "0");
        });

        img_location.setOnClickListener(view -> {
            readLoccation();
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getBasestate().equals("0")) {
            if (city_id.isEmpty()|job_id.isEmpty()) {
                mPresenter.onDefaultCity();
                mPresenter.onDefaultJob();
                notLoading = false;
                first = true;
                mPresenter.onViewList("0");
            }
        }

    }

    @Override
    public void listPerekrut(ArrayList<DashboardResponse.Recruter.Data> dataList, String message) {
        sr_dashboard.setRefreshing(false);
        ed_query.setText("");
        rv_work_item.setAdapter(candidateAdapter);
        if (dataList.size() == 0) {
            if (first) {
                view_empty.setVisibility(View.VISIBLE);
                rv_work_item.setVisibility(View.GONE);
                tv_message.setText("Tidak Ada Kandidat");
                txt_total.setText("");
            }
            if (notLoading) candidateAdapter.removeLoading();
        } else {
            view_empty.setVisibility(View.GONE);
            rv_work_item.setVisibility(View.VISIBLE);
            txt_total.setText("Ditemukan "+String.valueOf(dataList.size()) + " Kandidat " + message);
            if (notLoading) {
                candidateAdapter.removeLoading();
                candidateAdapter.updateAll(dataList);
                notLoading = false;
            } else {
                candidateAdapter.update(dataList);
            }
        }
        candidateAdapter.setOnclickListener(this);

    }

    @Override
    public void listCandidate(ArrayList<DashboardResponse.Candidate.Data> dataList, String message) {
        sr_dashboard.setRefreshing(false);
        rv_work_item.setAdapter(workAdapter);
        ed_query.setText("");
        if (dataList.size() == 0) {
            if (first) {
                view_empty.setVisibility(View.VISIBLE);
                rv_work_item.setVisibility(View.GONE);
                tv_message.setText("Tidak Ada Pekerjaan");
                txt_total.setText("");
            }
            if (notLoading) workAdapter.removeLoading();
        } else {
            view_empty.setVisibility(View.GONE);
            rv_work_item.setVisibility(View.VISIBLE);
            txt_total.setText("Ditemukan "+String.valueOf(dataList.size()) + " Pekerjaan " + message);
            if (notLoading) {
                workAdapter.removeLoading();
                workAdapter.updateAll(dataList);
                notLoading = false;
            } else {
                workAdapter.update(dataList);
            }
        }

        workAdapter.setOnclickListener(this);

    }

    @Override
    public void onResultApply(String id) {
        workAdapter.buttonLamar(id);
        ed_query.setText("");
        showMessage("Selamat! Anda telah berhasil melamar untuk pekerjaan ini");
    }

    @Override
    public void onErrorList() {
        sr_dashboard.setRefreshing(false);
        ed_query.setText("");
        txt_total.setText("");
        if (candidateAdapter != null)candidateAdapter.update(new ArrayList<>());
        if (workAdapter != null) workAdapter.update(new ArrayList<>());
    }

    @Override
    public void onClick(DashboardResponse.Recruter.Data model) {
        Intent intent = new Intent(getActivity(), DetailCandidateActivity.class);
        intent.putExtra("id", model.getPengguna_id());
        startActivity(intent);
    }

    @Override
    public void onChat(DashboardResponse.Recruter.Data model) {
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        intent.putExtra("target", model.getPengguna_id());
        intent.putExtra("image", model.getFoto_terbaru());
        intent.putExtra("name", model.getNama_ktp());
        startActivity(intent);

    }

    @Override
    public void onClick(DashboardResponse.Candidate.Data model) {
        Intent intent = new Intent(getActivity(), DetailWorkActivity.class);
        intent.putExtra("id", model.getId());
        startActivity(intent);
    }

    @Override
    public void onApply(DashboardResponse.Candidate.Data model) {
        mPresenter.onApply(model.getId());
    }

    private void showAutocomplete() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
            startActivityForResult(intent, REQUEST_SELECT_PLACE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("result", "onActivityResult");
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }

        if (requestCode == 11) {
            if (resultCode == 100) {
                hideKeyboard();
                job_id = data.getStringExtra("id");
                String job = data.getStringExtra("job");
                txt_job.setText(job);
                txt_job.clearFocus();
                notLoading = false;
                mPresenter.onViewListCityOrJob(city_id, job_id, "0");
                typeLoad = 2;
                first = true;
            }
        }


        if (requestCode == 22) {
            if (resultCode == 200){
                hideKeyboard();
                city_id = data.getStringExtra("id_city");
                String city = data.getStringExtra("city");
                txt_city.setText(city);
                txt_city.clearFocus();
                notLoading = false;
                mPresenter.onViewListCityOrJob(city_id, job_id, "0");
                typeLoad = 2;
                first = true;
            }
        }

        StateEvent stateEvent = new StateEvent();
        stateEvent.setBasestate("00");
        bus.postSticky(stateEvent);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == 1) showAutocomplete();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_location);
            dialog.setOnMessageClosed(this::showAutocomplete);
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        view_custom_location.setVisibility(View.VISIBLE);
        txt_custom_location.setText(place.getAddress());
        lat = String.valueOf(place.getLatLng().latitude);
        lng = String.valueOf(place.getLatLng().longitude);
        notLoading = false;
        mPresenter.onCustomLocation(lat, lng, "0");
        typeLoad = 3;
        first = true;
    }

    @Override
    public void onError(Status status) {}

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        rv_work_item.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void defaultJob(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> defaultJob) {
        //String jobName = defaultJob.get(0).getJenis_pekerjaan();
        String jobName = Util.dashboardJob(defaultJob);
        txt_job.setText(jobName);
        txt_job.dismissDropDown();
    }

    @Override
    public void defaultCity(ArrayList<UserResponse.Login.Data.Kota> defaultCity) {
        //String cityName = defaultCity.get(0).getName();
        String cityName = Util.dashboardCity(defaultCity);
        txt_city.setText(cityName);
        txt_city.dismissDropDown();
    }

    private void readLoccation(){
        boolean hasPermission = EasyPermissions.hasPermissions(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasPermission) {
            showAutocomplete();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.message_permission_location), 1,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

}
