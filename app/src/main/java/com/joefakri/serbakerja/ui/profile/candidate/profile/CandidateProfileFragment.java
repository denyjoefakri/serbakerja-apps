package com.joefakri.serbakerja.ui.profile.candidate.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.CandidateModel;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.candidate.profile_edit.EditCandidateProfileActivity;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.profile_edit.EditProfileActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by deny on bandung.
 */

public class CandidateProfileFragment extends BaseFragment implements CandidateProfileView{

    @Inject CandidateProfileMvpPresenter<CandidateProfileView> mPresenter;

    @BindView(R.id.btn_edit) LinearLayout btn_edit;
    @BindView(R.id.img_user) CircleImageView img_user;
    @BindView(R.id.txt_name_header) TextView txt_name_header;
    @BindView(R.id.txt_name) TextView txt_name;
    @BindView(R.id.txt_birthplace) TextView txt_birthplace;
    @BindView(R.id.txt_birthday) TextView txt_birthday;
    @BindView(R.id.txt_address) TextView txt_address;
    @BindView(R.id.txt_rtrw) TextView txt_rtrw;
    @BindView(R.id.txt_village) TextView txt_village;
    @BindView(R.id.txt_district) TextView txt_district;
    @BindView(R.id.txt_gender) TextView txt_gender;
    @BindView(R.id.txt_religion) TextView txt_religion;
    @BindView(R.id.txt_phone) TextView txt_phone;

    CandidateResponse.CandidateProfile.Data data;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_candidate_profile, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        btn_edit.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), EditCandidateProfileActivity.class);
            intent.putExtra("model", data);
            startActivity(intent);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewProfile();
        btn_edit.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultProfile(CandidateResponse.CandidateProfile.Data data) {
        this.data = data;
        btn_edit.setEnabled(true);
        Glide.with(this).load(data.getFoto_terbaru()).into(img_user);
        txt_name_header.setText(data.getNama_ktp());
        txt_name.setText(data.getNama_ktp());
        txt_birthplace.setText(data.getTempat_lahir());
        txt_birthday.setText(TimeUtils.getTime(data.getTgl_lahir()));
        txt_address.setText(data.getAlamat_ktp());
        txt_rtrw.setText(data.getRt_rw());
        txt_village.setText(data.getKelurahan());
        txt_district.setText(data.getKecamatan());
        txt_gender.setText(Constants.genderValue(getActivity(), data.getJenis_kelamin()));
        txt_religion.setText(data.getAgama());
        txt_phone.setText(data.getNo_hp());
    }
}
