package com.joefakri.serbakerja.ui.profile.candidate.education_edit;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceView;
import com.joefakri.serbakerja.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class EditEducationActivity extends BaseActivity implements EditEducationView{

    @Inject
    EditEducationMvpPresenter<EditEducationView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_education_graduated) EditText txt_education_graduated;
    @BindView(R.id.rd_education) RadioGroup rd_education;
    @BindView(R.id.txt_education_name_last_school) EditText txt_education_name_last_school;
    @BindView(R.id.txt_education_name_courses) EditText txt_education_name_courses;
    @BindView(R.id.btn_save) Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_candidate_education, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_form_4_kandidat));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {

        CandidateResponse.Education.Data data = (CandidateResponse.Education.Data) getIntent().getSerializableExtra("model");
        if (data != null) {
            if (data.getThn_pend_terakhir() != null) txt_education_graduated.setText(data.getThn_pend_terakhir());
            if (data.getPend_terakhir() != null)
                ((RadioButton)rd_education.getChildAt(Integer.parseInt(data.getPend_terakhir()))).setChecked(true);
            if (data.getSekolah_terakhir() != null) txt_education_name_last_school.setText(data.getSekolah_terakhir());
            if (data.getKursus() != null) txt_education_name_courses.setText(data.getKursus());
        }

        btn_save.setOnClickListener(view -> {
            if (txt_education_graduated.getText().toString().isEmpty()){
                onError("Tahun tamat pendidikan terakhir tidak boleh kosong");
                return;
            }

            if (rd_education.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu pendidikan terakhir yang tersedia");
                return;
            }

            if (txt_education_name_last_school.getText().toString().isEmpty()){
                onError("Nama sekolah terakhir tidak boleh kosong");
                return;
            }

            String ed = Constants.educationPosition(this, getEducation());
            presenter.onPostEducation(ed, txt_education_graduated.getText().toString(),
                    txt_education_name_last_school.getText().toString(), txt_education_name_courses.getText().toString());
        });
    }

    private String getEducation(){
        int selectedId= rd_education.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResultEducation(CandidateResponse.ChangeProfile workResponse) {
        finish();
    }
}
