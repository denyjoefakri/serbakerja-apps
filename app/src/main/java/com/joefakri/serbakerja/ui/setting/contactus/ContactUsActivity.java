package com.joefakri.serbakerja.ui.setting.contactus;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.LinearLayout;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainActivity;
import com.joefakri.serbakerja.ui.profile.history.CompanyHistoryActivity;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainActivity;
import com.joefakri.serbakerja.ui.setting.SettingMvpPresenter;
import com.joefakri.serbakerja.ui.setting.SettingView;
import com.joefakri.serbakerja.ui.widget.MessageDialog;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class ContactUsActivity extends BaseActivity implements ContactUsView, EasyPermissions.PermissionCallbacks{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.btn_call) LinearLayout btn_call;
    @BindView(R.id.btn_whatsapp) LinearLayout btn_whatsapp;
    @BindView(R.id.btn_email) LinearLayout btn_email;
    @Inject ContactUsMvpPresenter<ContactUsView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_contactus, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();
    }
    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_contact_us));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onInitCustomTab(this);

        btn_call.setOnClickListener( view -> {
            presenter.onCall(this);
        });

        btn_whatsapp.setOnClickListener(view -> {
            presenter.onWhatsapp(this);
        });

        btn_email.setOnClickListener(view -> {
            presenter.onEmail(this);
        });

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        presenter.onCall(this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_callphone);
            dialog.setOnMessageClosed(() -> {
                presenter.onCall(this);
            });
        }
    }

}
