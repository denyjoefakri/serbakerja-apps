package com.joefakri.serbakerja.ui.setting;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordView;

/**
 * Created by deny on bandung.
 */

public interface SettingMvpPresenter<V extends SettingView> extends SerbakerjaPresenter<V> {

    void onLogout();

}
