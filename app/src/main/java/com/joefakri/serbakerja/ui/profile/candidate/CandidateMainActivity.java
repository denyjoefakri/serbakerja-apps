package com.joefakri.serbakerja.ui.profile.candidate;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.profile.candidate.document.CandidateDocumentFragment;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationFragment;
import com.joefakri.serbakerja.ui.profile.candidate.experience.CandidateExperienceFragment;
import com.joefakri.serbakerja.ui.profile.candidate.profile.CandidateProfileFragment;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkFragment;
import com.joefakri.serbakerja.utils.TintableTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class CandidateMainActivity extends BaseActivity implements CandidateMainView {

    @Inject CandidateMainMvpPresenter<CandidateMainView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tab_menu) TabLayout tab_menu;
    @BindView(R.id.viewpager_content) ViewPager viewpager_content;
    ViewPagerAdapter adapter;

    String[] title = {"Profil", "Pekerjaan", "Pengalaman", "Pendidikan", "Dokumen"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_detail_recruiter_profile, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();

    }

    @Override
    protected void initComponent() {
        setupViewPager();
        setupTabIcons();

        toolbar.setTitle(getString(R.string.txt_profile));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());
    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new CandidateProfileFragment());
        adapter.addFrag(new CandidateWorkFragment());
        adapter.addFrag(new CandidateExperienceFragment());
        adapter.addFrag(new CandidateEducationFragment());
        adapter.addFrag(new CandidateDocumentFragment());
        viewpager_content.setAdapter(adapter);
        viewpager_content.setOffscreenPageLimit(4);
    }

    private void setupTabIcons() {
        tab_menu.setupWithViewPager(viewpager_content);
        tab_menu.getTabAt(0).setCustomView(getTabview(0));
        tab_menu.getTabAt(1).setCustomView(getTabview(1));
        tab_menu.getTabAt(2).setCustomView(getTabview(2));
        tab_menu.getTabAt(3).setCustomView(getTabview(3));
        tab_menu.getTabAt(4).setCustomView(getTabview(4));
    }

    public View getTabview(int position){
        View v = LayoutInflater.from(this).inflate(R.layout.tab_view_profile, null);
        TintableTextView tab_title = v.findViewById(R.id.tab_title);
        tab_title.setText(title[position]);
        return v;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
        }

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }


}
