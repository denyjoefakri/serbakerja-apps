package com.joefakri.serbakerja.ui.form.person;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormPersonBirthdayFragment extends BaseFragment implements FormPersonView, DatePickerDialog.OnDateSetListener {

    @Inject FormPersonMvpPresenter<FormPersonView> mPresenter;
    @Inject DataManager dataManager;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_person_birthplace) EditText txt_person_birthplace;
    @BindView(R.id.txt_person_birthday) EditText txt_person_birthday;

    DatePickerDialog dpd;
    View viewp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewp = inflater.inflate(R.layout.viewpager_form_person_birthday, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, viewp));
            mPresenter.onAttach(this);
        }
        return viewp;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        txt_person_birthday.setOnFocusChangeListener((view, b) -> {
            if (b){
                txt_person_birthday.setError(null);
                Calendar now = Calendar.getInstance();
                dpd = DatePickerDialog.newInstance(this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "DatepickerdialogBirthday");
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_next.setOnClickListener(view -> {
            if (txt_person_birthplace.getText().toString().isEmpty() ) {
                onError("Tempat lahir tidak boleh kosong");
                return;
            }

            if (txt_person_birthday.getText().toString().isEmpty() ) {
                onError("Tanggal lahir tidak boleh kosong");
                return;
            }

            if (dpd != null) {
                dpd.dismiss();
                dpd.unregisterForContextMenu(viewp);
            }

            if (dataManager.getTypeUser().equals("perekrut"))
                mPresenter.saveRecruterPersonBirthday(txt_person_birthday.getText().toString(), txt_person_birthplace.getText().toString());
            else mPresenter.saveCandidatePersonBirthday(txt_person_birthday.getText().toString(), txt_person_birthplace.getText().toString());
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(2);
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String mount = String.valueOf(monthOfYear + 1);
        String day = String.valueOf(dayOfMonth);

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
        try {
            calendar.setTime(dateFormat.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mount.length() <= 1 &&  day.length() <= 1){
            txt_person_birthday.setText("0"+dayOfMonth + "-" +"0"+(monthOfYear+1)+"-" + year);
        }else if (day.length() <= 1){
            txt_person_birthday.setText("0"+dayOfMonth +"-"+(monthOfYear+1)+"-"+ year);
        }else if (mount.length() <= 1){
            txt_person_birthday.setText(dayOfMonth+"-"+"0"+(monthOfYear+1)+"-"+ year);
        }else {
            txt_person_birthday.setText(dayOfMonth + "-" + (monthOfYear + 1)+"-"+ year);
        }
    }

    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void onResultCity(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultDistrict(List<AreaResponse.DistrictResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterInfo(RecruiterResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void onResultCandidateInfo(CandidateResponse.InfoResponse.Data dataList) {

    }

    @Override
    public void initRecruter(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm != null) {
            if (dataManager.getTypeUser().equals(Constants.ROLE_PEREKRUT)) {
                if (recruiterRealm.getPersonBirthday() != null || recruiterRealm.getPersonBirthplace() != null) {
                    txt_person_birthday.setText(recruiterRealm.getPersonBirthday());
                    txt_person_birthplace.setText(recruiterRealm.getPersonBirthplace());
                }
            }
        }

        if (candidateRealm != null) {
            if (dataManager.getTypeUser().equals(Constants.ROLE_KANDIDAT)) {
                if (candidateRealm.getPersonBirthday() != null || candidateRealm.getPersonBirthplace() != null) {
                    txt_person_birthday.setText(candidateRealm.getPersonBirthday());
                    txt_person_birthplace.setText(candidateRealm.getPersonBirthplace());
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }


}
