package com.joefakri.serbakerja.ui.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.ChatAdapter;
import com.joefakri.serbakerja.connection.response.ChatResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.ChatModel;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TextWatcherAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class ChatFragment extends BaseFragment implements ChatView, ChatAdapter.Listener{

    @Inject ChatMvpPresenter<ChatView> mPresenter;
    @Inject LinearLayoutManager mLayoutManager;

    @BindView(R.id.sr_chat) SwipeRefreshLayout sr_chat;
    @BindView(R.id.nv_main) NestedScrollView nv_main;
    @BindView(R.id.rv_chat) RecyclerView rv_chat;
    @BindView(R.id.ed_query) EditText ed_query;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.tv_message) TextView tv_message;
    ChatAdapter chatAdapter;
    @Inject DataManager dataManager;

    private EventBus bus = EventBus.getDefault();

    ArrayList<ChatResponse.ListMessage.Data> models;
    ArrayList<ChatResponse.ListMessage.Data> modelsSearch;

    private int currentPage = 0;
    private boolean notLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void onClick(ChatResponse.ListMessage.Data model) {
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        intent.putExtra("target", model.getTo_id());
        intent.putExtra("image", model.getFoto());
        intent.putExtra("phone", model.getNo_hp());
        if (model.getTipe_user().equals("perekrut")){
            intent.putExtra("name", model.getInfo().getNama_perusahaan());
        } else {
            intent.putExtra("name", model.getInfo().getNama_ktp());
        }
        intent.putExtra("target_id", model.getTo_id());
        startActivity(intent);
    }

    @Override
    protected void initComponent(View view) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_chat.setLayoutManager(mLayoutManager);
        rv_chat.setHasFixedSize(true);
        rv_chat.setNestedScrollingEnabled(false);
        rv_chat.setAdapter(chatAdapter = new ChatAdapter(getActivity()));

        chatAdapter.setOnclickListener(this);
        ed_query.addTextChangedListener(watcherAdapter);
        ed_query.setEnabled(false);

        nv_main.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v1, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v1.getChildAt(v1.getChildCount() - 1) != null) {
                if ((scrollY >= (v1.getChildAt(v1.getChildCount() - 1).getMeasuredHeight() - v1.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!notLoading){
                        notLoading = true;
                        currentPage += 20;
                        mPresenter.onViewList(String.valueOf(currentPage));
                        chatAdapter.addLoading();
                    }
                }
            }
        });

        sr_chat.setOnRefreshListener(() -> {
            mPresenter.onViewList("0");
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getBasestate().equals("1")) {
            mPresenter.onViewList("0");
        }
    }

    TextWatcherAdapter watcherAdapter = new TextWatcherAdapter(){
        @Override
        public void afterTextChanged(Editable s) {
            super.afterTextChanged(s);
            search(ed_query.getText().toString());
        }
    };

    private void search(String keyword) {
        if (keyword == null || keyword.equals("")) {
            chatAdapter.update(models);
        } else {
            modelsSearch = new ArrayList<>();
            for (int i = 0; i < models.size(); i++) {
                ChatResponse.ListMessage.Data model = models.get(i);
                if (model.getTipe_user().equals(Constants.ROLE_KANDIDAT)) {
                    if (model.getPesan().toLowerCase().contains(keyword.toLowerCase())
                            || model.getNama().toLowerCase().contains(keyword.toLowerCase())
                            || model.getInfo().getNama_ktp().toLowerCase().contains(keyword.toLowerCase())) {
                        modelsSearch.add(model);
                    }
                } else {
                    if (model.getPesan().toLowerCase().contains(keyword.toLowerCase())
                            || model.getNama().toLowerCase().contains(keyword.toLowerCase())
                            || model.getInfo().getNama_perusahaan().toLowerCase().contains(keyword.toLowerCase())) {
                        modelsSearch.add(model);
                    }
                }
            }
            chatAdapter.update(modelsSearch);
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        rv_chat.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void onResult(ArrayList<ChatResponse.ListMessage.Data> dataList) {
        sr_chat.setRefreshing(false);
        models = new ArrayList<>();
        if (dataList.isEmpty()){
            if (chatAdapter.getItemCount() == 0) {
                view_empty.setVisibility(View.VISIBLE);
                rv_chat.setVisibility(View.GONE);
                tv_message.setText(getString(R.string.txt_chat_empty));
            }
            if (notLoading) chatAdapter.removeLoading();
        } else {
            view_empty.setVisibility(View.GONE);
            rv_chat.setVisibility(View.VISIBLE);
            models = dataList;
            //chatAdapter.update(models);
            ed_query.setEnabled(true);
            if (notLoading) {
                chatAdapter.removeLoading();
                chatAdapter.updateAll(dataList);
                notLoading = false;
            } else {
                chatAdapter.update(dataList);
            }
        }
    }

}
