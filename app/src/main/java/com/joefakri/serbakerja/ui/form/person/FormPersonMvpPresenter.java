package com.joefakri.serbakerja.ui.form.person;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoView;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface FormPersonMvpPresenter<V extends FormPersonView> extends SerbakerjaPresenter<V> {

    void readCameraGallery(Activity activity, Fragment fragment, View v);

    void openImageChooser(Activity activity, View v);

    void onViewCity();

    void onViewDistrict(String city_id);

    void onPostRecruterInfo();

    void onPostCandidateInfo();

    void init();

    void saveRecruterPersonName(String data);
    void saveRecruterPersonBirthday(String birthday, String birthplace);
    void saveRecruterPersonAddress(String data);
    void saveRecruterPersonDistricts(String data);
    void saveRecruterPersonVillage(String code);
    void saveRecruterPersonRtrw(String data);
    void saveRecruterPersonPhotoKTP(String data);
    void saveRecruterPersonPhoto(String data);

    void saveCandidatePersonName(String data);
    void saveCandidatePersonBirthday(String birthday, String birthplace);
    void saveCandidatePersonAddress(String data);
    void saveCandidatePersonDistricts(String data);
    void saveCandidatePersonVillage(String code);
    void saveCandidatePersonRtrw(String data);
    void saveCandidateGender(String data);
    void saveCandidateReligion(String data);
}
