package com.joefakri.serbakerja.ui.register;

import android.app.Activity;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;

/**
 * Created by deny on bandung.
 */

public interface RegisterMvpPresenter<V extends RegisterView> extends SerbakerjaPresenter<V> {

    void StartOpen(Activity activity, View splash, View login);

    void initPhoneAuth(Activity activity);

    void startPhoneNumberVerification(Activity activity, String phoneNumber);

    void onRegister(String nama, String no_hp, String email, String password);

    void updateStatusVerification(String phone_number);
}
