package com.joefakri.serbakerja.ui.form.education;

import android.app.Activity;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardView;

/**
 * Created by deny on bandung.
 */

public interface FormEducationMvpPresenter<V extends FormEducationView> extends SerbakerjaPresenter<V> {

    void onPostCandidateEducation(Activity activity);

    void init();

    void saveEducationLast(String data);
    void saveEducationGraduated(String data);
    void saveEducationNameCourses(String data);
    void saveEducationNameLastSchool(String data);

}
