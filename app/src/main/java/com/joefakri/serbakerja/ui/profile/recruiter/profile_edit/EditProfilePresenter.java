/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.recruiter.profile_edit;

import android.Manifest;
import android.app.Activity;
import android.os.Environment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class EditProfilePresenter<V extends EditProfileView> extends BasePresenter<V>
        implements EditProfileMvpPresenter<V> {

    @Inject
    public EditProfilePresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void readCameraGallery(Activity activity, View v, int requestCode) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (hasPermission) {
            openImageChooser(activity, v, requestCode);
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.message_permission_camera), requestCode,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void openImageChooser(Activity activity, View v, int requestCode) {
        PopupMenu popupMenu = new PopupMenu(activity, v);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_camera) {
                takePhoto(activity, requestCode);
            } else {
                openGallery(activity, requestCode);
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    public void onUpdate(String photo, String ktp, String... strings) {
        RecruiterRequest.ChangeProfile profile = new RecruiterRequest.ChangeProfile(strings[0],
                strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]);
        RecruiterRequest.ChangeProfileFile profileFile = new RecruiterRequest.ChangeProfileFile(photo, ktp);
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());

        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.PROFILE_RECRUTER_UPDATE, profile, profileFile.getFoto());

        getCompositeDisposable().add(getDataManager()
                .changeRecruterProfile(id, profile, profileFile)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.PROFILE_RECRUTER_UPDATE);
                    Gson gson = new Gson();
                    RecruiterResponse.ChangeProfile response = gson.fromJson(jsonObject.toString(), RecruiterResponse.ChangeProfile.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResult(response);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }


    private void takePhoto(Activity activity, int requestCode){
        ShutterTakePhotoBuilder shutter = Shutter.Companion.with(activity)
                .takePhoto().usePrivateAppInternalStorage()
                .addPhotoToGallery();

        ((EditProfileActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().takePhoto(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath(), requestCode);
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void openGallery(Activity activity, int requestCode){
        ShutterPickPhotoGalleryBuilder shutter = Shutter.Companion.with(activity)
                .getPhotoFromGallery().usePrivateAppInternalStorage();

        ((EditProfileActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().openGallery(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath(), requestCode);
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private File compress(Activity activity, String file){
        File fileToSend;
        String nama = "";
        if (new File(file).getName().indexOf(".") > 0)
            nama = new File(file).getName().substring(0, new File(file).getName().lastIndexOf("."));

        String extension = new File(file).getAbsolutePath().substring(new File(file).getAbsolutePath().lastIndexOf("."));

        try {
            fileToSend = new Compressor(activity)
                    .setMaxWidth(1000)
                    .setMaxHeight(1000)
                    .setQuality(90)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(new File(file), nama + "_Serbakerja" + extension);
            Log.e("compress", fileToSend.getAbsolutePath());
            return fileToSend;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
