package com.joefakri.serbakerja.ui.form.company;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormCompanyCityFragment extends BaseFragment implements FormCompanyView{

    @Inject FormCompanyMvpPresenter<FormCompanyView> mPresenter;
    @BindView(R.id.txt_city) AutoCompleteTextView txt_city;
    @BindView(R.id.txt_area) EditText txt_area;
    @BindView(R.id.btn_next) Button btn_next;

    ArrayList<String> citys;
    ArrayList<String> ids;
    private String id = "";
    private String city = "";

    private EventBus bus = EventBus.getDefault();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_company_city, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        mPresenter.onViewCity();
        btn_next.setOnClickListener(view -> {
            if (txt_city.getText().toString().isEmpty() || !txt_city.getText().toString().equals(city)){
                onError("Pilih Kota / Kabupaten yang tersedia");
                return;
            }

            if (txt_area.getText().toString().isEmpty()) {
                onError("Daerah tidak boleh kosong");
                return;
            }

            mPresenter.saveCompanyCity(id, txt_city.getText().toString());
            mPresenter.saveCompanyArea(txt_area.getText().toString());
            txt_city.dismissDropDown();
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(3);
        });
    }

    @Override
    public void init(RecruiterRealm recruiterRealm) {
        if (recruiterRealm != null) {
            if (recruiterRealm.getCompanyCity() != null
                    && recruiterRealm.getCompanyArea() != null
                    && recruiterRealm.getCompanyCityId() != null){
                city = recruiterRealm.getCompanyCity();
                id = recruiterRealm.getCompanyCityId();
                txt_city.requestFocus();
                txt_area.setText(recruiterRealm.getCompanyArea());
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getState().equals("2")){
            if (txt_city != null) {
                txt_city.postDelayed(() -> {
                    if (txt_city != null) txt_city.showDropDown();
                },500);
                txt_city.setText(city);
                txt_city.setSelection(txt_city.getText().length());
            }
        }
    }


    @Override
    public void takePhoto(String file) {

    }

    @Override
    public void openGallery(String file) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {
        citys = new ArrayList<>();
        ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, citys);
        txt_city.setAdapter(adapter);
        txt_city.setOnItemClickListener((adapterView, view, i, l) -> {
            city = adapter.getItem(i).toString();
            id = ids.get(citys.indexOf(city));
            //txt_area.requestFocus();
            Log.e("job", ids.get(citys.indexOf(city)) + " | " + city);
        });
    }

    @Override
    public void onResultCompany(RecruiterResponse.CompanyResponse.Data companyResponses) {

    }

    @Override
    public void onDestroyView() {
        if (citys != null) citys.clear();
        if (ids != null) ids.clear();
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
