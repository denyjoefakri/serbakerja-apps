package com.joefakri.serbakerja.ui.profile.candidate.experience_edit;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.widget.WorkExperienceDialog;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class EditExperienceActivity extends BaseActivity implements EditExperienceView, WorkExperienceAdapter.Listener{

    @Inject
    EditExperienceMvpPresenter<EditExperienceView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.rd_status) RadioGroup rd_status;
    @BindView(R.id.rd_sallary) RadioGroup rd_sallary;
    @BindView(R.id.rv_work_experience) RecyclerView rv_work_experience;
    @BindView(R.id.txt_options_time_4) EditText txt_options_time_4;
    @BindView(R.id.btn_add) LinearLayout btn_add;
    @BindView(R.id.btn_save) Button btn_save;
    WorkExperienceAdapter experienceAdapter;

    @Inject LinearLayoutManager mLayoutManager;
    ArrayList<WorkModel> models;
    CandidateResponse.Experience.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_candidate_experience, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_form_3_kandidat));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work_experience.setLayoutManager(mLayoutManager);
        rv_work_experience.setHasFixedSize(true);
        rv_work_experience.setNestedScrollingEnabled(false);
        rv_work_experience.setAdapter(experienceAdapter = new WorkExperienceAdapter(this));
        experienceAdapter.setOnclickListener(this);

        data = (CandidateResponse.Experience.Data) getIntent().getSerializableExtra("model");
        if (data != null) {
            if (data.getStatus_pekerjaan() != null) {
                ((RadioButton)rd_status.getChildAt(Integer.parseInt(data.getStatus_pekerjaan()))).setChecked(true);

                if (data.getStatus_pekerjaan().equals("3")) {
                    txt_options_time_4.setVisibility(View.VISIBLE);
                    txt_options_time_4.setText(data.getStatus_pekerjaan_lainnya());
                }
            }

            String sallary = Constants.sallary(this, data.getGaji_min(), data.getGaji_max());
            if (!sallary.isEmpty()) ((RadioButton)rd_sallary.getChildAt(Constants.sallaryPosition(this, sallary))).setChecked(true);

            rd_status.setOnCheckedChangeListener((radioGroup, i) -> {
                RadioButton radioButton = findViewById(i);
                if (radioButton.getText().equals(getString(R.string.options_status_4))) {
                    txt_options_time_4.setVisibility(View.VISIBLE);
                    txt_options_time_4.setText(data.getStatus_pekerjaan_lainnya());
                }
                else {
                    txt_options_time_4.setVisibility(View.GONE);
                    txt_options_time_4.setText("");
                }
            });


            models = new ArrayList<>();
            if (data.getPengalaman() != null) {
                for (int i = 0; i < data.getPengalaman().size(); i++) {
                    WorkModel workModel = new WorkModel();
                    workModel.setId(data.getPengalaman().get(i).getId());
                    workModel.setCompany_name(data.getPengalaman().get(i).getNama_perusahaan());
                    workModel.setWork_as(data.getPengalaman().get(i).getPosisi());
                    workModel.setWork_long(data.getPengalaman().get(i).getLama_bekerja());
                    workModel.setPeriod(data.getPengalaman().get(i).getPeriod());
                    models.add(workModel);
                }
                experienceAdapter.update(models);
            }
        }

        btn_add.setOnClickListener(view -> {
            workExperienceDialog(false, new WorkModel()).setOnListener((txt_work_company_name,
                                                                        txt_work_as, txt_work_long, perio) -> {

                if (txt_work_company_name.isEmpty()|txt_work_as.isEmpty()|txt_work_long.isEmpty()|perio.isEmpty()){
                    onError("Form tidak boleh kosong");
                } else {
                    WorkModel experienceWorkModel = new WorkModel();
                    experienceWorkModel.setCompany_name(txt_work_company_name);
                    experienceWorkModel.setWork_as(txt_work_as);
                    experienceWorkModel.setWork_long(txt_work_long);
                    experienceWorkModel.setPeriod(perio);

                    presenter.onPostInsertExperience(experienceWorkModel,  txt_work_as, txt_work_long, perio, txt_work_company_name, data.getId());
                }

            });
        });
        btn_save.setOnClickListener(view -> {
            if (rd_status.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu status pekerjaan yang tersedia");
                return;
            }

            if (rd_sallary.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu gaji bulanan terakhir yang tersedia");
                return;
            }

            String status = Constants.workStatusPosition(this, getStatus());
            String min_sallary = Constants.min_sallary(this, getSallary());
            String max_sallary = Constants.max_sallary(this, getSallary());
            presenter.onPostAllExperience(min_sallary, max_sallary, status, txt_options_time_4.getText().toString());
        });
    }

    private String getStatus(){
        int selectedId= rd_status.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    private String getSallary(){
        int selectedId= rd_sallary.getCheckedRadioButtonId();
        RadioButton rd = findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    public WorkExperienceDialog workExperienceDialog (boolean edit, WorkModel model) {
        WorkExperienceDialog messageDialog = new WorkExperienceDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean("edit", edit);
        bundle.putSerializable("model", model);
        messageDialog.setArguments(bundle);
        messageDialog.show(getSupportFragmentManager(), "messageDialog");
        return messageDialog;
    }

    @Override
    public void onClick(WorkModel model, int position) {
        workExperienceDialog(true, model).setOnListener((txt_work_company_name,
                                                         txt_work_as, txt_work_long, perio) -> {

            WorkModel experienceWorkModel = models.get(position);
            experienceWorkModel.setId(model.getId());
            experienceWorkModel.setCompany_name(txt_work_company_name);
            experienceWorkModel.setWork_as(txt_work_as);
            experienceWorkModel.setWork_long(txt_work_long);
            Log.e("perio", perio);
            experienceWorkModel.setPeriod(perio);

            //experienceAdapter.update(models);

            presenter.onPostEditExperience(model.getId(), txt_work_as, txt_work_long, perio, txt_work_company_name);

        });

    }

    @Override
    public void onResultExperienceEdit(CandidateResponse.ExperienceMan changeProfileData) {
        experienceAdapter.update(models);
    }

    @Override
    public void onResultExperienceInsert(CandidateResponse.ExperienceMan changeProfileData, WorkModel workModel) {
        workModel.setId(changeProfileData.getData().getPengalaman_detail_id());
        models.add(workModel);
        experienceAdapter.update(models);
    }

    @Override
    public void onResultAllExperience(CandidateResponse.ChangeProfile changeProfileData) {
        finish();
    }
}
