package com.joefakri.serbakerja.ui.form.doc;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.form.company.FormCompanyMvpPresenter;
import com.joefakri.serbakerja.ui.form.company.FormCompanyView;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class FormDocPhotoIjazahFragment extends BaseFragment implements FormDocPhotoView, EasyPermissions.PermissionCallbacks{

    @Inject FormDocPhotoMvpPresenter<FormDocPhotoView> mPresenter;
    @BindView(R.id.btn_get_photo) Button btn_get_photo;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.img_photo) ImageView img_photo;

    private String fileToSend;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_doc_photo_ijazah, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view2 -> {
            if (fileToSend != null){
                mPresenter.saveDocIjazah(fileToSend);
            }

            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(3);
        });

        btn_get_photo.setOnClickListener(view2 -> {
            mPresenter.readCameraGallery(getActivity(), this, btn_get_photo);
        });
    }

    @Override
    public void openGallery(String file) {
        fileToSend = file;
        Log.e("fileToSend", fileToSend);
        Glide.with(this).load(fileToSend).into(img_photo);
    }

    @Override
    public void takePhoto(String file) {
        fileToSend = file;
        Log.e("fileToSend", fileToSend);
        Glide.with(this).load(fileToSend).into(img_photo);
    }

    @Override
    public void onResultCandidateDocument(CandidateResponse.DocumentResponse.Data dataList) {

    }

    @Override
    public void init(CandidateRealm candidateRealm) {
        if (candidateRealm != null){
            if (candidateRealm.getDocPhotoIjazah() != null) {
                fileToSend = candidateRealm.getDocPhotoIjazah();
                Glide.with(this).load(candidateRealm.getDocPhotoIjazah()).into(img_photo);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        mPresenter.openImageChooser(getActivity(), btn_get_photo);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                mPresenter.readCameraGallery(getActivity(), this, btn_get_photo);
            });
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
