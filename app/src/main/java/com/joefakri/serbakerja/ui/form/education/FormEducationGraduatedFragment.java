package com.joefakri.serbakerja.ui.form.education;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardMvpPresenter;
import com.joefakri.serbakerja.ui.main.dashboard.DashboardView;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormEducationGraduatedFragment extends BaseFragment implements FormEducationView{

    @Inject FormEducationMvpPresenter<FormEducationView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.rd_education) RadioGroup rd_education;
    RadioButton rd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_education_last, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (rd_education.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu pendidikan terakhir yang tersedia");
                return;
            }

            mPresenter.saveEducationGraduated(Constants.educationPosition(getActivity(), get()));
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(2);
        });
    }


    private String get(){
        int selectedId= rd_education.getCheckedRadioButtonId();
        rd = getActivity().findViewById(selectedId);
        return rd.getText().toString();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultCandidateEducation(CandidateResponse.EducationResponse.Data dataList) {

    }

    @Override
    public void init(CandidateRealm candidateRealm) {
        if (candidateRealm != null){
            if (candidateRealm.getEducationGraduated() != null)
                ((RadioButton)rd_education.getChildAt(Integer.parseInt(candidateRealm.getEducationGraduated()))).setChecked(true);
        }
    }
}
