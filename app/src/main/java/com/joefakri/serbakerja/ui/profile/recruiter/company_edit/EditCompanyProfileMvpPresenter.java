package com.joefakri.serbakerja.ui.profile.recruiter.company_edit;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.CandidateMainView;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface EditCompanyProfileMvpPresenter<V extends EditCompanyProfileView> extends SerbakerjaPresenter<V> {

    void onEdit(File file, String... strings);

    void onViewCity();
}
