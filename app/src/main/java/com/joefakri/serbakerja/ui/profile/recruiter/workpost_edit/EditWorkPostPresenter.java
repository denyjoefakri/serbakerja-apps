/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileMvpPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class EditWorkPostPresenter<V extends EditWorkPostView> extends BasePresenter<V>
        implements EditWorkPostMvpPresenter<V> {

    @Inject
    public EditWorkPostPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onPostEditWork(String... s) {
        getSerbakerjaView().showLoading();

        UserRequest.Id id = new UserRequest.Id(s[15]);
        RecruiterRequest.PostWork2 postWork = new RecruiterRequest.PostWork2(
                s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], s[12], s[13], s[14]);

        Logger.printLogRequest(Path.UPDATE_RECRUTER_WORK, postWork, null);
        getCompositeDisposable().add(getDataManager()
                .changeRecruterWork(id, postWork)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_RECRUTER_WORK);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    RecruiterResponse.ChangeProfile response = gson.fromJson(jsonObject.toString(), RecruiterResponse.ChangeProfile.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultEditRecruterWork();
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onViewCity() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCity()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(cityResponse -> {

                    LogResponse(cityResponse, Path.GET_CITY);
                    Gson gson = new Gson();
                    AreaResponse.CityResponse response = gson.fromJson(cityResponse.toString(), AreaResponse.CityResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().city(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostRecruterWork(String... s) {
        getSerbakerjaView().showLoading();

        RecruiterRequest.PostWork postWork = new RecruiterRequest.PostWork(getDataManager().getCurrentUserId(), s[0],
                s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], s[12], s[13], s[14]);

        Logger.printLogRequest(Path.POST_RECRUITER_WORK, postWork, null);
        getCompositeDisposable().add(getDataManager()
                .postRWork(postWork)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_RECRUITER_WORK);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    RecruiterResponse.WorkResponse response = gson.fromJson(jsonObject.toString(), RecruiterResponse.WorkResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultRecruterWork(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

}
