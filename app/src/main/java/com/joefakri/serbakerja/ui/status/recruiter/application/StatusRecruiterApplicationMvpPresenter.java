package com.joefakri.serbakerja.ui.status.recruiter.application;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.StatusRecruiterView;

/**
 * Created by deny on bandung.
 */

public interface StatusRecruiterApplicationMvpPresenter<V extends StatusRecruiterApplicationView> extends SerbakerjaPresenter<V> {

    void onViewJob(String offset);

    void onClosedJob(String pekerjaanId);

    void onViewApplicant(String jobId, String offset);

    void onAcceptApplicant(String lamaran_id);

    void onRejectApplicant(String lamaran_id);

}
