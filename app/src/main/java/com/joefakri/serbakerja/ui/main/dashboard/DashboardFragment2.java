package com.joefakri.serbakerja.ui.main.dashboard;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.DashboardCandidateAdapter;
import com.joefakri.serbakerja.adapter.DashboardCandidateAdapter2;
import com.joefakri.serbakerja.adapter.DashboardWorkAdapter;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.ui.city.DataCityActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.ui.work.DetailWorkActivity;
import com.joefakri.serbakerja.ui.work.data.DataWorkActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class DashboardFragment2 extends BaseFragment implements DashboardView,
        DashboardCandidateAdapter2.Listener, DashboardCandidateAdapter2.LoadMoreListener, DashboardCandidateAdapter2.HeaderListener,
        DashboardWorkAdapter.Listener,
        PlaceSelectionListener, EasyPermissions.PermissionCallbacks{

    @Inject DashboardMvpPresenter<DashboardView> mPresenter;

    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.sr_dashboard) SwipeRefreshLayout sr_dashboard;
    @BindView(R.id.rv_work_item) RecyclerView rv_work_item;
    @BindView(R.id.tv_message) TextView tv_message;
    final int REQUEST_SELECT_PLACE = 1001;
    DashboardCandidateAdapter2 candidateAdapter;
    DashboardWorkAdapter workAdapter;

    private EventBus bus = EventBus.getDefault();

    @Inject LinearLayoutManager mLayoutManager;
    @Inject DataManager dataManager;

    String job_id = "";
    String city_id = "";
    String lat = "";
    String lng = "";

    ArrayList<DashboardResponse.Recruter.Data> dataList;
    private int typeLoad = 0; // 0 = onViewList, 2 = onViewListCityOrJob, 3 = onCustomLocation
    private int currentPage = 0;
    private boolean loading = false;
    private int totalPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_2, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work_item.setLayoutManager(mLayoutManager);
        rv_work_item.setHasFixedSize(true);
        rv_work_item.setNestedScrollingEnabled(false);
        //rv_work_item.addItemDecoration(new VerticalLineDecorator(2));
        dataList = new ArrayList<>();

        candidateAdapter = new DashboardCandidateAdapter2(getActivity(), dataList);
        candidateAdapter.setLoadMoreListener(() -> {
            rv_work_item.post(() -> {
                int index = dataList.size() - 1;
                dataList.add(new DashboardResponse.Recruter.Data(true));
                candidateAdapter.notifyItemInserted(dataList.size()-1);
                mPresenter.onViewList(String.valueOf(index));
            });
        });

        sr_dashboard.setOnRefreshListener(() -> {
            if (dataManager.getTypeUser().equals(Constants.ROLE_KANDIDAT)) {
                if (workAdapter != null)
                    workAdapter.update(new ArrayList<>());
            }
            else {
                if (candidateAdapter != null)
                    candidateAdapter.update(new ArrayList<>());
            }

            currentPage = 0;
            loading = false;
            mPresenter.onDefaultCity();
            mPresenter.onDefaultJob();
            mPresenter.onViewList("0");
            candidateAdapter.setCustomLocationHeader("");
        });

        mPresenter.onDefaultCity();
        mPresenter.onDefaultJob();

    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getBasestate().equals("0")) {
            if (city_id.isEmpty()|job_id.isEmpty()) {
                mPresenter.onDefaultCity();
                mPresenter.onDefaultJob();
                mPresenter.onViewList("0");
            }
        }

    }

    @Override
    public void listPerekrut(ArrayList<DashboardResponse.Recruter.Data> dataList, String message) {
        Log.e("listPerekrut", "sukses");
        sr_dashboard.setRefreshing(false);
        /*candidateAdapter.setQeryHeader("");
        this.dataList.remove(this.dataList.size() - 1);*/

        if (dataList.size() != 0) {
            view_empty.setVisibility(View.GONE);
            rv_work_item.setVisibility(View.VISIBLE);
            candidateAdapter.update(dataList);
            /*this.dataList.addAll(new ArrayList<>());
            this.dataList.addAll(dataList);*/
            Log.e("datalist", ""+this.dataList.size());
            Log.e("datalist size", ""+candidateAdapter.getItemCount());
            candidateAdapter.setTotalHeader("Ditemukan "+String.valueOf(dataList.size()) + " Kandidat " + message);
        } else {
            view_empty.setVisibility(View.VISIBLE);
            rv_work_item.setVisibility(View.GONE);
            tv_message.setText("Tidak Ada Kandidat");
            candidateAdapter.setTotalHeader("");
            candidateAdapter.setMoreDataAvailable(false);
        }
        candidateAdapter.notifyDataChanged();

    }

    @Override
    public void listCandidate(ArrayList<DashboardResponse.Candidate.Data> dataList, String message) {
        sr_dashboard.setRefreshing(false);
        candidateAdapter.setQeryHeader("");
        Log.e("listCandidate", "sukses");
        if (dataList.size() != 0) {
            view_empty.setVisibility(View.GONE);
            rv_work_item.setVisibility(View.VISIBLE);
            candidateAdapter.setTotalHeader("Ditemukan "+String.valueOf(dataList.size()) + " Kandidat " + message);
        } else {
            view_empty.setVisibility(View.VISIBLE);
            rv_work_item.setVisibility(View.GONE);
            /*tv_message.setText("Tidak Ada Pekerjaan");
            txt_total.setText("");*/
        }
        rv_work_item.setAdapter(workAdapter = new DashboardWorkAdapter(getActivity()));
        workAdapter.setOnclickListener(this);
        workAdapter.update(dataList);
    }

    @Override
    public void onResultApply(String id) {
        workAdapter.buttonLamar(id);
        candidateAdapter.setQeryHeader("");
        showMessage("Selamat! Anda telah berhasil melamar untuk pekerjaan ini");
    }

    @Override
    public void onErrorList() {
        sr_dashboard.setRefreshing(false);
        candidateAdapter.setQeryHeader("");
        candidateAdapter.setTotalHeader("");
        if (candidateAdapter != null)candidateAdapter.update(new ArrayList<>());
        if (workAdapter != null) workAdapter.update(new ArrayList<>());
    }

    @Override
    public void onClick(DashboardResponse.Recruter.Data model) {
        Intent intent = new Intent(getActivity(), DetailCandidateActivity.class);
        intent.putExtra("id", model.getPengguna_id());
        startActivity(intent);
    }

    @Override
    public void onChat(DashboardResponse.Recruter.Data model) {
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        intent.putExtra("target", model.getPengguna_id());
        intent.putExtra("image", model.getFoto_terbaru());
        intent.putExtra("name", model.getNama_ktp());
        startActivity(intent);

    }

    @Override
    public void onClick(DashboardResponse.Candidate.Data model) {
        Intent intent = new Intent(getActivity(), DetailWorkActivity.class);
        intent.putExtra("id", model.getId());
        startActivity(intent);
    }

    @Override
    public void onApply(DashboardResponse.Candidate.Data model) {
        mPresenter.onApply(model.getId());
    }

    private void showAutocomplete() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
            startActivityForResult(intent, REQUEST_SELECT_PLACE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("result", "onActivityResult");
        if (requestCode == REQUEST_SELECT_PLACE) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }

        if (requestCode == 11) {
            if (resultCode == 100) {
                job_id = data.getStringExtra("id");
                String job = data.getStringExtra("job");
                candidateAdapter.setJobHeader(job);
                mPresenter.onViewListCityOrJob(city_id, job_id, "0");
                typeLoad = 2;
            }
        }


        if (requestCode == 22) {
            if (resultCode == 200){
                city_id = data.getStringExtra("id_city");
                String city = data.getStringExtra("city");
                candidateAdapter.setCityHeader(city);
                mPresenter.onViewListCityOrJob(city_id, job_id, "0");
                typeLoad = 2;
            }
        }

        StateEvent stateEvent = new StateEvent();
        stateEvent.setBasestate("00");
        bus.postSticky(stateEvent);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == 1) showAutocomplete();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_location);
            dialog.setOnMessageClosed(this::showAutocomplete);
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        /*view_custom_location.setVisibility(View.VISIBLE);
        txt_custom_location.setText(place.getAddress());*/
        lat = String.valueOf(place.getLatLng().latitude);
        lng = String.valueOf(place.getLatLng().longitude);
        mPresenter.onCustomLocation(lat, lng, "0");
    }

    @Override
    public void onError(Status status) {}

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        rv_work_item.setAdapter(null);
        super.onDestroyView();
    }

    @Override
    public void defaultJob(ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> defaultJob) {
        //String jobName = defaultJob.get(0).getJenis_pekerjaan();
        String jobName = Util.dashboardJob(defaultJob);
        candidateAdapter.setJobHeader(jobName);
    }

    @Override
    public void defaultCity(ArrayList<UserResponse.Login.Data.Kota> defaultCity) {
        //String cityName = defaultCity.get(0).getName();
        String cityName = Util.dashboardCity(defaultCity);
        candidateAdapter.setCityHeader(cityName);
    }

    private void readLoccation(){
        boolean hasPermission = EasyPermissions.hasPermissions(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasPermission) {
            showAutocomplete();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.message_permission_location), 1,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onClickJob() {
        Intent intent = new Intent(getActivity(), DataWorkActivity.class);
        intent.putExtra("models", new ArrayList<WorkModel>());
        intent.putExtra("dashboard", true);
        startActivityForResult(intent, 11);
    }

    @Override
    public void onClickCity() {
        Intent intent = new Intent(getActivity(), DataCityActivity.class);
        startActivityForResult(intent, 22);
    }

    @Override
    public void onClickOke(String query) {
        mPresenter.onViewSearch(query, "");
    }

    @Override
    public void onClickLocation() {
        readLoccation();
    }
}
