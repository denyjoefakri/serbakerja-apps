package com.joefakri.serbakerja.ui.recruters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExpandAdapter;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateMvpPresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateView;
import com.joefakri.serbakerja.ui.profile.photo.DetailPhotoActivity;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TimeUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailRecrutersActivity extends BaseActivity implements DetailRecrutersView{

    @Inject DetailRecrutersMvpPresenter<DetailRecrutersView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_company) CircleImageView img_company;
    @BindView(R.id.txt_company_name_header) TextView txt_company_name_header;
    @BindView(R.id.txt_company_name) TextView txt_company_name;
    @BindView(R.id.txt_company_address) TextView txt_company_address;
    @BindView(R.id.txt_company_city) TextView txt_company_city;
    @BindView(R.id.txt_company_area) TextView txt_company_area;
    @BindView(R.id.txt_company_total_employer) TextView txt_company_total_employer;
    @BindView(R.id.txt_company_website) TextView txt_company_website;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_detail_recruter, this);
        getActivityComponent().inject(this);

        presenter.onAttach(DetailRecrutersActivity.this);
        initComponent();

    }

    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_detail_recruters));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onViewDetail(getIntent().getStringExtra("id"));
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResultViewDetail(RecruiterResponse.CompanyProfile.Data detailInfo) {
        Glide.with(this).load(detailInfo.getLogo()).into(img_company);
        txt_company_name_header.setText(detailInfo.getNama_perusahaan());
        txt_company_name.setText(detailInfo.getNama_perusahaan());
        txt_company_address.setText(detailInfo.getAlamat_perusahaan());
        txt_company_city.setText(detailInfo.getNama_kota());
        txt_company_area.setText(detailInfo.getDaerah());
        txt_company_total_employer.setText(Constants.getTotalEmployee(this, detailInfo.getJumlah_karyawan()));
        txt_company_website.setText(detailInfo.getWebsite());

        img_company.setOnClickListener(view -> {
            Intent intent = new Intent(DetailRecrutersActivity.this, DetailPhotoActivity.class);
            intent.putExtra("image", detailInfo.getLogo());
            startActivity(intent);
        });
    }
}
