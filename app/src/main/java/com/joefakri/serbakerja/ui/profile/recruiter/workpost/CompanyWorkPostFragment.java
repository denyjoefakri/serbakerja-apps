package com.joefakri.serbakerja.ui.profile.recruiter.workpost;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkPostAdapter;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostActivity;
import com.joefakri.serbakerja.utils.Logger;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class CompanyWorkPostFragment extends BaseFragment implements CompanyWorkPostView{

    @BindView(R.id.rv_work) RecyclerView rv_work;
    @BindView(R.id.nv_main) NestedScrollView nv_main;
    @BindView(R.id.fab_add) FloatingActionButton fab_add;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.view_data) LinearLayout view_data;
    @BindView(R.id.tv_message) TextView tv_message;
    WorkPostAdapter workPostAdapter;

    @Inject CompanyWorkPostMvpPresenter<CompanyWorkPostView> mPresenter;
    @Inject LinearLayoutManager mLayoutManager;
    @Inject DataManager dataManager;

    private int currentPage = 0;
    private boolean notLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_company_work_post, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View view) {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work.setLayoutManager(mLayoutManager);
        rv_work.setHasFixedSize(true);
        rv_work.setNestedScrollingEnabled(false);
        rv_work.setAdapter(workPostAdapter = new WorkPostAdapter(getActivity()));
        workPostAdapter.setOnclickListener((model, position) -> {
            if (model.getStatus().equals("close") || model.getExpired_status().equals("1")) {
                showMessageDialog("Pekerjaan tidak bisa diakses karena sudah ditutup/kadaluarsa");
            } else {
                Intent intent = new Intent(getActivity(), EditWorkPostActivity.class);
                intent.putExtra("model", model);
                startActivity(intent);
            }
        });

        nv_main.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v1, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v1.getChildAt(v1.getChildCount() - 1) != null) {
                if ((scrollY >= (v1.getChildAt(v1.getChildCount() - 1).getMeasuredHeight() - v1.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!notLoading){
                        notLoading = true;
                        currentPage += 10;
                        mPresenter.onViewWork(String.valueOf(currentPage));
                        workPostAdapter.addLoading();
                    }
                }
            }
        });

        fab_add.setOnClickListener(view1 -> {
            startActivity(new Intent(getActivity(), EditWorkPostActivity.class));
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewWork("0");
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultProfileView(ArrayList<WorkResponse.List.Data> data) {
        if (data.isEmpty()){
            if (workPostAdapter.getItemCount() == 0){
                view_empty.setVisibility(View.VISIBLE);
                view_data.setVisibility(View.GONE);
                tv_message.setText(getString(R.string.txt_work_empty));
            }
            Log.e("data", "isEmpty");
            Log.e("notLoading", "true");
            if (notLoading) workPostAdapter.removeLoading();
        } else {
            view_empty.setVisibility(View.GONE);
            view_data.setVisibility(View.VISIBLE);
            if (notLoading) {
                workPostAdapter.removeLoading();
                workPostAdapter.updateAll(data);
                notLoading = false;
            } else {
                workPostAdapter.update(data);
            }

            ArrayList<UserResponse.Login.Data.Kota> citys = new ArrayList<>();
            ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> works = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getExpired_status().equals("0") && data.get(i).getStatus().equals("active")) {
                    UserResponse.Login.Data.Kota city = new UserResponse.Login.Data.Kota();
                    UserResponse.Login.Data.Jenis_pekerjaan work = new UserResponse.Login.Data.Jenis_pekerjaan();
                    city.setId(data.get(i).getKota_id());
                    city.setName(data.get(i).getKota());
                    work.setId(data.get(i).getJenis_pekerjaan_id());
                    work.setJenis_pekerjaan(data.get(i).getJenis_pekerjaan());
                    citys.add(city);
                    works.add(work);
                }
            }

            dataManager.setPreferenceNull();
            dataManager.setDefaultCity(citys);
            dataManager.setDefaultJob(works);

            DashboardRequest.Recruter recruter = new DashboardRequest.Recruter(works, citys);
            Logger.printLogRequest(recruter.getMultipleMap());
        }
        hideLoading();
    }
}
