/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.main.MainMvpPresenter;
import com.joefakri.serbakerja.ui.main.MainView;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class LoginPresenter<V extends LoginView> extends BasePresenter<V>
        implements LoginMvpPresenter<V> {

    CustomTabsClient mCustomTabsClient;
    CustomTabsSession mCustomTabsSession;
    CustomTabsServiceConnection mCustomTabsServiceConnection;
    CustomTabsIntent mCustomTabsIntent;
    ComponentName componentName;

    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private boolean mVerificationInProgress = false;

    @Inject
    public LoginPresenter(DataManager dataManager,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void initPhoneAuth(Activity activity) {
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                mVerificationInProgress = false;
                signInWithPhoneAuthCredential(activity, credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                mVerificationInProgress = false;
                getSerbakerjaView().hideLoading();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    getSerbakerjaView().showMessageDialog(R.string.message_invalid_phone_number);
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    getSerbakerjaView().showMessageDialog(R.string.message_quota_exceeded);
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                getSerbakerjaView().hideLoading();
                getSerbakerjaView().onCodeSent(verificationId, token);
            }
        };
    }

    @Override
    public void startPhoneNumberVerification(Activity activity, String phoneNumber) {
        Log.e("startPhoneNumberVerifi", phoneNumber);
        getSerbakerjaView().showLoading();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 120, TimeUnit.SECONDS, activity, mCallbacks);
        mVerificationInProgress = true;
    }

    private void signInWithPhoneAuthCredential(Activity activity, PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        getSerbakerjaView().signinSuccess(task.getResult().getUser());
                        getSerbakerjaView().hideLoading();
                    } else {
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            getSerbakerjaView().showMessageDialog(R.string.message_invalid_code);
                        }
                    }
                });
    }

    @Override
    public void contactusOption(Activity activity, View v) {
        PopupMenu popupMenu = new PopupMenu(activity, v);
        popupMenu.getMenuInflater().inflate(R.menu.option_contactus, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_call) {
                boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CALL_PHONE);
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+6281908283331"));
                if (hasPermission) activity.startActivity(callIntent);
            } else {
                mCustomTabsIntent.launchUrl(activity, Uri.parse("https://api.whatsapp.com/send?phone=+6281908283331"));
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    public void readPhone(Activity activity, View v) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CALL_PHONE);

        if (hasPermission) {
            contactusOption(activity, v);
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.message_permission_callphone), 0,
                    Manifest.permission.CALL_PHONE);
        }
    }

    @Override
    public void initCustomTab(Activity activity) {
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName component, CustomTabsClient customTabsClient) {
                mCustomTabsClient= customTabsClient;
                componentName = component;
                mCustomTabsClient.warmup(0L);
                mCustomTabsSession = mCustomTabsClient.newSession(null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mCustomTabsClient= null;
            }
        };

        CustomTabsClient.bindCustomTabsService(activity, "com.android.chrome", mCustomTabsServiceConnection);

        mCustomTabsIntent = new CustomTabsIntent.Builder(mCustomTabsSession)
                .setToolbarColor(Color.parseColor("#2a2a2a"))
                .setSecondaryToolbarColor(Color.parseColor("#628D97"))
                .setShowTitle(true)
                .build();
    }

    @Override
    public void onRegister(String email, String password) {
        UserRequest.Login login = new UserRequest.Login(email, password);
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.LOGIN, login, null);

        getCompositeDisposable().add(getDataManager()
                .login(login)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.LOGIN);
                    Gson gson = new Gson();
                    UserResponse.Login response = gson.fromJson(jsonObject.toString(), UserResponse.Login.class);

                    if (response.getStatus().equals("error")){
                        Log.e("Login", response.getMessage());
                        getSerbakerjaView().onError(response.getMessage());
                    } else if (!response.getData().getCount_proses_kandidat().equals("5") && response.getStatus().equals("success") || !response.getData().getCount_proses_perekrut().equals("3")
                            && response.getStatus().equals("success")){
                        Log.e("getCount_proses_", response.getMessage());
                        getDataManager().updateUserInfo("", response.getData().getId(), response.getData().getNama(),
                                response.getData().getNo_hp(), response.getData().getEmail(), password, "");

                        if (response.getData().getVerifikasi_hp().equals("0")){
                            getSerbakerjaView().onResultLoginNotVerified(response.getData().getNo_hp());
                        } else if (response.getData().getTipe_user() != null) {
                            getDataManager().setTypeUser(response.getData().getTipe_user());

                            if (response.getData().getTipe_user().equals(Constants.ROLE_PEREKRUT)) {
                                getDataManager().updateRecruter(response.getData().getProses_detail().getP_info_perusahaan(),
                                        response.getData().getProses_detail().getP_pekerjaan(), response.getData().getProses_detail().getP_info_perekrut());
                                if (!response.getData().getCount_proses_perekrut().equals("3")){
                                    getSerbakerjaView().onResultLoginNotRegistered("1");
                                } else {
                                    getDataManager().updateAdditionalUserInfo(response.getData().getTipe_user(), response.getData().getStatus(),
                                            response.getData().getKota(), response.getData().getJenis_pekerjaan());
                                    getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);
                                    getSerbakerjaView().onResultLogin(response.getData());
                                }
                            } else if (response.getData().getTipe_user().equals(Constants.ROLE_KANDIDAT)) {
                                getDataManager().updateCandidate(response.getData().getProses_detail().getK_info(),
                                        response.getData().getProses_detail().getK_minat(), response.getData().getProses_detail().getK_pengalaman(),
                                        response.getData().getProses_detail().getK_pendidikan(), response.getData().getProses_detail().getK_dokumen());
                                if (!response.getData().getCount_proses_kandidat().equals("5")) {
                                    getSerbakerjaView().onResultLoginNotRegistered("2");
                                } else {
                                    getDataManager().updateAdditionalUserInfo(response.getData().getTipe_user(), response.getData().getStatus(),
                                            response.getData().getKota(), response.getData().getJenis_pekerjaan());
                                    getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);
                                    getSerbakerjaView().onResultLogin(response.getData());
                                }
                            }
                        } else {
                            if (response.getData().getVerifikasi_hp().equals("0")){
                                getSerbakerjaView().onResultLoginNotVerified(response.getData().getNo_hp());
                            } else if (response.getData().getCount_proses_kandidat().equals("0")
                                    && response.getData().getVerifikasi_hp().equals("1")
                                    || response.getData().getCount_proses_perekrut().equals("0")
                                    && response.getData().getVerifikasi_hp().equals("1")) {
                                getSerbakerjaView().onResultLoginNotRegistered("0");
                            }
                        }


                    } else if (response.getStatus().equals("success")) {
                        if (response.getData().getTipe_user().equals("perekrut")){
                            getDataManager().updateUserInfo("", response.getData().getId(), response.getData().getInfo().getNama_perusahaan(),
                                    response.getData().getNo_hp(), response.getData().getEmail(), password, "");
                        } else {
                            getDataManager().updateUserInfo("", response.getData().getId(), response.getData().getInfo().getNama_ktp(),
                                    response.getData().getNo_hp(), response.getData().getEmail(), password, "");
                        }

                        getDataManager().updateAdditionalUserInfo(response.getData().getTipe_user(), response.getData().getStatus(),
                                response.getData().getKota(), response.getData().getJenis_pekerjaan());
                        getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);

                        Log.e("success", response.getMessage());
                        getSerbakerjaView().onResultLogin(response.getData());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void unRegisterListener() {
        mCustomTabsServiceConnection.onServiceDisconnected(componentName);
    }

    @Override
    public void updateStatusVerification(String phone_number) {
        getSerbakerjaView().showLoading();

        UserRequest.Verification verification = new UserRequest.Verification(getDataManager().getCurrentUserId(), phone_number);
        Logger.printLogRequest(Path.VERIFIKASI, verification, null);

        getCompositeDisposable().add(getDataManager()
                .verifikasi(verification)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.VERIFIKASI);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    UserResponse.Register response = gson.fromJson(jsonObject.toString(), UserResponse.Register.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultUpdateStatus();
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().hideLoading();
                    }

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
