package com.joefakri.serbakerja.ui.city;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.DataCityAdapter;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class DataCityActivity extends BaseActivity implements DataCityView, DataCityAdapter.Listener{

    @Inject DataCityMvpPresenter<DataCityView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.ed_job) AutoCompleteTextView ed_job;
    @BindView(R.id.rv_job) RecyclerView rv_job;
    @BindView(R.id.btn_choose) Button btn_choose;

    DataCityAdapter adapter;
    ArrayList<JobResponse.Data> models;

    @Inject
    LinearLayoutManager mLayoutManager;

    String id_selected = "";
    String city_selected = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_data_work, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.hint_company_city));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();

    }

    @Override
    protected void initComponent() {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        models = new ArrayList<>();
        rv_job.setLayoutManager(mLayoutManager);
        rv_job.setHasFixedSize(true);
        rv_job.setNestedScrollingEnabled(false);
        rv_job.setAdapter(adapter = new DataCityAdapter(this));
        adapter.setOnclickListener(this);

        presenter.onViewCity();

        btn_choose.setOnClickListener(view -> {
            finish();
        });

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {
        adapter.update(dataList);

        ArrayList<String> citys = new ArrayList<>();
        ArrayList<String> ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, citys);
        ed_job.setAdapter(adapter);
        ed_job.setOnItemClickListener((adapterView, view, i, l) -> {
            city_selected = adapter.getItem(i).toString();
            id_selected = ids.get(citys.indexOf(city_selected));
            Log.e("city", id_selected + " | " + city_selected);
        });
    }

    @Override
    public void finish() {
        Intent intentResult = new Intent();
        intentResult.putExtra("id_city", id_selected);
        intentResult.putExtra("city", city_selected);
        setResult(200, intentResult);
        super.finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_choose, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_pilih) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(AreaResponse.CityResponse.Data model) {
        id_selected = model.getId();
        city_selected = model.getName();
        Log.e("id job", id_selected);
    }


}
