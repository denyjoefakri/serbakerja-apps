package com.joefakri.serbakerja.ui.chat;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomView;

/**
 * Created by deny on bandung.
 */

public interface ChatMvpPresenter<V extends ChatView> extends SerbakerjaPresenter<V> {

    void onViewList(String offset);

}
