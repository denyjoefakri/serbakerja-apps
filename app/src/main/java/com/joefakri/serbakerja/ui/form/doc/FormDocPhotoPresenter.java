/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form.doc;

import android.Manifest;
import android.app.Activity;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.form.company.FormCompanyMvpPresenter;
import com.joefakri.serbakerja.ui.form.company.FormCompanyView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;
import com.levibostian.shutter_android.Shutter;
import com.levibostian.shutter_android.builder.ShutterPickPhotoGalleryBuilder;
import com.levibostian.shutter_android.builder.ShutterResultCallback;
import com.levibostian.shutter_android.builder.ShutterTakePhotoBuilder;
import com.levibostian.shutter_android.vo.ShutterResult;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class FormDocPhotoPresenter<V extends FormDocPhotoView> extends BasePresenter<V>
        implements FormDocPhotoMvpPresenter<V>{

    private Realm realm;

    @Inject
    public FormDocPhotoPresenter(DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void init() {
        realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        if (candidateRealm != null) getSerbakerjaView().init(candidateRealm);
    }

    @Override
    public void readCameraGallery(Activity activity, Fragment fragment, View v) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (hasPermission) {
            openImageChooser(activity, v);
        } else {
            EasyPermissions.requestPermissions(fragment, activity.getString(R.string.message_permission_camera), 0,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void openImageChooser(Activity activity, View v) {
        PopupMenu popupMenu = new PopupMenu(activity, v);
        popupMenu.getMenuInflater().inflate(R.menu.option_camera_ind, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_camera) {
                takePhoto(activity);
            } else {
                openGallery(activity);
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    public void onPostCandidateDocument() {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();

        CandidateRequest.PostDocument document = new CandidateRequest.PostDocument(getDataManager().getCurrentUserId());
        CandidateRequest.PostDocumentFile file = new CandidateRequest.PostDocumentFile(candidate.getDocPhotoKTP(),
                candidate.getDocPhotoIjazah(), candidate.getDocPhoto());

        Logger.printLogRequest(Path.POST_CANDIDATE_DOCUMENT, document, file.getFoto());
        getCompositeDisposable().add(getDataManager()
                .postCDocument(document, file)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_CANDIDATE_DOCUMENT);
                    Gson gson = new Gson();
                    CandidateResponse.DocumentResponse response = gson.fromJson(jsonObject.toString(), CandidateResponse.DocumentResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCandidateDocument(response.getData());
                        candidate.setPercent5(3);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void saveDocKTP(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setDocPhotoKTP(data);
            candidate.setPercent5(1);
        } else {
            candidate = realm.createObject(CandidateRealm.class);
            candidate.setId(1);
            candidate.setDocPhotoKTP(data);
            candidate.setPercent5(1);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveDocIjazah(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setDocPhotoIjazah(data);
            candidate.setPercent5(2);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveDocPhoto(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setDocPhoto(data);
        }
        realm.commitTransaction();
        realm.close();
    }


    private void takePhoto(Activity activity){
        ShutterTakePhotoBuilder shutter = Shutter.Companion.with(activity)
                .takePhoto().usePrivateAppInternalStorage()
                .addPhotoToGallery();

        ((FormActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().takePhoto(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath());
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void openGallery(Activity activity){
        ShutterPickPhotoGalleryBuilder shutter = Shutter.Companion.with(activity)
                .getPhotoFromGallery().usePrivateAppInternalStorage();

        ((FormActivity)activity).shutterListener = shutter.snap(new ShutterResultCallback() {
            @Override
            public void onComplete(ShutterResult shutterResult) {
                getSerbakerjaView().openGallery(compress(activity, shutterResult.getAbsoluteFilePath()).getAbsolutePath());
            }

            @Override
            public void onError(String s, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private File compress(Activity activity, String file){
        File fileToSend;
        String nama = "";
        if (new File(file).getName().indexOf(".") > 0)
            nama = new File(file).getName().substring(0, new File(file).getName().lastIndexOf("."));

        String extension = new File(file).getAbsolutePath().substring(new File(file).getAbsolutePath().lastIndexOf("."));

        try {
            fileToSend = new Compressor(activity)
                    .setMaxWidth(1000)
                    .setMaxHeight(1000)
                    .setQuality(90)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .compressToFile(new File(file), nama + "_Serbakerja" + extension);
            Log.e("compress", fileToSend.getAbsolutePath());
            return fileToSend;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
