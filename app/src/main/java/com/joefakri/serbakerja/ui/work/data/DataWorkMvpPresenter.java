package com.joefakri.serbakerja.ui.work.data;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.work.DetailWorkView;

/**
 * Created by deny on bandung.
 */

public interface DataWorkMvpPresenter<V extends DataWorkView> extends SerbakerjaPresenter<V> {

    void onViewJob();

}
