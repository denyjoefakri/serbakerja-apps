package com.joefakri.serbakerja.ui.work.data;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.DataWorkAdapter;
import com.joefakri.serbakerja.adapter.WorkCandidateRegisterAdapter;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.RecruiterMainActivity;
import com.joefakri.serbakerja.ui.work.DetailWorkMvpPresenter;
import com.joefakri.serbakerja.ui.work.DetailWorkView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class DataWorkActivity extends BaseActivity implements DataWorkView, DataWorkAdapter.Listener{

    @Inject DataWorkMvpPresenter<DataWorkView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.ed_job) AutoCompleteTextView ed_job;
    @BindView(R.id.rv_job) RecyclerView rv_job;
    @BindView(R.id.btn_choose) Button btn_choose;

    DataWorkAdapter adapter;
    ArrayList<JobResponse.Data> models;

    @Inject
    LinearLayoutManager mLayoutManager;

    String id_selected = "";
    String job_selected = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_data_work, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_work));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();

    }

    @Override
    protected void initComponent() {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        models = new ArrayList<>();
        rv_job.setLayoutManager(mLayoutManager);
        rv_job.setHasFixedSize(true);
        rv_job.setNestedScrollingEnabled(false);
        rv_job.setAdapter(adapter = new DataWorkAdapter(this, (ArrayList<WorkModel>) getIntent().getSerializableExtra("models")));
        adapter.setOnclickListener(this);

        presenter.onViewJob();

        btn_choose.setOnClickListener(view -> {
            finish();
        });

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(JobResponse.Data model) {
        id_selected = model.getId();
        job_selected = model.getNama();
        Log.e("id job", id_selected);
    }

    @Override
    public void onDisabledClick() {
        onError("Anda tidak boleh memilih pekerjaan yang sama");
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

        adapter.update(getIntent().getBooleanExtra("dashboard", false), dataList);

        ArrayList<String> jobs = new ArrayList<>();
        ArrayList<String> ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            jobs.add(dataList.get(i).getNama());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, jobs);
        ed_job.setAdapter(adapter);
        ed_job.setOnItemClickListener((adapterView, view, i, l) -> {
            job_selected = adapter.getItem(i).toString();
            id_selected = ids.get(jobs.indexOf(job_selected));
            Log.e("job", id_selected + " | " + job_selected);
        });
    }

    @Override
    public void finish() {
        Intent intentResult = new Intent();
        intentResult.putExtra("id", id_selected);
        intentResult.putExtra("job", job_selected);
        setResult(100, intentResult);
        super.finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_choose, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_pilih) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
