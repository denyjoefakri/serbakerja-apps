package com.joefakri.serbakerja.ui.main;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.chat.ChatView;

/**
 * Created by deny on bandung.
 */

public interface MainMvpPresenter<V extends MainView> extends SerbakerjaPresenter<V> {

    void onClickProfile();

    void ViewTab();

    void updateFCM();

}
