package com.joefakri.serbakerja.ui.profile.candidate.experience;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.adapter.WorkPostAdapter;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceActivity;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.work.CandidateWorkView;
import com.joefakri.serbakerja.ui.profile.candidate.work_edit.EditWorkActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class CandidateExperienceFragment extends BaseFragment implements CandidateExperienceView{

    @BindView(R.id.txt_work_status) TextView txt_work_status;
    @BindView(R.id.txt_work_last_sallary) TextView txt_work_last_sallary;
    @BindView(R.id.btn_edit) LinearLayout btn_edit;
    @BindView(R.id.rv_work_experience) RecyclerView rv_work_experience;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.tv_message) TextView tv_message;

    WorkExperienceAdapter experienceAdapter;

    @Inject CandidateExperienceMvpPresenter<CandidateExperienceView> mPresenter;
    @Inject LinearLayoutManager mLayoutManager;
    CandidateResponse.Experience.Data data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_candidate_experience, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View view) {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_work_experience.setLayoutManager(mLayoutManager);
        rv_work_experience.setHasFixedSize(true);
        rv_work_experience.setNestedScrollingEnabled(false);
        rv_work_experience.setAdapter(experienceAdapter = new WorkExperienceAdapter(getActivity()));

        btn_edit.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), EditExperienceActivity.class);
            intent.putExtra("model", data);
            startActivity(intent);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewExperience();
        btn_edit.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultExperienceView(CandidateResponse.Experience.Data data) {
        btn_edit.setEnabled(true);
        this.data = data;
        txt_work_last_sallary.setText(Constants.sallary(getActivity(), data.getGaji_min(), data.getGaji_max()));
        if (!data.getStatus_pekerjaan().equals("3"))
            txt_work_status.setText(Constants.workStatusValue(getActivity(), data.getStatus_pekerjaan()));
        else txt_work_status.setText(data.getStatus_pekerjaan_lainnya());

        if (data.getPengalaman().isEmpty()){
            view_empty.setVisibility(View.VISIBLE);
            tv_message.setText(getString(R.string.txt_experience_empty));
            rv_work_experience.setVisibility(View.GONE);
        } else {
            ArrayList<WorkModel> workModels = new ArrayList<>();
            for (int i = 0; i < data.getPengalaman().size(); i++) {
                WorkModel workModel = new WorkModel();
                workModel.setCompany_name(data.getPengalaman().get(i).getNama_perusahaan());
                workModel.setWork_as(data.getPengalaman().get(i).getPosisi());
                workModel.setWork_long(data.getPengalaman().get(i).getLama_bekerja());
                workModel.setPeriod(data.getPengalaman().get(i).getPeriod());
                workModels.add(workModel);
            }
            experienceAdapter.update(workModels);
            view_empty.setVisibility(View.GONE);
            rv_work_experience.setVisibility(View.VISIBLE);
        }

    }
}
