package com.joefakri.serbakerja.ui.form.education;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormEducationLastFragment extends BaseFragment implements FormEducationView{

    @Inject FormEducationMvpPresenter<FormEducationView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_education_graduated) EditText txt_education_graduated;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_education_graduated, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {
            if (txt_education_graduated.getText().toString().isEmpty()){
                onError("Tahun tamat pendidikan terakhir tidak boleh kosong");
                return;
            }
            mPresenter.saveEducationLast(txt_education_graduated.getText().toString());
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(1);
        });
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultCandidateEducation(CandidateResponse.EducationResponse.Data dataList) {

    }

    @Override
    public void init(CandidateRealm candidateRealm) {
        if (candidateRealm != null)
            txt_education_graduated.setText(candidateRealm.getEducationLast());
    }
}
