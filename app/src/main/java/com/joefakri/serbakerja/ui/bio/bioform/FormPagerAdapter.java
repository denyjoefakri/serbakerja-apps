/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.bio.bioform;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.joefakri.serbakerja.ui.form.company.FormCompanyAddressFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyCityFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyLogoFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyNameFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyTotalEmployeeFragment;
import com.joefakri.serbakerja.ui.form.company.FormCompanyWebsiteFragment;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoIjazahFragment;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoKTPFragment;
import com.joefakri.serbakerja.ui.form.doc.FormDocPhotoPersonFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationGraduatedFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationLastFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationNameCoursesFragment;
import com.joefakri.serbakerja.ui.form.education.FormEducationNameLastSchoolFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonAddressFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonBirthdayFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonDistrictsFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonGenderFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonNameFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonPhotoFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonPhotoKTPFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonReligionFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonRtrwFragment;
import com.joefakri.serbakerja.ui.form.person.FormPersonVillageFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkAreaAddressFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkCandidateFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkCustomRequirement;
import com.joefakri.serbakerja.ui.form.work.FormWorkDayTimeFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkEducationFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkExperienceFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkExpiredDateFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkGenderFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkLastSallaryFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkMinimalExperienceFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkScopeFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkStatusFragment;
import com.joefakri.serbakerja.ui.form.work.FormWorkTypeSallaryFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Janisharali on 25/05/2017.
 */

public class FormPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;
    private List<Fragment> mFragmentList;

    private void tab(int type){
        clearReference();
        switch (type){
            case 1:
                setupViewPager1_recruiters();
                break;
            case 2:
                setupViewPager2_recruiters();
                break;
            case 3:
                setupViewPager3_recruiters();
                break;
            case 11:
                setupViewPager1_candidate();
                break;
            case 12:
                setupViewPager2_candidate();
                break;
            case 13:
                setupViewPager3_candidate();
                break;
            case 14:
                setupViewPager4_candidate();
                break;
            case 15:
                setupViewPager5_candidate();
                break;
        }
    }

    private void setupViewPager1_recruiters(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormCompanyNameFragment());
        mFragmentList.add(new FormCompanyAddressFragment());
        mFragmentList.add(new FormCompanyCityFragment());
        mFragmentList.add(new FormCompanyTotalEmployeeFragment());
        mFragmentList.add(new FormCompanyWebsiteFragment());
        mFragmentList.add(new FormCompanyLogoFragment());
    }

    private void setupViewPager2_recruiters(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormWorkTypeSallaryFragment());
        mFragmentList.add(new FormWorkAreaAddressFragment());
        mFragmentList.add(new FormWorkDayTimeFragment());
        mFragmentList.add(new FormWorkGenderFragment());
        mFragmentList.add(new FormWorkMinimalExperienceFragment());
        mFragmentList.add(new FormWorkEducationFragment());
        mFragmentList.add(new FormWorkCustomRequirement());
        mFragmentList.add(new FormWorkScopeFragment());
        mFragmentList.add(new FormWorkExpiredDateFragment());
    }

    private void setupViewPager3_recruiters(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormPersonNameFragment());
        mFragmentList.add(new FormPersonBirthdayFragment());
        mFragmentList.add(new FormPersonAddressFragment());
        mFragmentList.add(new FormPersonDistrictsFragment());
        mFragmentList.add(new FormPersonVillageFragment());
        mFragmentList.add(new FormPersonRtrwFragment());
        mFragmentList.add(new FormPersonPhotoKTPFragment());
        mFragmentList.add(new FormPersonPhotoFragment());
    }

    private void setupViewPager1_candidate(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormPersonNameFragment());
        mFragmentList.add(new FormPersonBirthdayFragment());
        mFragmentList.add(new FormPersonAddressFragment());
        mFragmentList.add(new FormPersonDistrictsFragment());
        mFragmentList.add(new FormPersonVillageFragment());
        mFragmentList.add(new FormPersonRtrwFragment());
        mFragmentList.add(new FormPersonGenderFragment());
        mFragmentList.add(new FormPersonReligionFragment());
    }

    private void setupViewPager2_candidate(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormWorkCandidateFragment());
        /*mFragmentList.add(new FormWorkTypeFragment());
        mFragmentList.add(new FormWorkSallaryFragment());
        mFragmentList.add(new FormWorkAreaFragment());
        mFragmentList.add(new FormWorkTimeFragment());
        mFragmentList.add(new FormWorkDayFragment());*/
    }

    private void setupViewPager3_candidate(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormWorkStatusFragment());
        mFragmentList.add(new FormWorkLastSallaryFragment());
        mFragmentList.add(new FormWorkExperienceFragment());
    }

    private void setupViewPager4_candidate(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormEducationLastFragment());
        mFragmentList.add(new FormEducationGraduatedFragment());
        mFragmentList.add(new FormEducationNameLastSchoolFragment());
        mFragmentList.add(new FormEducationNameCoursesFragment());
    }

    private void setupViewPager5_candidate(){
        mFragmentList = new ArrayList<>();
        mFragmentList.add(new FormDocPhotoKTPFragment());
        mFragmentList.add(new FormDocPhotoIjazahFragment());
        mFragmentList.add(new FormDocPhotoPersonFragment());
    }

    public FormPagerAdapter(FragmentManager fragmentManager, int type) {
        super(fragmentManager);
        this.mTabCount = 0;
        this.tab(type);
    }

    public void addFrag(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void setCount(int count) {
        mTabCount = count;
    }

    public void clearReference() {
        if (mFragmentList != null) {
            mFragmentList.clear();
        }
    }
}
