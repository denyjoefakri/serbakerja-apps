package com.joefakri.serbakerja.ui.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.joefakri.serbakerja.BuildConfig;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.ui.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class TNCActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_tnc, this);
        initComponent();
    }

    @Override
    protected void initComponent() {

    }

}
