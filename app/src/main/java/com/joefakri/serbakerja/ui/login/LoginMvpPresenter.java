package com.joefakri.serbakerja.ui.login;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.main.MainView;

/**
 * Created by deny on bandung.
 */

public interface LoginMvpPresenter<V extends LoginView> extends SerbakerjaPresenter<V> {

    void initPhoneAuth(Activity activity);

    void startPhoneNumberVerification(Activity activity, String phoneNumber);

    void contactusOption(Activity activity, View v);

    void readPhone(Activity activity, View v);

    void initCustomTab(Activity activity);

    void onRegister(String email, String password);

    void unRegisterListener();

    void updateStatusVerification(String phone_number);
}
