package com.joefakri.serbakerja.ui.profile.candidate.document_edit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceView;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.levibostian.shutter_android.builder.ShutterResultListener;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class EditDocumentActivity extends BaseActivity implements EditDocumentView, EasyPermissions.PermissionCallbacks{

    @Inject
    EditDocumentMvpPresenter<EditDocumentView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_photo_ktp) ImageView img_photo_ktp;
    @BindView(R.id.btn_change_photo_ktp) LinearLayout btn_change_photo_ktp;
    @BindView(R.id.img_ijazah) ImageView img_ijazah;
    @BindView(R.id.btn_change_ijazah) LinearLayout btn_change_ijazah;
    @BindView(R.id.img_photo) ImageView img_photo;
    @BindView(R.id.btn_change_photo) LinearLayout btn_change_photo;
    @BindView(R.id.btn_save) Button btn_save;

    public ShutterResultListener shutterListener;
    String photo_ktp, ijazah, photo;
    CandidateResponse.Document.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_candidate_document, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_form_5_kandidat));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        data = (CandidateResponse.Document.Data) getIntent().getSerializableExtra("model");

        if (data != null) {
            if (data.getFoto_ktp() != null) Glide.with(this).load(data.getFoto_ktp()).into(img_photo_ktp);
            if (data.getFoto_terbaru() != null) Glide.with(this).load(data.getFoto_terbaru()).into(img_photo);
            if (data.getFoto_ijazah() != null) Glide.with(this).load(data.getFoto_ijazah()).into(img_ijazah);
        } else {
            data = new CandidateResponse.Document.Data();
        }

        btn_change_photo_ktp.setOnClickListener(view -> {
            presenter.readCameraGallery(this, view, 0);
        });
        btn_change_ijazah.setOnClickListener(view -> {
            presenter.readCameraGallery(this, view, 1);
        });
        btn_change_photo.setOnClickListener(view -> {
            presenter.readCameraGallery(this, view, 2);
        });

        btn_save.setOnClickListener(view -> {
            if (photo_ktp == null && data.getFoto_ktp() == null){
                onError("Foto KTP tidak boleh kosong");
                return;
            }

            if (photo == null && data.getFoto_terbaru() == null){
                onError("Foto tidak boleh kosong");
                return;
            }

            if (photo_ktp != null || photo != null || ijazah != null)
                presenter.onUpdate(photo, ijazah, photo_ktp);
            else showMessageDialog("Anda tidak mengubah dokumen");

        });
    }

    @Override
    public void onClosedMessageDialog() {
        super.onClosedMessageDialog();
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void takePhoto(String file, int requestCode) {
        switch (requestCode){
            case 0:
                photo_ktp = file;
                data.setFoto_ktp(file);
                Glide.with(this).load(file).into(img_photo_ktp);
                break;
            case 1:
                ijazah = file;
                Glide.with(this).load(file).into(img_ijazah);
                break;
            case 2:
                photo = file;
                data.setFoto_terbaru(file);
                Glide.with(this).load(file).into(img_photo);
                break;
        }
    }

    @Override
    public void openGallery(String file, int requestCode) {
        switch (requestCode){
            case 0:
                photo_ktp = file;
                data.setFoto_ktp(file);
                Glide.with(this).load(file).into(img_photo_ktp);
                break;
            case 1:
                ijazah = file;
                Glide.with(this).load(file).into(img_ijazah);
                break;
            case 2:
                photo = file;
                data.setFoto_terbaru(file);
                Glide.with(this).load(file).into(img_photo);
                break;
        }
    }

    @Override
    public void onResult(CandidateResponse.ChangeProfile changeProfileData) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!shutterListener.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        presenter.openImageChooser(this, btn_change_photo, requestCode);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                presenter.readCameraGallery(this, btn_change_photo, requestCode);
            });
        }
    }
}
