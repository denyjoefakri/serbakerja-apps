/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.register;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.androidnetworking.error.ANError;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.ChatRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.login.LoginMvpPresenter;
import com.joefakri.serbakerja.ui.login.LoginView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

import static com.joefakri.serbakerja.R.id.view_splash;

/**
 * Created by janisharali on 27/01/17.
 */

public class RegisterPresenter<V extends RegisterView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    private FirebaseAuth mAuth;

    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    @Inject
    public RegisterPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    private void decideNextActivity(View splash, View login) {
        if (getDataManager().getCurrentUserLoggedInMode()
                == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            splash.setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);
        } else if (getDataManager().getCurrentUserLoggedInMode()
                == DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER.getType()){
            getSerbakerjaView().openMainActivity();
        }
    }

    @Override
    public void StartOpen(Activity activity, View splash, View login) {
        new Handler().postDelayed(() -> {
            final Animation animScale = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
            splash.startAnimation(animScale);

            animScale.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (!isViewAttached()) {
                        return;
                    }
                    decideNextActivity(splash, login);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }, 800);

    }

    @Override
    public void initPhoneAuth(Activity activity) {
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                mVerificationInProgress = false;
                Log.e("auth", "onVerificationCompleted");
                signInWithPhoneAuthCredential(activity, credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                mVerificationInProgress = false;
                getSerbakerjaView().hideLoading();
                Log.e("auth", "onVerificationFailed");
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    getSerbakerjaView().showMessageDialog(R.string.message_invalid_phone_number);
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    getSerbakerjaView().showMessageDialog(R.string.message_quota_exceeded);
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                getSerbakerjaView().hideLoading();
                Log.e("auth", "onCodeSent");
                getSerbakerjaView().onCodeSent(verificationId, token);
            }
        };
    }

    @Override
    public void startPhoneNumberVerification(Activity activity, String phoneNumber) {
        Log.e("startPhoneNumberVerifi", phoneNumber);
        getSerbakerjaView().showLoading();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 120, TimeUnit.SECONDS, activity, mCallbacks);
        mVerificationInProgress = true;
    }

    private void signInWithPhoneAuthCredential(Activity activity, PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        getSerbakerjaView().signinSuccess(task.getResult().getUser());
                        getSerbakerjaView().hideLoading();
                    } else {
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            getSerbakerjaView().showMessageDialog(R.string.message_invalid_code);
                        }
                    }
                });
    }

    @Override
    public void onRegister(String nama, String no_hp, String email, String password) {
        getSerbakerjaView().showLoading();

        UserRequest.Register register = new UserRequest.Register(nama, no_hp, email, password);
        Logger.printLogRequest(Path.REGISTER, register, null);

        getCompositeDisposable().add(getDataManager()
                .register(register)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.REGISTER);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    UserResponse.Register response = gson.fromJson(jsonObject.toString(), UserResponse.Register.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getDataManager().updateUserInfo("", response.getData().getId(), response.getData().getNama(),
                                response.getData().getNo_hp(), response.getData().getEmail(), password, "");
                        getSerbakerjaView().registerResponse(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().hideLoading();
                    }

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void updateStatusVerification(String phone_number) {
        getSerbakerjaView().showLoading();

        UserRequest.Verification verification = new UserRequest.Verification(getDataManager().getCurrentUserId(), phone_number);
        Logger.printLogRequest(Path.VERIFIKASI, verification, null);

        getCompositeDisposable().add(getDataManager()
                .verifikasi(verification)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.VERIFIKASI);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    UserResponse.Register response = gson.fromJson(jsonObject.toString(), UserResponse.Register.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultUpdateStatus();
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().hideLoading();
                    }

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
