package com.joefakri.serbakerja.ui.verify;

import android.app.Activity;

import com.google.firebase.auth.PhoneAuthProvider;
import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.register.RegisterView;

/**
 * Created by deny on bandung.
 */

public interface VerifyMvpPresenter<V extends VerifyView> extends SerbakerjaPresenter<V> {

    void initPhoneAuth(Activity activity);

    void startPhoneNumberVerification(Activity activity, String phoneNumber);

    void verifyPhoneNumberWithCode(Activity activity, String verificationId, String code);

    void resendVerificationCode(Activity activity, String phoneNumber, PhoneAuthProvider.ForceResendingToken token);

    void countdown();

    void cancelCountdown();

    void updateStatusVerification(String phone_number);
}
