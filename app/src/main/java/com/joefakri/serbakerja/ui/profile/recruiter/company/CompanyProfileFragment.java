package com.joefakri.serbakerja.ui.profile.recruiter.company;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.data.model.ProfileModel;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.profile.photo.DetailPhotoActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileActivity;
import com.joefakri.serbakerja.ui.profile.password.EditPasswordActivity;
import com.joefakri.serbakerja.utils.Constants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by deny on bandung.
 */

public class CompanyProfileFragment extends BaseFragment implements CompanyProfileView{

    @Inject CompanyProfileMvpPresenter<CompanyProfileView> mPresenter;

    @BindView(R.id.btn_edit) LinearLayout btn_edit;
    @BindView(R.id.img_company) CircleImageView img_company;
    @BindView(R.id.txt_company_name_header) TextView txt_company_name_header;
    @BindView(R.id.txt_company_name) TextView txt_company_name;
    @BindView(R.id.txt_company_address) TextView txt_company_address;
    @BindView(R.id.txt_company_city) TextView txt_company_city;
    @BindView(R.id.txt_company_area) TextView txt_company_area;
    @BindView(R.id.txt_company_total_employer) TextView txt_company_total_employer;
    @BindView(R.id.txt_company_website) TextView txt_company_website;

    RecruiterResponse.CompanyProfile.Data companyProfile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_company_profile, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        btn_edit.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), EditCompanyProfileActivity.class);
            intent.putExtra("model", companyProfile);
            startActivity(intent);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewProfile();
        btn_edit.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultProfileView(RecruiterResponse.CompanyProfile.Data companyProfile) {
        btn_edit.setEnabled(true);
        this.companyProfile = companyProfile;
        Glide.with(this).load(companyProfile.getLogo()).into(img_company);
        txt_company_name_header.setText(companyProfile.getNama_perusahaan());
        txt_company_name.setText(companyProfile.getNama_perusahaan());
        txt_company_address.setText(companyProfile.getAlamat_perusahaan());
        txt_company_city.setText(companyProfile.getNama_kota());
        txt_company_area.setText(companyProfile.getDaerah());
        txt_company_total_employer.setText(Constants.getTotalEmployee(getActivity(), companyProfile.getJumlah_karyawan()));
        txt_company_website.setText(companyProfile.getWebsite());

        img_company.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), DetailPhotoActivity.class);
            intent.putExtra("image", companyProfile.getLogo());
            startActivity(intent);
        });

        hideLoading();
    }
}
