/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form.work;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.RecruiterRequest;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.CityRealm;
import com.joefakri.serbakerja.data.realm.ExperienceRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.data.realm.WorkRealm;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by janisharali on 27/01/17.
 */

public class FormWorkPresenter<V extends FormWorkView> extends BasePresenter<V>
        implements FormWorkMvpPresenter<V> {

    private CustomTabsClient mCustomTabsClient;
    private CustomTabsSession mCustomTabsSession;
    private CustomTabsServiceConnection mCustomTabsServiceConnection;
    private CustomTabsIntent mCustomTabsIntent;
    private Realm realm;

    @Inject
    public FormWorkPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void contactusOption(Activity activity, View v) {
        PopupMenu popupMenu = new PopupMenu(activity, v);
        popupMenu.getMenuInflater().inflate(R.menu.option_contactus, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.option_call) {
                boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CALL_PHONE);
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+6281908283331"));
                if (hasPermission) activity.startActivity(callIntent);
            } else {
                mCustomTabsIntent.launchUrl(activity, Uri.parse(Constants.API_WHATSAPP));
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    public void readPhone(Activity activity, Fragment fragment, View v) {
        boolean hasPermission = EasyPermissions.hasPermissions(activity, Manifest.permission.CALL_PHONE);
        if (hasPermission) {
            contactusOption(activity, v);
        } else {
            EasyPermissions.requestPermissions(fragment, activity.getString(R.string.message_permission_callphone), 0,
                    Manifest.permission.CALL_PHONE);
        }
    }

    @Override
    public void initCustomTab(Activity activity) {
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                mCustomTabsClient= customTabsClient;
                mCustomTabsClient.warmup(0L);
                mCustomTabsSession = mCustomTabsClient.newSession(null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mCustomTabsClient= null;
            }
        };

        CustomTabsClient.bindCustomTabsService(activity, "com.android.chrome", mCustomTabsServiceConnection);

        mCustomTabsIntent = new CustomTabsIntent.Builder(mCustomTabsSession)
                .setToolbarColor(Color.parseColor("#2a2a2a"))
                .setSecondaryToolbarColor(Color.parseColor("#628D97"))
                .setShowTitle(true)
                .build();
    }

    @Override
    public void onViewJob() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getJenisPekerjaan()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jobResponse -> {
                    LogResponse(jobResponse, Path.GET_JOB_TYPE);
                    Gson gson = new Gson();
                    JobResponse response = gson.fromJson(jobResponse.toString(), JobResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().job(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onViewCity() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCity()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(cityResponse -> {

                    LogResponse(cityResponse, Path.GET_CITY);
                    Gson gson = new Gson();
                    AreaResponse.CityResponse response = gson.fromJson(cityResponse.toString(), AreaResponse.CityResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().city(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostRecruterWork(Activity activity) {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();

        String sallary = Constants.sallary(activity, recruiter.getWorkSallary());
        String min_sallary = Constants.min_sallary(activity, sallary);
        String max_sallary = Constants.max_sallary(activity, sallary);
        RecruiterRequest.PostWork postWork = new RecruiterRequest.PostWork(getDataManager().getCurrentUserId(), recruiter.getWorkTypeId(),
                min_sallary, max_sallary, recruiter.getWorkExpiredDate(), recruiter.getWorkAddress(), recruiter.getWorkArea(), recruiter.getWorkCityId(),
                recruiter.getWorkTime(), recruiter.getWorkTimeOption(), recruiter.getWorkDay(), recruiter.getWorkGender(), recruiter.getWorkMinimalExperience(),
                recruiter.getWorkEducation(), recruiter.getWorkRequirement(), recruiter.getWorkScope());

        Logger.printLogRequest(Path.POST_RECRUITER_WORK, postWork, null);
        getCompositeDisposable().add(getDataManager()
                .postRWork(postWork)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_RECRUITER_WORK);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    RecruiterResponse.WorkResponse response = gson.fromJson(jsonObject.toString(), RecruiterResponse.WorkResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultRecruterWork(response.getData());
                        recruiter.setPercent2(13);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostCandidateWork(Activity activity) {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();

        ArrayList<WorkModel> workModels = new ArrayList<>();
        if (candidateRealm != null) {
            for (int i = 0; i < candidateRealm.getWorkRealms().size(); i++) {
                WorkModel workModel = new WorkModel();
                workModel.setWork(candidateRealm.getWorkRealms().get(i).getWorkType());
                workModel.setWorkId(candidateRealm.getWorkRealms().get(i).getWorkTypeId());
                workModel.setWork_day(candidateRealm.getWorkRealms().get(i).getWorkDay());
                workModel.setArea(candidateRealm.getWorkRealms().get(i).getWorkArea());
                workModel.setWork_time(candidateRealm.getWorkRealms().get(i).getWorkTime());
                workModel.setSalary(candidateRealm.getWorkRealms().get(i).getWorkSallary());
                workModel.setSalarymax(Constants.max_sallary(activity, candidateRealm.getWorkRealms().get(i).getWorkSallary()));
                workModel.setSalarymin(Constants.min_sallary(activity, candidateRealm.getWorkRealms().get(i).getWorkSallary()));
                workModel.setWork_time_position(Constants.waktuKerjaPosition(activity, candidateRealm.getWorkRealms().get(i).getWorkTime()));
                workModel.setWork_time_other(candidateRealm.getWorkRealms().get(i).getWorkTimeOther());
                workModel.setCity_id(candidateRealm.getWorkRealms().get(i).getWorkCityId());
                for (int j = 0; j < candidateRealm.getWorkRealms().get(i).getCityRealms().size(); j++) {
                    WorkModel.City city = new WorkModel().new City();
                    city.setId(candidateRealm.getWorkRealms().get(i).getCityRealms().get(j).getId());
                    city.setName(candidateRealm.getWorkRealms().get(i).getCityRealms().get(j).getName());
                    workModel.getCitys().add(city);
                }
                workModels.add(workModel);
            }
        } else getSerbakerjaView().onError("Anda Belum Memasukkan Data");


        CandidateRequest.PostWork postWork = new CandidateRequest.PostWork(workModels);
        CandidateRequest.PostDocument user = new CandidateRequest.PostDocument(getDataManager().getCurrentUserId());

        Logger.printLogRequest(Path.POST_CANDIDATE_WORK, user, null);
        Logger.printLogRequest(postWork.getMultipleMap());
        getCompositeDisposable().add(getDataManager()
                .postCWork(user, postWork)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_CANDIDATE_WORK);
                    Gson gson = new Gson();
                    CandidateResponse.WorkResponse response = gson.fromJson(jsonObject.toString(), CandidateResponse.WorkResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCandidateWork();
                        candidateRealm.setPercent2(1);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onPostCandidateExperience(Activity activity) {
        getSerbakerjaView().showLoading();

        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidateRealm = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();

        String sallary = Constants.sallary(activity, candidateRealm.getWorkLastSallary());
        String min_sallary = Constants.min_sallary(activity, sallary);
        String max_sallary = Constants.max_sallary(activity, sallary);
        ArrayList<WorkModel> workModels = new ArrayList<>();
        for (int i = 0; i < candidateRealm.getExperienceRealms().size(); i++) {
            WorkModel workModel = new WorkModel();
            workModel.setCompany_name(candidateRealm.getExperienceRealms().get(i).getCompanyName());
            workModel.setWork_as(candidateRealm.getExperienceRealms().get(i).getWorkAs());
            workModel.setWork_long(candidateRealm.getExperienceRealms().get(i).getWorkLongTime());
            workModel.setPeriod(candidateRealm.getExperienceRealms().get(i).getWorkPeriod());
            workModels.add(workModel);
        }

        CandidateRequest.PostExperience experience = new CandidateRequest.PostExperience(getDataManager().getCurrentUserId(),
                candidateRealm.getWorkStatus(), candidateRealm.getWorkStatusOptional(), min_sallary, max_sallary);
        CandidateRequest.Experience experienceMultiple = new CandidateRequest.Experience(workModels);

        Logger.printLogRequest(Path.POST_CANDIDATE_EXPERIENCE, experience, null);
        Logger.printLogRequest(experienceMultiple.getMultipleMap());
        getCompositeDisposable().add(getDataManager()
                .postCExperience(experience, experienceMultiple)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_CANDIDATE_EXPERIENCE);
                    Gson gson = new Gson();
                    CandidateResponse.ExperienceResponse response = gson.fromJson(jsonObject.toString(), CandidateResponse.ExperienceResponse.class);

                    realm.beginTransaction();
                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultCandidateExperience(response.getData());
                        candidateRealm.setPercent3(3);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    realm.commitTransaction();
                    realm.close();
                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void init() {
        realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        if (recruiter != null && candidate != null) getSerbakerjaView().init(recruiter, candidate);
    }

    @Override
    public void saveRecruterWorkTypeSallary(String idType, String type, String sallary) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkTypeId(idType);
            recruiter.setWorkType(type);
            recruiter.setWorkSallary(sallary);
            recruiter.setPercent2(2);
        } else {
            recruiter = realm.createObject(RecruiterRealm.class);
            recruiter.setId(1);
            recruiter.setWorkTypeId(idType);
            recruiter.setWorkType(type);
            recruiter.setWorkSallary(sallary);
            recruiter.setPercent2(2);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkAreaAddress(String idCity, String city, String area, String address) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkArea(area);
            recruiter.setWorkCityId(idCity);
            recruiter.setWorkCity(city);
            recruiter.setWorkAddress(address);
            recruiter.setPercent2(5);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkDayTime(String day, String time, String timeOption) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkDay(day);
            recruiter.setWorkTime(time);
            recruiter.setWorkTimeOption(timeOption);
            recruiter.setPercent2(7);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkGender(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkGender(data);
            recruiter.setPercent2(8);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkMinimalExperience(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkMinimalExperience(data);
            recruiter.setPercent2(9);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkEducation(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkEducation(data);
            recruiter.setPercent2(10);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkRequirement(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkRequirement(data);
            recruiter.setPercent2(11);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkScope(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkScope(data);
            recruiter.setPercent2(12);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveRecruterWorkExpiredDate(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (recruiter != null){
            recruiter.setWorkExpiredDate(data);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidateWork(WorkModel workModel) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            WorkRealm work = new WorkRealm();
            work.setWorkType(workModel.getWork());
            work.setWorkTypeId(workModel.getWorkId());
            work.setWorkDay(workModel.getWork_day());
            work.setWorkArea(workModel.getArea());
            work.setWorkTime(workModel.getWork_time());
            work.setWorkTimeOther(workModel.getWork_time_other());
            work.setWorkSallary(workModel.getSalary());
            work.setWorkCityId(workModel.getCity_id());
            for (int i = 0; i < workModel.getCitys().size(); i++) {
                CityRealm city = new CityRealm();
                city.setId(workModel.getCitys().get(i).getId());
                city.setName(workModel.getCitys().get(i).getName());
                work.getCityRealms().add(city);
            }
            candidate.getWorkRealms().add(work);
            candidate.setPercent2(0);
        } else {
            candidate = realm.createObject(CandidateRealm.class);
            candidate.setId(1);
            WorkRealm work = new WorkRealm();
            work.setWorkType(workModel.getWork());
            work.setWorkTypeId(workModel.getWorkId());
            work.setWorkDay(workModel.getWork_day());
            work.setWorkArea(workModel.getArea());
            work.setWorkTime(workModel.getWork_time());
            work.setWorkTimeOther(workModel.getWork_time_other());
            work.setWorkSallary(workModel.getSalary());
            work.setWorkCityId(workModel.getCity_id());
            for (int i = 0; i < workModel.getCitys().size(); i++) {
                CityRealm city = new CityRealm();
                city.setId(workModel.getCitys().get(i).getId());
                city.setName(workModel.getCitys().get(i).getName());
                work.getCityRealms().add(city);
            }
            candidate.getWorkRealms().add(work);
            candidate.setPercent2(0);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void editCandidateWork(WorkModel workModel, int position) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();

        WorkRealm work = candidate.getWorkRealms().get(position);
        work.setWorkType(workModel.getWork());
        work.setWorkTypeId(workModel.getWorkId());
        work.setWorkDay(workModel.getWork_day());
        work.setWorkArea(workModel.getArea());
        work.setWorkTime(workModel.getWork_time());
        work.setWorkTimeOther(workModel.getWork_time_other());
        work.setWorkSallary(workModel.getSalary());
        work.setWorkCityId(workModel.getCity_id());
        for (int i = 0; i < workModel.getCitys().size(); i++) {
            CityRealm city = new CityRealm();
            city.setId(workModel.getCitys().get(i).getId());
            city.setName(workModel.getCitys().get(i).getName());
            work.getCityRealms().add(city);
        }
        candidate.setPercent2(0);

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidateExperienceStatus(String data, String optional) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setWorkStatus(data);
            candidate.setWorkStatusOptional(optional);
            candidate.setPercent3(1);
        } else {
            candidate = realm.createObject(CandidateRealm.class);
            candidate.setId(1);
            candidate.setWorkStatus(data);
            candidate.setWorkStatusOptional(optional);
            candidate.setPercent3(1);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidateExperienceSallary(String data) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            candidate.setWorkLastSallary(data);
            candidate.setPercent3(2);
        }
        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void saveCandidateExperienceWork(WorkModel workModel) {
        if (realm.isClosed()) realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).equalTo("id", 1).findFirst();
        realm.beginTransaction();
        if (candidate != null){
            ExperienceRealm work = new ExperienceRealm();
            work.setCompanyName(workModel.getCompany_name());
            work.setWorkAs(workModel.getWork_as());
            work.setWorkLongTime(workModel.getWork_long());
            work.setWorkPeriod(workModel.getPeriod());
            candidate.getExperienceRealms().add(work);
        } else {
            candidate = realm.createObject(CandidateRealm.class);
            candidate.setId(1);
            ExperienceRealm work = new ExperienceRealm();
            work.setCompanyName(workModel.getCompany_name());
            work.setWorkAs(workModel.getWork_as());
            work.setWorkLongTime(workModel.getWork_long());
            work.setWorkPeriod(workModel.getPeriod());
            candidate.getExperienceRealms().add(work);
        }
        realm.commitTransaction();
        realm.close();
    }


}
