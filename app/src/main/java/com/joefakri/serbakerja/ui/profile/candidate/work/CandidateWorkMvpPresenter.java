package com.joefakri.serbakerja.ui.profile.candidate.work;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost.CompanyWorkPostView;

/**
 * Created by deny on bandung.
 */

public interface CandidateWorkMvpPresenter<V extends CandidateWorkView> extends SerbakerjaPresenter<V> {

    void onViewWork();

}
