package com.joefakri.serbakerja.ui.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.utils.Constants;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class WorkExperienceDialog extends DialogFragment {

    private OnListener onListener;

    public void setOnListener(OnListener onListener) {
        this.onListener = onListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_work_experience, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);

        EditText txt_work_as = v.findViewById(R.id.txt_work_as);
        EditText txt_work_long = v.findViewById(R.id.txt_work_long);
        EditText txt_work_company_name = v.findViewById(R.id.txt_work_company_name);
        RadioGroup rd_time_dialog = v.findViewById(R.id.rd_time_dialog);

        if (getArguments().getBoolean("edit")){
            WorkModel experienceWorkModel = (WorkModel) getArguments().getSerializable("model");
            txt_work_company_name.setText(experienceWorkModel.getCompany_name());
            txt_work_as.setText(experienceWorkModel.getWork_as());
            txt_work_long.setText(experienceWorkModel.getWork_long());
            Log.e("periode", experienceWorkModel.getPeriod());
            if (experienceWorkModel.getPeriod() != null)
                ((RadioButton)rd_time_dialog.getChildAt(Integer.parseInt(experienceWorkModel.getPeriod()))).setChecked(true);
        }


        Button btn_save = v.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(v1 -> {
            if (onListener != null) {
                if (rd_time_dialog.getCheckedRadioButtonId() == -1){
                    Toast.makeText(getContext(), "Pilih salah satu waktu kerja yang tersedia", Toast.LENGTH_SHORT).show();
                    return;
                }

                int selectedId = rd_time_dialog.getCheckedRadioButtonId();
                RadioButton rdio = v.findViewById(selectedId);
                Log.e("period WorkExperDialog", rdio.getText().toString());
                onListener.onSave(txt_work_company_name.getText().toString(),
                        txt_work_as.getText().toString(), txt_work_long.getText().toString(),
                        Constants.getPeriodCode(rdio.getText().toString()));
            }
            dismiss();
        });

    }

    public interface OnListener {
        void onSave(String txt_work_company_name,
                    String txt_work_as, String txt_work_long, String perio);
    }
}