package com.joefakri.serbakerja.ui.status.candidate;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.register.RegisterView;

/**
 * Created by deny on bandung.
 */

public interface StatusCandidateMvpPresenter<V extends StatusCandidateView> extends SerbakerjaPresenter<V> {

    void onViewStatus(String offset);

}
