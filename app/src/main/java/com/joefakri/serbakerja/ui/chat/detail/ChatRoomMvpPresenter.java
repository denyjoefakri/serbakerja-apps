package com.joefakri.serbakerja.ui.chat.detail;

import android.app.Activity;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

/**
 * Created by deny on bandung.
 */

public interface ChatRoomMvpPresenter<V extends ChatRoomView> extends SerbakerjaPresenter<V> {

    void onViewPrepared(String targetId);

    void onRead(String targetId);

    void onSend(String targetId, String idMessage, String message);

    void permissionCall(Activity activity, String phone);

}
