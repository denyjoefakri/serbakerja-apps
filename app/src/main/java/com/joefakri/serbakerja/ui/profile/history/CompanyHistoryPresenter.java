/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.history;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.HistoryResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class CompanyHistoryPresenter<V extends CompanyHistoryView> extends BasePresenter<V>
        implements CompanyHistoryMvpPresenter<V> {

    @Inject
    public CompanyHistoryPresenter(DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onGetHistory() {
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());
        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.HISTORY, id, null);

        getCompositeDisposable().add(getDataManager()
                .history(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.HISTORY);
                    Gson gson = new Gson();
                    HistoryResponse response = gson.fromJson(jsonObject.toString(), HistoryResponse.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultHistory(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
