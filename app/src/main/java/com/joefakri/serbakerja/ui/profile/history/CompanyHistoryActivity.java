package com.joefakri.serbakerja.ui.profile.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.HistoryAdapter;
import com.joefakri.serbakerja.connection.response.HistoryResponse;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class CompanyHistoryActivity extends BaseActivity implements CompanyHistoryView{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.rv_history) RecyclerView rv_history;
    HistoryAdapter historyAdapter;

    @Inject CompanyHistoryMvpPresenter<CompanyHistoryView> mPresenter;
    @Inject LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_history, this);
        getActivityComponent().inject(this);

        mPresenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_history));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_history.setLayoutManager(mLayoutManager);
        rv_history.setHasFixedSize(true);
        rv_history.setNestedScrollingEnabled(false);
        rv_history.setAdapter(historyAdapter = new HistoryAdapter(this));

        mPresenter.onGetHistory();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResultHistory(ArrayList<HistoryResponse.Data> response) {
        historyAdapter.update(response);
    }
}
