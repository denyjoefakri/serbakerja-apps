package com.joefakri.serbakerja.ui.profile.recruiter.profile;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

/**
 * Created by deny on bandung.
 */

public interface ProfileMvpPresenter<V extends ProfileView> extends SerbakerjaPresenter<V> {

    void onViewProfile();
}
