package com.joefakri.serbakerja.ui.notification;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.main.MainView;

/**
 * Created by deny on bandung.
 */

public interface NotificationMvpPresenter<V extends NotificationView> extends SerbakerjaPresenter<V> {

    void onViewPrepared();

    void onRead(String id, String type);
}
