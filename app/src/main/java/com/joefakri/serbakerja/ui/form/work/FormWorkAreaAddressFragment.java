package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.data.realm.StateRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by deny on bandung.
 */

public class FormWorkAreaAddressFragment extends BaseFragment implements FormWorkView{

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.txt_city) AutoCompleteTextView txt_city;
    @BindView(R.id.txt_area) EditText txt_area;
    @BindView(R.id.txt_address_work) EditText txt_address_work;
    @BindView(R.id.btn_next) Button btn_next;

    ArrayList<String> citys;
    ArrayList<String> ids;
    private String id = "";
    private String city = "";

    private EventBus bus = EventBus.getDefault();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_area_address, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        mPresenter.onViewCity();
        btn_next.setOnClickListener(view -> {
            if (txt_city.getText().toString().isEmpty() || !txt_city.getText().toString().equals(city)) {
                onError("Pilih Kota / Kabupaten yang tersedia");
                return;
            }

            if (txt_area.getText().toString().isEmpty()) {
                onError("Daerah tidak boleh kosong");
                return;
            }

            if (txt_address_work.getText().toString().isEmpty()) {
                onError("Alamat lengkap tidak boleh kosong");
                return;
            }

            mPresenter.saveRecruterWorkAreaAddress(id, txt_city.getText().toString(),
                    txt_area.getText().toString(), txt_address_work.getText().toString());
            txt_city.dismissDropDown();
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(2);
        });
    }


    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {
        citys = new ArrayList<>();
        ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            citys.add(dataList.get(i).getName());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, citys);
        txt_city.setAdapter(adapter);
        txt_city.setOnItemClickListener((adapterView, view, i, l) -> {
            city = adapter.getItem(i).toString();
            id = ids.get(citys.indexOf(city));
            Log.e("job", ids.get(citys.indexOf(city)) + " | " + city);
        });
    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }

    @Override
    public void onDestroyView() {
        if (citys != null) citys.clear();
        if (ids != null) ids.clear();
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm != null) {
            id = recruiterRealm.getWorkCityId();
            city = recruiterRealm.getWorkCity();
            txt_area.setText(recruiterRealm.getWorkArea());
            txt_address_work.setText(recruiterRealm.getWorkAddress());
        }
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getState().equals("1")){
            txt_city.postDelayed(() -> txt_city.showDropDown(),500);
            txt_city.setText(city);
            txt_city.setSelection(txt_city.getText().length());
        }
    }
}
