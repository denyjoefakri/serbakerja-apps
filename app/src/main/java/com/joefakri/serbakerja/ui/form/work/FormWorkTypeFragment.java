package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class FormWorkTypeFragment extends BaseFragment implements FormWorkView, EasyPermissions.PermissionCallbacks{

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.txt_work_type) AutoCompleteTextView txt_work_type;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.btn_contact_us) Button btn_contact_us;

    private EventBus bus = EventBus.getDefault();
    ArrayList<String> jobs;
    ArrayList<String> ids;
    private String id = "";
    private String job = "";

    StateEvent stateEvent;
    FormEvent recruiterEvent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_type, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.onViewJob();
        mPresenter.initCustomTab(getActivity());

        btn_next.setOnClickListener(view -> {
            if (txt_work_type.getText().toString().isEmpty() || !txt_work_type.getText().toString().equals(job)) {
                onError("Pilih jenis pekerjaan yang tersedia");
                return;
            }

            setEvent(1);
            ((FormOptionalActivity)getActivity()).view_pager_tab.setCurrentItem(1);
        });

        btn_contact_us.setOnClickListener(view -> {
            mPresenter.readPhone(getActivity(), this, view);
        });
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {

    }

    @Override
    public void job(List<JobResponse.Data> dataList) {
        jobs = new ArrayList<>();
        ids = new ArrayList<>();

        for (int i = 0; i < dataList.size(); i++) {
            jobs.add(dataList.get(i).getNama());
            ids.add(dataList.get(i).getId());
        }

        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, jobs);
        txt_work_type.setAdapter(adapter);
        txt_work_type.setOnItemClickListener((adapterView, view, i, l) -> {
            job = adapter.getItem(i).toString();
            id = ids.get(jobs.indexOf(job));
            Log.e("job", ids.get(jobs.indexOf(job)) + " | " + job);
        });
    }

    private void setEvent(int value){
        stateEvent.setValue(value);
        stateEvent.setTotal(6);
        stateEvent.setType(22);
        recruiterEvent.setWork(txt_work_type.getText().toString());
        recruiterEvent.setWork_id(id);
        bus.postSticky(stateEvent);
        bus.postSticky(recruiterEvent);
    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }

    @Override
    public void onResume() {
        super.onResume();
        txt_work_type.requestFocus();
    }


    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.stateEvent = state;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onRecruiterEvent(FormEvent state){
        this.recruiterEvent = state;
        txt_work_type.setText(recruiterEvent.getWork());
        id = recruiterEvent.getWork_id();
    }

    @Override
    public void onDestroyView() {
        if (ids != null) ids.clear();
        if (jobs != null) jobs.clear();
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        mPresenter.contactusOption(getActivity(), btn_contact_us);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                mPresenter.readPhone(getActivity(), this, btn_contact_us);
            });
        }
    }


}
