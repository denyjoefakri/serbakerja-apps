package com.joefakri.serbakerja.ui.profile.candidate.profile;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

/**
 * Created by deny on bandung.
 */

public interface CandidateProfileMvpPresenter<V extends CandidateProfileView> extends SerbakerjaPresenter<V> {

    void onViewProfile();

}
