package com.joefakri.serbakerja.ui.setting.contactus;

import android.app.Activity;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.setting.SettingView;

/**
 * Created by deny on bandung.
 */

public interface ContactUsMvpPresenter<V extends ContactUsView> extends SerbakerjaPresenter<V> {

    void onInitCustomTab(Activity activity);

    void onCall(Activity activity);

    void onWhatsapp(Activity activity);

    void onEmail(Activity activity);

}
