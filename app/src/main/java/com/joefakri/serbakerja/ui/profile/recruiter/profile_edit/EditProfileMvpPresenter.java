package com.joefakri.serbakerja.ui.profile.recruiter.profile_edit;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

import java.io.File;

/**
 * Created by deny on bandung.
 */

public interface EditProfileMvpPresenter<V extends EditProfileView> extends SerbakerjaPresenter<V> {

    void readCameraGallery(Activity activity, View v, int requestCode);

    void openImageChooser(Activity activity, View v, int requestCode);

    void onUpdate(String photo, String ktp, String... strings);

}
