package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkTimeFragment extends BaseFragment implements FormWorkView{

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.rd_work_time) RadioGroup rd_work_time;
    RadioButton rd;
    @BindView(R.id.rd_work_time_1) RadioButton rd_1;
    @BindView(R.id.rd_work_time_2) RadioButton rd_2;
    @BindView(R.id.rd_work_time_3) RadioButton rd_3;

    private EventBus bus = EventBus.getDefault();
    StateEvent stateEvent;
    FormEvent recruiterEvent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_time, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        btn_next.setOnClickListener(view -> {
            if (rd_work_time.getCheckedRadioButtonId() == -1){
                onError("Pilih salah satu waktu bekerja yang tersedia");
                return;
            }

            setEvent(5);
            ((FormOptionalActivity)getActivity()).view_pager_tab.setCurrentItem(5);
        });
    }

    private String get(){
        int selectedId= rd_work_time.getCheckedRadioButtonId();
        rd = getActivity().findViewById(selectedId);
        return rd.getText().toString();
    }

    private void setEvent(int value){
        stateEvent.setValue(value);
        stateEvent.setTotal(6);
        stateEvent.setType(22);
        recruiterEvent.setTime(get());
        bus.postSticky(stateEvent);
        bus.postSticky(recruiterEvent);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.stateEvent = state;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onRecruiterEvent(FormEvent state){
        this.recruiterEvent = state;
        if (rd_1.getText().toString().equals(recruiterEvent.getTime()))
            rd_1.setChecked(true);
        else if (rd_2.getText().toString().equals(recruiterEvent.getTime()))
            rd_2.setChecked(true);
        else if (rd_3.getText().toString().equals(recruiterEvent.getTime()))
            rd_3.setChecked(true);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {

    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }
}
