package com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.recruiter.company_edit.EditCompanyProfileView;

/**
 * Created by deny on bandung.
 */

public interface EditWorkPostMvpPresenter<V extends EditWorkPostView> extends SerbakerjaPresenter<V> {

    void onPostEditWork(String... s);

    void onViewCity();

    void onPostRecruterWork(String... s);

}
