/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.main.dashboard;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class DashboardPresenter<V extends DashboardView> extends BasePresenter<V>
        implements DashboardMvpPresenter<V> {

    @Inject
    public DashboardPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onDefaultJob() {
        getSerbakerjaView().defaultJob(getDataManager().getDefaultJob());
    }

    @Override
    public void onDefaultCity() {
        getSerbakerjaView().defaultCity(getDataManager().getDefaultCity());
    }

    @Override
    public void onViewList(String offset) {
        DashboardRequest.Recruter recruter = new DashboardRequest.Recruter(getDataManager().getDefaultJob(), getDataManager().getDefaultCity());
        DashboardRequest.Candidate candidate = new DashboardRequest.Candidate(getDataManager().getDefaultJob(), getDataManager().getDefaultCity());

        if (getDataManager().getTypeUser().equals("perekrut")){
            requestRecruter(recruter, null, offset,"Terdekat");
        } else {
            requestCandidate(candidate, null, offset,"Terdekat");
        }
    }

    @Override
    public void onViewListCityOrJob(String city, String job, String offset) {
        ArrayList<UserResponse.Login.Data.Jenis_pekerjaan> jenis_pekerjaen;
        //jenis_pekerjaen = new ArrayList<>();
        if (!job.isEmpty() || job.equals("0")) {
            jenis_pekerjaen = new ArrayList<>();
        } else {
            jenis_pekerjaen = getDataManager().getDefaultJob();
        }
        UserResponse.Login.Data.Jenis_pekerjaan pekerjaan = new UserResponse.Login.Data.Jenis_pekerjaan();
        pekerjaan.setId(job);
        jenis_pekerjaen.add(pekerjaan);

        ArrayList<UserResponse.Login.Data.Kota> kotas;
        //kotas = new ArrayList<>();
        if (!city.isEmpty() || city.equals("0")){
            kotas = new ArrayList<>();
        } else {
            kotas = getDataManager().getDefaultCity();
        }
        UserResponse.Login.Data.Kota kota = new UserResponse.Login.Data.Kota();
        kota.setId(city);
        kotas.add(kota);

        DashboardRequest.Recruter recruter = new DashboardRequest.Recruter(jenis_pekerjaen, kotas);
        DashboardRequest.Candidate candidate = new DashboardRequest.Candidate(jenis_pekerjaen, kotas);

        if (getDataManager().getTypeUser().equals(Constants.ROLE_PEREKRUT)){
            requestRecruter(recruter, null, offset, "");
        } else {
            requestCandidate(candidate, null, offset,"");
        }
    }

    @Override
    public void onViewSearch(String keyword, String offset) {

        if (getDataManager().getTypeUser().equals(Constants.ROLE_PEREKRUT)){
            DashboardRequest.RecruterSearch search = new DashboardRequest.RecruterSearch(keyword);
            getSerbakerjaView().showLoading();
            Logger.printLogRequest(Path.DASHBOARD_RECRUTER_SEARCH, search, null);

            getCompositeDisposable().add(getDataManager()
                    .dashboardPerekrutSearch(search, offset)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(jsonObject -> {

                        LogResponse(jsonObject, Path.DASHBOARD_RECRUTER_SEARCH);
                        Gson gson = new Gson();
                        DashboardResponse.Recruter response = gson.fromJson(jsonObject.toString(), DashboardResponse.Recruter.class);

                        getSerbakerjaView().listPerekrut(response.getData(), "");
                        getSerbakerjaView().hideLoading();

                    }, throwable -> {
                        if (!isViewAttached()) {
                            return;
                        }

                        getSerbakerjaView().onErrorList();
                        getSerbakerjaView().hideLoading();

                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }));
        } else {
            UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());
            DashboardRequest.CandidateSearch search = new DashboardRequest.CandidateSearch(keyword);
            getSerbakerjaView().showLoading();
            Logger.printLogRequest(Path.DASHBOARD_CANDIDATE_SEARCH, search, null);

            getCompositeDisposable().add(getDataManager()
                    .dashboardCandidateSearch(id, search, offset)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(jsonObject -> {
                        LogResponse(jsonObject, Path.DASHBOARD_CANDIDATE_SEARCH);
                        Gson gson = new Gson();
                        DashboardResponse.Candidate response = gson.fromJson(jsonObject.toString(), DashboardResponse.Candidate.class);

                        for (int i = 0; i < response.getData().size(); i++) {
                            if (!response.getData().get(i).getStatus().equals("active")){
                                response.getData().remove(i);
                            }
                        }

                        getSerbakerjaView().listCandidate(response.getData(), "");
                        getSerbakerjaView().hideLoading();

                    }, throwable -> {
                        if (!isViewAttached()) {
                            return;
                        }

                        getSerbakerjaView().onErrorList();
                        getSerbakerjaView().hideLoading();

                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }));
        }

    }

    @Override
    public void onApply(String id) {
        UserRequest.Id idd = new UserRequest.Id(id);
        UserRequest.Pengguna pengguna = new UserRequest.Pengguna(getDataManager().getCurrentUserId());
        Log.e("user", getDataManager().getCurrentUserId());

        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.DASHBOARD_CANDIDATE_APPLY, idd, null);

        getCompositeDisposable().add(getDataManager()
                .dashboardCandidateApply(idd, pengguna)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.DASHBOARD_CANDIDATE_APPLY);
                    Gson gson = new Gson();
                    DashboardResponse.Apply response = gson.fromJson(jsonObject.toString(), DashboardResponse.Apply.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultApply(id);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                        //getSerbakerjaView().onErrorList();
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    //getSerbakerjaView().onErrorList();
                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onCustomLocation(String lat, String lng, String offset) {
        DashboardRequest.CustomLocation location = new DashboardRequest.CustomLocation(lat, lng);
        if (getDataManager().getTypeUser().equals("perekrut")){
            requestRecruter(null, location,  offset, "Terdekat");
        } else {
            requestCandidate(null, location, offset,"Terdekat");
        }
    }

    private void requestRecruter(DashboardRequest.Recruter recruter, DashboardRequest.CustomLocation location, String offset, String message){
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.DASHBOARD_RECRUTER, null, null);

        if (recruter != null)
            Logger.printLogRequest(recruter.getMultipleMap());
        else if (location != null)
            Logger.printLogRequest(Path.DASHBOARD_RECRUTER, location, null);

        getCompositeDisposable().add(getDataManager()
                .dashboardPerekrut(recruter, location, offset)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {

                    LogResponse(jsonObject, Path.DASHBOARD_RECRUTER);
                    Gson gson = new Gson();
                    DashboardResponse.Recruter response = gson.fromJson(jsonObject.toString(), DashboardResponse.Recruter.class);

                    if (response != null)
                        getSerbakerjaView().listPerekrut(response.getData(), message);
                    else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().onErrorList();
                    }
                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().onErrorList();
                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private void requestCandidate(DashboardRequest.Candidate candidate, DashboardRequest.CustomLocation location, String offset, String message){
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.DASHBOARD_CANDIDATE, null, null);

        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());

        if (candidate != null)
            Logger.printLogRequest(candidate.getMultipleMap());
        else if (location != null)
            Logger.printLogRequest(Path.DASHBOARD_CANDIDATE, location, null);

        getCompositeDisposable().add(getDataManager()
                .dashboardCandidate(id, candidate, location, offset)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.DASHBOARD_CANDIDATE);
                    Gson gson = new Gson();
                    DashboardResponse.Candidate response = gson.fromJson(jsonObject.toString(), DashboardResponse.Candidate.class);

                    for (int i = 0; i < response.getData().size(); i++) {
                        if (!response.getData().get(i).getStatus().equals("active")){
                            response.getData().remove(i);
                        }
                    }

                    if (response != null)
                        getSerbakerjaView().listCandidate(response.getData(), message);
                    else {
                        getSerbakerjaView().onError(response.getMessage());
                        getSerbakerjaView().onErrorList();
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().onErrorList();
                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

}
