package com.joefakri.serbakerja.ui.city;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

/**
 * Created by deny on bandung.
 */

public interface DataCityMvpPresenter<V extends DataCityView> extends SerbakerjaPresenter<V> {

    void onViewCity();

}
