package com.joefakri.serbakerja.ui.form.work;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.form.education.FormEducationView;

import java.util.ArrayList;

/**
 * Created by deny on bandung.
 */

public interface FormWorkMvpPresenter<V extends FormWorkView> extends SerbakerjaPresenter<V> {

    void contactusOption(Activity activity, View v);

    void readPhone(Activity activity, Fragment fragment, View v);

    void initCustomTab(Activity activity);

    void onViewJob();

    void onViewCity();

    void onPostRecruterWork(Activity activity);

    void onPostCandidateWork(Activity activity);

    void onPostCandidateExperience(Activity activity);

    void init();

    void saveRecruterWorkTypeSallary(String idType, String type, String sallary);
    void saveRecruterWorkAreaAddress(String idCity, String city, String area, String address);
    void saveRecruterWorkDayTime(String day, String time, String timeOption);
    void saveRecruterWorkGender(String data);
    void saveRecruterWorkMinimalExperience(String data);
    void saveRecruterWorkEducation(String data);
    void saveRecruterWorkRequirement(String data);
    void saveRecruterWorkScope(String data);
    void saveRecruterWorkExpiredDate(String data);

    void saveCandidateWork(WorkModel workModel);
    void editCandidateWork(WorkModel workModel, int position);

    void saveCandidateExperienceStatus(String data, String optional);
    void saveCandidateExperienceSallary(String data);
    void saveCandidateExperienceWork(WorkModel workModel);

}
