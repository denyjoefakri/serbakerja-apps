package com.joefakri.serbakerja.ui.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.joefakri.serbakerja.BuildConfig;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.ui.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class AppInfoActivity extends BaseActivity {

    @BindView(R.id.txt_version) TextView txt_version;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_app_info, this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        txt_version.setText("Versi " + BuildConfig.VERSION_NAME);
    }

}
