package com.joefakri.serbakerja.ui.profile.candidate.education_edit;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.experience_edit.EditExperienceView;

/**
 * Created by deny on bandung.
 */

public interface EditEducationMvpPresenter<V extends EditEducationView> extends SerbakerjaPresenter<V> {

    void onPostEducation(String... strings);

}
