package com.joefakri.serbakerja.ui.status.recruiter.application;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.StatusRecruitersAdapter;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.chat.detail.ChatRoomActivity;
import com.joefakri.serbakerja.ui.profile.recruiter.workpost_edit.EditWorkPostActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TextWatcherAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class StatusRecruiterFragmentApplication extends BaseFragment implements StatusRecruiterApplicationView,
        StatusRecruitersAdapter.Listener, StatusRecruitersAdapter.ApplicantsListener{

    @BindView(R.id.fab_add) FloatingActionButton fab_add;
    @BindView(R.id.nv_main) NestedScrollView nv_main;
    @BindView(R.id.rv_status) RecyclerView rv_status;
    @BindView(R.id.ed_query) EditText ed_query;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.tv_message) TextView tv_message;
    @BindView(R.id.txt_candidate_total) TextView txt_candidate_total;
    StatusRecruitersAdapter statusAdapter;

    ArrayList<WorkResponse.List.Data> modelsJob;
    ArrayList<WorkResponse.List.Data> modelsSearch;
    ArrayList<WorkResponse.List.Data> modelsApplicant;

    @Inject LinearLayoutManager mLayoutManager;
    @Inject StatusRecruiterApplicationMvpPresenter<StatusRecruiterApplicationView> presenter;

    private EventBus bus = EventBus.getDefault();
    StateEvent state;
    String jobid = "";
    int type = 0;

    private int currentPage = 0;
    private boolean notLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status_recruiter_application, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_status.setLayoutManager(mLayoutManager);
        rv_status.setHasFixedSize(true);
        rv_status.setNestedScrollingEnabled(false);
        rv_status.setAdapter(statusAdapter = new StatusRecruitersAdapter(getActivity()));
        statusAdapter.setOnclickListener(this);
        statusAdapter.setApplicantsListener(this);

        presenter.onViewJob("0");
        ed_query.addTextChangedListener(watcherAdapter);
        ed_query.setEnabled(false);

        nv_main.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v1, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v1.getChildAt(v1.getChildCount() - 1) != null) {
                if ((scrollY >= (v1.getChildAt(v1.getChildCount() - 1).getMeasuredHeight() - v1.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!notLoading){
                        notLoading = true;
                        currentPage += 20;
                        if (type == 1) {
                            presenter.onViewJob(String.valueOf(currentPage));
                        } else {
                            presenter.onViewApplicant(jobid, String.valueOf(currentPage));
                        }
                        statusAdapter.addLoading();
                    }
                }
            }
        });

        fab_add.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), EditWorkPostActivity.class));
        });
    }

    TextWatcherAdapter watcherAdapter = new TextWatcherAdapter(){
        @Override
        public void afterTextChanged(Editable s) {
            super.afterTextChanged(s);
            search(ed_query.getText().toString());
        }
    };

    private void search(String keyword) {
        if (keyword == null || keyword.equals("")) {
            if (type == 1)
                statusAdapter.update(modelsJob, 1);
            else
                statusAdapter.update(modelsApplicant, 2);
        } else {
            modelsSearch = new ArrayList<>();
            if (type == 1){
                for (int i = 0; i < modelsJob.size(); i++) {
                    WorkResponse.List.Data model = modelsJob.get(i);
                    if (model.getJenis_pekerjaan().toLowerCase().contains(keyword.toLowerCase())
                            || model.getLokasi().toLowerCase().contains(keyword.toLowerCase())
                            || model.getKota().toLowerCase().contains(keyword.toLowerCase())
                            || model.getAlamat().toLowerCase().contains(keyword.toLowerCase())
                            || model.getGaji_min().toLowerCase().contains(keyword.toLowerCase())
                            || model.getGaji_max().toLowerCase().contains(keyword.toLowerCase())
                            || model.getExpired_date().toLowerCase().contains(keyword.toLowerCase())) {
                        modelsSearch.add(model);
                    }
                }
            } else {
                for (int i = 0; i < modelsApplicant.size(); i++) {
                    WorkResponse.List.Data model = modelsApplicant.get(i);
                    if (model.getNama_ktp().toLowerCase().contains(keyword.toLowerCase())
                            || model.getPosisi().toLowerCase().contains(keyword.toLowerCase())
                            || model.getGaji_min().toLowerCase().contains(keyword.toLowerCase())
                            || model.getGaji_max().toLowerCase().contains(keyword.toLowerCase())
                            || Constants.waktuKerjaValue(getActivity(), model.getWaktu_kerja()).toLowerCase().contains(keyword.toLowerCase())
                            || Constants.education(getActivity(), model.getPend_terakhir()).toLowerCase().contains(keyword.toLowerCase())) {
                        modelsSearch.add(model);
                    }
                }
            }

            statusAdapter.update(modelsSearch, type);
        }
    }

    @Override
    public void onResultClosed() {
        presenter.onViewJob("0");
    }

    @Override
    public void onResultJob(ArrayList<WorkResponse.List.Data> data) {
        if (data.isEmpty()) {
            if (statusAdapter.getItemCount() == 0){
                view_empty.setVisibility(View.VISIBLE);
                rv_status.setVisibility(View.GONE);
                tv_message.setText(getString(R.string.txt_work_empty));
            }
            if (notLoading) {
                statusAdapter.removeLoading();
            }
        } else {
            view_empty.setVisibility(View.GONE);
            rv_status.setVisibility(View.VISIBLE);
            type = 1;
            modelsJob = data;
            //statusAdapter.update(modelsJob, type);
            ed_query.setEnabled(true);
            if (notLoading) {
                statusAdapter.removeLoading();
                statusAdapter.updateAll(data, type);
                notLoading = false;
            } else {
                statusAdapter.update(data, type);
            }
        }


    }

    @Override
    public void onResultApplicant(ArrayList<WorkResponse.List.Data> data) {
        if (data.isEmpty()) {
            if (statusAdapter.getItemCount() == 0){
                view_empty.setVisibility(View.VISIBLE);
                rv_status.setVisibility(View.GONE);
                tv_message.setText(getString(R.string.txt_applicant_empty));
            }
            if (notLoading) {
                statusAdapter.removeLoading();
            }
        } else {
            view_empty.setVisibility(View.GONE);
            rv_status.setVisibility(View.VISIBLE);
            type = 2;
            modelsApplicant = data;
            /*statusAdapter.update(modelsApplicant,type);
            txt_candidate_total.setText(modelsApplicant.size() + " " + getString(R.string.txt_message_candidate_apply));
            txt_candidate_total.setVisibility(View.VISIBLE);*/
            if (notLoading) {
                statusAdapter.removeLoading();
                statusAdapter.updateAll(data, type);
                notLoading = false;
            } else {
                statusAdapter.update(data, type);
            }
        }



    }

    @Override
    public void onResultApplicantStatus(WorkResponse.ApplyStatus data, String status) {
        presenter.onViewApplicant(jobid, "0");
        showMessage(data.getMessage());
        state.setType(1);
        bus.postSticky(state);
    }

    @Override
    public void onError() {
        if (statusAdapter.getItemCount() == 0){
            view_empty.setVisibility(View.VISIBLE);
            rv_status.setVisibility(View.GONE);
            if (type == 1) tv_message.setText(getString(R.string.txt_work_empty));
            else tv_message.setText(getString(R.string.txt_applicant_empty));
        }
    }

    @Override
    public void onJobClick(WorkResponse.List.Data model) {
        if (!model.getExpired_status().equals("1") && model.getStatus().equals("active")) {
            bus.postSticky(new StateEvent("2"));
            jobid = model.getPekerjaan_id();
            presenter.onViewApplicant(jobid, "0");
            fab_add.setVisibility(View.GONE);
            txt_candidate_total.setVisibility(View.GONE);
        } else {
            showMessageDialog("Pekerjaan tidak bisa diakses karena sudah ditutup/kadaluarsa");
        }

    }

    @Override
    public void onJobClosed(WorkResponse.List.Data model) {
        MessageDialog dialog = showConfirmMessage(R.string.message_close_job);
        dialog.setOnMessageClosed(() -> {
            presenter.onClosedJob(model.getPekerjaan_id());
        });
    }

    @Override
    public void onAccept(WorkResponse.List.Data model) {
        presenter.onAcceptApplicant(model.getLamaran_id());
    }

    @Override
    public void onReject(WorkResponse.List.Data model) {
        presenter.onRejectApplicant(model.getLamaran_id());
    }

    @Override
    public void onChat(WorkResponse.List.Data model) {
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        intent.putExtra("pekerjaan_id", model.getPekerjaan_id());
        intent.putExtra("target", model.getPengguna_id());
        startActivity(intent);
    }

    @Override
    public void onApplicantClick(WorkResponse.List.Data model) {
        Intent intent = new Intent(getActivity(), DetailCandidateActivity.class);
        intent.putExtra("id", model.getPengguna_id());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        presenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        this.state = state;
        Log.e("state", state.getStatus());
        if (state.getStatus().equals("1")) {
            notLoading = false;
            presenter.onViewJob("0");
            fab_add.setVisibility(View.VISIBLE);
            txt_candidate_total.setVisibility(View.VISIBLE);
        }
    }



}
