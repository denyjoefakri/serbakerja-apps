/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.work;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.DashboardResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.verify.VerifyMvpPresenter;
import com.joefakri.serbakerja.ui.verify.VerifyView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class DetailWorkPresenter<V extends DetailWorkView> extends BasePresenter<V>
        implements DetailWorkMvpPresenter<V> {

    @Inject
    public DetailWorkPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onApply(String id) {
        getSerbakerjaView().showLoading();

        UserRequest.Id idd = new UserRequest.Id(id);
        UserRequest.Pengguna pengguna = new UserRequest.Pengguna(getDataManager().getCurrentUserId());

        Logger.printLogRequest(Path.DASHBOARD_CANDIDATE_APPLY, idd, null);

        getCompositeDisposable().add(getDataManager()
                .dashboardCandidateApply(idd, pengguna)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.DASHBOARD_CANDIDATE_APPLY);
                    Gson gson = new Gson();
                    DashboardResponse.Apply response = gson.fromJson(jsonObject.toString(), DashboardResponse.Apply.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultApply();
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().onErrorApply();
                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onViewDetail(String work_id) {
        getSerbakerjaView().showLoading();

        DashboardRequest.CandidateDetail candidate = new DashboardRequest.CandidateDetail(work_id);
        UserRequest.Pengguna pengguna = new UserRequest.Pengguna(getDataManager().getCurrentUserId());
        Logger.printLogRequest(Path.DASHBOARD_CANDIDATE_DETAIL, candidate, null);

        getCompositeDisposable().add(getDataManager()
                .dashboardCandidateDetail(pengguna, candidate)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {

                    LogResponse(jsonObject, Path.DASHBOARD_CANDIDATE_DETAIL);
                    Gson gson = new Gson();
                    WorkResponse.Detail response = gson.fromJson(jsonObject.toString(), WorkResponse.Detail.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultViewDetail(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
