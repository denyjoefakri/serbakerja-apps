package com.joefakri.serbakerja.ui.form.work;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.WorkCandidateRegisterAdapter;
import com.joefakri.serbakerja.adapter.WorkExperienceAdapter;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.model.FormEvent;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.form.FormOptionalActivity;
import com.joefakri.serbakerja.ui.form.input_work.InputWorkActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class FormWorkCandidateFragment extends BaseFragment implements FormWorkView, WorkCandidateRegisterAdapter.Listener {

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.rv_work) RecyclerView rv_work;
    @BindView(R.id.btn_save) Button btn_save;
    @BindView(R.id.btn_add) LinearLayout btn_add;

    WorkCandidateRegisterAdapter adapter;
    ArrayList<WorkModel> models;

    @Inject
    LinearLayoutManager mLayoutManager;
    boolean edit;
    int positon = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_candidate, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        models = new ArrayList<>();
        rv_work.setLayoutManager(mLayoutManager);
        rv_work.setHasFixedSize(true);
        rv_work.setNestedScrollingEnabled(false);
        rv_work.setAdapter(adapter = new WorkCandidateRegisterAdapter(getActivity()));
        adapter.setOnclickListener(this);
        mPresenter.init();

        btn_add.setOnClickListener(view -> {
            edit = false;
            Intent intent = new Intent(getActivity(), InputWorkActivity.class);
            intent.putExtra("edit", edit);
            intent.putExtra("models", models);
            startActivityForResult(intent, 11);
        });

        btn_save.setOnClickListener(view -> {
            MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_2_kandidat);
            dialog.setOnMessageClosed(() -> {
                mPresenter.onPostCandidateWork(getActivity());
            });

        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 11 && resultCode == 11) {
            WorkModel workModel = (WorkModel) data.getSerializableExtra("model");
            if (workModel.isData()){
                if (!edit) {
                    models.add(workModel);
                    mPresenter.saveCandidateWork(workModel);
                    Log.e("edit", "false");
                }
                else {
                    WorkModel dataEdit = models.get(positon);
                    dataEdit.setWork(workModel.getWork());
                    dataEdit.setWork_day(workModel.getWork_day());
                    dataEdit.setArea(workModel.getArea());
                    dataEdit.setWork_time(workModel.getWork_time());
                    dataEdit.setWork_time_other(workModel.getWork_time_other());
                    dataEdit.setSalary(workModel.getSalary());
                    dataEdit.setCitys(workModel.getCitys());
                    dataEdit.setCity_id(workModel.getCity_id());
                    Log.e("other", workModel.getWork_time_other());
                    Log.e("models", ""+models.size());
                    mPresenter.editCandidateWork(workModel, positon);
                }
                adapter.update(models);
            }
        }
    }

    @Override
    public void onClick(WorkModel model, int position) {
        this.edit = true;
        this.positon = position;
        Intent intent = new Intent(getActivity(), InputWorkActivity.class);
        intent.putExtra("edit", edit);
        intent.putExtra("model", model);
        intent.putExtra("models", models);
        startActivityForResult(intent, 11);
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (candidateRealm != null){
            models = new ArrayList<>();
            for (int i = 0; i < candidateRealm.getWorkRealms().size(); i++) {
                WorkModel workModel = new WorkModel();
                workModel.setWork(candidateRealm.getWorkRealms().get(i).getWorkType());
                workModel.setWorkId(candidateRealm.getWorkRealms().get(i).getWorkTypeId());
                workModel.setWork_day(candidateRealm.getWorkRealms().get(i).getWorkDay());
                workModel.setArea(candidateRealm.getWorkRealms().get(i).getWorkArea());
                workModel.setWork_time(candidateRealm.getWorkRealms().get(i).getWorkTime());
                workModel.setWork_time_other(candidateRealm.getWorkRealms().get(i).getWorkTimeOther());
                workModel.setSalary(candidateRealm.getWorkRealms().get(i).getWorkSallary());
                workModel.setCity_id(candidateRealm.getWorkRealms().get(i).getWorkCityId());
                for (int j = 0; j < candidateRealm.getWorkRealms().get(i).getCityRealms().size(); j++) {
                    WorkModel.City city = new WorkModel().new City();
                    city.setId(candidateRealm.getWorkRealms().get(i).getCityRealms().get(j).getId());
                    city.setName(candidateRealm.getWorkRealms().get(i).getCityRealms().get(j).getName());
                    workModel.getCitys().add(city);
                }
                models.add(workModel);
            }
            adapter.update(models);
        }
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {
        getActivity().finish();
        /*setEvent(models.size() + 1);
        ((FormOptionalActivity)getActivity()).last_form_candidate();*/
    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
