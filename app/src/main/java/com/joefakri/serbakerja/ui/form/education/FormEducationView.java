/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.form.education;

import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.ui.base.SerbakerjaView;

/**
 * Created by janisharali on 25/05/17.
 */

public interface FormEducationView extends SerbakerjaView {

    void onResultCandidateEducation(CandidateResponse.EducationResponse.Data dataList);

    void init(CandidateRealm candidateRealm);
}
