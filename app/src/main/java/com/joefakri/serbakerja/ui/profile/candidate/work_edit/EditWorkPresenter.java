/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.candidate.work_edit;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.CandidateRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.model.WorkModel;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class EditWorkPresenter<V extends EditWorkView> extends BasePresenter<V>
        implements EditWorkMvpPresenter<V> {

    @Inject
    public EditWorkPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewCity() {
        getSerbakerjaView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .getCity()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(cityResponse -> {

                    LogResponse(cityResponse, Path.GET_CITY);
                    Gson gson = new Gson();
                    AreaResponse.CityResponse response = gson.fromJson(cityResponse.toString(), AreaResponse.CityResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().city(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onEditWork(String... s) {
        getSerbakerjaView().showLoading();

        CandidateRequest.ChangeProfileWork postWork = new CandidateRequest.ChangeProfileWork(
                s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7],s[8]);
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());

        Logger.printLogRequest(Path.UPDATE_PROFILE_CANDIDATE_WORK, postWork, null);
        getCompositeDisposable().add(getDataManager()
                .profileUpdateCandidateWork(id, postWork)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_PROFILE_CANDIDATE_WORK);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    CandidateResponse.ChangeProfile response = gson.fromJson(jsonObject.toString(), CandidateResponse.ChangeProfile.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultEditWork(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    @Override
    public void onInsertWork(String... s) {
        getSerbakerjaView().showLoading();

        ArrayList<WorkModel> workModels = new ArrayList<>();
        WorkModel workModel = new WorkModel();
        workModel.setWorkId(s[0]);
        workModel.setSalarymin(s[1]);
        workModel.setSalarymax(s[2]);
        workModel.setCity_id(s[3]);
        workModel.setArea(s[4]);
        workModel.setWork_time_position(s[5]);
        workModel.setWork_time_other(s[6]);
        workModel.setWork_day(s[7]);
        workModels.add(workModel);

        CandidateRequest.PostWork postWork = new CandidateRequest.PostWork(workModels);
        CandidateRequest.PostDocument user = new CandidateRequest.PostDocument(getDataManager().getCurrentUserId());

        Logger.printLogRequest(Path.POST_CANDIDATE_WORK, user, null);
        Logger.printLogRequest(postWork.getMultipleMap());
        getCompositeDisposable().add(getDataManager()
                .postCWork(user, postWork)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.POST_CANDIDATE_WORK);
                    JSONObject data = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    CandidateResponse.WorkResponse response = gson.fromJson(jsonObject.toString(), CandidateResponse.WorkResponse.class);

                    if (response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultInsertWork();
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }


}
