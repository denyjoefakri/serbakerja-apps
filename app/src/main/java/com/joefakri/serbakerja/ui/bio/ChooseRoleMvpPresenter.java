package com.joefakri.serbakerja.ui.bio;

import android.app.Activity;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateView;

/**
 * Created by deny on bandung.
 */

public interface ChooseRoleMvpPresenter<V extends ChooseRoleView> extends SerbakerjaPresenter<V> {

    void initPercent(int from);

    void onLogin(Activity activity, String type);

    void close();

}
