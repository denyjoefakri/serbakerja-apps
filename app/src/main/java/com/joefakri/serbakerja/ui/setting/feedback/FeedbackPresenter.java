/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.setting.feedback;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.MessageResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.setting.SettingMvpPresenter;
import com.joefakri.serbakerja.ui.setting.SettingView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class FeedbackPresenter<V extends FeedbackView> extends BasePresenter<V>
        implements FeedbackMvpPresenter<V> {

    @Inject
    public FeedbackPresenter(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onInitFrom() {
        getSerbakerjaView().onFrom(getDataManager().getCurrentUserPhone(), getDataManager().getCurrentUserName());
    }

    @Override
    public void onSend(String subjek, String pesan) {
        UserRequest.Saran saran = new UserRequest.Saran(getDataManager().getCurrentUserId(), subjek, pesan);
        getSerbakerjaView().showLoading();
        Logger.printLogRequest(Path.SARAN, saran, null);

        getCompositeDisposable().add(getDataManager()
                .saran(saran)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.SARAN);
                    Gson gson = new Gson();
                    MessageResponse response = gson.fromJson(jsonObject.toString(), MessageResponse.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().showMessage(response.getMessage());
                        getSerbakerjaView().onResult(response);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
