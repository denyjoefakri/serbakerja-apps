package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkExpiredDateFragment extends BaseFragment implements FormWorkView, DatePickerDialog.OnDateSetListener {

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_work_expired_date) EditText txt_work_expired_date;

    DatePickerDialog dpd;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.viewpager_form_work_expired_date, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        txt_work_expired_date.setOnFocusChangeListener((view, b) -> {
            if (b){
                txt_work_expired_date.setError(null);
                Calendar now = Calendar.getInstance();
                dpd = DatePickerDialog.newInstance(this, now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, 0);
                dpd.setMinDate(c);
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_next.setOnClickListener(view -> {
            if (txt_work_expired_date.getText().toString().isEmpty() ) {
                onError("Tanggal kadaluarsa postingan pekerjaan tidak boleh kosong");
                return;
            }

            MessageDialog dialog = showConfirmMessage(R.string.message_save_bio_form_2_perekrut);
            dialog.setOnMessageClosed(() -> {
                mPresenter.saveRecruterWorkExpiredDate(txt_work_expired_date.getText().toString());
                mPresenter.onPostRecruterWork(getActivity());
            });

        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String mount = String.valueOf(monthOfYear + 1);
        String day = String.valueOf(dayOfMonth);

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            calendar.setTime(dateFormat.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mount.length() <= 1 &&  day.length() <= 1){
            txt_work_expired_date.setText("0"+dayOfMonth + "-" +"0"+(monthOfYear+1)+"-" + year);
        }else if (day.length() <= 1){
            txt_work_expired_date.setText("0"+dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
        }else if (mount.length() <= 1){
            txt_work_expired_date.setText(dayOfMonth+"-"+"0"+(monthOfYear+1)+"-"+year);
        }else {
            txt_work_expired_date.setText(dayOfMonth + "-" + (monthOfYear + 1)+"-"+year);
        }

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        if (format.format(date).equals(txt_work_expired_date.getText().toString())) {
            onError("Tidak bisa pilih tanggal hari ini");
            txt_work_expired_date.setText("");
        }
        txt_work_expired_date.clearFocus();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm.getWorkExpiredDate() != null)
            txt_work_expired_date.setText(recruiterRealm.getWorkExpiredDate());
    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {
        view.clearFocus();
        getActivity().finish();
    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }
}
