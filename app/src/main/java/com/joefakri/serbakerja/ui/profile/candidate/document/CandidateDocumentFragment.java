package com.joefakri.serbakerja.ui.profile.candidate.document;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.profile.candidate.document_edit.EditDocumentActivity;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationView;
import com.joefakri.serbakerja.ui.profile.candidate.education_edit.EditEducationActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class CandidateDocumentFragment extends BaseFragment implements CandidateDocumentView{

    @BindView(R.id.img_photo_ktp) ImageView img_photo_ktp;
    @BindView(R.id.img_ijazah) ImageView img_ijazah;
    @BindView(R.id.img_photo) ImageView img_photo;
    @BindView(R.id.btn_edit) LinearLayout btn_edit;

    @Inject CandidateDocumentMvpPresenter<CandidateDocumentView> mPresenter;
    CandidateResponse.Document.Data data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_candidate_document, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View view) {

        btn_edit.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), EditDocumentActivity.class);
            intent.putExtra("model", data);
            startActivity(intent);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onViewDocument();
        btn_edit.setEnabled(false);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultDocumentView(CandidateResponse.Document.Data data) {
        btn_edit.setEnabled(true);
        this.data = data;
        Glide.with(this).load(data.getFoto_ktp()).into(img_photo_ktp);
        Glide.with(this).load(data.getFoto_terbaru()).into(img_photo);
        if (data.getFoto_ijazah() != null)
            Glide.with(this).load(data.getFoto_ijazah()).into(img_ijazah);
    }
}
