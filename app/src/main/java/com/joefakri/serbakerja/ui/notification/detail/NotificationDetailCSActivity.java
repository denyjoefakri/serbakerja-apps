package com.joefakri.serbakerja.ui.notification.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.NotificationAdapter;
import com.joefakri.serbakerja.connection.response.NotificationResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.notification.NotificationMvpPresenter;
import com.joefakri.serbakerja.ui.notification.NotificationView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class NotificationDetailCSActivity extends BaseActivity implements NotificationDetailView{

    @Inject NotificationDetailMvpPresenter<NotificationDetailView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_notification_cs, this);
        getActivityComponent().inject(this);


        toolbar.setTitle(getString(R.string.txt_notification_cs));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {


    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onResult(List<NotificationResponse.ListNotif.Data> listNotifs) {

    }
}
