/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.profile.candidate.document;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationMvpPresenter;
import com.joefakri.serbakerja.ui.profile.candidate.education.CandidateEducationView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class CandidateDocumentPresenter<V extends CandidateDocumentView> extends BasePresenter<V>
        implements CandidateDocumentMvpPresenter<V> {

    @Inject
    public CandidateDocumentPresenter(DataManager dataManager,
                                      SchedulerProvider schedulerProvider,
                                      CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewDocument() {
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());
        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.PROFILE_CANDIDATE_DOCUMENT, id, null);

        getCompositeDisposable().add(getDataManager()
                .profileCandidateDocument(id)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.PROFILE_CANDIDATE_DOCUMENT);
                    Gson gson = new Gson();
                    CandidateResponse.Document response = gson.fromJson(jsonObject.toString(), CandidateResponse.Document.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultDocumentView(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
