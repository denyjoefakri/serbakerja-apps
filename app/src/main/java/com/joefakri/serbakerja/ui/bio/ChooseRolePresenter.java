/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.bio;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.DashboardRequest;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.UserResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.CityRealm;
import com.joefakri.serbakerja.data.realm.ExperienceRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.data.realm.StateRealm;
import com.joefakri.serbakerja.data.realm.WorkRealm;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateMvpPresenter;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateView;
import com.joefakri.serbakerja.ui.main.MainActivity;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by janisharali on 27/01/17.
 */

public class ChooseRolePresenter<V extends ChooseRoleView> extends BasePresenter<V>
        implements ChooseRoleMvpPresenter<V> {

    Realm realm;

    @Inject
    public ChooseRolePresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void initPercent(int from) {
        realm = Realm.getDefaultInstance();
        CandidateRealm candidate = realm.where(CandidateRealm.class).findFirst();
        String cPercent1 = "";
        String cPercent2 = "";
        String cPercent3 = "";
        String cPercent4 = "";
        String cPercent5 = "";

        if (candidate != null){
            cPercent1 = String.valueOf(candidate.getPercent1());
            cPercent2 = String.valueOf(candidate.getPercent2());
            cPercent3 = String.valueOf(candidate.getPercent3());
            cPercent4 = String.valueOf(candidate.getPercent4());
            cPercent5 = String.valueOf(candidate.getPercent5());
        }

        RecruiterRealm recruiter = realm.where(RecruiterRealm.class).findFirst();
        String rPercent1 = "";
        String rPercent2 = "";
        String rPercent3 = "";
        if (recruiter != null){
            rPercent1 = String.valueOf(recruiter.getPercent1());
            rPercent2 = String.valueOf(recruiter.getPercent2());
            rPercent3 = String.valueOf(recruiter.getPercent3());
        }


        if (from == 0){
            empty(rPercent1, rPercent2, rPercent3, cPercent1, cPercent2, cPercent3, cPercent4, cPercent5);
        } else {
            if (getDataManager().getTypeUser() != null) {
                if (getDataManager().getTypeUser().equals("perekrut")){
                    getSerbakerjaView().onRecruter(
                            getDataManager().getRecruter1(),
                            getDataManager().getRecruter2(),
                            getDataManager().getRecruter3(),
                            rPercent1,
                            rPercent2,
                            rPercent3
                    );
                } else {
                    getSerbakerjaView().onCandidate(
                            getDataManager().getCandidate1(),
                            getDataManager().getCandidate2(),
                            getDataManager().getCandidate3(),
                            getDataManager().getCandidate4(),
                            getDataManager().getCandidate5(),
                            cPercent1,
                            cPercent2,
                            cPercent3,
                            cPercent4,
                            cPercent5
                    );

                    Log.e("dokumen 1", getDataManager().getCandidate5());
                    Log.e("dokumen 2", cPercent5);
                }
            } else {
                empty(rPercent1, rPercent2, rPercent3, cPercent1, cPercent2, cPercent3, cPercent4, cPercent5);
            }

        }

    }

    private void empty(String rPercent1, String rPercent2, String rPercent3,
                       String cPercent1, String cPercent2, String cPercent3,
                       String cPercent4, String cPercent5){
        getDataManager().updateRecruter("","","");
        getDataManager().updateCandidate("","","","","");

        getSerbakerjaView().onRecruter(
                getDataManager().getRecruter1(),
                getDataManager().getRecruter2(),
                getDataManager().getRecruter3(),
                rPercent1,
                rPercent2,
                rPercent3
        );

        getSerbakerjaView().onCandidate(
                getDataManager().getCandidate1(),
                getDataManager().getCandidate2(),
                getDataManager().getCandidate3(),
                getDataManager().getCandidate4(),
                getDataManager().getCandidate5(),
                cPercent1,
                cPercent2,
                cPercent3,
                cPercent4,
                cPercent5
        );

    }

    @Override
    public void onLogin(Activity activity, String type) {
        getSerbakerjaView().showLoading();
        UserRequest.Login login = new UserRequest.Login(getDataManager().getCurrentUserPhone(), getDataManager().getCurrentUserPassword());
        Logger.printLogRequest(Path.LOGIN, login, null);
        if (realm.isClosed()) realm = Realm.getDefaultInstance();

        getCompositeDisposable().add(getDataManager()
                .login(login)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.LOGIN);
                    Gson gson = new Gson();
                    UserResponse.Login response = gson.fromJson(jsonObject.toString(), UserResponse.Login.class);

                    if (response != null && response.getStatus().equals("success")) {
                        realm.beginTransaction();
                        getDataManager().setDefaultCity(null);
                        getDataManager().setDefaultJob(null);
                        getDataManager().updateUserInfo("", response.getData().getId(), response.getData().getNama(),
                                response.getData().getNo_hp(), response.getData().getEmail(), getDataManager().getCurrentUserPassword(), "");
                        getDataManager().updateAdditionalUserInfo(response.getData().getTipe_user(), response.getData().getStatus(),
                                response.getData().getKota(), response.getData().getJenis_pekerjaan());
                        getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);

                        RealmResults<RecruiterRealm> realmR = realm.where(RecruiterRealm.class).findAll();
                        RealmResults<CandidateRealm> realmC = realm.where(CandidateRealm.class).findAll();
                        RealmResults<CityRealm> realmCity = realm.where(CityRealm.class).findAll();
                        RealmResults<ExperienceRealm> realmExpe = realm.where(ExperienceRealm.class).findAll();
                        RealmResults<WorkRealm> realmWork = realm.where(WorkRealm.class).findAll();
                        RealmResults<StateRealm> stateRealms = realm.where(StateRealm.class).findAll();
                        realmR.deleteAllFromRealm();
                        realmC.deleteAllFromRealm();
                        realmCity.deleteAllFromRealm();
                        realmExpe.deleteAllFromRealm();
                        realmWork.deleteAllFromRealm();
                        stateRealms.deleteAllFromRealm();
                        realm.commitTransaction();
                        realm.close();
                        resultLogin(activity, type);
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }

    private void resultLogin(Activity activity, String type){
        getDataManager().setTypeUser(type);
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
        getDataManager().loginMode(DataManager.LoggedInMode.LOGGED_IN_MODE_REGISTER_SERVER);
    }

    @Override
    public void close() {
        realm.close();
    }
}
