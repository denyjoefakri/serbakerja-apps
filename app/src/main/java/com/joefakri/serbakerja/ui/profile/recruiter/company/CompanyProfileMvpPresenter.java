package com.joefakri.serbakerja.ui.profile.recruiter.company;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

/**
 * Created by deny on bandung.
 */

public interface CompanyProfileMvpPresenter<V extends CompanyProfileView> extends SerbakerjaPresenter<V> {

    void onViewProfile();
}
