package com.joefakri.serbakerja.ui.profile.recruiter.profile_edit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.widget.MessageDialog;
import com.joefakri.serbakerja.utils.TimeUtils;
import com.levibostian.shutter_android.builder.ShutterResultListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by deny on bandung.
 */

public class EditProfileActivity extends BaseActivity implements EditProfileView, EasyPermissions.PermissionCallbacks,
        DatePickerDialog.OnDateSetListener{

    @Inject EditProfileMvpPresenter<EditProfileView> presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.btn_change_photo_ktp) LinearLayout btn_change_photo_ktp;
    @BindView(R.id.btn_change_photo) LinearLayout btn_change_photo;
    @BindView(R.id.btn_save) Button btn_save;

    @BindView(R.id.txt_name) TextView txt_name;
    @BindView(R.id.txt_birthday) TextView txt_birthday;
    @BindView(R.id.txt_birthplace) TextView txt_birthplace;
    @BindView(R.id.txt_address) TextView txt_address;
    @BindView(R.id.txt_district) TextView txt_district;
    @BindView(R.id.txt_village) TextView txt_village;
    @BindView(R.id.txt_rtrw) TextView txt_rtrw;
    @BindView(R.id.img_photo_ktp) ImageView img_photo_ktp;
    @BindView(R.id.img_photo) ImageView img_photo;
    DatePickerDialog dpd;

    public ShutterResultListener shutterListener;
    String foto;
    String ktp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_edit_profile, this);
        getActivityComponent().inject(this);

        toolbar.setTitle(getString(R.string.txt_profile));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());

        presenter.onAttach(this);
        initComponent();
    }

    @Override
    protected void initComponent() {
        RecruiterResponse.Profile.Data data = (RecruiterResponse.Profile.Data) getIntent().getSerializableExtra("model");

        if (data != null) {
            if (data.getNama_ktp() != null) txt_name.setText(data.getNama_ktp());
            if (data.getTgl_lahir() != null) txt_birthday.setText(TimeUtils.getTime(data.getTgl_lahir()));
            if (data.getTempat_lahir() != null) txt_birthplace.setText(data.getTempat_lahir());
            if (data.getAlamat_ktp() != null) txt_address.setText(data.getAlamat_ktp());
            if (data.getKecamatan() != null) txt_district.setText(data.getKecamatan());
            if (data.getKelurahan() != null) txt_village.setText(data.getKelurahan());
            if (data.getRt_rw() != null) txt_rtrw.setText(data.getRt_rw());
            if (data.getFoto() != null) Glide.with(this).load(data.getFoto()).into(img_photo);
            if (data.getFoto_ktp() != null) Glide.with(this).load(data.getFoto_ktp()).into(img_photo_ktp);
        }

        btn_save.setOnClickListener(view -> {

            if (txt_name.getText().toString().isEmpty() ) {
                onError("Nama tidak boleh kosong");
                return;
            } else if (txt_birthplace.getText().toString().isEmpty() ) {
                onError("Tempat lahir tidak boleh kosong");
                return;
            } else if (txt_birthday.getText().toString().isEmpty() ) {
                onError("Tanggal lahir tidak boleh kosong");
                return;
            } else if (txt_address.getText().toString().isEmpty()) {
                onError("Alamat tidak boleh kosong");
                return;
            } else if (txt_district.getText().toString().isEmpty()) {
                onError("Kecamatan tidak boleh kosong");
                return;
            } else if (txt_village.getText().toString().isEmpty()) {
                onError("Kelurahan / Desa tidak boleh kosong");
                return;
            } else if (txt_rtrw.getText().toString().isEmpty() ) {
                onError("RT / RW tidak boleh kosong");
                return;
            } else if (ktp == null && data.getFoto_ktp() == null){
                onError("Foto KTP tidak boleh kosong");
                return;
            } else if (foto == null && data.getFoto() == null){
                onError("Foto tidak boleh kosong");
                return;
            }

            presenter.onUpdate(foto, ktp, txt_name.getText().toString(), txt_birthplace.getText().toString(),
                    txt_birthday.getText().toString(), txt_address.getText().toString(), txt_rtrw.getText().toString(),
                    txt_village.getText().toString(), txt_district.getText().toString());

            /*if (ktp != null && data.getFoto_ktp() != null && foto != null && data.getFoto() != null) {
                presenter.onUpdate(foto, ktp, txt_name.getText().toString(), txt_birthplace.getText().toString(),
                        txt_birthday.getText().toString(), txt_address.getText().toString(), txt_rtrw.getText().toString(),
                        txt_village.getText().toString(), txt_district.getText().toString());
            } else showMessageDialog("Anda tidak mengubah data");*/

        });

        btn_change_photo.setOnClickListener(view -> {
            presenter.readCameraGallery(this, view, 0);
        });

        btn_change_photo_ktp.setOnClickListener(view -> {
            presenter.readCameraGallery(this, view, 1);
        });

        txt_birthday.setOnFocusChangeListener((view, b) -> {
            if (b){
                Calendar now = Calendar.getInstance();
                dpd = DatePickerDialog.newInstance(this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "DatepickerdialogBirthday");
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
            }
        });

    }

    @Override
    public void onClosedMessageDialog() {
        super.onClosedMessageDialog();
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String mount = String.valueOf(monthOfYear + 1);
        String day = String.valueOf(dayOfMonth);

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
        try {
            calendar.setTime(dateFormat.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (mount.length() <= 1 &&  day.length() <= 1){
            txt_birthday.setText("0"+dayOfMonth + "-" +"0"+(monthOfYear+1)+"-" + year);
        }else if (day.length() <= 1){
            txt_birthday.setText("0"+dayOfMonth +"-"+(monthOfYear+1)+"-"+ year);
        }else if (mount.length() <= 1){
            txt_birthday.setText(dayOfMonth+"-"+"0"+(monthOfYear+1)+"-"+ year);
        }else {
            txt_birthday.setText(dayOfMonth + "-" + (monthOfYear + 1)+"-"+ year);
        }
    }

    @Override
    public void takePhoto(String file, int requestCode) {
        if (requestCode == 0) {
            Glide.with(this).load(file).into(img_photo);
            foto = file;
        }
        else {
            Glide.with(this).load(file).into(img_photo_ktp);
            ktp = file;
        }
    }

    @Override
    public void openGallery(String file, int requestCode) {
        if (requestCode == 0) {
            Glide.with(this).load(file).into(img_photo);
            foto = file;
        }
        else {
            Glide.with(this).load(file).into(img_photo_ktp);
            ktp = file;
        }
    }

    @Override
    public void onResult(RecruiterResponse.ChangeProfile changeProfileData) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!shutterListener.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (requestCode == 0) presenter.openImageChooser(this, btn_change_photo, requestCode);
        else presenter.openImageChooser(this, btn_change_photo_ktp, requestCode);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            MessageDialog dialog = showConfirmMessage(R.string.message_permission_camera);
            dialog.setOnMessageClosed(() -> {
                if (requestCode == 0) presenter.readCameraGallery(this, btn_change_photo, requestCode);
                else presenter.readCameraGallery(this, btn_change_photo_ktp, requestCode);
            });
        }
    }

}
