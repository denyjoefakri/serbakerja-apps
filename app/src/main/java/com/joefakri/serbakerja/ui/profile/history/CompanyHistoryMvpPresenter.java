package com.joefakri.serbakerja.ui.profile.history;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;

/**
 * Created by deny on bandung.
 */

public interface CompanyHistoryMvpPresenter<V extends CompanyHistoryView> extends SerbakerjaPresenter<V> {

    void onGetHistory();

}
