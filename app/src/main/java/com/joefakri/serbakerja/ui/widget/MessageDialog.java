package com.joefakri.serbakerja.ui.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.joefakri.serbakerja.R;

/**
 * Created by pristyanchandra on 10/14/16.
 */
public class MessageDialog extends DialogFragment {

    private OnMessageClosed messageClosed;

    /*public MessageDialog newIntance(String message) {
        MessageDialog messageDialog = new MessageDialog();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("message", message);
        messageDialog.setArguments(bundle);
        return messageDialog;
    }*/

    public void setOnMessageClosed(OnMessageClosed messageClosed) {
        this.messageClosed = messageClosed;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_message, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);
        TextView txtMessage = v.findViewById(R.id.dialog_message);
        txtMessage.setText(getArguments().getString("message"));

        TextView btnClose = v.findViewById(R.id.dialog_close);
        btnClose.setOnClickListener(v1 -> {
            if (messageClosed != null) {
                messageClosed.onClosed();
            }
            dismiss();
        });

    }

    public interface OnMessageClosed {
        public void onClosed();
    }
}