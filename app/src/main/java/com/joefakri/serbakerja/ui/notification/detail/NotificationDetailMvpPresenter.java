package com.joefakri.serbakerja.ui.notification.detail;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.notification.NotificationView;

/**
 * Created by deny on bandung.
 */

public interface NotificationDetailMvpPresenter<V extends NotificationDetailView> extends SerbakerjaPresenter<V> {

    void onViewPrepared();
}
