package com.joefakri.serbakerja.ui.form.work;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.AreaResponse;
import com.joefakri.serbakerja.connection.response.CandidateResponse;
import com.joefakri.serbakerja.connection.response.JobResponse;
import com.joefakri.serbakerja.connection.response.RecruiterResponse;
import com.joefakri.serbakerja.data.realm.CandidateRealm;
import com.joefakri.serbakerja.data.realm.RecruiterRealm;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.bio.bioform.FormActivity;
import com.joefakri.serbakerja.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class FormWorkDayTimeFragment extends BaseFragment implements FormWorkView {

    @Inject FormWorkMvpPresenter<FormWorkView> mPresenter;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txt_options_time_3) EditText txt_options_time_3;
    @BindView(R.id.txt_work_day) EditText txt_work_day;
    @BindView(R.id.rd_work_time) RadioGroup rd_work_time;
    RadioButton rd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_form_work_day_time, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void initComponent(View v) {
        mPresenter.init();
        btn_next.setOnClickListener(view -> {

            if (txt_work_day.getText().toString().isEmpty()) {
                onError("Hari kerja tidak boleh kosong");
                return;
            }

            if (rd_work_time.getCheckedRadioButtonId() == -1){
                onError("Pilih salah waktu bekerja yang tersedia");
                return;
            }

            mPresenter.saveRecruterWorkDayTime(txt_work_day.getText().toString(), Constants.waktuKerjaPosition(getActivity(), getTime()), txt_options_time_3.getText().toString());
            ((FormActivity)getActivity()).view_pager_tab.setCurrentItem(3);
        });

        rd_work_time.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton radioButton = v.findViewById(i);
            if (radioButton.getText().equals(getString(R.string.options_time_3))) {
                txt_options_time_3.setVisibility(View.VISIBLE);
                txt_options_time_3.setText("");
            }
            else txt_options_time_3.setVisibility(View.GONE);
        });
    }

    private String getTime(){
        int selectedId= rd_work_time.getCheckedRadioButtonId();
        RadioButton rd = getActivity().findViewById(selectedId);
        String result = "";
        Log.e("getTime", rd.getText().toString());
        if (!rd.getText().toString().equals(getString(R.string.options_time_3))){
            result = rd.getText().toString();
        } else {
            result = txt_options_time_3.getText().toString();
        }
        return result;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void init(RecruiterRealm recruiterRealm, CandidateRealm candidateRealm) {
        if (recruiterRealm != null) {
            if (recruiterRealm.getWorkDay() != null
                    && recruiterRealm.getWorkTime() != null){
                txt_work_day.setText(recruiterRealm.getWorkDay());
                if (recruiterRealm.getWorkTime() != null)
                    ((RadioButton)rd_work_time.getChildAt(Integer.parseInt(recruiterRealm.getWorkTime()))).setChecked(true);
                txt_options_time_3.setText(recruiterRealm.getWorkTimeOption());
            }
        }

    }

    @Override
    public void job(List<JobResponse.Data> dataList) {

    }

    @Override
    public void city(List<AreaResponse.CityResponse.Data> dataList) {

    }

    @Override
    public void onResultRecruterWork(RecruiterResponse.WorkResponse.Data workResponse) {

    }

    @Override
    public void onResultCandidateWork() {

    }

    @Override
    public void onResultCandidateExperience(CandidateResponse.ExperienceResponse.Data experienceData) {

    }
}
