package com.joefakri.serbakerja.ui.setting.manageaccount;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackView;

/**
 * Created by deny on bandung.
 */

public interface ManageAccountMvpPresenter<V extends ManageAccountView> extends SerbakerjaPresenter<V> {

    void onDeactive();

}
