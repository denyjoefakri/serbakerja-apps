package com.joefakri.serbakerja.ui.status.recruiter.verify;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.adapter.StatusRecruitersAdapter;
import com.joefakri.serbakerja.adapter.StatusRecruitersVerifyAdapter;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.model.Dummy;
import com.joefakri.serbakerja.data.model.StateEvent;
import com.joefakri.serbakerja.data.model.StatusModel;
import com.joefakri.serbakerja.di.component.ActivityComponent;
import com.joefakri.serbakerja.ui.base.BaseFragment;
import com.joefakri.serbakerja.ui.candidate.DetailCandidateActivity;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationMvpPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationView;
import com.joefakri.serbakerja.utils.Constants;
import com.joefakri.serbakerja.utils.TextWatcherAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by deny on bandung.
 */

public class StatusRecruiterFragmentVerify extends BaseFragment implements StatusRecruiterVerifyView {

    @BindView(R.id.rv_status) RecyclerView rv_status;
    @BindView(R.id.ed_query) EditText ed_query;
    @BindView(R.id.view_empty) LinearLayout view_empty;
    @BindView(R.id.tv_message) TextView tv_message;
    @BindView(R.id.nv_main) NestedScrollView nv_main;
    StatusRecruitersVerifyAdapter statusAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject StatusRecruiterVerifyMvpPresenter<StatusRecruiterVerifyView> presenter;
    private EventBus bus = EventBus.getDefault();

    ArrayList<WorkResponse.List.Data> modelsSearch;
    ArrayList<WorkResponse.List.Data> modelsApplicant;

    private int currentPage = 0;
    private boolean notLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status_recruiter_verify, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
        }
        return view;
    }



    @Override
    protected void initComponent(View view) {

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_status.setLayoutManager(mLayoutManager);
        rv_status.setHasFixedSize(true);
        rv_status.setNestedScrollingEnabled(false);
        rv_status.setAdapter(statusAdapter = new StatusRecruitersVerifyAdapter(getActivity()));
        statusAdapter.setApplicantsListener(new StatusRecruitersVerifyAdapter.ApplicantsListener() {
            @Override
            public void onAccept(WorkResponse.List.Data model) {
                presenter.onAcceptApplicant(model.getLamaran_id());
            }

            @Override
            public void onReject(WorkResponse.List.Data model) {
                presenter.onRejectApplicant(model.getLamaran_id());
            }

            @Override
            public void onApplicant(WorkResponse.List.Data model) {
                Intent intent = new Intent(getActivity(), DetailCandidateActivity.class);
                intent.putExtra("id", model.getPengguna_id());
                startActivity(intent);
            }

        });

        nv_main.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v1, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v1.getChildAt(v1.getChildCount() - 1) != null) {
                if ((scrollY >= (v1.getChildAt(v1.getChildCount() - 1).getMeasuredHeight() - v1.getMeasuredHeight())) && scrollY > oldScrollY) {
                    if (!notLoading){
                        notLoading = true;
                        currentPage += 20;
                        presenter.onViewApplicant(String.valueOf(currentPage));
                        statusAdapter.addLoading();
                    }
                }
            }
        });

        ed_query.addTextChangedListener(watcherAdapter);
        ed_query.setEnabled(false);

    }


    TextWatcherAdapter watcherAdapter = new TextWatcherAdapter(){
        @Override
        public void afterTextChanged(Editable s) {
            super.afterTextChanged(s);
            search(ed_query.getText().toString());
        }
    };

    private void search(String keyword) {
        if (keyword == null || keyword.equals("")) {
            statusAdapter.update(modelsApplicant);
        } else {
            modelsSearch = new ArrayList<>();
            for (int i = 0; i < modelsApplicant.size(); i++) {
                WorkResponse.List.Data model = modelsApplicant.get(i);
                if (model.getNama_ktp().toLowerCase().contains(keyword.toLowerCase())
                        || model.getNama_pekerjaan().toLowerCase().contains(keyword.toLowerCase())
                        || model.getDaerah().toLowerCase().contains(keyword.toLowerCase())
                        || model.getName().toLowerCase().contains(keyword.toLowerCase())
                        || model.getPosisi().toLowerCase().contains(keyword.toLowerCase())
                        || model.getGaji_min().toLowerCase().contains(keyword.toLowerCase())
                        || model.getGaji_max().toLowerCase().contains(keyword.toLowerCase())
                        || Constants.waktuKerjaValue(getActivity(), model.getWaktu_kerja()).toLowerCase().contains(keyword.toLowerCase())
                        || Constants.education(getActivity(), model.getPend_terakhir()).toLowerCase().contains(keyword.toLowerCase())) {
                    modelsSearch.add(model);
                }
            }

            statusAdapter.update(modelsSearch);
        }
    }

    /*@Override
    public void onResume() {
        super.onResume();
        presenter.onViewApplicant();
    }*/

    @Override
    public void onDestroyView() {
        presenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onResultApplicant(ArrayList<WorkResponse.List.Data> data) {
        if (data.isEmpty()) {
            if (statusAdapter.getItemCount() == 0){
                view_empty.setVisibility(View.VISIBLE);
                rv_status.setVisibility(View.GONE);
                tv_message.setText(getString(R.string.txt_applicant_empty));
            }
            if (notLoading) statusAdapter.removeLoading();
        } else {
            modelsApplicant = data;
            ed_query.setEnabled(true);
            if (notLoading) {
                statusAdapter.removeLoading();
                statusAdapter.updateAll(data);
                notLoading = false;
            } else {
                statusAdapter.update(data);
            }
        }

    }

    @Override
    public void onResultApplicantStatus(WorkResponse.ApplyStatus data, String status) {
        showMessage(data.getMessage());
        presenter.onViewApplicant("0");
    }

    @Override
    public void onErrorResult() {
        presenter.onViewApplicant("0");
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStateEvent(StateEvent state){
        if (state.getType() == 1) presenter.onViewApplicant("0");
    }
}
