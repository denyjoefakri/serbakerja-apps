package com.joefakri.serbakerja.ui.setting.manageaccount;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joefakri.serbakerja.R;
import com.joefakri.serbakerja.connection.response.MessageResponse;
import com.joefakri.serbakerja.ui.base.BaseActivity;
import com.joefakri.serbakerja.ui.login.LoginActivity;
import com.joefakri.serbakerja.ui.setting.SettingActivity;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackMvpPresenter;
import com.joefakri.serbakerja.ui.setting.feedback.FeedbackView;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by deny on bandung.
 */

public class ManageAccountActivity extends BaseActivity implements ManageAccountView{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.txt_message) TextView txt_message;
    @BindView(R.id.txt_status) TextView txt_status;
    @BindView(R.id.btn_deactive) LinearLayout btn_deactive;
    @Inject ManageAccountMvpPresenter<ManageAccountView> presenter;

    int active = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.activity_manage_account, this);
        getActivityComponent().inject(this);

        presenter.onAttach(this);
        initComponent();
    }
    @Override
    protected void initComponent() {
        toolbar.setTitle(getString(R.string.txt_manage_account));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> finish());


        btn_deactive.setOnClickListener(view -> {
            presenter.onDeactive();
        });

    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }


    @Override
    public void onResult() {
        Intent intent = new Intent(ManageAccountActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
