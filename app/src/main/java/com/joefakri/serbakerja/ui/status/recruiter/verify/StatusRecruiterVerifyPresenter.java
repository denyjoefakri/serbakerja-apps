/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.joefakri.serbakerja.ui.status.recruiter.verify;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.joefakri.serbakerja.connection.Path;
import com.joefakri.serbakerja.connection.request.UserRequest;
import com.joefakri.serbakerja.connection.response.WorkResponse;
import com.joefakri.serbakerja.data.DataManager;
import com.joefakri.serbakerja.ui.base.BasePresenter;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationMvpPresenter;
import com.joefakri.serbakerja.ui.status.recruiter.application.StatusRecruiterApplicationView;
import com.joefakri.serbakerja.utils.Logger;
import com.joefakri.serbakerja.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

public class StatusRecruiterVerifyPresenter<V extends StatusRecruiterVerifyView> extends BasePresenter<V>
        implements StatusRecruiterVerifyMvpPresenter<V> {

    @Inject
    public StatusRecruiterVerifyPresenter(DataManager dataManager,
                                          SchedulerProvider schedulerProvider,
                                          CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewApplicant(String offset) {
        UserRequest.Id id = new UserRequest.Id(getDataManager().getCurrentUserId());
        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.VERIFY, id, null);

        getCompositeDisposable().add(getDataManager()
                .verify(id, offset)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.VERIFY);
                    Gson gson = new Gson();
                    WorkResponse.List response = gson.fromJson(jsonObject.toString(), WorkResponse.List.class);

                    if (response != null) {
                        getSerbakerjaView().onResultApplicant(response.getData());
                    } else {
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }


    @Override
    public void onAcceptApplicant(String lamaran_id) {
        updateStatus(lamaran_id, "accept");
    }

    @Override
    public void onRejectApplicant(String lamaran_id) {
        updateStatus(lamaran_id, "reject");
    }

    private void updateStatus(String lamaran_id, String status){
        UserRequest.Status param = new UserRequest.Status(lamaran_id, status);
        getSerbakerjaView().showLoading();

        Logger.printLogRequest(Path.UPDATE_APPLICANT, param, null);

        getCompositeDisposable().add(getDataManager()
                .update_status_applicant(param)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject -> {
                    LogResponse(jsonObject, Path.UPDATE_APPLICANT);
                    Gson gson = new Gson();
                    WorkResponse.ApplyStatus response = gson.fromJson(jsonObject.toString(), WorkResponse.ApplyStatus.class);

                    if (response != null && response.getStatus().equals("success")) {
                        getSerbakerjaView().onResultApplicantStatus(response, status);
                    } else {
                        getSerbakerjaView().onErrorResult();
                        getSerbakerjaView().onError(response.getMessage());
                    }

                    getSerbakerjaView().hideLoading();

                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }

                    getSerbakerjaView().hideLoading();

                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    }
                }));
    }
}
