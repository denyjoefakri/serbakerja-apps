package com.joefakri.serbakerja.ui.bio.bioform;

import com.joefakri.serbakerja.ui.base.SerbakerjaPresenter;
import com.joefakri.serbakerja.ui.bio.ChooseRoleView;

/**
 * Created by deny on bandung.
 */

public interface FormMvpPresenter<V extends FormView> extends SerbakerjaPresenter<V> {

    void close();

}
